<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Configurations
Route::redirect('/', '/home');
Auth::routes(['verify' => true]);

// Free Views
Route::view('/about', 'app.about')->name('about');
Route::view('/contact-us', 'app.contact-us')->name('contact-us');
Route::view('/faqs', 'app.faqs')->name('faqs');
Route::view('/posting-policy', 'app.posting-policy')->name('posting-policy');
Route::view('/privacy-policy', 'app.privacy-policy')->name('privacy-policy');
Route::view('/terms-of-use', 'app.terms-of-use')->name('terms-of-use');

// Free Routes
Route::get('/home', 'HomeController')->name('home');
Route::get('/retails', 'RetailController')->name('retails');
Route::get('/wholesales', 'WholesaleController')->name('wholesales');
Route::get('/auctions', 'AuctionController')->name('auctions');
Route::get('/reverse-auctions', 'ReverseAuctionController')->name('reverse-auctions');
Route::get('/search', 'SearchController@search')->name('search');
Route::get('/suggestion', 'SearchController@suggestion')->name('suggestion');
Route::get('/jobs/search', 'SearchController@searchJob')->name('search.job');
Route::get('/real-estates/search', 'SearchController@searchRealEstate')->name('search.real-estate');
Route::get('/travels/search', 'SearchController@searchTravel')->name('search.travel');
Route::get('/vehicles/search', 'SearchController@searchVehicle')->name('search.vehicle');

// Throttle
Route::group(['middleware' => ['throttle:60,1']], function () {
    // User Auth
    Route::namespace('Auth')->group(function () {
        Route::get('/logout', 'LoginController@logout')->name('logout');

        Route::get('/login/facebook', 'LoginController@facebookLogin')->name('facebook.login');
        Route::get('/login/facebook/callback', 'LoginController@facebookCallback')->name('facebook.callback');

        Route::get('/login/google', 'LoginController@googleLogin')->name('google.login');
        Route::get('/login/google/callback', 'LoginController@googleCallback')->name('google.callback');

        Route::get('/login/linkedin', 'LoginController@linkedinLogin')->name('linkedin.login');
        Route::get('/login/linkedin/callback', 'LoginController@linkedinCallback')->name('linkedin.callback');

        Route::get('/login/instagram', 'LoginController@instagramLogin')->name('instagram.login');
        Route::get('/login/instagram/callback', 'LoginController@instagramCallback')->name('instagram.callback');
    });

    // Community Resources
    Route::resource('communities.categories', 'CommunityCategoryController')->except(['create', 'edit'])->shallow();
    Route::resource('communities.comments', 'CommunityCommentController')->except(['create', 'edit'])->shallow();
    Route::resource('communities.images', 'CommunityImageController')->except(['create', 'edit'])->shallow();

    // Job Resources
    Route::resource('jobs.addresses', 'JobAddressController')->except(['create', 'edit'])->shallow();
    Route::resource('jobs.categories', 'JobCategoryController')->except(['create', 'edit'])->shallow();
    Route::resource('jobs.comments', 'JobCommentController')->except(['create', 'edit'])->shallow();
    Route::resource('jobs.images', 'JobImageController')->except(['create', 'edit'])->shallow();

    // Product Resources
    Route::resource('products.addresses', 'ProductAddressController')->except(['create', 'edit'])->shallow();
    Route::resource('products.bids', 'ProductBidController')->except(['create', 'edit'])->shallow();
    Route::resource('products.categories', 'ProductCategoryController')->except(['create', 'edit'])->shallow();
    Route::resource('products.comments', 'ProductCommentController')->except(['create', 'edit'])->shallow();
    Route::resource('products.images', 'ProductImageController')->except(['create', 'edit'])->shallow();
    Route::resource('products.watchables', 'ProductWatchableController')->except(['create', 'edit'])->shallow();

    // Real Estate Resources
    Route::resource('real-estates.addresses', 'RealEstateAddressController')->except(['create', 'edit'])->shallow();
    Route::resource('real-estates.bids', 'RealEstateBidController')->except(['create', 'edit'])->shallow();
    Route::resource('real-estates.categories', 'RealEstateCategoryController')->except(['create', 'edit'])->shallow();
    Route::resource('real-estates.comments', 'RealEstateCommentController')->except(['create', 'edit'])->shallow();
    Route::resource('real-estates.images', 'RealEstateImageController')->except(['create', 'edit'])->shallow();
    Route::resource('real-estates.watchables', 'RealEstateWatchableController')->except(['create', 'edit'])->shallow();

    // Service Resources
    Route::resource('services.addresses', 'ServiceAddressController')->except(['create', 'edit'])->shallow();
    Route::resource('services.bids', 'ServiceBidController')->except(['create', 'edit'])->shallow();
    Route::resource('services.categories', 'ServiceCategoryController')->except(['create', 'edit'])->shallow();
    Route::resource('services.comments', 'ServiceCommentController')->except(['create', 'edit'])->shallow();
    Route::resource('services.feedbacks', 'ServiceFeedbackController')->except(['create', 'edit'])->shallow();
    Route::resource('services.images', 'ServiceImageController')->except(['create', 'edit'])->shallow();
    Route::resource('services.watchables', 'ServiceWatchableController')->except(['create', 'edit'])->shallow();

    // Travel Resources
    Route::resource('travels.addresses', 'TravelAddressController')->except(['create', 'edit'])->shallow();
    Route::resource('travels.bids', 'TravelBidController')->except(['create', 'edit'])->shallow();
    Route::resource('travels.categories', 'TravelCategoryController')->except(['create', 'edit'])->shallow();
    Route::resource('travels.comments', 'TravelCommentController')->except(['create', 'edit'])->shallow();
    Route::resource('travels.images', 'TravelImageController')->except(['create', 'edit'])->shallow();
    Route::resource('travels.watchables', 'TravelWatchableController')->except(['create', 'edit'])->shallow();

    // User Resources
    Route::resource('users.addresses', 'UserAddressController')->except(['create', 'edit'])->shallow();
    Route::resource('users.ads', 'UserAdController')->except(['edit'])->shallow();
    Route::resource('users.bids', 'UserBidController')->except(['create', 'edit'])->shallow();
    Route::resource('users.comments', 'UserCommentController')->except(['create', 'edit'])->shallow();
    Route::resource('users.communities', 'UserCommunityController')->except(['edit'])->shallow();
    Route::resource('users.feedbacks', 'UserFeedbackController')->except(['create', 'edit'])->shallow();
    Route::resource('users.images', 'UserImageController')->except(['create', 'edit'])->shallow();
    Route::resource('users.jobs', 'UserJobController')->except(['edit'])->shallow();
    Route::resource('users.products', 'UserProductController')->except(['edit'])->shallow();
    Route::resource('users.real-estates', 'UserRealEstateController')->except(['edit'])->shallow();
    Route::resource('users.services', 'UserServiceController')->except(['edit'])->shallow();
    Route::resource('users.travels', 'UserTravelController')->except(['edit'])->shallow();
    Route::resource('users.vehicles', 'UserVehicleController')->except(['edit'])->shallow();
    Route::resource('users.watchables', 'UserWatchableController')->except(['create', 'edit'])->shallow();

    // Vehicle Resources
    Route::resource('vehicles.addresses', 'VehicleAddressController')->except(['create', 'edit'])->shallow();
    Route::resource('vehicles.bids', 'VehicleBidController')->except(['create', 'edit'])->shallow();
    Route::resource('vehicles.categories', 'VehicleCategoryController')->except(['create', 'edit'])->shallow();
    Route::resource('vehicles.comments', 'VehicleCommentController')->except(['create', 'edit'])->shallow();
    Route::resource('vehicles.images', 'VehicleImageController')->except(['create', 'edit'])->shallow();
    Route::resource('vehicles.watchables', 'VehicleWatchableController')->except(['create', 'edit'])->shallow();

    // Wholesale Actions
    Route::get('wholesales/requests', 'WholesaleRequestController@index')->name('wholesales.requests.index');
    Route::post('wholesales/requests', 'WholesaleRequestController@request')->name('wholesales.requests.store');
    Route::put('wholesales/requests/{wholesaleRequest}/approve', 'WholesaleRequestController@approve')->name('wholesales.requests.approve');
    Route::put('wholesales/requests/{wholesaleRequest}/deny', 'WholesaleRequestController@deny')->name('wholesales.requests.deny');

    // Accounts
    Route::get('/account', 'AccountController')->name('account');
    Route::get('/listings', 'ListingController')->name('listings');
    Route::get('/analytics', 'AnalyticsController')->name('analytics');

    
    // Stripe
    Route::post('/stripe/checkout', 'StripeController@checkout')->name('stripe.checkout');
    Route::get('/stripe/success', 'StripeController@success')->name('stripe.success');
    Route::get('/stripe/cancel', 'StripeController@cancel')->name('stripe.cancel');
    Route::get('/stripe/connect/success', 'StripeController@connectSuccess')->name('stripe.connect.success');

    // Cart
    Route::get('/cart', 'CartController@show')->name('cart.show');
    Route::post('/cart', 'CartController@add')->name('cart.add');
    Route::put('/cart/{id}', 'CartController@update')->name('cart.update');
    Route::delete('/cart/{id}', 'CartController@remove')->name('cart.remove');
    Route::delete('/cart/clear', 'CartController@clear')->name('cart.clear');

    // User Action
    Route::post('/contact', 'ContactUsController')->name('contact');
    Route::post('/email-subscription', 'EmailSubscriptionController')->name('email-subscription.store');

    // Resources Controllers
    Route::resources([
        'ads' => 'AdController',
        'addresses' => 'AddressController',
        'bids' => 'BidController',
        'categories' => 'CategoryController',
        'comments' => 'CommentController',
        'communities' => 'CommunityController',
        'images' => 'ImageController',
        'jobs' => 'JobController',
        'products' => 'ProductController',
        'real-estates' => 'RealEstateController',
        'services' => 'ServiceController',
        'travels' => 'TravelController',
        'users' => 'UserController',
        'vehicles' => 'VehicleController',
        'videos' => 'VideoController',
        'watchables' => 'WatchableController',
    ]);

    // Admin
    Route::name('admin.')->prefix('admin')
        ->group(function () {
            Route::view('/', 'admin.auth.login')->name('login.show');
            Route::post('/login', 'AdminController@login')->name('login');
            Route::get('/logout', 'AdminController@logout')->name('logout');

            Route::get('/communities', 'AdminController@index')->name('index');
            Route::get('/communities/{id}', 'AdminController@show')->name('communities.show');
            Route::put('/communities/{community}/toggle', 'CommunityController@toggle')->name('communities.toggle');
            Route::view('/email-reminders', 'admin.email-reminders')->name('email-reminders');
            Route::post('/email-reminders', 'AdminController@send')->name('email-reminders.send');
        });
});
