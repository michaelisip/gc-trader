/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports) {

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $("meta[name='csrf-token']").attr('content')
  }
});
$(document).ready(function () {
  $('#goToTop').hide();
});
$(document).scroll(function () {
  if ($(this).scrollTop() > 800) {
    $('#goToTop').fadeIn();
  } else {
    $('#goToTop').fadeOut();
  }
});
$('.ui.checkbox').checkbox();
$('.ui.dropdown').dropdown();
$('.ui.accordion').accordion();
$('.popup').popup();
$('.special.cards .image').dimmer({
  on: 'hover'
});
$('.message .close').on('click', function () {
  $(this).closest('.message').transition('fade');
});
$('#goToTop').on('click', function () {
  $('html, body').animate({
    scrollTop: 0
  }, 'slow');
});
$('.add-to-watchlist').on('click', function () {
  $('#addToWatchlistForm').attr('action', $(this).data('url'));
  $('#addToWatchlistForm').submit();
});
$('.remove-from-watchlist').on('click', function () {
  $('#removeFromWatchlistForm').attr('action', $(this).data('url'));
  $('#removeFromWatchlistForm').submit();
});
$('.add-to-cart').on('click', function () {
  $("#addToCartForm input[name='id']").val($(this).data('id'));
  $("#addToCartForm input[name='type']").val($(this).data('type'));
  $('#addToCartForm').submit();
});
$('.update-cart').on('click', function () {
  $('#updateCartForm').attr('action', $(this).data('url'));
  $("#updateCartForm input[name='quantity']").val($(this).data('quantity'));
  $('#updateCartForm').submit();
});
$('.remove-to-cart').on('click', function () {
  $('#removeToCartForm').attr('action', $(this).data('url'));
  $('#removeToCartForm').submit();
});
$('.wholesale-request').on('click', function () {
  $("#wholesaleRequestForm input[name='product_id']").val($(this).data('product-id'));
  $('#wholesaleRequestForm').submit();
});
$('.wholesale-request-approve').on('click', function () {
  $('#wholesaleRequestApproveForm').attr('action', $(this).data('url'));
  $('#wholesaleRequestApproveForm').submit();
});
$('.wholesale-request-deny').on('click', function () {
  $('#wholesaleRequestDenyForm').attr('action', $(this).data('url'));
  $('#wholesaleRequestDenyForm').submit();
});
$('.ui.search').search({
  apiSettings: {
    url: "".concat($('input[name="search_route"]').val(), "?search={query}")
  },
  type: 'category',
  minCharacters: 3,
  cache: true,
  selectFirstResult: true,
  showNoResults: true,
  selector: {
    prompt: '#searchBar'
  }
});

/***/ }),

/***/ "./resources/sass/app.scss":
/*!*********************************!*\
  !*** ./resources/sass/app.scss ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/app.scss ***!
  \*************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! C:\xampp\htdocs\gc-trader\resources\js\app.js */"./resources/js/app.js");
module.exports = __webpack_require__(/*! C:\xampp\htdocs\gc-trader\resources\sass\app.scss */"./resources/sass/app.scss");


/***/ })

/******/ });