<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('type');
            $table->longText('description');
            $table->decimal('price', 14)->unsigned();
            $table->decimal('wholesale_price', 14)->unsigned()->nullable();
            $table->integer('sales')->unsigned()->nullable()->default(0);
            $table->integer('quantity')->unsigned()->nullable()->default(1);
            $table->string('condition')->nullable()->default('New');
            $table->string('pick_up_type')->nullable()->default('Pick-Up');
            $table->timestamp('closed_at')->nullable();
            $table->enum('status', ['active', 'pending', 'inactive'])->nullable()->default('active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
