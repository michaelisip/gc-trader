<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRealEstatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('real_estates', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('company_name');
            $table->longText('description');
            $table->double('size');
            $table->string('type');
            $table->decimal('price', 14)->unsigned()->nullable();
            $table->integer('bedrooms')->unsigned()->nullable()->default(0);
            $table->integer('bathrooms')->unsigned()->nullable()->default(0);
            $table->string('property_status')->nullable()->default('New');
            $table->enum('status', ['active', 'inactive'])->nullable()->default('active');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('real_estates');
    }
}
