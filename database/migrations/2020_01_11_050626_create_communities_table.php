<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCommunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('communities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned()->nullable();
            $table->string('name');
            $table->string('email')->unique();
            $table->longText('description');
            $table->string('company_number')->nullable();
            $table->text('registration')->nullable();
            $table->string('po_box_address');
            $table->dateTime('date_time')->nullable();
            $table->timestamp('start_time')->nullable();
            $table->timestamp('finish_time')->nullable();
            $table->boolean('parking_or_disability_drop_box')->nullable()->default(0);
            $table->string('pick_up_type')->nullable()->default('Pick-Up');
            $table->enum('status', ['active', 'inactive'])->nullable()->default('inactive');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('communities');
    }
}
