<?php

use App\Bid;
use App\Product;
use App\RealEstate;
use App\Service;
use App\Travel;
use App\User;
use App\Vehicle;
use Illuminate\Database\Seeder;

class BidsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 5; $i++) {
            // Auctions
            for ($j = 0; $j < 4; $j++) {
                $bid = new Bid(['bid' => rand(1001, 5000)]);

                $product = Product::withoutGlobalScopes()->auction()->whereDoesntHave('bids')->first();
                $product->bids()->save($bid);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->bids()->save($bid);
            }

            // Reverse Auctions
            for ($j = 0; $j < 4; $j++) {
                $bid = new Bid(['bid' => rand(100, 1000)]);

                $product = Product::withoutGlobalScopes()->reverseAuction()->whereDoesntHave('bids')->first();
                $product->bids()->save($bid);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->bids()->save($bid);
            }

            // Real Estate
            for ($j = 0; $j < 5; $j++) {
                $bid = new Bid(['bid' => rand(100, 1000)]);

                $realEstate = RealEstate::withoutGlobalScopes()->findOrFail($j + 1);
                $realEstate->bids()->save($bid);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->bids()->save($bid);
            }

            // Service
            for ($j = 0; $j < 5; $j++) {
                $bid = new Bid(['bid' => rand(100, 1000)]);

                $service = Service::withoutGlobalScopes()->findOrFail($j + 1);
                $service->bids()->save($bid);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->bids()->save($bid);
            }

            // Travel
            for ($j = 0; $j < 5; $j++) {
                $bid = new Bid(['bid' => rand(100, 1000)]);

                $travel = Travel::withoutGlobalScopes()->findOrFail($j + 1);
                $travel->bids()->save($bid);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->bids()->save($bid);
            }

            // Vehicle
            for ($j = 0; $j < 5; $j++) {
                $bid = new Bid(['bid' => rand(100, 1000)]);

                $vehicle = Vehicle::withoutGlobalScopes()->findOrFail($j + 1);
                $vehicle->bids()->save($bid);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->bids()->save($bid);
            }
        }
    }
}
