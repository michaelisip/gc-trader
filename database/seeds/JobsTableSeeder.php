<?php

use App\Image;
use App\Job;
use Faker\Factory;
use App\User;
use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 5; $i++) {
            $job = new Job([
                'name' => $faker->name,
                'company_name' => $faker->company,
                'type' => $faker->randomElement([
                    'Full Time',
                    'Part Time',
                    'Contract',
                ]),
                'description' => $faker->realText(1000),
                'status' => $faker->boolean(75) ? 'active' : 'inactive',
            ]);

            $image = new Image(['src' => config('services.aws.url') . '/gctrader/img/company-logo-' . rand(1, 15) . '.jpg']);

            $user = User::withoutGlobalScopes()->findOrFail($i + 1);
            $user->jobs()->save($job);
            $job->image()->save($image);
        }
    }
}
