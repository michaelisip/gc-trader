<?php

use App\Image;
use App\Product;
use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class WholesalesTableSeeder extends Seeder
{
    public $names = [
        "Vacancy for a Cleaner",
        "Wholesale Turkish Handmade Jewelry Gems Animal 925 Silver Women Wedding Ring HOT",
        "Wholesale Lot Natural Gemstone Round Spacer Loose Beads 4mm 6mm 8mm 10mm 12mm",
        "Wholesale bulk 10 pcs Fashion PU leather string black necklace cord 18 'DIY",
        "Wholesale Free Ship 100PCS Useful Plastic Earrings Back Stopper Earplug Stud Ear",
        "40pcs Wholesale Lip Nose Piercing Jewelry Hoop Rings Tongue Ring Stud Piercing",
        "Wholesale Natural Gemstone Labaradorite Amethyst Lapis Round Loose Beads Pick",
        "Wholesale Crystal Women 18K Gold Plated Drop Dangle Earrings Ear Hoop Jewelry",
        "Wholesale 925 Sterling Silver Chain Necklace Women Men Collar inch 2MM",
        "Wholesale 10Pcs Poly Bubble Mailers Padded Envelopes Shipping Bags Self Seal",
        "Wholesale 10Pcs/Lot Charms Real Leather Cord Chain Necklace with Lobster Clasp",
        "Wholesale 10-100PCS beautiful pheasant tail & peacock feathers 3-20cm/2-8inches",
        "Wholesale 100pcs Bulk Lots Tibetan Silver Mix Charm Pendants Jewelry DIY @@",
        "Wholesale 10-100ps Heart Bulk Natural Gemstone Silver Plated Beads Pendant FREE",
        "Wholesale Silver Vintage Wedding Sapphire Engagement White Topaz 6-10 Ring 925",
        "Wholesale Turkish Handmade Jewelry Gems Animal 925 Silver Women Wedding Ring HOT",
        "Fashion 925 Silver Fire Opal White Topaz Flower Ring Wedding Engagement Jewelry",
        "Creative Two Tone 925 Silver Rings Women White Sapphire Wedding Ring Size 6-10",
        "Vintage 925 Silver White Topaz Sapphire Engagement Wedding Ring 6-10",
        "14k Emerald Diamond Ring Women Anniversary Engagement Wedding Gemstone Ring",
        "925 Silver Plating Topaz Dragonfly Ring Wedding Engagement Jewelry Wholesale",
    ];

    public $descriptions = [
        "An innovative logistics solutions provider is seeking experienced cleaners to assist with the expansion of its operations. Role Responsibilities •Cleaning commercial and residential properties •Dusting and cleaning ceiling vents, surface areas, and counter surfaces •Performing general sweeping, scrubbing, mopping of hardwood, laminate, or tiled floors •Using vacuuming equipment; vacuuming and cleaning carpets, performing further treatments such as shampoo or stain-removal as needed •Disposing of trash from bins and containers •Polishing furniture and room accessories as needed •Scrubbing sinks, basins, and toilets in private and public bathrooms •Cleaning windows, glass surfaces, and mirrors •Assisting with maintenance and upkeep of all cleaning equipment, supplies, and products •Liaising with suppliers and placing orders •Ensuring safe and sanitary storage and care of products •Keeping all public spaces neat and tidy •Reporting repairs and replacements needed when encountered on the job •Maintaining regular condition reports for all properties; documenting and reporting damages caused by patrons •Performing administrative tasks related to cleaning work Desired Attributes •Previous housekeeping/ cleaning experience •Ability to follow verbal directions •Ability to work well alone and with a partner or team •Reliable and punctual with strong commitment to responsibilities •Able to adapt to changing schedules or routines; excellent time management skills •Attention to detail and strong organizational skills •Physically able to reach, stretch, bend, and walk during the daily routine, with ability to stand for long lengths of time; physically able to push vacuum cleaner for extended periods of time; ability to lift up to 20-25kg •General knowledge of cleaning products, supplies, and techniques for cleaning •Full Availability for work including evenings, weekends, and occasional holidays •Strong written and verbal communication skills (English) •Ability to use essential business software •Availability for immediate start Benefits •Supportive and inclusive culture •A professional and safe working environment •Good employee benefits and entitlements The position would suit someone who thrives in a fast-paced and enterprising work environment. Furthermore, the abilities to work to stringent deadlines, multi-task and perform under pressure are integral to the role. The role may involve working long shifts and weekends. It’s imperative that candidates consider their ability to commit to a serious, time-intensive role before applying. The Firm’s work culture offers rapid career progression with additional incentives to reward high-performance. Interested candidates may submit their applications by completing the following form: https://app.smartsheet.com/b/form/819d7c9b175747948428cc0417d8158c Interviews will be scheduled at short notice as the Firm has an urgent need for human resources. ",
        "Condition:  New without tags: A brand-new, unused, and unworn item (including handmade items) that is not in original packaging or may be missing original packaging materials (such as the original box or bag). The original tags may not be attached. See all condition definitions Country/Region of Manufacture: China Main Stone Treatment: Filling Style: Band Main Stone: Ruby/Emerald/Moonstone/Amethyst/Sapphire Base Metal: 925 Silver/Yellow Gold Filled Main Stone Creation: Lab-Created/Cultured Material: Stone Brand:  Unbranded Theme: Beauty Metal: 925 Silver/Yellow Gold Filled Secondary Stone: Gemstone Ring Size: 6 7 8 9 10 (US) Color:  As The Pictures",
        "Condition: New with tags: A brand-new, unused, and unworn item (including handmade items) in the original packaging (such as ... Read more Shape: Round Size: 4mm/6mm/8mm/10mm Color: Multi-Color Brand: Unbranded Material: Gemstone Hole Size: 1mm Metal: No Metal Country/Region of Manufacture: China Style: Spacer",
        "Condition: New without tags: A brand-new, unused, and unworn item (including handmade items) that is not in original packaging or ... Read more Brand: Unbranded Material: Leather Style: Chain Length (inches): Length: approx. 46+5cm Metal: Alloy Country/Region of Manufacture: China Theme: Beauty Thickness: approx. 2mm Main Stone: No Stone UPC: Does not apply",
        "Condition: New with tags: A brand-new, unused, and unworn item (including handmade items) in the original packaging (such as ... Read more Metal Purity: plastic Fast Fulfillment: YES Country/Region of Manufacture: Hong Kong Color: White Style: Stud Main Stone: No Stone Fastening: Clip-On Brand: Unbranded Material: Plastic Metal: plastic Main Color: White",
        "Condition: New without tags: A brand-new, unused, and unworn item (including handmade items) that is not in original packaging or ... Read more Type: Ring Metal: Surgical Steel Country/Region of Manufacture: China Brand: Unbranded Body Area: Ear, Lip, Nose MPN: Does Not Apply",
        "Condition: New with tags: A brand-new, unused, and unworn item (including handmade items) in the original packaging (such as ... Read more Size: 4MM 6MM 8MM 10MM 12MM Color: Multi-Color Shape: Round Hole Size: 1-1.5mm Country/Region of Manufacture: China Metal: Natural Gemstone Style: Spacer Brand: Unbranded Material: Natural Gemstone",
        "Condition: New without tags: A brand-new, unused, and unworn item (including handmade items) that is not in original packaging or ... Read more MPN: Does not apply Secondary Stone: Rhinestone Shape: Round Main Stone: Cubic Zirconia Modified Item: No Brand: Unbranded Country/Region of Manufacture: China Metal: 18k Gold Filled Style: Drop/Dangle lot: No Fastening: Hanging Signed?: Unsigned Theme: Beauty",
        "Condition: New with tags: A brand-new, unused, and unworn item (including handmade items) in the original packaging (such as ... Read more Country/Region of Manufacture: China Theme: Beauty Base Metal: 925 Silver Style: Chain Material: 925 Silver Brand: Unbranded Length (inches): 16'-30' MPN: Does Not Apply Color: Silver Package include: 1PC Metal: 925 Silver",
        "Condition: New: A brand-new, unused, unopened, undamaged item in its original packaging (where packaging is ... Read more Non-Domestic Product: No Custom Bundle: No Model: EP-02 MPN: Does Not Apply Modified Item: No Brand: Unbranded Country/Region of Manufacture: China",
        "Condition: New with tags: A brand-new, unused, and unworn item (including handmade items) in the original packaging (such as ... Read more Modified Item: No Theme: Beauty Country/Region of Manufacture: China Thickness: approx. 2mm Style: Chain Main Stone: No Stone Length (inches): Length: approx. 46+5cm Metal: Alloy Material: Leather Brand: Unbranded",
        "Condition: New: A brand-new, unused, unopened, undamaged item (including handmade items). See the seller's ... Read more Non-Domestic Product: No Country/Region of Manufacture: China Color: Please look at pictures Custom Bundle: No Modified Item: No Brand: Unbranded MPN: Does not apply Feather Type: Wedding decoration feather",
        "Condition: New with tags: A brand-new, unused, and unworn item (including handmade items) in the original packaging (such as ... Read more Brand: Unbranded Material: Alloy Style: Charm Beads Pendant Jewelry UPC: Does not apply Theme: DIY EAN: Does not apply Color: Silver MPN: Does not apply",
        "Condition: New without tags: A brand-new, unused, and unworn item (including handmade items) that is not in original packaging or ... Read more Brand: Unbranded Theme: Family & Friends Material: Gemstone & Alloy size: 20*20mm Country/Region of Manufacture: China Main Stone: Natural Gemstone UPC: Does not apply",
        "Condition: New with tags: A brand-new, unused, and unworn item (including handmade items) in the original packaging (such as ... Read more Base Metal: 925 Silver Main Stone Treatment: Filling Brand: Unbranded Material: 925 Silver Country/Region of Manufacture: China Metal: Alloy Gender: Female Secondary Stone: Sapphire Main Stone: White Topaz Style: Cocktail Main Stone Creation: Lab-Created/Cultured Theme: Beauty",
        "Condition: New without tags: A brand-new, unused, and unworn item (including handmade items) that is not in original packaging or ... Read more Country/Region of Manufacture: China Main Stone Treatment: Filling Style: Band Main Stone: Ruby/Emerald/Moonstone/Amethyst/Sapphire Base Metal: 925 Silver/Yellow Gold Filled Main Stone Creation: Lab-Created/Cultured Material: Stone Brand: Unbranded Theme: Beauty Metal: 925 Silver/Yellow Gold Filled Secondary Stone: Gemstone Ring Size: 6 7 8 9 10 (US) Color: As The Pictures",
        "Condition: New without tags: A brand-new, unused, and unworn item (including handmade items) that is not in original packaging or ... Read more Country/Region of Manufacture: China Color: As The Pictures Style: Band Ring Shape: Flower Base Metal: 925 Silver Main Stone Treatment: Filling Material: Stone Main Stone: Opal& Topaz Theme: Flowers & Plants Main Stone Creation: Lab-Created/Cultured Gender: Men/Women/Gril Brand: Unbranded Use Occasion: Wedding/Engagement/Party/Anniversary Gift Metal: 925 Silver Secondary Stone: White Sapphire Ring Size: 6 7 8 9 10 (US)",
        "Condition: New without tags :  Seller Notes: “High Quality & Good Service Assurance” Gender: Women/Gril/Men's Type: Christmas Ring Country/Region of Manufacture: China Use Occasio: Wedding,Engagment,Party,Anniversary Gift Style: Charm Secondary Stone: White Topaz Base Metal: 925 Silver Main Stone: White Sapphire Material: Stone Brand: Unbranded Theme: Family & Friends Metal: 925 Silver",
        "Condition: New with tags: A brand-new, unused, and unworn item (including handmade items) in the original packaging (such as ... Read more Base Metal: 925 Silver Plated Metal: 925 Silver plated Brand: Unbranded Secondary Stone: Sapphire Country/Region of Manufacture: China Style: Cocktail Gender: Female Theme: Beauty Main Stone: White Topaz UPC: Does not apply Main Stone Treatment: Filling",
        "Condition: New with tags: A brand-new, unused, and unworn item (including handmade items) in the original packaging (such as ... Read more Brand: Unbranded Main Stone: Zircon Country/Region of Manufacture: China Ring Size: US 6 / 7 / 8 / 9 / 10 EAN: Does not apply UPC: Does not apply Main Colour: Gold",
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $counter = 1;

        for ($i = 0; $i < 5; $i++) {
            for ($j = 0; $j < 4; $j++) {
                $price = rand(100, 1000);
                $product = new Product([
                    'name' => $this->names[$counter - 1],
                    'description' => $this->descriptions[$counter - 1],
                    'quantity' => rand(1, 20),
                    'price' => $price,
                    'wholesale_price' => $price * rand(10, 100),
                    'type' => 'wholesale',
                    'condition' => $faker->boolean() ? 'New' : 'Used',
                    'pick_up_type' => $faker->boolean() ? 'Pick-Up' : 'Drop-Off', 
                    'sales' => rand(1, 100),
                    'status' => $faker->boolean(75) ? 'active' : 'inactive',
                ]);

                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->products()->save($product);
                $product->images()->saveMany([
                    new Image(['src' => config('services.aws.url') . '/gctrader/img/wholesale-' . $counter . '-1.jpg']),
                    new Image(['src' => config('services.aws.url') . '/gctrader/img/wholesale-' . $counter . '-2.jpg']),
                ]);

                $counter++;
            }
        }
    }
}
