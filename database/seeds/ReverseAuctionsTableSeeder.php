<?php

use App\Image;
use App\Product;
use Faker\Factory;
use App\User;
use Illuminate\Database\Seeder;

class ReverseAuctionsTableSeeder extends Seeder
{
    public $names = [
        "Digital Marketing for my new streetwear brand.",
        "Design logo",
        "Social media advertising",
        "Small business tax return-2018 and 2019",
        "Professional CV/Cover Letter/LinkedIn",
        "I need resume writing",
        "Website for a small busniess",
        "Copywriter to create a media kit for an influencer",
        "Help tax return",
        "TIME AND DATE BOOKING FORM FOR WEBSITE",
        "One recliner chair (electric)& small chest of draw",
        "Resume & CV",
        "Fully integrate my shopify store into a Facebook s",
        "Logo and business card design - feminine",
        "Facebook & Pinterest & YouTube Social Media Gurus",
        "Google and Facebook account HELP 5 mins max",
        "I need ironing",
        "Fully integrate my shopify store into a Facebook s",
        "Good review",
        "AU / British university websites searchin",
    ];

    public $descriptions = [
        "I am looking for someone who has experience in fashion and clothing marketing. The task includes management of Facebook, Instagram and Pinterest for one month.",
        "I can work on a creative, unique and professional logo design for your business as per your requirement.I can provide you my best shot accordingly as I have more than 9 years of experience in graphic designing and development and to check my quality and creativity you can visit my profile to see my work and good reviews.",
        "Looking for someone to handle my social media section for my bathroom renovation business.  I want to generate leads to mu business.",
        "Prepare tax return for 2 years and do the needful.Not much income and expenses.",
        "I am looking for someone to create a professional CV tailored to the IT Industry.  A cover letter specific to a job role and one general one that I can modify and LinkedIn profile done.",
        "Applying for sales consultant Jewellery store.Previous employment Executive Sales Consultant 13 years Caravan industry.",
        "I need some one good at what they do to create a website for my painting busniess.I had my busniess for 9 years and all this years I had one big client and I didn’t needed other work for all this years. That client went broke and from employing 8 workers now I need to start from the start. Can any one tell me what can they do to help and how much .",
        "Hi, I need a copywriter who can help me to create 2 pages media kit for a social influencer.",
        "Annual Tax return for the company 4 personal tax returm",
        "I have a website and i am looking for time and date booking appointment form for wordpress. For Make Business !",
        "Hey, my name is Christine, I need some help moving.",
        "Simple resume and cv for tutoring jobs",
        "I've set up a shopify store, just need an whiz to integrate all my products into.a new Facebook store fully linked, so I can start advertising. Would love a shopify expert with tips and tricks to help. Also may integrate to Amazon eventually so knowledge of this would be advantageous.",
        "Hi we are after a logo and business card to be designed for a new small business. We are after a feminine vintage inspired design. We have all the details of font, colour and design look we are after. We just need someone to put it together for us. We are after a logo for the business card and one comparable with social media. We are hoping to allow for a few edits to get it right. Thank you",
        "Im looking for someone to Manage my social media accounts !!!",
        "I am after someone with a local google guide account and a Facebook account to help me quickly, I will tell you what to do, only take 2-5 minutes maximum. Thank you",
        "Fortnightly or monthly ironing of men’s business shirts and possible couple of other items. Pick up and deliver is preferred. Thanks",
        "I've set up a shopify store, just need an whiz to integrate all my products into.a new Facebook store fully linked, so I can start advertising. Would love a shopify expert with tips and tricks to help. Also may integrate to Amazon eventually so knowledge of this would be advantageous.",
        "Please give me a good review on Airtasker as I am new to this.",
        "I would like to have a list( on a table formate ) of the following requests from the universities names that I will provide you :",
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $counter = 1;

        for ($i = 0; $i < 5; $i++) {
            for ($j = 0; $j < 4; $j++) {
                $closedAt = now()->addDays(rand(1, 10));
                $product = new Product([
                    'name' => $this->names[$counter - 1],
                    'description' => $this->descriptions[$counter - 1],
                    'price' => rand(1001, 5000),
                    'type' => 'reverse_auction',
                    'condition' => $faker->boolean() ? 'New' : 'Used',
                    'pick_up_type' => $faker->boolean() ? 'Pick-Up' : 'Drop-Off',
                    'closed_at' => $closedAt,
                    'status' => $faker->boolean(75) ? 'active' : 'inactive',
                ]);

                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->products()->save($product);
                $product->images()->saveMany([
                    new Image(['src' => config('services.aws.url') . '/gctrader/img/reverse-auction-' . $counter . '.jpg']),
                ]);

                $counter++;
            }
        }
    }
}
