<?php

use App\Job;
use App\Product;
use App\RealEstate;
use App\Service;
use App\Travel;
use App\User;
use App\Vehicle;
use Illuminate\Database\Seeder;

class WatchablesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 5; $i++) { 
            $user = User::findOrFail($i + 1);

            $products = Product::all()->random(rand(5, 8));
            $jobs = Job::all()->random(rand(1, 2));
            $realEstates = RealEstate::all()->random(rand(5, 8));
            $services = Service::all()->random(rand(5, 8));
            $travels = Travel::all()->random(rand(5, 8));
            $vehicles = Vehicle::all()->random(rand(5, 8));

            $user->productWatchlist()->sync($products->pluck('id'));
            $user->jobWatchlist()->sync($jobs->pluck('id'));
            $user->realEstateWatchlist()->sync($realEstates->pluck('id'));
            $user->serviceWatchlist()->sync($services->pluck('id'));
            $user->travelWatchlist()->sync($travels->pluck('id'));
            $user->vehicleWatchlist()->sync($vehicles->pluck('id'));
        }
    }
}
