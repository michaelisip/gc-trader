<?php

use App\User;
use App\Membership;
use Illuminate\Database\Seeder;

class MembershipsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Membership::create([
            'name' => 'Basic',
            'price' => 10.00,
            'span' => 'Annually',
            'sales_count_limit' => 1000,
            'sales_cost_limit' => 20000.00,
            'sales_percentage' => 2,
            'description' => 'Basic membership will charge the user $10 annually with 1,000 products sold under $20,000 with a fee of 2% of each product sold.',
        ]);

        Membership::create([
            'name' => 'Business',
            'price' => 399.00,
            'span' => 'Annually',
            'sales_count_limit' => null,
            'sales_cost_limit' => null,
            'sales_percentage' => 5,
            'description' => 'Business membership will charge the user $399 annually with unlimited products with a fee of 5% of each product sold. Business membership don’t have access to wholesale form.',
        ]);

        Membership::create([
            'name' => 'Wholesale',
            'price' => 299.00,
            'span' => 'Annually',
            'sales_count_limit' => null,
            'sales_cost_limit' => null,
            'sales_percentage' => 2,
            'description' => 'Wholesale membership will charge the user $299 annually with unlimited products with a fee of 2% of each product sold.',
        ]);

        Membership::create([
            'name' => 'Service',
            'price' => 120.00,
            'span' => 'Annually',
            'sales_count_limit' => null,
            'sales_cost_limit' => null,
            'sales_percentage' => null,
            'description' => 'Service listing is $120 annually.',
        ]);

        Membership::create([
            'name' => 'Gold Tier',
            'price' => 599.00,
            'span' => 'Annually',
            'sales_count_limit' => null,
            'sales_cost_limit' => null,
            'sales_percentage' => null,
            'description' => 'For each URL and ad space, there will be a percentage price increase Of 5%, however if you purchase an ad space for 4 or more links, it will keep the entry price on all ad space pending on your package that you have acquired. Starts at 10%.',
        ]);

        Membership::create([
            'name' => 'Platinum Tier',
            'price' => 999.00,
            'span' => 'Annually',
            'sales_count_limit' => null,
            'sales_cost_limit' => null,
            'sales_percentage' => null,
            'description' => 'For each URL and ad space, there will be a percentage price increase Of 5%, however if you purchase an ad space for 4 or more links, it will keep the entry price on all ad space pending on your package that you have acquired. Starts at 20%.',
        ]);

        // User memberships
        // $memberships = Membership::all();
        // for ($i=0; $i < 5; $i++) { 
        //     $user = User::withoutGlobalScopes()->findOrFail($i + 1);
        //     $user->memberships()->sync($memberships->pluck('id'));
        // }
    }
}
