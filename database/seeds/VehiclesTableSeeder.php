<?php

use App\Image;
use App\Vehicle;
use Faker\Factory;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class VehiclesTableSeeder extends Seeder
{
    public $names = [
        "1997 Mitsubishi Verada Ei 4 Sp Automatic 4d Sedan - GREAT FRIST CAR!!",
        "Lexus is250 with RWC and MAY 2020 REGO",
        "2012 Toyota Landcruiser Prado Gxl (4x4) 5 Sp Sequential Auto 4...",
        "Own this beast from $99pw - 2014 Ford Ranger 3.2L 5 cylinder",
        "2004 Toyota Avensis Verso ACM21R GLX Silver 4 Speed Automatic Wagon",
        "2014 Ford Ranger PX Wildtrak Double Cab Orange 6 Speed Sports Automatic Utility",
        "2018 Toyota Corolla ZWE211R SX E-CVT Hybrid White 10 Speed Constant Variable Hatchback Hybrid",
        "2019 Hyundai Santa Fe TM MY19 Active Red 8 Speed Sports Automatic Wagon",
        "2018 Kia Sportage QL MY18 SI (FWD) Silver 6 Speed Automatic Wagon",
        "2018 Hyundai Kona OS MY18 Active 2WD Orange 6 Speed Sports Automatic Wagon",
        "2018 Holden Astra BL MY18 LS+ White 6 Speed Sports Automatic Sedan",
        "2008 MITSUBISHI LANCER VR AUTOMATIC EXCELLENT CONDITION FREE WARRANTY",
        "2012 Mitsubishi Lancer CJ ES Sedan 4dr CVT 6sp 2.0i [MY12] Black Constant Variable Sedan",
        "RWC Roadworthy & Registration Holden Cruze 1.8 CDX Auto only 91ks",
        "TOYOTA TARAGO 92 MODEL WITH REGO",
        "2006 馬自達 Mazda Mazda2 NEO Automatic Hatchback",
        "2009 Volkswagen Polo 9N MY08 Upgrade Pacific Red 6 Speed Automatic Hatchback",
        "real estate",
        "Rent to buy Acreage",
        "VANUATU REAL ESTATE SANTO",
    ];

    public $descriptions = [
        "This car comes from a non smoking environment that has been serviced on a consistent basis. We just put it through service and there is new coolant, engine oil, spark plugs, brake pads and filters. The tyres are nearly 90% new, as we only just changed it a couple months ago. We also recently changed the exhaust and it is like new.",
        "Selling this very clean for the kms 2008 Lexus is250 this car is automatic with power steering and air conditioning and has all the usual Lexus features this car drive like it’s done 150,000 but without the massive price tag as it’s done 305,000km this car would suit someone who has always wanted a is250 but couldn’t afford or wanted not wanting to spend more then this price range this car comes with",
        "Very Impressive Black Prado 3.0 Litre Turbo Diesel 7 Seater Fully tinted windows 150L Extra large fuel tank Dual 12V battery system with rear power outlet for camping/fridge 2 x new car batteries Rhino Rack slimline roofrack Huge towing capacity Front and Rear climate control Keyless entry Great service history Very neat and tidy interior",
        "2014 Ford Ranger 5 Cylinder 3.2L Turbo diesel fully loaded from $22880 Features Include..... Mud Tyres Imitation Bead Locker Rims Iron Man Lift Kit Reverse camera Bull Bar AC 6 Speed Automatic 3.2L Turbo Diesel engine Cruise Control Full Electrics Towbar Kutsnake flares Spot Lights AND MORE........ ",
        "2004 Toyota Avensis Verso ACM21R GLX Silver 4 Speed Automatic Wagon",
        "LOCATED ONLY A 5 MIN DRIVE FROM WEST GATE BRIDGE OPEN 7 DAYS WILDTRAK...LOTS OF EXTRAS......FORD'S MOST POPULAR 4X4 DUAL CAB THE RANGER WILDTRAK 3.2ltr TURBO DIESEL.... LOOKS DRIVES AND HANDLES EXTREMELY WELL..... LOADED WITH FEATURES: ARB BULL BAR, CANOPY, SAT NAV, CLIMATE CONTROL, LEATHER, WINCH. SNORKEL, MUD TYRES WITH ALLOY WHEELS, LED DRIVING LIGHTS, 4X4, WINDOW WINDSHIELDS, BONNET PROCTER, POWER WINDOWS, REAR DIFF LOCK, REVERSE CAMERA, MULTIPLE SAFETY AIRBAGS, FOG LIGHTS, TUB LINER, SIDE AIRBAGS, CD PLAYER, KEYLESS ENTRY, ABS, POWER STEERING, AND MUCH MORE... INSPECTION A MUST AND GUARANTEED TO IMPRESS... WILL BE SOLD WITH A CURRENT ROADWORTHY AND REGISTRATION..... WE ALSO OFFER FINANCE, TRADE INS, AND 5 YEARS EXTENDED WARRANTY ON ALL OUR VEHICLES OVER 60 VEHICLES IN STOCK WITH A WIDE RANGE OF COMMERCIAL UTILITIES LOCATED ONLY A 5 MIN DRIVE FROM WEST GATE BRIDGE OPEN 7 DAYS For more information or a test drive, please call us on ********1150 / ******** 337 or email us directly at inf******@******.au",
        "This is a TOYOTA CERTIFIED vehicle. Certified by a Toyota Dealer in Western Australia, Not your average VEHICLE? Its amazingly affordable Toyota Certified Pre-Owned Vehicle Promise - only the best vehicles are eligible It will have a full-service history It will have 2x keys It will have undergone an independent Car History background check It may include a statutory warranty, depending on your circumstances It will have undergone a comprehensive quality inspection by qualified Toyota Technicians It will offer 1 year of nationwide Toyota Roadside Assist It will have been reconditioned to Toyota's exacting standards by Toyota technicians It will have the option of flexible finance and insurance options available - The best new cars make the best used cars, so getting a high-quality Toyota at a price is a very smart move and getting a high-quality Certified Pre-Owned Toyota at a price is an even smarter move. Enquire TODAY and see for yourself how affordable ACCESS is?? Ask the QUESTION?? This is a TOYOTA CERTIFIED vehicle. Certified by a Toyota Dealer in Western Australia, Not your average VEHICLE? Ask about TOYOTA ACCESS?.. Its amazingly affordable Toyota Certified Pre-Owned Vehicle Promise - only the best vehicles are eligible 10 Reasons to Call us today! 5 Star Toyota Dealer Competitive Flexible Toyota Finance Toyota Certified Pre-owned vehicles 130pt Safety check on all stock We want your trade, Obligation Free onsite valuation Customer satisfaction is our No1 priority Located in the Metro area just 22mins from the CBD We deal Nationwide Too Busy to get to us? We will come to your Home/Office State of the Art workshop with Toyota trained technicians",
        "As new with only 8,000kms, new car warranty until 2024, very economical 2.2 litre turbo diesel engine, sensational eight speed Tiptronic auto, hill descent mode, 360 Â° camera, adaptive cruise control, blind spot monitoring, lane departure warning, diff lock, Bluetooth, front side airbags, alloy wheels, reverse camera and sensors plus more. Unmarked. We are a friendly and efficient company, trading with integrity and determined to give our customers the very best of service. WA's most trusted car dealer? Absolutely! ",
        "FULL SERVICE HISTORY, REVERSE CAMERA AND SENSORS, BLUETOOTH AUDIO, BALA NCE OF NEW CAR WARRANTY, We have over 500 cars and commercials to view onsite and can provide great same day finance packages. We sell vehicle s all over Australia using our quick and easy transport. We are always chasing stock and will trade anything!",
        "2018 Hyundai Kona OS MY18 Active 2WD Orange 6 Speed Sports Automatic Wagon",
        "You can feel safe knowing this vehicle has undergone a rigorous 100-point mechanical and safety check. We also back your new car with a free 3 year/up to 175,000km protection plan giving you peace of mind. Your finance needs are covered with onsite finance options from one of Australia's largest banks to make driving away easier than ever. Our friendly Business Manager is here for you to tailor a repayment that suits your budget. Your time is valuable so we have checked everything, your new car has a clear title and has undergone a full national government database check (PPSR) to ensure it has not been written off, stolen or have money owing on it. Need to trade? We TRADE ALL cars, bikes, boats and vans. The price shown is drive away and includes stamp duty, all transfer fees and shiny new plates with 6 months registration. You do not get that in a private sale! Why wait, enquire today and drive away happy!",
        "2008 Mitsubishi Lancer VR CJ 4D SEDAN 4 Cylinders 2.0 Litre Petrol Cvt Auto 6 Speed Sequen ▪︎▪︎▪︎▪︎▪︎▪︎▪︎ ▪︎Cruise Control ! ▪︎EXCELLENT CONDITION ! ▪︎Auto 6 Speed TRANSMISSION ! ▪︎SERVICE BOOK ! ▪︎USB & AUX ! ▪︎CRUISE CONTROL ! ▪︎Parking Sensors ! ---------------------------------------------------------------- - 5 Years Warranty applicable! - Good Tyres! - Icy Cold AC! - 100% Solid Body! - 100% Sound Engine! - 100% Smooth Transmission! _______________________________ ♡ Trade In's Welcome! Our moto : ********THE BEST PRICE ON THE MARKET***** Stunning is the best way to describe this very popular car. Reliable and priced for a quick sale. A credit to its previous owner this beauty is sure to find a home for the fussy buyer. All vehicles are tested for two major things: No Major or Minor mechanical issue and No structural damage. Extended warranty available (Up to 5 years )Covering Both Parts And Labour! ___________________________________ We are open Mon to Fri 9 am till 6 pm, Sat 9 am till 1 pm Random viewing could possibly be arranged. We are located 15 munities from Perth CDB and 5 munities from Public Transport Please contact us anytime Shan Car Works Group ",
        "2012 MITSUBISHI LANCER ES AUTOMATIC SEDAN $8,499 *EXTENDED WARRANTIES AVAILABLE* THIS LANCER DRIVES IN MINT CONDITION.THIS SEDAN IS VERY FUEL-EFFICIENT AND ECONOMICAL. SPACIOUS AND COMFORTABLE INSIDE. GREAT CAR FOR FIRST TIME CAR BUYERS!!! COME TEST DRIVE THIS GEM TODAY. LANCER FEATURES INCLUDES.......... AIR CONDITIONER POWER WINDOWS,P/S POWER MIRRORS FM RADIO WITH CD PLAYER AIR-BAGS GOOD TYRES,CRUISE CONTROL TRIP COMPUTER. 116,000KMS MUCH MUCH MORE........... WE ARE LOCATED JUST 2 MINUTES DRIVE FROM MADDINGTON TRAIN STATION IN KENWICK 1-5 YEARS WARRANTY OPTIONS AND ROAD SIDE ASSISTANCE AVAILABLE* COMPETITIVE NO DEPOSIT FINANCE AVAILABLE FOR ALL PRICE RANGE APPROVAL WITHIN 24 HOURS (NO APPLICATION REFUSED)* FINANCE AVAILABLE AND MOST APPLICATIONS ARE NOT REJECTED* â€¨ TRADE IN WELCOME !!!! FRIENDLY PROFESSIONAL SALES TEAM. â€¨RELIABLE AFTER SALES AND SERVICE !!! PLEASE CALL US ******0770 MOTORLINE WA 1770 ALBANY HWY, KENWICK 6107 P*****1560 â€œWE SELL QUALITY USED CARS THAT FITS YOUR BUDGETâ€ ",
        "RWC Roadworthy & Registration to 08/03/2020 Ready to Drive Away Only 91000ks Automatic Full Leather Interior Heated Seats Tinted Privacy Glass Bluetooth Aux input Recently Serviced Cruise Control Alloy Wheels New Tyres Cold Air Conditioning Power Steering Clear PPSR History Certificate No Accidents No Finance Outstanding Please Call Anytime ",
        "Toyota Tarago 92 model with Rego till April 2020 Very well presented people mover... Very clean and tidy Tarago... great for that back paper looking to travel OZ ... Can also supply camping gear to get you on the road... extra $$ .. Any questions contact Mike on ******1604.. Located in Crib Point.. $1500... Can provide a RWC at buyer's expense.. as you can see from pictures the last RWC was done in July 2019... NOTHING AT ALL WAS NEEDED FOR THE RWC... ",
        "Low kms. Just done service. Only done 172ks!! Full service history, Smooth and powerful 4 cylinder 1.6ltr engine. Sports auto. Freeze A/C. Power windows. ",
        "USED CAR CLEARACE CENTER. We are located in Melbourne’s West, only 25 Minutes from Melbourne CBD. Open 7 days a week, MONDAY TO SATURDAY 9AM to 5PM and SUNDAY 10AM to 2PM TO SPEED UP YOUR FINACE APPLICATION, PLEASE BRING WITH YOU *Your 3 most recent payslips *Most recent bank statement *Rental statement or rates notice *Centerlink income salary statement Come and see us today – Affordable, Reliable, Cheap cars. Cheap cars arriving daily. ",
        "Fully furnished rooms available in rooming house in Cowes. Single rooms (2) from $220 per week. 2 double rooms from $250 per week. Double rooms suitable for couples or two people sharing. All rooms with individually controlled reverse cycle aircon/heating. House equipped with all mod cons. Regular gardening and maintenance, professional cleaning to all shared areas each fortnight. Rent includes all utility bills as well as NBN. Fully managed by owners. No more to pay for accommodation. Move in with your suitcase and worry free living. Call Lucy to view of know more.",
        "Neat little timber dwelling for sale on 8ha acreage property with a small dam, Millmerran, $98,000 or Rent to buy $250 a week. No Deposit. Small App Fee and first months payment in advance. An ideal weekend escape or a bush retreat, very quiet with rural views. 8 Hectares (20 acres) timbered bushland. Looking to assist someone to own their own property without a large deposit. Will be managed by local real estate and inspected regularly until settlement.",
        "Northern Vanuatu Real Estate and LJ Hooker Vanuatu are now selling a limited number waterfront and beachfront house and land packages on Aore Island Santo, Vanuatu. Beachfront Land only blocks are also for sale from $140,000 Our featured house and land development, Lapita Plantation Aore Island Vanuatu, offer eco lifestyles beachfront house and land packages. Lapita has over of 6km of pristine white sand beach fringed with aqua marine waters, a true piece of paradise in the high demand real estate market on Aore Island, Santo, Vanuatu. Surrounded by aquamarine waters with some 835 acres of lush green coconut plantation Lapita’s luxury resort and residential land and villa and land development covers the entire western portion of Aore Island. Lapita is now partnering with Australian builder Tiny Blox to build dream holiday home or rental villa. Andrew from Tiny Blox is based in Queensland Australia and his key focus is to help you build your dream holiday home or Airbnb investment property. Andrew can offer a complete building solution from design to the complete furnished villa with no hidden costs or surprises. All Lapita villas are built to the highest environmental standards. There are only a limited number villa and land packages available. Vendor Finance on Waterfront Real Estate Santo, Aore Island Vanuatu Special Offer For a limited time Lapita is offering vendor finance for waterfront and island View land. Finance packages are in Vatu and Australian dollars. There are 2 finance packages for you to choose from 20% deposit and 7% interest and principal fixed for 5 years. 30% deposit and 7% interest and fixed for 10 years. Both these packages are available on Island View blocks starting from $60,000 and Waterfront Blocks from $139,000. The block of land you purchase on Lapita is all that is need for security. Applying for finance is quite easy. 1. Fill out finance application 2. Provide proof of income and employment 3. Fortnightly direct debit to Lapita nominated trust account For more information call us on our Vanuatu number 6785980005 WhatsApp *******9191 or click here For more information contact Matthew ",
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $counter = 1;

        for ($i = 0; $i < 5; $i++) {
            for ($j = 0; $j < 4; $j++) {
                $vehicle = new Vehicle([
                    'name' => $this->names[$counter - 1],
                    'model' => $faker->name,
                    'manufacturer' => $faker->lastName,
                    'year' => $faker->year(),
                    'price' => rand(1000, 100000),
                    'description' => $this->descriptions[$counter - 1],
                    'status' => $faker->boolean(75) ? 'active' : 'inactive',
                ]);

                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->vehicles()->save($vehicle);
                $vehicle->images()->saveMany([
                    new Image(['src' => config('services.aws.url') . '/gctrader/img/vehicle-' . $counter . '-1.jpg']),
                    new Image(['src' => config('services.aws.url') . '/gctrader/img/vehicle-' . $counter . '-2.jpg']),
                ]);

                $counter++;
            }
        }
    }
}
