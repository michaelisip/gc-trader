<?php

use App\Feedback;
use App\Service;
use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class FeedbacksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $counter = 1;

        for ($i = 0; $i < 5; $i++) {
            for ($j = 0; $j < 4; $j++) {
                $feedback = new Feedback([
                    'time_management' => $faker->randomElement([
                        'Punctual',
                        'Sometimes late',
                        'Always late',
                    ]),
                    'approachability' => $faker->randomElement([
                        'Friendly',
                        'Professional',
                        'Unsociable',
                    ]),
                    'cost' => $faker->randomElement([
                        'Affordable',
                        'Fair',
                        'Expensive',
                    ]),
                    'performance' => $faker->randomElement([
                        'Over and above',
                        'Average',
                        'Not recommended',
                    ]),
                ]);

                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->feedbacks()->save($feedback);
                $service = Service::withoutGlobalScopes()->findOrFail($counter);
                $service->feedbacks()->save($feedback);

                $counter++;
            }
        }
    }
}
