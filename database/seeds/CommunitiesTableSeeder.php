<?php

use App\Community;
use App\Image;
use Faker\Factory;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class CommunitiesTableSeeder extends Seeder
{
    private $phoneNumbers = [
        '0491 570 156',
        '0491 570 157',
        '0491 570 158',
        '0491 570 159',
        '0491 570 110',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $dateTime = now()->addMonth(rand(1, 12));
        $imageCounter = 1;

        for ($i = 0; $i < 5; $i++) {
            $community = new Community([
                'name' => $faker->company,
                'company_number' => Arr::random($this->phoneNumbers),
                'email' => $faker->email,
                // 'registration' => $faker->numberBetween(10000000000, 99999999999),
                'po_box_address' => $faker->address,
                'description' => $faker->realText(1000),
                'date_time' => $dateTime->toDateTimeString(),
                'start_time' => $dateTime->timestamp,
                'finish_time' => $dateTime->addHours(rand(1, 12))->addDays(rand(1, 31))->timestamp,
                'parking_or_disability_drop_box' => $faker->boolean(),
                'pick_up_type' => $faker->boolean() ? 'Pick-Up' : 'Drop-Off',
                'status' => $faker->boolean(75) ? 'active' : 'inactive',
            ]);

            $user = User::withoutGlobalScopes()->findOrFail($i + 1);
            $user->communities()->save($community);
            $community->images()->saveMany([
                new Image(['src' => config('services.aws.url') . '/gctrader/img/company-logo-' . ($imageCounter) . '.jpg']),
                new Image(['src' => config('services.aws.url') . '/gctrader/img/company-logo-' . ($imageCounter + 1) . '.jpg']),
                new Image(['src' => config('services.aws.url') . '/gctrader/img/company-logo-' . ($imageCounter + 2) . '.jpg']),
            ]);
        }
    }
}
