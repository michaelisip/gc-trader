<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Admin::class)->create();
        factory(App\EmailSubscription::class, 10)->create();

        Model::unguard();

        $this->call([
            UsersTableSeeder::class,
            AdsTableSeeder::class,
            CategoriesTableSeeder::class,
            MembershipsTableSeeder::class,
            AuctionsTableSeeder::class,
            RetailsTableSeeder::class,
            ReverseAuctionsTableSeeder::class,
            WholesalesTableSeeder::class,
            CommunitiesTableSeeder::class,
            JobsTableSeeder::class,
            RealEstatesTableSeeder::class,
            ServicesTableSeeder::class,
            TravelsTableSeeder::class,
            VehiclesTableSeeder::class,
            AddressesTableSeeder::class,
            BidsTableSeeder::class,
            CommentsTableSeeder::class,
            FeedbacksTableSeeder::class,
            // WatchablesTableSeeder::class,
        ]);

        Model::reguard();
    }
}
