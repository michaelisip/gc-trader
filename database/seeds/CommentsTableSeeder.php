<?php

use App\Comment;
use App\Community;
use App\Job;
use App\Product;
use App\RealEstate;
use App\Service;
use App\Travel;
use App\User;
use App\Vehicle;
use Faker\Factory;
use Illuminate\Database\Seeder;

class CommentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 5; $i++) {
            // Community
            for ($j = 0; $j < 5; $j++) {
                $comment = new Comment([
                    'comment' => $faker->realText(),
                    'reply' => $faker->realText(),
                    'replied_at' => now(),
                ]);

                $community = Community::withoutGlobalScopes()->findOrFail($j + 1);
                $community->comments()->save($comment);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->comments()->save($comment);
            }

            // Job
            for ($j = 0; $j < 5; $j++) {
                $comment = new Comment([
                    'comment' => $faker->realText(),
                    'reply' => $faker->realText(),
                    'replied_at' => now(),
                ]);

                $job = Job::withoutGlobalScopes()->findOrFail($j + 1);
                $job->comments()->save($comment);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->comments()->save($comment);
            }

            // Product
            for ($j = 0; $j < 80; $j++) {
                $comment = new Comment([
                    'comment' => $faker->realText(),
                    'reply' => $faker->realText(),
                    'replied_at' => now(),
                ]);

                $product = Product::withoutGlobalScopes()->findOrFail($j + 1);
                $product->comments()->save($comment);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->comments()->save($comment);
            }

            // Real Estate
            for ($j = 0; $j < 20; $j++) {
                $comment = new Comment([
                    'comment' => $faker->realText(),
                    'reply' => $faker->realText(),
                    'replied_at' => now(),
                ]);

                $realEstate = RealEstate::withoutGlobalScopes()->findOrFail($j + 1);
                $realEstate->comments()->save($comment);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->comments()->save($comment);
            }

            // Service
            for ($j = 0; $j < 20; $j++) {
                $comment = new Comment([
                    'comment' => $faker->realText(),
                    'reply' => $faker->realText(),
                    'replied_at' => now(),
                ]);

                $service = Service::withoutGlobalScopes()->findOrFail($j + 1);
                $service->comments()->save($comment);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->comments()->save($comment);
            }

            // Travel
            for ($j = 0; $j < 20; $j++) {
                $comment = new Comment([
                    'comment' => $faker->realText(),
                    'reply' => $faker->realText(),
                    'replied_at' => now(),
                ]);

                $travel = Travel::withoutGlobalScopes()->findOrFail($j + 1);
                $travel->comments()->save($comment);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->comments()->save($comment);
            }

            // Vehicle
            for ($j = 0; $j < 20; $j++) {
                $comment = new Comment([
                    'comment' => $faker->realText(),
                    'reply' => $faker->realText(),
                    'replied_at' => now(),
                ]);

                $vehicle = Vehicle::withoutGlobalScopes()->findOrFail($j + 1);
                $vehicle->comments()->save($comment);
                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->comments()->save($comment);
            }
        }
    }
}
