<?php

use App\Image;
use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    private $fullNames = [
        'Oliver Young',
        'Dominic Anderson',
        'Jude James',
        'Joel Kane',
        'Sonny Bishop',
        'Harolletta Whitlock',
    ];

    private $phoneNumbers = [
        '0491 570 156',
        '0491 570 157',
        '0491 570 158',
        '0491 570 159',
        '0491 570 110',
        '0491 570 111',
    ];

    private $emails = [
        'BellaBarney@jourrapide.com',
        'MariamBurley@teleworm.us',
        'AlannahWatterston@armyspy.com',
        'VictoriaWheller@teleworm.us',
        'CalebHodgkinson@jourrapide.com',
        'support@harallijewels.com.au',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 6; $i++) {
            $fullName = explode(' ', $this->fullNames[$i]);
            // $abnRegistration = $faker->boolean(75) ? $faker->numberBetween(10000000000, 99999999999) : null;

            $user = User::create([
                'first_name' => $fullName[0],
                'last_name' => $fullName[1],
                'email' => $this->emails[$i],
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                // 'type' => is_null($abnRegistration) ? 'Non-Business' : 'Business',
                // 'abn_registration' => $abnRegistration,
                'type' => 'Non-Business',
                'abn_registration' => null,
                'status' => $faker->boolean(75) ? 'active' : 'inactive',
                'gender' => $faker->boolean() ? 'Male' : 'Female',
                'birth_date' => $faker->date,
                'phone_number' => $this->phoneNumbers[$i],
                'remember_token' => Str::random(10),
            ]);

            $user->image()->save(new Image(['src' => config('services.aws.url') . '/gctrader/img/user-profile-' . ($i + 1) . '.jpg']));
        }
    }
}
