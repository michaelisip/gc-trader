<?php

use App\Address;
use App\Community;
use App\Job;
use App\Product;
use App\RealEstate;
use App\Service;
use App\Travel;
use App\User;
use App\Vehicle;

use Faker\Factory;
use Illuminate\Database\Seeder;

class AddressesTableSeeder extends Seeder
{
    private $states = [
        'New South Wales',
        'Victoria',
        'Queensland',
        'Western Australia',
        'South Australia',
    ];

    private $suburbs = [
        'Bankstown',
        'Carlton',
        'Albion',
        'Alexander Heights',
        'Adelaide',
    ];

    private $cities = [
        'Sydney',
        'Melbourne',
        'Brisbane',
        'Perth',
        'Adelaide',
    ];

    private $postcodes = [
        '2055',
        '3002',
        '4001',
        '6004',
        '5001',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faker = Factory::create();

        // Job
        for ($i = 0; $i < 5; $i++) {
            $set = rand(0, 4);
            $job = Job::withoutGlobalScopes()->findOrFail($i + 1);
            $job->address()->save(new Address([
                'country' => 'Australia',
                'state' => $this->states[$set],
                'suburb' => $this->suburbs[$set],
                'city' => $this->cities[$set],
                'post_code' => $this->postcodes[$set],
                'street_name' =>  $faker->streetName,
                'number' =>  $faker->buildingNumber,
            ]));
        }

        // Product
        for ($i = 0; $i < 80; $i++) {
            $set = rand(0, 4);

            $product = Product::withoutGlobalScopes()->findOrFail($i + 1);
            $product->address()->save(new Address([
                'country' => 'Australia',
                'state' => $this->states[$set],
                'suburb' => $this->suburbs[$set],
                'city' => $this->cities[$set],
                'post_code' => $this->postcodes[$set],
                'street_name' =>  $faker->streetName,
                'number' =>  $faker->buildingNumber,
            ]));
        }

        // Real Estate
        for ($i = 0; $i < 20; $i++) {
            $set = rand(0, 4);

            $realEstate = RealEstate::withoutGlobalScopes()->findOrFail($i + 1);
            $realEstate->address()->save(new Address([
                'country' => 'Australia',
                'state' => $this->states[$set],
                'suburb' => $this->suburbs[$set],
                'city' => $this->cities[$set],
                'post_code' => $this->postcodes[$set],
                'street_name' =>  $faker->streetName,
                'number' =>  $faker->buildingNumber,
            ]));
        }

        // Service
        for ($i = 0; $i < 20; $i++) {
            $set = rand(0, 4);

            $service = Service::withoutGlobalScopes()->findOrFail($i + 1);
            $service->address()->save(new Address([
                'country' => 'Australia',
                'state' => $this->states[$set],
                'suburb' => $this->suburbs[$set],
                'city' => $this->cities[$set],
                'post_code' => $this->postcodes[$set],
                'street_name' =>  $faker->streetName,
                'number' =>  $faker->buildingNumber,
            ]));
        }

        // Travel
        for ($i = 0; $i < 20; $i++) {
            $set = rand(0, 4);

            $travel = Travel::withoutGlobalScopes()->findOrFail($i + 1);
            $travel->address()->save(new Address([
                'country' => 'Australia',
                'state' => $this->states[$set],
                'suburb' => $this->suburbs[$set],
                'city' => $this->cities[$set],
                'post_code' => $this->postcodes[$set],
                'street_name' =>  $faker->streetName,
                'number' =>  $faker->buildingNumber,
            ]));
        }

        // User
        for ($i = 0; $i < 5; $i++) {
            $set = rand(0, 4);

            $user = User::withoutGlobalScopes()->findOrFail($i + 1);
            $user->address()->save(new Address([
                'country' => 'Australia',
                'state' => $this->states[$set],
                'suburb' => $this->suburbs[$set],
                'city' => $this->cities[$set],
                'post_code' => $this->postcodes[$set],
                'street_name' =>  $faker->streetName,
                'number' =>  $faker->buildingNumber,
            ]));
        }

        // Vehicle
        for ($i = 0; $i < 20; $i++) {
            $set = rand(0, 4);

            $vehicle = Vehicle::withoutGlobalScopes()->findOrFail($i + 1);
            $vehicle->address()->save(new Address([
                'country' => 'Australia',
                'state' => $this->states[$set],
                'suburb' => $this->suburbs[$set],
                'city' => $this->cities[$set],
                'post_code' => $this->postcodes[$set],
                'street_name' =>  $faker->streetName,
                'number' =>  $faker->buildingNumber,
            ]));
        }
    }
}
