<?php

use App\Ad;
use App\Image;
use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        for ($i = 0; $i < 5; $i++) {
            $ad = new Ad([
                'type' => $faker->boolean() ? 'Gold Tier' : 'Platinum Tier',
                'status' => $faker->boolean(75) ? 'active' : 'inactive'
            ]);

            if ($ad->type === 'Gold Tier') {
                $image = new Image(['src' => config('services.aws.url') . '/gctrader/img/gold-ad.jpg']);
            } else {
                $image = new Image(['src' => $faker->randomElement([
                    config('services.aws.url') . '/gctrader/img/platinum-ad-1.jpg',
                    config('services.aws.url') . '/gctrader/img/platinum-ad-2.jpg',
                    config('services.aws.url') . '/gctrader/img/platinum-ad-3.jpg',
                ])]);
            }

            $user = User::withoutGlobalScopes()->findOrFail($i + 1);
            $user->ads()->save($ad);
            $ad->image()->save($image);
        }
    }
}
