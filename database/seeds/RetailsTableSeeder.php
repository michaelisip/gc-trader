<?php

use App\Image;
use App\Product;
use Faker\Factory;
use App\User;
use Illuminate\Database\Seeder;


class RetailsTableSeeder extends Seeder
{
    public $names = [
        "Wholesale New 925 Sterling Silver Filled 2mm Classic Chain Necklace For Pendants",
        "Black Woven Necklace Rope Leather Cord Stainless Steel Men Women Lobster Clasp",
        "Stunning 925 Sterling Silver Filled 4MM Classic Curb Necklace Chain Wholesale",
        "Hippy 90s Stretch Tattoo Elastic Boho Choker Necklace Bracelet Cord Retro Gothic",
        "Lady Women Ocean Heart Austrian Crystal Chain Jewelry Bracelet Bangle Fashion",
        "925 Sterling Silver Hoop Ring Bead Sleeper Earrings Lip Ear Nose Body Piercing",
        "3PCS Boho Turquoise Turtle Beaded Ankle Anklet Bracelet Foot Chain Beach Set",
        "Titanium Stainless Steel 8mm Brushed Finish Men Women Wedding Band Spinner Ring",
        "8mm Blue Spinner Chain Band Men's Stainless Steel Wedding Black Ring Size 7-13",
        "GREAT CONDITION iPhone 7 Plus 128GB BLACK WITH WARRANTY RECEIPT",
        "SAMSUNG S7 EDGE 32GB GOLD/SILVER COLOUR",
        "IPHONE 8 64 SPACE GREY COLOUR ON SALE WITH WARRANTY",
        "SAMSUNG S10 PLUS 128GB WHITE/GREEN COLOUR",
        "AS NEW IPHONE XS 256GB SILVER COLOUR",
        "Samsung galaxy s10 e 128gb brand new in box warranty warranty Invoice",
        "Yamaha Bb Trumpet w/ Accessories",
        "GIBSON Larry Carlton 'Mr-335' GUITAR custom shop, Mint, AS NEW!",
        "Yamaha PSR-SX900 Workstation Keyboard",
        "Cornish & Co. Pump Organ",
        "Playwood Blue Hard Marimba Mallets - M-402",
    ];

    public $descriptions = [
        "Wholesale New 925 Sterling Silver Filled 2mm Classic Chain Necklace For Pendants",
        "Black Woven Necklace Rope Leather Cord Stainless Steel Men Women Lobster Clasp",
        "AU STOCK, Good Quality, Next Day Dispatch",
        "Buy 2 get 1 free ( add 3 to cart please, otherwise no free one).Au stock, fast shipping. 12 colors available.",
        "New with tags ",
        "Brand New, Genuine 925 Sterling Silver, Allergy Free",
        "Turquoise Turtle Beaded Ankle",
        "Titanium Stainless Steel 8mm Brushed Finish Men Women Wedding Band Spinner Ring",
        "Blue Spinner Chain Band Men's Stainless Steel Wedding Black Ring",
        "POPULAR AND LICENSED MOBILE PHONE RETAIL DEALER *PRICE IS FIXED, NO OFFERS PLEASE CALL 0 40 131 8182, CASH DISCOUNT FOR TRADE-INS MODEL: iPHONE 7 PLUS 128GB MATT BLACK CONDITION: GREAT CONDITION, FULLY WORKING AND UNLOCKED ACCESSORIES: USB CHARGER CABLE WARRANTY: 3 MONTHS SHOP WARRANTY BUY SELL TRADE IPHONE, SAMSUNG PHONES, IPAD AND TABLETS EFTPOS AND CARD WELCOME 3% Address: SHOP 822 / 824 GYMPIE RD, CHERMSIDE, 4032. BRISBANE LOCATED OPPOSITE WESTFIELD CHERMSIDE SHOPPING CENTRE, THE CORNER OF GYMPIE ROAD AND HAMILTON RD. PARKING IS AVAILABLE AT THE BACK OF THE SHOP OR OPPOSITE WESTFIELD CHERMSIDE SHOPPING CENTRE Trade in your used unwanted phone to get a discount on this phone or cash! Our Opening Hours: Monday - Thursday 9:15 AM - 6:00 PM Friday - Saturday 9:15 AM - 5:30 PM Sunday- BY APPOINTMENT",
        "*Please note, Phones are fixed price, not negotiable. Thank you. Platinum Phone Services We buy, sell, trade, fix, repair iPhone, Samsung Galaxy, iPad We pay cash for brand new and used mobile phones (secondhand cell phone) Please call ********8453 _____________ PRODUCT INFO _____________ MODEL: Samsung s7 edge 32gb COLOUR: GOLD - $270 (good condition) SILVER - $270 (good condition) CONDITION: Unlocked to any network, comes with brand-new charging cable WARRANTY: Provided with 1 (one) months free warranty, you can purchase additional warranty $10 per 1 (one) month. Payment Option: Cash or Bank-transfer payment is most welcomed, 3% extra surcharge for credit card payment, No lay-by option will be considered. _____________ BUSINESS INFO ",
        "platinumphoneservices.com.au *Please note, Phones are fixed price, not negotiable. Thank you. Platinum Phone Services We buy, sell, trade, fix, repair iPhone, Samsung Galaxy, iPad We pay cash for brand new and used mobile phones (secondhand cell phone) Please call ********8453 _____________ PRODUCT INFO _____________ MODEL: iPhone 8 64gb COLOUR: SPACE GREY - $500 (new replacement from apple) CONDITION: Unlocked to any network, comes with brand-new charging cable WARRANTY: Provided with 3 (three) months free warranty, you can purchase additional warranty $10 per 1 (one) month. Payment Option: Cash or Bank-transfer payment is most welcomed, 3% extra surcharge for credit card payment, No lay-by option will be considered. _____________ BUSINESS INFO _____________ Platinum Phone Services 53Address: Shop 1A, 126 Scarborough st, Southport, QLD, 4215 Trading hours: Weekdays 9.30am – 5.30pm, weekends 9.30am-5.00pm Parking: Street parking is available right in front of the shop",
        "platinumphoneservices.com.au *Please note, Phones are fixed price, not negotiable. Thank you. Platinum Phone Services We buy, sell, trade, fix, repair iPhone, Samsung Galaxy, iPad We pay cash for brand new and used mobile phones (secondhand cell phone) Please call ********8453 _____________ PRODUCT INFO _____________ MODEL: Samsung s10 PLUS 128gb COLOUR: PRISM WHITE - $840 (as new condition) PRISM GREEN - $830 (good condition with box) CONDITION: Unlocked to any network, comes with brand-new charging cable WARRANTY: 2 year Samsung Warranty Payment Option: Cash or Bank-transfer payment is most welcomed, 3% extra surcharge for credit card payment, No lay-by option will be considered. _____________ BUSINESS INFO _____________ Platinum Phone Services 53Address: Shop 1A, 126 Scarborough st, Southport, QLD, 4215 Trading hours: Weekdays 9.30am – 5.30pm, weekends 9.30am-5.00pm Parking: Street parking is available right in front of the shop",
        "platinumphoneservices.com.au *Please note, Phones are fixed price, not negotiable. Thank you. Platinum Phone Services We buy, sell, trade, fix, repair iPhone, Samsung Galaxy, iPad We pay cash for brand new and used mobile phones (secondhand cell phone) Please call ********8453 MODEL: iPhone Xs 256gb COLOUR: SILVER - $980 (as new) CONDITION: Unlocked to any network, comes with brand-new charging cable WARRANTY: Provided with 3 (three) months free warranty, you can purchase additional warranty $10 per 1 (one) month. Payment Option: Cash or Bank-transfer payment is most welcomed, 3% extra surcharge for credit card payment, No lay-by option will be considered. _____________ BUSINESS INFO _____________ Platinum Phone Services Address: Shop 1A, 126 Scarborough st, Southport, QLD, 4215 Trading hours: Weekdays 9.30am – 5.30pm, weekends 9.30am-5.00pm Parking: Street parking is available right in front of the shop",
        "L&co mobile Eftpos available (2% Bank surcharge credit) Will provide receipt and warranty for item sold Address: Shop 6 554 south pine road Everton park 4053 Opening hour Mon: closed Tue-Fri 10am- 6pm Sat-Sunday 10am-5pm -------------- Samsung galaxy s10 E 128gb Brand new in box with plastic still in phone, Australian model Network unlocked Price is firm please no offers Trade in your used unwanted phone to get discount on this phone or cash your way!!!! Postage available !!!!! Thanks for reading !",
        "On offer is my Yamaha 2335 Bb Trumpet ($870 equivalent new). Included is a second upgraded Bach 3C mouthpiece ($100 new), a shhmute trumpet practise mute ($70 new), and a leather valve guard ($25 new). The trumpet is in perfect functioning condition. No dents or mechanical issues. It does have a slight “vintage” appearance due to it’s age (20 years) which is purely cosmetic and in no way affects the quality of the instrument (in fact, it only makes it sound funkier!) I am now a high school music teacher and this trumpet has served me well. It is an excellet and affordable instrument for a student who is serious about learning :) Here is some further detail on the Yamaha 2335 Bb Trumpet: https://www.trumpethub.com/yamaha-ytr-2335-student-trumpet-review/ Here is the equivalent new Yamaha 2330 Bb Trumpet that retails for $870: https://au.yamaha.com/en/products/musical_instruments/winds/trumpets/bb_trumpets/ytr-2330/index.html Pick up from Calamvale MacDonalds carpark.",
        "GIBSON COLLECTOR'S GUITAR: This is the fabulous and unique Gibson Custom Shop Signature Model LARRY CARLTON 'Mr-335' model guitar -- with all Gibson original factory inclusions, and absolutely as new mint condition.",
        "Take your performance to a whole new dimension with the PSR-SX. Replacing the hugely successful PSR-S series, the PSR-SX900 is the new generation in Digital Workstation sound, design and user experience. These instruments will inspire and intensify your musical performance and enjoyment.",
        "I am selling this beautiful pump organ from Cornish and Co., more than a hundred years old. Its charm makes it a beautiful musical instrument as well as a fascinating antique ornament, especially in parlours and living rooms. In working order, it has recently been restored. Available for pick ups (two people needed) or delivery in Brisbane/Ipswich areas for a small fee. Please feel free to watch my other ads, Thank you, Alice ",
        "Previously used. Good condition. One stick has dints from hitting rim of musical instrument. Heads slightly frayed",
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();
        $counter = 1;

        for ($i = 0; $i < 5; $i++) {
            for ($j = 0; $j < 4; $j++) {
                $product = new Product([
                    'name' => $this->names[$counter - 1],
                    'description' => $this->descriptions[$counter - 1],
                    'quantity' => rand(1, 20),
                    'price' => rand(100, 1000),
                    'type' => 'retail',
                    'condition' => $faker->boolean() ? 'New' : 'Used',
                    'pick_up_type' => $faker->boolean() ? 'Pick-Up' : 'Drop-Off',
                    'sales' => rand(1, 100),
                    'status' => $faker->boolean(75) ? 'active' : 'inactive',
                ]);

                $user = User::withoutGlobalScopes()->findOrFail($i + 1);
                $user->products()->save($product);
                $product->images()->saveMany([
                    new Image(['src' => config('services.aws.url') . '/gctrader/img/retail-' . $counter . '-1.jpg']),
                    new Image(['src' => config('services.aws.url') . '/gctrader/img/retail-' . $counter . '-2.jpg']),
                ]);

                $counter++;
            }
        }
    }
}
