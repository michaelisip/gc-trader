<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Community
        Category::create([
            'parent_id' => 0,
            'name' => 'Family Support, Social Services and Economic Development',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Housing and Accommodation',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Food',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Electricity',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Medical',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hospitals',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Medical Centre’s',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Transport',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Income Crisis',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Clothing',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Disabilities',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Sports and Recreation Groups and Associations',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Burleigh Bears',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Varsity Rowing Club',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Hobbies',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Lapidary',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Woodwork',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Knitting',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Community Centre',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Senior Centre',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Cultural Groups',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Craft and Artisans',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Health and Well Being Support Groups',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hospital',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Medical Centre’s',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Weight Loss',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Counseling',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sexual Victims',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bereavement and Grief counseling',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Addiction Help',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Drugs',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Gambling',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Natural Disaster Emergency Relief',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Flooding',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fire',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Earthquake',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Tornado',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Rural Groups',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Employment Education',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Charity',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fund-raising',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Promotion of volunteering',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Environment and Conservation',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Care and Protection of Animals',
            'type' => 'community',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'RSPCA',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Cultural Groups',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Religious Activities',
            'type' => 'community',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Other',
            'type' => 'community',
        ]);

        // Job
        Category::create([
            'parent_id' => 0,
            'name' => 'Accounting',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Administration & Office Support',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Advertising, Arts & Media',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Banking & Financial Services',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Call Centre & Customer Service',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'CEO & General Management',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Community Services & Development',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Construction',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Consulting & Strategy',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Design & Architecture',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Education & Training',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Engineering',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Farming, Animals & Conservation',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Government & Defence',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Healthcare & Medical',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hospitality & Tourism',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Human Resources & Recruitment',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Information & communication Technology',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Insurance & Superannuation',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Legal',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Manufacturing, Transport & Logistics',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Marketing & Communications',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Mining, Resources & Energy',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Real Estate & Property',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Retail & Consumer Products',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sales',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Science & Technology',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Self Employment',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sports & Recreation',
            'type' => 'job',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Trades & Services',
            'type' => 'job',
        ]);

        // Product
        Category::create([
            'parent_id' => 0,
            'name' => 'Antiques, Home and Jewelry Links',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Antiques & collectables',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Advertising',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Appliances',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Art deco & retro',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Automotive & transport',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Banknotes',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bottles',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Clocks & scientific instruments',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Coins',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Culture & ethnic',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Documents & maps',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Flags',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Food & drink',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Furniture & woodenware',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Millitary',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Movie & TV memorabilia',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Museum pieces &artifacts',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'New Zealand & Maori',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Ornaments & figurines',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Phonecards',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Pins, badges & patches',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Postcards & writing',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Roya Family',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Silver metalwires & tins',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Stamps',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Textiles & linen',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Tabacco related',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Other',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Art',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Art supplies & Equipment',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Carvings & sculpture',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Drawings',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hangging sculptures',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'NZ Artists',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Paintings',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Photographs',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Paints',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Tattoos',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Baby gear',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Baby carriers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Baby monitors',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Baby rooms & furniture',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Baby walkers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bath time',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Blankets & covers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bouncers & jolly jumpers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Car seats',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Clothing',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Cots & bassinets',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Feeding',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Nappies & changing',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Prams & stroller',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Safely',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sleep aids',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Teething necklaces',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Toys',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Book',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Audio books',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bulk',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Childer & babies',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Comics & graphic novels',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fiction & literature',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Foreign language',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Magazines',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Non-fiction',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Rare & collectable',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Teaching resources & education',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Young adult fiction',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Building & renovation',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Building supplies',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Carpet, tiles & flooring',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Door, windows & moundings',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Electrical & lights',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fixture & fittings',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Paintings & wallpaper',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Plumbing & gas',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Portable cabins',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Tools',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Business, farming & industry',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Businesses for sale',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Carbon credits',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Farming & forestry',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Industrial',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Office equipment & supplies',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Office furniture',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Postage & packing supplies',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Retail & hospitality',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Wholesale lots',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Clothing & Fashion',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Boys',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Girls',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Men',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Women',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Computers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => '3D Printers & supplies',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Blank discs & cases',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Cables & adaptors',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Components',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Computer furniture',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Desktops',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Domain names',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'External Storage',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Laptops',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Monitors',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Networking & modems',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Peripherals',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Printer accessories & supplies',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Printers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Servers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Software',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Tables & E-book readers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Vintage',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Electronics & photography',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Batteries',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Binoculars & telescopes',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Camera accessories',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Digital cameras',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'DVD & Blu-ray players',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Film cameras',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'GPS',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Home audio',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'iPod & MP3 accessories',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'iPod',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'MP3 players',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Phone & fax',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Projectors & screens',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Radio equipment',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'TVs',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Video cameras',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Other electornics',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Other music players',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Flatmates wanted',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Gaming',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Arcade & pinball machines',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Joysticks & gamepads',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Nintendo 3DS',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Nintendo DS',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Nintendo Switch',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Nintendo Wii',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Nintendo Wii U',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'PC games',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'PlayStation',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'PlayStation2',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'PlayStation3',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'PlayStation4',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'PlayStation Vita',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'PSP',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sega ',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Trading cards',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'War games',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Xbox',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Xbox 360',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Xbox One',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Xbox Nintendo',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Health & beauty',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bath & shower',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Body moisturisers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Face care',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fragrance gift sets',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fragrance miniatures',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Gift packs',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Glasses & contacts',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hair accessories',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hair care products',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hair dryers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hair straighteners',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hand & foot care',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Makeup',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Massage',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Medical supplies',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Medicines',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Men\'s fragrances',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Mobility aids',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Naturopathy',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Personal hygiene',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Pregnancy & maternity',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Relaxation & hypnosis',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Scissors & clippers',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Shaving & hair removal',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sun care & tanning',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Weight logistics',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Women\'s fragrances',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Other hair care & grooming',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Home & living',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bathroom',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Beddings & towels',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bedroom furniture',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Beds',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Cleaning & bins',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Curtains & blinds',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Food & beverage',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Heating & cooling',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Home deco',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Kitchen ',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Lumps',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Laundry',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Lifestyle',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Lounge, dining & hall',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Luggage & travel accessories',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Outdoor, garden & conservation',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Party & festive supplies',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Security lock & alarm',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Wine',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Jewellery & watches',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Body jewellery & pierchings',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bracelets & bangles',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Brooches',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Earrings',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Gemstones',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Jewellery',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Matching sets',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Men\'s jewellery',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Necklaces & pendants',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Rings',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Smart Watches',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Watches',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Mobile phones',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Accessories',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Replacement parts & components',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'SIM cards',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Movies & TV',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Blu-ray',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'DVDs',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'VHS',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Music & instruments',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'CDs',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'instruments',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Music DVDs',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Music memorabilia',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Music tuition',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'PA, pro audio & DJ equipment',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sheet music',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Tapes',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Vinyl',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Pets & animals',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Birds',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Cats',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Dogs',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fish',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Lost & found',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Mice & rodents',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Rabbits & turtles',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Pottery & glass',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Glass & Crystal',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Porcelain & pottery',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Services',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Domestic services',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Events & entertainment',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Health & wellbeing',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Other services',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sports',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Aviation',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Basketball',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bowling',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Camoung',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Cricket',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Cycling',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Dancing & gymnastics',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Darts',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Equestrian',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Exercise equipment & weights',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fishing',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Golf',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Gym membershios',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hockey',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hunting & shooting',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Kayaks',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Kites & kitesurfing',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Martial arts & boxing',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Netball',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Paintball',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Protective gear & armour',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Racquet sports',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Rugby & league',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Running, track & field',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'SC snorkelling',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Skateboarding & rollerblading',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Ski & board',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Snooker & pool',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Soccer',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Softball & baseball',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sports bags',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sports memorabilia',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sports nutrition & supplements',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Surfing',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Swimming',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Waterskiing & wakeboarding',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Wetsuits & lifejackets',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Windsurfing',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Toys & models',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bath toys',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Battery & wind-up',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bears',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bulk lots',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Die casts',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Dolls',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Educational toys',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fidget toys',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Figurines & miniatures',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Games, puzzles & tricks',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Kids\' arts & crafts',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Kids\' furniture',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Lego & Building toys',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'models',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Outdoor toys & trampolines',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Play instruments & microphones',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Pretend playing',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Radio control & robots',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Ride-on toys',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Slot cars & tracks',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Soft toys & brears',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Vehicle toys',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Wooden',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Travel, events & activities',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Accommodation',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Activities',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Event tickets',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Flights',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Holiday packages',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Trade Me Jobs',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Accounting',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Agriculture, fishing & forestry',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Architecture',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Constraction & roading',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Customer service',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Education',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Engineering',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Executive & general management',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Govenment & council',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Healthcare',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hospitality & tourism',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'HR & recruitment',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'IT',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Legal',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Manufacturing & operation',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Marketing medua & communications',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Office & administration',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Property',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Retail',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sales',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Science & technology',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Trades & services',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Transport & logistics',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Trade Me Motors',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Aircraft',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Boats & marine',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Buses',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Car parts & accessories',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Car stereos',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Caravans & motorhomes',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Cars',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Horse floats',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Motorbikes',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Specialist cars',
            'type' => 'product',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Trailers',
            'type' => 'product',
        ]);

        // Service

        Category::create([
            'parent_id' => 0,
            'name' => 'Domestic services',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Animals & pets',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Architecture & design',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Babysitters',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Cleaning',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Computing',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Moving & storage',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Nannies & carers',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Outdoor & garden',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Repairs & maintenance',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Other domestic services',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Caterers',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Entertainers',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Party rentals & supplies',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Photography & video',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Wedding',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Other event services',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Beauty & relaxation',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Life coaching & counselling',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Medical services',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Personal training',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Weight loss',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Other health services',
            'type' => 'service',
        ]);


        Category::create([
            'parent_id' => 0,
            'name' => 'Trades',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Alarm & security',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Automotive',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Building & construction',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Electrical & gas',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Flooring',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Glass',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Painting & plastering',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Plumbing',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Roofing',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Other trade services',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Experiences & tourism',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Farming & agriculture',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Financial & legal',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Gifts & hampers',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Property & real estate',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Spiritual',
            'type' => 'service',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Tuition',
            'type' => 'service',
        ]);

        // Travel
        Category::create([
            'parent_id' => 0,
            'name' => 'Accommodation',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hotel',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Motel',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Apartments',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Luxury Lodges',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Holiday Homes',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Holiday Packages',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hostel',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Backpackers',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Bed and Breakfast',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Camping',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Holiday Parks and Campgrounds',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'House Boat',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Tiny House',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Home Stay',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Farm Stay',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Activities',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Theme Parks',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Movieworld',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Wet N Wild',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sea World',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Dreamworld',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Zoo',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Currumbin Wildlife Park',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Water',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Kayaking',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'River Cruises',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Island Cruises',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Surfing',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Wind Surfing',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Yachting',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Aqua Splash',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Whale watching',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Aqua Duck',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Wheels',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Go Carts',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'E Bikes',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Push Bikes',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Segways',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Scooter Hire',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'E Scooters',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Flying Fox',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Glow Worms',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Ifly',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Wax Museum',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Ripley’s believe it or not',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Q1 Sky Walk',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sky Casino',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Hiking Tracks and Trails',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Adventure Trails',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Tambourine Mountain',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Natural Arch',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Horse Riding',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Outback Spectacular',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Holoverse',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sling Shot',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Shopping',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Golf',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Tropical Fruit World',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Ex Stream Activities',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Air Balloon',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Jet Boat',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sky Diving',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Para Gliding',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Mountain Climbing',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Jet Ski',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fly Board',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Paintball',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Free Activities',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Southbank Park',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Southport Parklands',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Beach',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Mountain Climbing',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Trekking',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Flights',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Flight Centre',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Domestic',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'International',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Events and Tickets',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Music',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Sports',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Theatre',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Expo',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Food and Wine',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Fashion',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Racing Turf Club',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Mudgeeraba Show',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'GC Show',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'GC Expo',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Single and Ready to Mingle Expo',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Travel Expo',
            'type' => 'travel',
        ]);

        Category::create([
            'parent_id' => 0,
            'name' => 'Vouchers',
            'type' => 'travel',
        ]);
    }
}
