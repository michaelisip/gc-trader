<?php

namespace App;

use App\Scopes\StatusScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ad extends Model
{
    use SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new StatusScope);
    }

    protected $guarded = ['status'];

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function scopeGoldTier($query)
    {
        return $query->where('type', 'Gold Tier');
    }

    public function scopePlatinumTier($query)
    {
        return $query->where('type', 'Platinum Tier');
    }

    public function getIsGoldTierAttribute()
    {
        return $this->type === 'Gold Tier' ? true : false;
    }

    public function getIsPlatinumTierAttribute()
    {
        return $this->type === 'Platinum Tier' ? true : false;
    }
}
