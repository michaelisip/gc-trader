<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WholesaleRequest extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'vendor_id',
        'buyer_id',
        'product_id',
    ];

    public static function boot()
    {
        parent::boot();

        static::addGlobalScope('status', function (Builder $builder) {
            $builder->where('status', 'pending');
        });
    }

    public function vendor()
    {
        return $this->belongsTo('App\User', 'vendor_id');
    }

    public function buyer()
    {
        return $this->belongsTo('App\User', 'buyer_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
