<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('buy-items', 'App\Policies\BuyingPolicy@buy');
        Gate::define('bid-items', 'App\Policies\BuyingPolicy@bid');
        Gate::define('watch-items', 'App\Policies\BuyingPolicy@watch');

        Gate::define('create-gold-ad', 'App\Policies\AdPolicy@createGold');
        Gate::define('create-platinum-ad', 'App\Policies\AdPolicy@createPlatinum');

        Gate::define('create-comment', 'App\Policies\CommentPolicy@create');
        Gate::define('update-comment', 'App\Policies\CommentPolicy@update');

        Gate::define('view-any-auction-product', 'App\Policies\ProductPolicy@viewAnyAuctions');
        Gate::define('view-any-retail-product', 'App\Policies\ProductPolicy@viewAnyRetails');
        Gate::define('view-any-reverse-auction-product', 'App\Policies\ProductPolicy@viewAnyReverseAuctions');
        Gate::define('view-any-wholesale-product', 'App\Policies\ProductPolicy@viewAnyWholesales');

        Gate::define('view-auction-product', 'App\Policies\ProductPolicy@viewAuction');
        Gate::define('view-retail-product', 'App\Policies\ProductPolicy@viewRetail');
        Gate::define('view-reverse-auction-product', 'App\Policies\ProductPolicy@viewReverseAuction');
        Gate::define('view-wholesale-product', 'App\Policies\ProductPolicy@viewWholesale');

        Gate::define('create-auction-product', 'App\Policies\ProductPolicy@createAuction');
        Gate::define('create-retail-product', 'App\Policies\ProductPolicy@createRetail');
        Gate::define('create-reverse-auction-product', 'App\Policies\ProductPolicy@createReverseAuction');
        Gate::define('create-wholesale-product', 'App\Policies\ProductPolicy@createWholesale');

        Gate::define('create-service', 'App\Policies\ServicePolicy@create');
    }
}
