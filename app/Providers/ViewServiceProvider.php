<?php

namespace App\Providers;

use App\Http\View\Composers\Admin\Admin;
use App\Http\View\Composers\App\App;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('admin.*', Admin::class);
        View::composer('auth.*', App::class);
        View::composer('app.*', App::class);
    }
}
