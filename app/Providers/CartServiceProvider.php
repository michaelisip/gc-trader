<?php

namespace App\Providers;

use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;

class CartServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Collection::macro('totalCartItems', function () {
            return $this->reduce(function ($carry, $item) {
                return $carry + $item->quantity;
            });
        });

        Collection::macro('totalCartPrice', function () {
            return $this->reduce(function ($carry, $item) {
                return $carry + ($item->price * $item->quantity);
            });
        });
    }
}
