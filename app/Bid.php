<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bid extends Model
{
    use SoftDeletes;

    protected $guarded = [
        'bid',
        'payment_intent',
    ];

    public function biddable()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getFormattedBidAttribute()
    {
        return number_format($this->bid, 2, '.', ',');
    }
}
