<?php

namespace App;

use App\Scopes\QuantityScope;
use App\Scopes\StatusScope;
use App\Scopes\UserStatusScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Nicolaslopezj\Searchable\SearchableTrait;

class Product extends Model
{
    use SearchableTrait, SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new StatusScope);
        static::addGlobalScope(new QuantityScope);
        static::addGlobalScope(new UserStatusScope);
    }

    protected $guarded = [
        'price',
        'wholesale_price',
        'quantity',
        'status',
    ];

    protected $searchable = [
        'columns' => [
            'products.name' => 10,
        ],
    ];

    protected $dates = ['closed_at'];

    public function address()
    {
        return $this->morphOne('App\Address', 'addressable');
    }

    public function bids()
    {
        return $this->morphMany('App\Bid', 'biddable');
    }

    public function categories()
    {
        return $this->morphToMany('App\Category', 'categorizable');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function users()
    {
        return $this->morphToMany('App\User', 'watchable');
    }

    public function wholesaleRequests()
    {
        return $this->hasMany('App\WholesaleRequest');
    }

    public function scopeAuction($query)
    {
        return $query->where('type', 'auction');
    }

    public function scopeRetail($query)
    {
        return $query->where('type', 'retail');
    }

    public function scopeReverseAuction($query)
    {
        return $query->where('type', 'reverse_auction');
    }

    public function scopeWholesale($query)
    {
        return $query->where('type', 'wholesale');
    }

    public function scopeNotFromUser($query)
    {
        return $query->whereHas('user', function ($subQuery) {
            if (Auth::check()) {
                $subQuery->whereNotIn('id', [Auth::id()]);
            }
        });
    }

    public function scopeWithSales($query)
    {
        return $query->whereIn('type', ['wholesale', 'retail']);
    }

    public function getFormattedPriceAttribute()
    {
        return number_format($this->price, 2, '.', ',');
    }

    public function getFormattedWholesalePriceAttribute()
    {
        return number_format($this->wholesale_price, 2, '.', ',');
    }

    public function getFormattedStartingBidAttribute()
    {
        if ($this->bids->count()) {
            return number_format(
                ($this->is_auction ? $this->bids->max('bid') : $this->bids->max('bid')),
                2,
                '.',
                ','
            );
        }

        return $this->formatted_price;
    }

    public function getIsAuctionAttribute()
    {
        return $this->type === 'auction' ? true : false;
    }

    public function getIsRetailAttribute()
    {
        return $this->type === 'retail' ? true : false;
    }

    public function getIsReverseAuctionAttribute()
    {
        return $this->type === 'reverse_auction' ? true : false;
    }

    public function getIsWholesaleAttribute()
    {
        return $this->type === 'wholesale' ? true : false;
    }
}
