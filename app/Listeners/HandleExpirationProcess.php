<?php

namespace App\Listeners;

use App\Bid;
use App\Events\CloseTimeExpiration;
use App\Notifications\NotifyReverseAuctionWinner;
use App\Traits\MembershipTrait;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Stripe\PaymentIntent;
use Stripe\Refund;
use Stripe\Stripe;
use Stripe\Transfer;
use Throwable;

class HandleExpirationProcess
{
    use MembershipTrait;

    public function __construct()
    {
        Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function handle(CloseTimeExpiration $event)
    {
        $product = $event->product;

        try {
            $winningBid = $product->type === 'auction' ? $product->bids->SortByDesc('bid')->first() : $product->bids->sortBy('bid')->first();
            $losingBids = $product->bids()->whereNotIn('id', [$winningBid->id]);

            if ($product->type === 'auction') {
                $this->handleExpiredAuctions($winningBid, $losingBids);
            } else {
                $winner = $winningBid->user;
                $this->handleExpiredReverseAuctions($winner, $product);
            }

            $product->status = $product->type === 'auction' ? 'inactive': 'pending';
            $product->save();
        } catch (Throwable $th) {
            Log::info($th->getMessage());
        }
    }

    public function handleExpiredReverseAuctions($winner, $product)
    {
        $winner->notify(new NotifyReverseAuctionWinner($winner, $product));
    }

    public function handleExpiredAuctions($winningBid, $losingBids)
    {
        $this->transferVendorPayout($winningBid);

        foreach ($losingBids as $losingBid) {
            $this->refundLosingBids($losingBid);
        }
    }

    public function transferVendorPayout(Bid $bid)
    {
        $paymentIntent = PaymentIntent::retrieve($bid->payment_intent);
        $item = $bid->product;
        $vendor = User::findOrFail($item->user->id);

        $availableProductMembership = $this->checkProductMembership($item, $vendor);
        $amount = ((int) $item->price - ($item->price * ($availableProductMembership->sales_percentage / 100)));

        $transfer = Transfer::create([
            'amount' => $amount,
            'currency' => config('services.cashier.currency'),
            'destination' => $vendor->stripe_user_id,
        ]);

        Log::info($transfer);
    }

    public function refundLosingBids(Bid $bid)
    {
        $paymentIntent = PaymentIntent::retrieve($bid->payment_intent);
        $charge = $paymentIntent->charges->data[0];

        $refund = Refund::create([
            'charge' => $charge->id,
            'amount' => $paymentIntent->amount,
        ]);

        Log::info($refund);
    }
}
