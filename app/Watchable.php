<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Watchable extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function watchable()
    {
        return $this->morphTo();
    }
}
