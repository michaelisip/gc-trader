<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProduct extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'nullable|numeric|exists:users,id',
            'name' => 'required|string',
            'type' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|numeric',
            'wholesale_price' => 'nullable|numeric|lt:price|required_if:type,wholesale',
            'sales' => 'nullable|numeric',
            'quantity' => 'nullable|numeric',
            'condition' => 'nullable|string',
            'pick_up_type' => 'nullable|string',
            'closed_at' => 'nullable|date|required_if:type,auction,reverse_auction',
            'status' => 'nullable|string|in:active,inactive',
        ];
    }
}
