<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCommunity extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'nullable|numeric|exists:users,id',
            'name' => 'required|string',
            'email' => 'required|email|unique:communities,email',
            'facebook_page' => 'nullable|url',
            'description' => 'required|string',
            'company_number' => 'nullable|string',
            'registration' => 'nullable|string',
            'po_box_address' => 'required|string',
            'date_time' => 'nullable|date',
            'start_time' => 'nullable|date',
            'finish_time' => 'nullable|date',
            'parking_or_disability_drop_box' => 'nullable|string',
            'pick_up_type' => 'nullable|string',
            'status' => 'nullable|string|in:active,inactive',
        ];
    }
}
