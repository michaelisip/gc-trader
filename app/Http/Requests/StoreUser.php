<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stripe_user_id' => 'nullable|string',
            'first_name' => 'required|string',
            'last_name' => 'nullable|string',
            'email_verified_at' => 'nullable|date',
            'phone_number' => 'nullable|string',
            'birth_date' => 'nullable|date',
            'abn_registration' => 'nullable|string',
            'gender' => 'nullable|string|in:Male,Female,Other',
            'status' => 'nullable|string|in:active,inactive',
            'type' => 'nullable|string|in:Non-Business,Business',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'birth_date.date' => 'The birth date format should be Y-m-d (e.g. 1999-12-24).',
        ];
    }
}
