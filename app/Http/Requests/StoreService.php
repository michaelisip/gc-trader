<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreService extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'nullable|numeric|exists:users,id',
            'name' => 'required|string',
            'company_name' => 'required|string',
            'description' => 'required|string',
            'website_url' => 'nullable|string|url',
            'is_abn_registered' => 'nullable|boolean',
            'abn_registration' => 'nullable|string',
            'status' => 'nullable|string|in:active,inactive',
        ];
    }
}
