<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFeedback extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'nullable|numeric|exists:users,id',
            'service_id' => 'nullable|numeric|exists:services,id',
            'time_management' => 'required|string',
            'approachability' => 'required|string',
            'cost' => 'required|string',
            'performance' => 'required|string',
        ];
    }
}
