<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRealEstate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'nullable|numeric|exists:users,id',
            'name' => 'required|string',
            'company_name' => 'required|string',
            'description' => 'required|string',
            'size' => 'required|numeric',
            'type' => 'required|string',
            'price' => 'required|numeric',
            'bedrooms' => 'nullable|numeric',
            'bathrooms' => 'nullable|numeric',
            'property_status' => 'nullable|string',
            'status' => 'nullable|string|in:active,inactive',
        ];
    }
}
