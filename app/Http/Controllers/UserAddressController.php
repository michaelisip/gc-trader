<?php

namespace App\Http\Controllers;

use App\User;
use App\Traits\AddressTrait;
use App\Http\Requests\StoreAddress;

class UserAddressController extends Controller
{
    use AddressTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreAddress $request, User $user)
    {
        return $this->storeAddress($request->validated(), $user);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
