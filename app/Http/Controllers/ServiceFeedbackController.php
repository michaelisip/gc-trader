<?php

namespace App\Http\Controllers;

use App\Service;
use App\Traits\FeedbackTrait;
use App\Http\Requests\StoreFeedback;

class ServiceFeedbackController extends Controller
{
    use FeedbackTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreFeedback $request, Service $service)
    {
        $this->storeFeedback($request->validated(), $service);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
