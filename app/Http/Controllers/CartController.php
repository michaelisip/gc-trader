<?php

namespace App\Http\Controllers;

use App\Travel;
use App\Product;
use App\Vehicle;
use App\RealEstate;
use App\Traits\CartTrait;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CartController extends Controller
{
    use CartTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function show(Request $request)
    {
        foreach ($this->cartGetAll()->all() as $cartItem) {
            switch ($cartItem->item_type) {
                case 'product':
                    if (!Product::find($cartItem->item_id)->count()) {
                        $this->cartRemove($request, $cartItem->id);
                    }
                    break;
                case 'real_estate':
                    if (!RealEstate::find($cartItem->item_id)->count()) {
                        $this->cartRemove($request, $cartItem->id);
                    }
                    break;
                case 'travel':
                    if (!Travel::find($cartItem->item_id)->count()) {
                        $this->cartRemove($request, $cartItem->id);
                    }
                    break;
                default:
                    if (!Vehicle::find($cartItem->item_id)->count()) {
                        $this->cartRemove($request, $cartItem->id);
                    }
                    break;
            }
        }

        return view('app.cart')->with('cart', $this->cartGetAll());
    }

    public function add(Request $request)
    {
        try {
            $id = $request->input('id');
            $cartItem = $this->cartGetAll()->firstWhere('item_id', $id);

            switch ($request->input('type')) {
                case 'product':
                    $item = Product::findOrFail($id);
                    if ($cartItem) {
                        $request->merge(['quantity' => $cartItem->quantity+1]);
                        return $this->update($request, $cartItem->id);
                    }
                    break;
                case 'real_estate':
                    $item = RealEstate::findOrFail($id);
                    break;
                case 'travel':
                    $item = Travel::findOrFail($id);
                    break;
                default:
                    $item = Vehicle::findOrFail($id);
                    break;
            }

            $this->cartAdd(
                $request,
                uniqid(),
                $item->id,
                $request->input('type'),
                $item->name,
                $item->price,
                1
            );

            Alert::success('Success!', 'Added to cart.');
            return back();
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add to cart.');
            return back();
        }
    }

    public function update(Request $request, $id)
    {
        $quantity = $request->input('quantity');

        if (!$quantity) {
            $this->remove($request, $id);
        }

        try {
            $cartItem = $this->cartGetAll()->firstWhere('id', $id);

            switch ($cartItem->item_type) {
                case 'product':
                    $product = Product::find($cartItem->item_id);
                    if ($product->quantity < $quantity) {
                        Alert::error('Oops!', 'Can\'t add more quantity.');
                        return back();
                    }
                    break;
                case 'real_estate':
                    $product = RealEstate::find($cartItem->item_id);
                    if ($product->quantity < $quantity) {
                        Alert::error('Oops!', 'Can\'t add more quantity.');
                        return back();
                    }
                    break;
                case 'travel':
                    $product = Travel::find($cartItem->item_id);
                    if ($product->quantity < $quantity) {
                        Alert::error('Oops!', 'Can\'t add more quantity.');
                        return back();
                    }
                    break;
                default:
                    $product = Vehicle::find($cartItem->item_id);
                    if ($product->quantity < $quantity) {
                        Alert::error('Oops!', 'Can\'t add more quantity.');
                        return back();
                    }
                    break;
            }

            $this->cartUpdate($request, $id, $quantity);
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update item.');
            return back();
        }

        Alert::success('Success!', 'Item updated.');
        return back();
    }

    public function remove(Request $request, $id)
    {
        try {
            $this->cartRemove($request, $id);
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove to cart.');
            return back();
        }

        Alert::success('Success!', 'Removed to cart.');
        return back();
    }

    public function clear(Request $request)
    {
        try {
            $this->cartClear($request);
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t clear cart.');
            return back();
        }

        Alert::success('Success!', 'Cart cleared.');
        return back();
    }
}
