<?php

namespace App\Http\Controllers;

use App\Job;
use App\User;
use App\Image;
use App\Address;
use App\Category;
use App\Http\Requests\StoreJob;
use App\Http\Requests\StoreAddress;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserJobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create(User $user)
    {
        return view('app.job-form')->with('jobCategories', Category::job()->get());
    }

    public function store(StoreJob $jobRequest, StoreAddress $addressRequest, User $user)
    {
        DB::beginTransaction();

        try {
            $job = new Job($jobRequest->only([
                'name',
                'company_name',
                'type',
                'description',
            ]));
            $user->jobs()->save($job);

            if ($jobRequest->has('photo')) {
                $job->image()->save(new Image(['src' => $jobRequest->photo]));
            }

            if ($jobRequest->has('categories')) {
                foreach ($jobRequest->categories as $category) {
                    $job->categories()->attach($category);
                    $job->save();
                }
            }

            $job->address()->save(new Address($addressRequest->only([
                'country',
                'state',
                'suburb',
                'city',
                'post_code',
                'street_name',
                'number',
            ])));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add job.');
            return back();
        }

        Alert::success('Success!', 'Job added.');
        return back();
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
