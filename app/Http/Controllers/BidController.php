<?php

namespace App\Http\Controllers;

use App\Bid;
use App\Http\Requests\StoreBid;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;

class BidController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreBid $request, Bid $bid)
    {
        DB::beginTransaction();

        try {
            $bid->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update bid.');
            return back();
        }

        Alert::success('Success!', 'Bid updated.');
        return back();
    }

    public function destroy(Bid $bid)
    {
        DB::beginTransaction();

        try {
            $bid->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove bid.');
            return back();
        }

        Alert::success('Success!', 'Bid removed.');
        return back();
    }
}
