<?php

namespace App\Http\Controllers;

use App\Product;

use Illuminate\Support\Facades\Gate;

class RetailController extends Controller
{
    public function __construct() 
    {
        $this->middleware('verified');
    }

    public function __invoke()
    {
        Gate::authorize('view-any-retail-product', Product::class);

        return view('app.retails')->with('retails', Product::notFromUser()->retail()->paginate());
    }
}
