<?php

namespace App\Http\Controllers;

use App\Service;
use App\Traits\AddressTrait;
use App\Http\Requests\StoreAddress;

class ServiceAddressController extends Controller
{
    use AddressTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function store(StoreAddress $request, Service $service)
    {
        $this->storeAddress($request->validated(), $service);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
