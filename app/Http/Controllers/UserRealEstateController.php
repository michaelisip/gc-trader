<?php

namespace App\Http\Controllers;

use App\User;
use App\Image;
use App\Address;
use App\Category;
use App\RealEstate;
use App\Http\Requests\StoreAddress;
use App\Http\Requests\StoreRealEstate;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;

class UserRealEstateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        return view('app.real-estate-form')->with('realEstateCategories', Category::realEstate()->get());
    }

    public function store(StoreRealEstate $realEstateRequest, StoreAddress $addressRequest, User $user)
    {
        DB::beginTransaction();

        try {
            $realEstate = new RealEstate($realEstateRequest->only([
                'name',
                'company_name',
                'bedrooms',
                'bathrooms',
                'type',
                'property_status',
                'price',
                'size',
                'description',
            ]));
            $realEstate->price = $realEstateRequest->price;
            $realEstate->save();
            $user->realEstates()->save($realEstate);

            if ($realEstateRequest->has('photo')) {
                foreach ($realEstateRequest->photo as $photo) {
                    $realEstate->image()->save(new Image(['src' => $photo]));
                }
            }

            if ($realEstateRequest->has('categories')) {
                foreach ($realEstateRequest->categories as $category) {
                    $realEstate->categories()->attach($category);
                    $realEstate->save();
                }
            }

            $realEstate->address()->save(new Address($addressRequest->only([
                'country',
                'state',
                'suburb',
                'city',
                'post_code',
                'street_name',
                'number',
            ])));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add real estate.');
            return back();
        }

        Alert::success('Success!', 'Real estate added.');
        return back();
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
