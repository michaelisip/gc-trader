<?php

namespace App\Http\Controllers;

use App\Image;
use App\Service;
use App\Traits\ImageTrait;
use App\Http\Requests\StoreImage;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ServiceImageController extends Controller
{
    use ImageTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreImage $request, Service $service)
    {
        $path = $this->uploadImage($request);

        DB::beginTransaction();

        try {
            $service->images()->save(new Image(['src' => $path]));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            return response()->json(['success' => false]);
        }

        return response()->json([
            'success' => true,
            'src' => $path,
        ]);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
