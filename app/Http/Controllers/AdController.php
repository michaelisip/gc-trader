<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Http\Requests\StoreAd;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreAd $request, Ad $ad)
    {
        DB::beginTransaction();

        try {
            $ad->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update ad.');
            return back();
        }

        Alert::success('Success!', 'Ad updated.');
        return back();
    }

    public function destroy(Ad $ad)
    {
        DB::beginTransaction();

        try {
            $ad->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove ad.');
            return back();
        }

        Alert::success('Success!', 'Ad removed.');
        return back();
    }
}
