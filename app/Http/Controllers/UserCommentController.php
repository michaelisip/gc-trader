<?php

namespace App\Http\Controllers;

use App\User;
use App\Traits\CommentTrait;
use App\Http\Requests\StoreComment;

class UserCommentController extends Controller
{
    use CommentTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreComment $request, User $user)
    {
        return $this->storeComment($request->validated(), $user);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
