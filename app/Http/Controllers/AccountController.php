<?php

namespace App\Http\Controllers;

use App\User;
use App\Membership;

use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function __invoke()
    {
        $user = User::findOrFail(Auth::id());
        $stripeState = uniqid('ss_');
        session(['stripe_state' => $stripeState]);
        $stripeOAuthLink =
            'https://connect.stripe.com/express/oauth/authorize?client_id=' .
            config('services.stripe.client_id') .
            '&redirect_uri=' .
            route('stripe.connect.success') .
            '&state=' .
            $stripeState .
            '&stripe_user[business_type]=individual' .
            '&stripe_user[email]=' .
            $user->email .
            (isset($user->phone_number) ?
                '&stripe_user[phone_number]=' .
                $user->phone_number
                : '') .
            '&stripe_user[first_name]=' .
            $user->first_name .
            (isset($user->last_name) ?
                '&stripe_user[last_name]=' .
                $user->last_name
                : '') .
            (isset($user->birth_date) ?
                ('&stripe_user[dob_day]=' .
                    $user->birth_date->day .
                    '&stripe_user[dob_month]=' .
                    $user->birth_date->month .
                    '&stripe_user[dob_year]=' .
                    $user->birth_date->year)
                : '') .
            (isset($user->address) ?
                '&stripe_user[country]=' .
                ($user->address->country === 'Australia' ? 'AU' : 'NZ')
                : '');

        return view('app.account')->with([
            'user' => $user,
            'memberships' => Membership::all(),
            'stripeOAuthLink' => $stripeOAuthLink,
        ]);
    }
}
