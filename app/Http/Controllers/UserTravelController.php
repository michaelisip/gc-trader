<?php

namespace App\Http\Controllers;

use App\User;
use App\Image;
use App\Travel;
use App\Address;
use App\Category;
use App\Http\Requests\StoreTravel;
use App\Http\Requests\StoreAddress;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;

class UserTravelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        return view('app.travel-form')->with('travelCategories', Category::travel()->get());
    }

    public function store(StoreTravel $travelRequest, StoreAddress $addressRequest, User $user)
    {
        DB::beginTransaction();

        try {
            $travel = new Travel($travelRequest->only([
                'name',
                'date_from',
                'date_to',
                'description',
                'price',
            ]));
            $travel->price = $travelRequest->price;
            $travel->save();
            $user->travels()->save($travel);

            if ($travelRequest->has('photo')) {
                foreach ($travelRequest->photo as $photo) {
                    $travel->images()->save(new Image(['src' => $photo]));
                }
            }

            if ($travelRequest->has('categories')) {
                foreach ($travelRequest->categories as $category) {
                    $travel->categories()->attach($category);
                    $travel->save();
                }
            }

            $travel->address()->save(new Address($addressRequest->only([
                'country',
                'state',
                'suburb',
                'city',
                'post_code',
                'street_name',
                'number',
            ])));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add travel.');
            return back();
        }

        Alert::success('Success!', 'Travel added.');
        return back();
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
