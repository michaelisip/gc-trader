<?php

namespace App\Http\Controllers;

use App\Ad;
use App\Product;

class HomeController extends Controller
{
    public function __construct() 
    {
        $this->middleware('verified');
    }

    public function __invoke()
    {
        return view('app.home')->with([
            'carouselAds' => Ad::platinumTier()->get(),
            'auctions' => Product::notFromUser()->auction()->take(4)->get(),
            'retails' => Product::notFromUser()->retail()->take(4)->get(),
            'reverseAuctions' => Product::notFromUser()->reverseAuction()->take(4)->get(),
            'wholesales' => Product::notFromUser()->wholesale()->take(4)->get(),
        ]);
    }
}
