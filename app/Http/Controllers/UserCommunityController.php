<?php

namespace App\Http\Controllers;

use App\User;
use App\Image;
use App\Video;
use App\Category;
use App\Community;
use App\Http\Requests\StoreCommunity;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserCommunityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create(User $user)
    {
        return view('app.community-form')->with('communityCategories', Category::community()->get());
    }

    public function store(StoreCommunity $communityRequest, User $user)
    {
        DB::beginTransaction();

        try {
            $communityRequest->merge(['parking_or_disability_drop_box' => isset($communityRequest->parking_or_disability_drop_box) ? 1 : 0]);
            $communityRequest->merge(['start_time' => date('Y-m-d H:i:s', strtotime($communityRequest->start_time))]);
            $communityRequest->merge(['finish_time' => date('Y-m-d H:i:s', strtotime($communityRequest->finish_time))]);

            $community = new Community($communityRequest->only([
                'name',
                'company_number',
                'email',
                'registration',
                'po_box_address',
                'description',
                'date_time',
                'start_time',
                'finish_time',
                'parking_or_disability_drop_box',
                'pick_up_type',
            ]));
            $user->communities()->save($community);

            if ($communityRequest->has('photo')) {
                foreach ($communityRequest->photo as $photo) {
                    $community->images()->save(new Image(['src' => $photo]));
                }
            }

            if ($communityRequest->has('video')) {
                $community->video()->save(new Video(['src' => $communityRequest->video]));
            }

            if ($communityRequest->has('categories')) {
                foreach ($communityRequest->categories as $category) {
                    $community->categories()->attach($category);
                    $community->save();
                }
            }

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add community.');
            return back();
        }

        Alert::success('Success!', 'Community added.');
        return back();
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
