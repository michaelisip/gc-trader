<?php

namespace App\Http\Controllers;

use App\Job;
use App\User;
use App\Travel;
use App\Product;
use App\Service;
use App\Vehicle;
use App\Community;
use App\RealEstate;
use App\Traits\JobSearchTrait;
use App\Traits\TravelSearchTrait;
use App\Traits\VehicleSearchTrait;
use App\Traits\RealEstateSearchTrait;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class SearchController extends Controller
{
    use JobSearchTrait,
        RealEstateSearchTrait,
        TravelSearchTrait,
        VehicleSearchTrait;

    public function __construct() 
    {
        $this->middleware('verified')->only('search');
    }

    public function search(Request $request)
    {
        $products =  Product::notFromUser();

        if ($request->input('sort')) {
            $isDesc = $request->input('desc') ? 'DESC' : 'ASC';
            $products->orderBy($request->input('sort'), $isDesc);
        }

        if ($request->input('filter')) {
            if ($request->input('filter') !== 'All links for Categories') {
                $products = $products->whereHas('categories', function ($query) use ($request) {
                    $query->whereName($request->input('filter'));
                });
            }
        }

        if (!Auth::check() || !Auth::user()->is_business_man) {
            $products = $products->reverseAuction();
        }

        if (Auth::check() && !Gate::check('view-any-wholesale-product')) {
            $products = $products->whereNotIn('type', ['wholesale']);
        }

        if ($request->input('search')) {
            $products = $products->search($request->input('search'));
        }

        return view('app.search-result')->with('products', $products->paginate());
    }

    public function suggestion(Request $request)
    {
        $baseURL = config('app.url');

        $communityResults = Community::search($request->input('search'))->with('images')
            ->get()
            ->map(function ($item) use ($baseURL) {
                return [
                    'title' => $item->name,
                    'description' => $item->description,
                    'image' => $item->image ? $item->image->src : $baseURL . '/img/placeholder.jpg',
                    'url' => $baseURL . '/communities/' . $item->id,
                ];
            });

        $jobResults = Job::search($request->input('search'))->get()
            ->map(function ($item) use ($baseURL) {
                return [
                    'title' => $item->name,
                    'description' => $item->company_name,
                    'url' => $baseURL . '/jobs/' . $item->id,
                ];
            });

        $product = Product::notFromUser();

        if (!Auth::check() || !Auth::user()->is_business_man) {
            $product = $product->reverseAuction();
        }

        if (Auth::check() && !Gate::check('view-any-wholesale-product')) {
            $product = $product->whereNotIn('type', ['wholesale']);
        }

        $productResults = $product->search($request->input('search'))->with('images')
            ->get()
            ->map(function ($item) use ($baseURL) {
                return [
                    'title' => $item->name,
                    'price' => $item->price,
                    'description' => Str::limit($item->description, 50),
                    'image' => $item->images->count() ? $item->images[0]->src : $baseURL . '/img/placeholder.jpg',
                    'url' => $baseURL . '/products/' . $item->id,
                ];
            });

        $realEstateResults = RealEstate::notFromUser()->search($request->input('search'))->with('images')
            ->get()
            ->map(function ($item) use ($baseURL) {
                return [
                    'title' => $item->name,
                    'price' => $item->price,
                    'description' => $item->company_name,
                    'image' => $item->images->count() ? $item->images[0]->src : $baseURL . '/img/placeholder.jpg',
                    'url' => $baseURL . '/real-estates/' . $item->id,
                ];
            });

        $serviceResults = Service::search($request->input('search'))->with('images')
            ->get()
            ->map(function ($item) use ($baseURL) {
                return [
                    'title' => $item->name,
                    'description' => $item->company_name,
                    'image' => $item->images->count() ? $item->images[0]->src : $baseURL . '/img/placeholder.jpg',
                    'url' => $baseURL . '/services/' . $item->id,
                ];
            });

        $travelResults = Travel::notFromUser()->search($request->input('search'))->with('images')
            ->get()
            ->map(function ($item) use ($baseURL) {
                return [
                    'title' => $item->name,
                    'price' => $item->price,
                    'description' => Str::limit($item->description, 50),
                    'image' => $item->images->count() ? $item->images[0]->src : $baseURL . '/img/placeholder.jpg',
                    'url' => $baseURL . '/travels/' . $item->id,
                ];
            });

        $userResults = User::search($request->input('search'))->with('image')
            ->get()
            ->map(function ($item) use ($baseURL) {
                return [
                    'title' => $item->full_name,
                    'image' => $item->image ? $item->image->src : $baseURL . '/img/placeholder.jpg',
                    'url' => $baseURL . '/users/' . $item->id,
                ];
            });

        $vehicleResults = Vehicle::notFromUser()->search($request->input('search'))->with('images')
            ->get()
            ->map(function ($item) use ($baseURL) {
                return [
                    'title' => $item->name,
                    'price' => $item->price,
                    'description' => $item['model'],
                    'image' => $item->images->count() ? $item->images[0]->src : $baseURL . '/img/placeholder.jpg',
                    'url' => $baseURL . '/vehicles/' . $item->id,
                ];
            });

        return [
            'results' => [
                'community' => [
                    'name' => 'Community',
                    'results' => $communityResults,
                ],
                'job' => [
                    'name' => 'Job',
                    'results' => $jobResults,
                ],
                'product' => [
                    'name' => 'Product',
                    'results' => $productResults,
                ],
                'realEstate' => [
                    'name' => 'Real Estate',
                    'results' => $realEstateResults,
                ],
                'service' => [
                    'name' => 'Service',
                    'results' => $serviceResults,
                ],
                'travel' => [
                    'name' => 'Travel',
                    'results' => $travelResults,
                ],
                'user' => [
                    'name' => 'User',
                    'results' => $userResults,
                ],
                'vehicle' => [
                    'name' => 'Vehicle',
                    'results' => $vehicleResults,
                ],
            ],
            'action' => [
                'url' => '/search?search=' . $request->input('search'),
                'text' => 'View More',
            ],
        ];
    }
}
