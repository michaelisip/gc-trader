<?php

namespace App\Http\Controllers;

use App\Product;

use Illuminate\Support\Facades\Gate;

class AuctionController extends Controller
{
    public function __construct() 
    {
        $this->middleware('verified');
    }

    public function __invoke()
    {
        Gate::authorize('view-any-auction-product', Product::class);

        return view('app.auctions')->with('auctions', Product::notFromUser()->auction()->paginate());
    }
}
