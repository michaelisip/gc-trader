<?php

namespace App\Http\Controllers;

use App\Address;
use App\Http\Requests\StoreAddress;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AddressController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreAddress $request, Address $address)
    {
        DB::beginTransaction();

        try {
            $address->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update address.');
            return back();
        }

        Alert::success('Success!', 'Address updated.');
        return back();
    }

    public function destroy(Address $address)
    {
        DB::beginTransaction();

        try {
            $address->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove address.');
            return back();
        }

        Alert::success('Success!', 'Address removed.');
        return back();
    }
}
