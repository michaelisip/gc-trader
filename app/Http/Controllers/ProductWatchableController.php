<?php

namespace App\Http\Controllers;

use App\User;
use App\Product;
use App\Watchable;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class ProductWatchableController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index(Product $product)
    {
        abort(404);
    }

    public function create(Product $product)
    {
        abort(404);
    }

    public function store(Request $request, Product $product)
    {
        DB::beginTransaction();

        try {

            $product->users()->save(User::findOrFail(Auth::id()));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add product to watchlist.');
            return back();
        }

        Alert::success('Success!', 'Product added to watchlist.');
        return back();
    }

    public function show(Product $product, Watchable $watchable)
    {
        abort(404);
    }

    public function edit(Product $product, Watchable $watchable)
    {
        abort(404);
    }

    public function update(Request $request, Product $product, Watchable $watchable)
    {
        abort(404);
    }

    public function destroy(Product $product, Watchable $watchable)
    {
        abort(404);
    }
}
