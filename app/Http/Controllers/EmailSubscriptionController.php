<?php

namespace App\Http\Controllers;

use App\EmailSubscription;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EmailSubscriptionController extends Controller
{
    public function __invoke(Request $request)
    {
        DB::beginTransaction();

        try {
            EmailSubscription::create($request->all());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t subscribe right now.');
            return back();
        }

        Alert::success('Success!', 'You have subscribed successfully.');
        return back();
    }
}
