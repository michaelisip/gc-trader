<?php

namespace App\Http\Controllers;

use App\Community;
use App\Http\Requests\StoreCommunity;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CommunityController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show', 'toggle']);
        $this->middleware('verified');
        $this->middleware('auth:admin')->only('toggle');
    }

    public function index()
    {
        return view('app.communities')->with('communities', Community::paginate());
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show(Community $community)
    {
        return view('app.community-info')->with('community', $community);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreCommunity $request, Community $community)
    {
        DB::beginTransaction();

        try {
            $community->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update community.');
            return back();
        }

        Alert::success('Success!', 'Community updated.');
        return back();
    }

    public function destroy(Community $community)
    {
        DB::beginTransaction();

        try {
            $community->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove community.');
            return back();
        }

        Alert::success('Success!', 'Community removed.');
        return back();
    }

    public function toggle($community, Request $request)
    {
        DB::beginTransaction();

        try {
            $community = Community::withoutGlobalScopes()->findOrFail($community);
            $community->status = $request->input('status');
            $community->save();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t toggle community status.');
            return back();
        }

        Alert::success('Success!', 'Community updated.');
        return back();
    }
}
