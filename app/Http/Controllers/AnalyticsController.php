<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Support\Facades\Auth;

class AnalyticsController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function __invoke()
    {
        return view('app.analytics')->with('user', User::findOrFail(Auth::id()));
    }
}
