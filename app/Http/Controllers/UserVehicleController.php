<?php

namespace App\Http\Controllers;

use App\User;
use App\Image;
use App\Travel;
use App\Address;
use App\Vehicle;
use App\Category;
use App\Http\Requests\StoreAddress;
use App\Http\Requests\StoreVehicle;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserVehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        return view('app.vehicle-form')->with('vehicleCategories', Category::vehicle()->get());
    }

    public function store(StoreVehicle $vehicleRequest, StoreAddress $addressRequest, User $user)
    {
        DB::beginTransaction();

        try {
            $vehicle = new Vehicle($vehicleRequest->only([
                'name',
                'model',
                'manufacturer',
                'year',
                'price',
                'description',
            ]));
            $vehicle->price = $vehicleRequest->price;
            $vehicle->save();
            $user->vehicles()->save($vehicle);

            if ($vehicleRequest->has('photo')) {
                foreach ($vehicleRequest->photo as $photo) {
                    $vehicle->images()->save(new Image(['src' => $photo]));
                }
            }

            if ($vehicleRequest->has('categories')) {
                foreach ($vehicleRequest->categories as $category) {
                    $vehicle->categories()->attach($category);
                    $vehicle->save();
                }
            }

            $vehicle->address()->save(new Address($addressRequest->only([
                'country',
                'state',
                'suburb',
                'city',
                'post_code',
                'street_name',
                'number',
            ])));


            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add vehicle.');
            return back();
        }

        Alert::success('Success!', 'Vehicle added.');
        return back();
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
