<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;

class ListingController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function __invoke()
    {
        $user = User::findOrFail(Auth::id());
        $user->load('products', 'communities', 'jobs', 'realEstates', 'services', 'travels', 'vehicles');

        return view('app.listings')->with('user', $user);
    }
}
