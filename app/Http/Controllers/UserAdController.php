<?php

namespace App\Http\Controllers;

use App\Ad;
use App\User;
use App\Image;
use App\Http\Requests\StoreAd;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class UserAdController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index(User $user)
    {
        return view('app.ads')->with([
            'platinumTierAds' => $user->ads()->platinumTier()->get(),
            'goldTierAds' => $user->ads()->goldTier()->get(),
        ]);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreAd $request, User $user)
    {
        if ($request->input('type') === 'Gold Tier') {
            Gate::authorize('create-gold-ad', Ad::class);
        } else {
            Gate::authorize('create-platinum-ad', Ad::class);
        }

        DB::beginTransaction();

        try {
            $ad = new Ad($request->only(['type']));
            $user->ads()->save($ad);
            $ad->image()->save(new Image(['src' => $request->input('photo')]));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add ad.');
            return back();
        }

        Alert::success('Success!', 'Ad added.');
        return back();
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
