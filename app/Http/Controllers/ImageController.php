<?php

namespace App\Http\Controllers;

use App\Traits\ImageTrait;
use App\Http\Requests\StoreImage;

use Throwable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ImageController extends Controller
{
    use ImageTrait;

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreImage $request)
    {
        // Todo: Refactor
        if ($request['ad_type']) {
            // $this->validateAdImage($request);        
            $width = $request['ad_type'] === 'Gold Tier' ? '468' : '1080';
            $height = $request['ad_type'] === 'Gold Tier' ? '60' : '400';

            $validator = Validator::make($request->all(), [
                'photo' => "dimensions:width=${width},height=${height}",
            ], [
                'photo.dimensions' => "{$request['ad_type']} Ads must have dimensions of :width width and :height height."
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'success' => false,
                    'message' => $validator->errors()->first('photo'),
                    'src' => null,
                ]);
            }
        }

        try {
            if (config('services.aws.access_key_id')) {
                $path = Storage::disk('s3')->put('gctrader/img', $request->file('photo'), 'public');
                $path = config('services.aws.url') . '/' . $path;
            } else {
                $path = Storage::put(public_path('storage'), $request->file('photo'));
            }
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            return response()->json([
                'success' => false,
                'src' => null
            ]);
        }

        return response()->json([
            'success' => true,
            'src' => $path,
        ]);
    }

    public function validateAdImage($request)
    {
        // 
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
