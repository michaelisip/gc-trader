<?php

namespace App\Http\Controllers;

use App\User;
use App\Traits\FeedbackTrait;
use App\Http\Requests\StoreFeedback;

class UserFeedbackController extends Controller
{
    use FeedbackTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreFeedback $request, User $user)
    {
        return $this->storeFeedback($request->validated(), $user);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
