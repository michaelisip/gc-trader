<?php

namespace App\Http\Controllers;

use App\Category;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(Request $request, Category $category)
    {
        abort(404);
    }

    public function destroy(Category $category)
    {
        DB::beginTransaction();

        try {
            $category->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove category.');
            return back();
        }

        Alert::success('Success!', 'Category removed.');
        return back();
    }
}
