<?php

namespace App\Http\Controllers;

use App\Bid;
use App\Product;
use App\User;
use Stripe\OAuth;
use Stripe\Stripe;
use App\Traits\BidTrait;
use App\Traits\CartTrait;
use App\Traits\MembershipTrait;
use App\Traits\ProductFilterTrait;
use App\Traits\ProductPurchaseTrait;
use App\Traits\RealEstatePurchaseTrait;
use App\Traits\StripePaymentTrait;
use App\Traits\TravelPurchaseTrait;
use App\Traits\UserActivationTrait;
use App\Traits\VehiclePurchaseTrait;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StripeController extends Controller
{
    use BidTrait,
        CartTrait,
        MembershipTrait,
        ProductFilterTrait,
        StripePaymentTrait,
        ProductPurchaseTrait,
        RealEstatePurchaseTrait,
        TravelPurchaseTrait,
        VehiclePurchaseTrait,
        UserActivationTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');

        Stripe::setApiKey(config('services.stripe.secret'));
    }

    public function checkout(Request $request)
    {
        $lineItems = [];
        session([
            'stripe_item_id' => $request->input('item_id'),
            'stripe_item_type' => $request->input('item_type'),
            'stripe_payment_type' => $request->input('payment_type'),
            'stripe_bid' => $request->input('bid'),
        ]);

        if ($request->input('payment_type') !== 'cart') {
            $item = $this->filterProduct($request->input('item_id'), $request->input('item_type'));
            $price = $request->input('payment_type') === 'bid' ? $request->input('bid') : $item->price;
            $lineItems = [
                [
                    'name' => $item->name,
                    'description' => $item->description ?? 'No Description Available.',
                    'amount' => floatval($price) * 100,
                    'currency' => config('services.cashier.currency'),
                    'quantity' => 1,
                ],
            ];
        } else {
            foreach ($this->cartGetAll()->all() as $cartItem) {
                $item = $this->filterProduct($cartItem->item_id, $cartItem->item_type);
                array_push($lineItems, [
                    'name' => $item->name,
                    'description' => $item->description ?? 'No Description Available.',
                    'amount' => floatval($item->price) * 100,
                    'currency' => config('services.cashier.currency'),
                    'quantity' => $cartItem->quantity,
                ]);
            }
        }

        return $this->createSession($lineItems);
    }

    public function success(Request $request)
    {
        $itemID = $request->session()->pull('stripe_item_id', null);
        $itemType = $request->session()->pull('stripe_item_type', null);
        $paymentType = $request->session()->pull('stripe_payment_type', null);
        $bid = $request->session()->pull('stripe_bid', null);

        if ($paymentType === 'single') {
            $this->updateProductPurchase($itemID, $itemType);
            $this->transferVendorPayouts($itemID, $itemType, $paymentType);
        } elseif ($paymentType === 'bid') {
            $this->processUserBid($request, $itemID, $bid);
            Alert::success('Success!', 'Bid successful.');
        } else {
            foreach ($this->cartGetAll()->all() as $cartItem) {
                $this->updateProductPurchase($cartItem->item_id, $cartItem->item_type, $cartItem->quantity);
            }

            Alert::success('Success!', 'Purchase successful.');
        }

        $this->cartClear($request);

        return redirect()->route('home');
    }

    public function cancel()
    {
        Alert::error('Oops!', 'Purchase canceled.');
        return redirect()->route('home');
    }

    public function connectSuccess(Request $request)
    {
        $stripeState = $request->session()->pull('stripe_state');

        if ($request->state === $stripeState) {
            $response = OAuth::token([
                'grant_type' => 'authorization_code',
                'code' => $request->code,
            ]);

            if ($response->error) {
                Alert::error('Oops!', 'Something went wrong. Please contact support.');
                return redirect()->route('account');
            }

            DB::beginTransaction();

            try {
                $user = User::findOrFail(Auth::id());
                $user->stripe_user_id = $response->stripe_user_id;
                $user->type = 'Business';
                $user->save();

                DB::commit();
            } catch (Throwable $th) {
                DB::rollBack();
                Log::error($th->getMessage());

                Alert::error('Oops!', 'Can\'t convert user to Business. Please contact support.');
                return redirect()->route('account');
            }
        } else {
            Alert::error('Oops!', 'Invalid token. Please contact support.');
            return redirect()->route('account');
        }

        Alert::success('Success!', 'You just converted your account to Business!');
        return redirect()->route('account');
    }
}
