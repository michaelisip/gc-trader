<?php

namespace App\Http\Controllers;

use App\Product;
use App\Traits\AddressTrait;
use App\Http\Requests\StoreAddress;

class ProductAddressController extends Controller
{
    use AddressTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreAddress $request, Product $product)
    {
        return $this->storeAddress($request->validated(), $product);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
