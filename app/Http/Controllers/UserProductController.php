<?php

namespace App\Http\Controllers;

use App\User;
use App\Image;
use App\Product;
use App\Address;
use App\Traits\ImageTrait;
use App\Http\Requests\StoreAddress;
use App\Http\Requests\StoreProduct;
use App\Traits\ProductCheckpointTrait;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class UserProductController extends Controller
{
    use ImageTrait,
        ProductCheckpointTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create(User $user)
    {
        return view('app.product-form');
    }

    public function store(StoreProduct $productRequest, StoreAddress $addressRequest, User $user)
    {
        switch ($productRequest->input('type')) {
            case 'retail':
                Gate::authorize('create-retail-product', $productRequest);
                break;
            case 'wholesale':
                Gate::authorize('create-wholesale-product', $productRequest);
                break;
            case 'auction':
                Gate::authorize('create-auction-product', $productRequest);
                break;

            default:
                Gate::authorize('create-reverse-auction-product', $productRequest);
                break;
        }

        if ($this->checkKeywords($productRequest->input('description'))) {
            Alert::error('Oops!', 'Your product was flagged as invalid free listing. Please review your product details.');
            return back();
        }

        DB::beginTransaction();

        try {
            $product = new Product($productRequest->only([
                'name',
                'description',
                'type',
                'condition',
                'pick_up_type',
                'closed_at',
            ]));
            $product->price = $productRequest->input('price');
            $product->wholesale_price = $productRequest->input('wholesale_price');
            $product->quantity = $productRequest->input('quantity');
            $product->save();
            $user->products()->save($product);

            if ($productRequest->has('photo')) {
                foreach ($productRequest->photo as $photo) {
                    $product->images()->save(new Image(['src' => $photo]));
                }
            }

            if ($productRequest->has('categories')) {
                foreach ($productRequest->categories as $category) {
                    $product->categories()->attach($category);
                    $product->save();
                }
            }

            $product->address()->save(new Address($addressRequest->only([
                'country',
                'state',
                'suburb',
                'city',
                'post_code',
                'street_name',
                'number',
            ])));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add product.');
            return back();
        }

        Alert::success('Success!', 'Product added.');
        return back();
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
