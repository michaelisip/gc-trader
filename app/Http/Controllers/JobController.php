<?php

namespace App\Http\Controllers;

use App\Job;
use App\Category;
use App\Http\Requests\StoreJob;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        return view('app.jobs')->with([
            'jobs' => Job::paginate(),
            'jobCategories' => Category::job()->get(),
        ]);
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show(Job $job)
    {
        return view('app.job-info')->with('job', $job);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreJob $request, Job $job)
    {
        DB::beginTransaction();

        try {
            $job->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update job.');
            return back();
        }

        Alert::success('Success!', 'Job updated.');
        return back();
    }

    public function destroy(Job $job)
    {
        DB::beginTransaction();

        try {
            $job->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove job.');
            return back();
        }

        Alert::success('Success!', 'Job removed.');
        return back();
    }
}
