<?php

namespace App\Http\Controllers;

use App\User;
use App\Image;
use App\Http\Requests\StoreUser;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreUser $request, User $user)
    {
        DB::beginTransaction();

        try {
            $user->update($request->validated());

            if ($request->has('photo')) {
                foreach ($request->photo as $photo) {
                    $user->image()->save(new Image(['src' => $photo]));
                }
            }

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update user.');
            return back();
        }

        Alert::success('Success!', 'User updated.');
        return back();
    }

    public function destroy(User $user)
    {
        DB::beginTransaction();

        try {
            $user->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove user.');
            return back();
        }

        Alert::success('Success!', 'User removed.');
        return back();
    }
}
