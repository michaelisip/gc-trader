<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Requests\StoreProduct;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Gate;
use RealRashid\SweetAlert\Facades\Alert;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('show');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show(Product $product)
    {
        switch ($product->type) {
            case 'auction':
                Gate::authorize('view-auction-product', $product);
                break;
            case 'retail':
                Gate::authorize('view-retail-product', $product);
                break;
            case 'reverse_auction':
                Gate::authorize('view-reverse-auction-product', $product);
                break;

            default:
                break;
        }

        return view('app.product-info')->with('product', $product);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreProduct $request, Product $product)
    {
        DB::beginTransaction();

        try {
            $product->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update product.');
            return back();
        }

        Alert::success('Success!', 'Product updated.');
        return back();
    }

    public function destroy(Product $product)
    {
        DB::beginTransaction();

        try {
            $product->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove product.');
            return back();
        }

        Alert::success('Success!', 'Product removed.');
        return back();
    }
}
