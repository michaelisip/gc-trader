<?php

namespace App\Http\Controllers;

use App\User;
use App\Product;
use App\Membership;
use App\WholesaleRequest;
use App\Mail\RequestMail;
use App\Traits\MembershipTrait;
use RealRashid\SweetAlert\Facades\Alert;

use stdClass;
use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class WholesaleRequestController extends Controller
{
    use MembershipTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        $user = User::findOrFail(Auth::id());
        $myRequests = $user->requests()->with(['product' => function($q) {
            return $q->withoutGlobalScopes();
        }]);
        $buyerRequests = $user->wholesaleRequests()->with(['product' => function($q) {
            return $q->withoutGlobalScopes();
        }]);

        return view('app.wholesale-requests')->with([
            'buyerRequests' => $myRequests->paginate(),
            'myRequests' => $buyerRequests->paginate(),
            'wholesales' => $user->products()->wholesale()->get(),
        ]);
    }

    public function request(Request $request)
    {
        DB::beginTransaction();

        try {
            $user = User::findOrFail(Auth::id());
            $product = Product::findOrFail($request->product_id);

            $wholesaleRequest = new WholesaleRequest([
                'vendor_id' => $product->user->id,
                'product_id' => $product->id
            ]);

            $user->wholesaleRequests()->save($wholesaleRequest);

            $message = new stdClass;
            $message->url = route('wholesales.requests.index');
            $message->button = 'View Requests';
            $message->subject = "New Wholesale Request";
            $message->body = "Hi! {$user->full_name} is requesting your approval to buy your wholesale product! If you wish to approve or deny the request, please click the button to take further action.";
            
            Mail::to($product->user->email)
                ->send(new RequestMail($message));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Something went wrong!');
            return back();
        }

        Alert::success('Success!', 'Successfull sent request!');
        return back();
    }

    public function approve(WholesaleRequest $wholesaleRequest)
    {
        DB::beginTransaction();

        try {
            $product = $wholesaleRequest->product;
            $approvedRequestsCount = $product->wholesaleRequests()->withoutGlobalScopes()->whereStatus('approved')->count();

            if ($product->quantity == $approvedRequestsCount) {
                Alert::error('Oops!', 'Insufficient product quantity.');
                return back();
            }

            $buyer = $wholesaleRequest->buyer;
            $vendor = $wholesaleRequest->vendor;

            $message = new stdClass;
            $message->url = route('wholesales.requests.index');
            $message->button = 'View Wholesale';
            $message->subject = "Wholesale Request Approval";
            $message->body = "Good news! {$vendor->full_name} just approved your request on wholesale '{$product->name}'! You may click the button to pay for the product to complete the transaction.";
            
            Mail::to($buyer->email)
                ->send(new RequestMail($message));

            $wholesaleRequest->status = 'approved';
            $wholesaleRequest->save();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Something went wrong. Please contact support.');
            return back();
        }

        Alert::success('Success!', 'Successfully approved request.');
        return back();
    }

    public function deny(WholesaleRequest $wholesaleRequest)
    {
        DB::beginTransaction();

        try {
            $product = $wholesaleRequest->product;
            $buyer = $wholesaleRequest->buyer;

            $message = new stdClass;
            $message->url = route('wholesales');
            $message->button = 'Browse Wholesales';
            $message->subject = "Wholesale Request Denial";
            $message->body = "I'm sorry, but your request for the wholesale '{$product->name}' has been denied. You may browse other awesome products by clicking the button.";
            
            Mail::to($buyer->email)
                ->send(new RequestMail($message));

            $wholesaleRequest->status = 'denied';
            $wholesaleRequest->save();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Something went wrong.');
            return back();
        }

        Alert::success('Success!', 'Successfully denied request.');
        return back();
    }
}
