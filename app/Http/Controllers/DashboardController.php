<?php

namespace App\Http\Controllers;

use App\User;

use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function __invoke()
    {
        return view('app.dashboard')->with('user', User::findOrFail(Auth::id()));
    }
}
