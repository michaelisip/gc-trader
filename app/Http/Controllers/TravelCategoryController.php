<?php

namespace App\Http\Controllers;

use App\Travel;
use App\Traits\CategoryTrait;
use App\Http\Requests\StoreCategory;

class TravelCategoryController extends Controller
{
    use CategoryTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreCategory $request, Travel $travel)
    {
        $this->storeCategory($request->validate(), $travel);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
