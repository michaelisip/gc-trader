<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Http\Requests\StoreFeedback;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FeedbackController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreFeedback $request, Feedback $feedback)
    {
        DB::beginTransaction();

        try {
            $feedback->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update feedback.');
            return back();
        }

        Alert::success('Success!', 'Feedback updated.');
        return back();
    }

    public function destroy(Feedback $feedback)
    {
        DB::beginTransaction();

        try {
            $feedback->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove feedback.');
            return back();
        }

        Alert::success('Success!', 'Feedback removed.');
        return back();
    }
}
