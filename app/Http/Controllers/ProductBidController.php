<?php

namespace App\Http\Controllers;

use App\Product;
use App\Traits\BidTrait;
use App\Http\Requests\StoreBid;

class ProductBidController extends Controller
{
    use BidTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreBid $request, Product $product)
    {
        return $this->storeBid($request->validated(), $product);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
