<?php

namespace App\Http\Controllers;

use App\Service;
use App\Traits\BidTrait;
use App\Http\Requests\StoreBid;

class ServiceBidController extends Controller
{
    use BidTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreBid $request, Service $service)
    {
        $this->storeBid($request->validated(), $service);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
