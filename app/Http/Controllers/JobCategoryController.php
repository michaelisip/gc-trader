<?php

namespace App\Http\Controllers;

use App\Job;
use App\Category;
use App\Traits\CategoryTrait;
use App\Http\Requests\StoreCategory;

use Illuminate\Http\Request;

class JobCategoryController extends Controller
{
    use CategoryTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreCategory $request, Job $job)
    {
        return $this->storeCategory($request->validate(), $job);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
