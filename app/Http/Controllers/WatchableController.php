<?php

namespace App\Http\Controllers;

use App\User;
use App\Watchable;
use RealRashid\SweetAlert\Facades\Alert;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class WatchableController extends Controller
{
    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(Request $request)
    {
        abort(404);
    }

    public function show(Watchable $watchable)
    {
        abort(404);
    }

    public function edit(Watchable $watchable)
    {
        abort(404);
    }

    public function update(Request $request, Watchable $watchable)
    {
        abort(404);
    }

    public function destroy(Watchable $watchable)
    {
        DB::beginTransaction();

        try {
            $user = User::findOrFail(Auth::id());

            switch ($watchable->watchable_type) {
                case 'App\Product':
                    $user->productWatchlist()->detach($watchable->watchable_id);
                    break;
                case 'App\Job':
                    $user->jobWatchlist()->detach($watchable->watchable_id);
                    break;
                case 'App\RealEstate':
                    $user->realEstateWatchlist()->detach($watchable->watchable_id);
                    break;
                case 'App\Service':
                    $user->serviceWatchlist()->detach($watchable->watchable_id);
                    break;
                case 'App\Travel':
                    $user->travelWatchlist()->detach($watchable->watchable_id);
                    break;
                case 'App\Vehicle':
                    $user->vehicleWatchlist()->detach($watchable->watchable_id);
                    break;
                default:
                    Alert::error('Oops!', 'Something went wrong!');
                    return back();
                    break;
            }

            DB::commit();
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            DB::rollBack();

            Alert::error('Oops!', 'Something went wrong!');
            return back();
        }

        Alert::success('Success!', 'Successfully removed from watchlist!');
        return back();
    }
}
