<?php

namespace App\Http\Controllers;

use App\RealEstate;
use App\Traits\CommentTrait;
use App\Http\Requests\StoreComment;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class RealEstateCommentController extends Controller
{
    use CommentTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreComment $request, RealEstate $realEstate)
    {
        DB::beginTransaction();

        try {
          $this->storeComment($request->validated(), $realEstate);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add comment.');
            return back();
        }

        Alert::success('Success!', 'Comment added.');
        return back();
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
