<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\Traits\AddressTrait;
use App\Http\Requests\StoreAddress;

class VehicleAddressController extends Controller
{
    use AddressTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreAddress $request, Vehicle $vehicle)
    {
        return $this->storeAddress($request->validated(), $vehicle);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
