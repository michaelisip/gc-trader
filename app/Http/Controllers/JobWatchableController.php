<?php

namespace App\Http\Controllers;

use App\Job;
use App\User;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class JobWatchableController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(Job $job)
    {
        DB::beginTransaction();

        try {
            $job->users()->save(User::findOrFail(Auth::id()));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add job to watchlist.');
            return back();
        }

        Alert::success('Success!', 'Job added to watchlist.');
        return back();
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
