<?php

namespace App\Http\Controllers;

use App\RealEstate;
use App\Http\Requests\StoreRealEstate;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;

class RealEstateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        return view('app.real-estates')->with('realEstates', RealEstate::notFromUser()->paginate());
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show(RealEstate $realEstate)
    {
        return view('app.real-estate-info')->with('realEstate', $realEstate);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreRealEstate $request, RealEstate $realEstate)
    {
        DB::beginTransaction();

        try {
            $realEstate->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update real estate.');
            return back();
        }

        Alert::success('Success!', 'Real estate updated.');
        return back();
    }

    public function destroy(RealEstate $realEstate)
    {
        DB::beginTransaction();

        try {
            $realEstate->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove real estate.');
            return back();
        }

        Alert::success('Success!', 'Real estate removed.');
        return back();
    }
}
