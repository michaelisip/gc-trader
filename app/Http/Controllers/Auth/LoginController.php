<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Services\FacebookUserService;
use App\Services\GoogleUserService;
use App\Services\InstagramUserService;
use App\Services\LinkedInUserService;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function googleLogin()
    {
        try {
            return Socialite::driver('google')->redirect();
        } catch (Throwable $th) {
            Log::error($th->getMessage());
        }

        Alert::error('Oops!', 'Can\'t contact google at the moment.');
        return back();
    }

    public function googleCallback(GoogleUserService $service)
    {
        try {
            $socialUser = Socialite::driver('google')->user();
        } catch (Throwable $th) {
            Alert::error('Oops!', 'Can\'t fetch google information at the moment.');
            return redirect()->route('register');
        }

        DB::beginTransaction();

        try {
            $user = $service->createOrGetUser($socialUser);

            Auth::login($user);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t login using google.');
            return redirect()->route('register');
        }

        return redirect()->route('home');
    }

    public function facebookLogin()
    {
        try {
            return Socialite::driver('facebook')->redirect();
        } catch (Throwable $th) {
            Log::error($th->getMessage());
        }

        Alert::error('Oops!', 'Can\'t contact facebook at the moment.');
        return back();
    }

    public function facebookCallback(FacebookUserService $service)
    {
        try {
            $socialUser = Socialite::driver('facebook')->user();
        } catch (Throwable $th) {
            Alert::error('Oops!', 'Can\'t fetch facebook information at the moment.');
            return redirect()->route('register');
        }

        DB::beginTransaction();

        try {
            $user = $service->createOrGetUser($socialUser);

            Auth::login($user);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t login using google.');
            return redirect()->route('register');
        }

        return redirect()->route('home');
    }

    public function instagramLogin()
    {
        try {
            return Socialite::driver('instagram')->redirect();
        } catch (Throwable $th) {
            Log::error($th->getMessage());
        }

        Alert::error('Oops!', 'Can\'t contact instagram at the moment.');
        return back();
    }

    public function instagramCallback(InstagramUserService $service)
    {
        try {
            $socialUser = Socialite::driver('instagram')->user();
        } catch (Throwable $th) {
            Alert::error('Oops!', 'Can\'t fetch instagram information at the moment.');
            return redirect()->route('register');
        }

        DB::beginTransaction();

        try {
            $user = $service->createOrGetUser($socialUser);

            Auth::login($user);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t login using google.');
            return redirect()->route('register');
        }

        return redirect()->route('home');
    }

    public function linkedInLogin()
    {
        try {
            return Socialite::driver('linkedin')->redirect();
        } catch (Throwable $th) {
            Log::error($th->getMessage());
        }

        Alert::error('Oops!', 'Can\'t contact linkedin at the moment.');
        return back();
    }

    public function linkedInCallback(LinkedInUserService $service)
    {
        try {
            $socialUser = Socialite::driver('linkedin')->user();
        } catch (Throwable $th) {
            Alert::error('Oops!', 'Can\'t fetch linkedin information at the moment.');
            return redirect()->route('register');
        }

        DB::beginTransaction();

        try {
            $user = $service->createOrGetUser($socialUser);

            Auth::login($user);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t login using google.');
            return redirect()->route('register');
        }

        return redirect()->route('home');
    }
}
