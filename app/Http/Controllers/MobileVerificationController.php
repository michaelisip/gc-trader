<?php

namespace App\Http\Controllers;

use App\User;
use Twilio\Rest\Client;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class MobileVerificationController extends Controller
{
    public function request(Request $request)
    {
        try {
            $twilio = new Client(config('services.twilio.sid'), config('services.twilio.auth_token'));
            $twilio->verify
                ->v2
                ->services(config('services.twilio.verify_sid'))
                ->verifications
                ->create($request->input('phone_number'), 'sms');
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'Something went wrong.',
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Verification code sent.',
        ]);
    }

    public function verify(Request $request)
    {
        try {
            $twilio = new Client(config('twilio.'), config('twilio.auth_token'));
            $verification = $twilio->verify
                ->v2
                ->services(config('twilio.verify_sid'))
                ->verificationChecks
                ->create($request->input('verification_code'), array('to' => $request->input('phone_number')));
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'Can\'t verify your phone number at the moment. Please contact support',
            ]);
        }

        if (!$verification->valid) {
            return response()->json([
                'success' => false,
                'message' => 'Wrong Verification Number.',
            ]);
        } else {
            DB::beginTransaction();

            try {
                $user = User::findOrFail(Auth::id());
                $user->phone_verified_at = now();
                $user->save();

                DB::commit();
            } catch (Throwable $th) {
                DB::rollBack();
                Log::error($th->getMessage());

                return response()->json([
                    'success' => false,
                    'message' => 'Something went wrong. Please contact support.',
                ]);
            }
        }

        return response()->json([
            'success' => true,
            'message' => 'Phone Number Verified.',
        ]);
    }
}
