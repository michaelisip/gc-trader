<?php

namespace App\Http\Controllers;

use App\Travel;
use App\Category;
use App\Http\Requests\StoreTravel;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TravelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        return view('app.travels')->with([
            'travels' =>  Travel::notFromUser()->paginate(),
            'travelCategories' => Category::travel()->get(),
        ]);
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show(Travel $travel)
    {
        return view('app.travel-info')->with('travel', $travel);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreTravel $request, Travel $travel)
    {
        DB::beginTransaction();

        try {
            $travel->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update travel.');
            return back();
        }

        Alert::success('Success!', 'Travel updated.');
        return back();
    }

    public function destroy(Travel $travel)
    {
        DB::beginTransaction();

        try {
            $travel->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove travel.');
            return back();
        }

        Alert::success('Success!', 'Travel removed.');
        return back();
    }
}
