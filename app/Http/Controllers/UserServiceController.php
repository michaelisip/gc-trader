<?php

namespace App\Http\Controllers;

use App\User;
use App\Image;
use App\Service;
use App\Address;
use App\Category;
use App\Http\Requests\StoreAddress;
use App\Http\Requests\StoreService;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Gate;

class UserServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        return view('app.service-form')->with('serviceCategories', Category::service()->get());
    }

    public function store(StoreService $serviceRequest, StoreAddress $addressRequest, User $user)
    {
        Gate::authorize('create-service', Service::class);

        DB::beginTransaction();

        try {
            $serviceRequest->merge(['is_abn_registered' => isset($serviceRequest->abn_registration) ? 1 : 0]);
            $service = new Service($serviceRequest->only([
                'name',
                'company_name',
                'is_abn_registered',
                'abn_registration',
                'website_url',
                'description',
            ]));
            $user->services()->save($service);

            if ($serviceRequest->has('photo')) {
                foreach ($serviceRequest->photo as $photo) {
                    $service->images()->save(new Image(['src' => $photo]));
                }
            }

            if ($serviceRequest->has('categories')) {
                foreach ($serviceRequest->categories as $category) {
                    $service->categories()->attach($category);
                    $service->save();
                }
            }

            $service->address()->save(new Address($addressRequest->only([
                'country',
                'state',
                'suburb',
                'city',
                'post_code',
                'street_name',
                'number',
            ])));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add service.');
            return back();
        }

        Alert::success('Success!', 'Service added.');
        return back();
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
