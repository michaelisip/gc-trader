<?php

namespace App\Http\Controllers;

use App\Community;
use App\Traits\EmailReminderTrait;
use RealRashid\SweetAlert\Facades\Alert;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    use EmailReminderTrait;

    public function __construct()
    {
        $this->middleware('auth:admin')->except(['login', 'logout']);
    }

    public function login(Request $request)
    {
        if (Auth::guard('admin')->attempt($request->only('email', 'password'))) {
            return redirect()->intended(route('admin.index'));
        }

        Alert::error('Oops!', 'Incorrect Username or Password.');
        return back();
    }

    public function logout()
    {
        Auth::guard('admin')->logout();

        return redirect()->route('admin.login.show');
    }

    public function index()
    {
        return view('admin.communities')->with('communities', Community::withoutGlobalScopes()->paginate());
    }

    public function show($id)
    {
        $community = Community::withoutGlobalScopes()->findOrFail($id);
        $community->load('user');

        return response()->json(['community' => $community]);
    }

}
