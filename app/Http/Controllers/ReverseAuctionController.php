<?php

namespace App\Http\Controllers;

use App\Product;

use Illuminate\Support\Facades\Gate;

class ReverseAuctionController extends Controller
{
    public function __construct() 
    {
        $this->middleware('verified');
    }

    public function __invoke()
    {
        Gate::authorize('view-any-reverse-auction-product', Product::class);

        return view('app.reverse-auctions')->with('reverseAuctions', Product::notFromUser()->reverseAuction()->paginate());
    }
}
