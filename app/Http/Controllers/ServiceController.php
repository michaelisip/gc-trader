<?php

namespace App\Http\Controllers;

use App\Service;
use App\Http\Requests\StoreService;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ServiceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        return view('app.services')->with('services', Service::paginate());
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show(Service $service)
    {
        return view('app.service-info')->with('service', $service);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreService $request, Service $service)
    {
        DB::beginTransaction();

        try {
            $service->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update service.');
            return back();
        }

        Alert::success('Success!', 'Service updated.');
        return back();
    }

    public function destroy(Service $service)
    {
        DB::beginTransaction();

        try {
            $service->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove service.');
            return back();
        }

        Alert::success('Success!', 'Service removed.');
        return back();
    }
}
