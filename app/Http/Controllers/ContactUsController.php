<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function __invoke(Request $request)
    {
        try {
            Mail::send(new ContactMail($request));
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            Alert::error('Oops!', 'Message not sent.');
            return back();
        }

        Alert::success('Success!', 'Submitted successfully!');
        return back();
    }
}
