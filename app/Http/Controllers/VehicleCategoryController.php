<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\Traits\CategoryTrait;
use App\Http\Requests\StoreCategory;

class VehicleCategoryController extends Controller
{
    use CategoryTrait;

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreCategory $request, Vehicle $vehicle)
    {
        return $this->storeCategory($request->validated(), $vehicle);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
