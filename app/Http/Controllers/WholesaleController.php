<?php

namespace App\Http\Controllers;

use App\Product;

use Illuminate\Support\Facades\Gate;

class WholesaleController extends Controller
{
    public function __construct() 
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function __invoke()
    {
        return view('app.wholesales')->with('wholesales', Product::notFromUser()->wholesale()->paginate());
    }
}
