<?php

namespace App\Http\Controllers;

use App\Vehicle;
use App\Http\Requests\StoreVehicle;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class VehicleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    public function index()
    {
        return view('app.vehicles')->with('vehicles', Vehicle::notFromUser()->paginate());
    }

    public function create()
    {
        abort(404);
    }

    public function store()
    {
        abort(404);
    }

    public function show(Vehicle $vehicle)
    {
        return view('app.vehicle-info')->with('vehicle', $vehicle);
    }

    public function edit()
    {
        abort(404);
    }

    public function update(StoreVehicle $request, Vehicle $vehicle)
    {
        DB::beginTransaction();

        try {
            $vehicle->update($request->validated());

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update vehicle.');
            return back();
        }

        Alert::success('Success!', 'Vehicle updated.');
        return back();
    }

    public function destroy(Vehicle $vehicle)
    {
        DB::beginTransaction();

        try {
            $vehicle->delete();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t remove vehicle.');
            return back();
        }

        Alert::success('Success!', 'Vehicle removed.');
        return back();
    }
}
