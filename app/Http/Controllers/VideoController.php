<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreVideo;

use Throwable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class VideoController extends Controller
{
    public function index()
    {
        abort(404);
    }

    public function create()
    {
        abort(404);
    }

    public function store(StoreVideo $request)
    {
        try {
          if (config('services.aws.access_key_id')) {
                $path = Storage::disk('s3')->put('gctrader/video', $request->file('video'), 'public');
              $path = config('services.aws.url') . '/' . $path;
            } else {
                $path = Storage::put(public_path('storage'), $request->file('video'));
            }
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            return response()->json([
                'success' => false,
                'src' => $path,
            ]);
        }

        return response()->json([
            'success' => true,
            'src' => $path,
        ]);
    }

    public function show()
    {
        abort(404);
    }

    public function edit()
    {
        abort(404);
    }

    public function update()
    {
        abort(404);
    }

    public function destroy()
    {
        abort(404);
    }
}
