<?php

namespace App\Http\View\Composers\Admin;

use App\Ad;
use App\EmailSubscription;
use App\Product;
use App\User;
use Illuminate\View\View;

class Admin
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'userCount' => User::active()->count(),
            'productCount' => Product::count(),
            'subscriptionCount' => EmailSubscription::count(),
            'adCount' => Ad::count(),
        ]);
    }
}
