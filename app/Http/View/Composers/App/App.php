<?php

namespace App\Http\View\Composers\App;

use App\Ad;
use App\Category;
use Illuminate\View\View;

class App
{
    public function compose(View $view)
    {
        $tagColors = [
            'red',
            'orange',
            'yellow',
            'olive',
            'green',
            'teal',
            'blue',
            'violet',
            'purple',
            'pink',
            'brown',
            'grey',
            'black',
        ];

        $view->with([
            'productCategories' => Category::product()->orderBy('name')->get(),
            'bannerAds' => Ad::goldTier()->get(),
            'tagColors' => $tagColors,
        ]);
    }
}
