<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NotifyReverseAuctionWinner extends Notification
{
    use Queueable;

    public $winner;
    public $product;

    public function __construct($winner, $product)
    {
        $this->winner = $winner;
        $this->product = $product;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject(config('app.name') . '|' . 'Reverse Auction Participant')
                    ->greeting('Congratulations!')
                    ->line("You just won {$this->product->name}! You can contact the buyer to take further actions.")
                    ->action('Visit GC Trader', route('home'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
