<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable, SoftDeletes, SearchableTrait;

    protected $guarded = [
        'type',
        'status',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = ['birth_date'];

    protected $searchable = [
        'columns' => [
            'users.first_name' => 10,
            'users.last_name' => 10,
        ],
    ];

    public function address()
    {
        return $this->morphOne('App\Address', 'addressable');
    }

    public function ads()
    {
        return $this->hasMany('App\Ad');
    }

    public function bids()
    {
        return $this->hasMany('App\Bid');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function communities()
    {
        return $this->hasMany('App\Community');
    }

    public function feedbacks()
    {
        return $this->hasMany('App\Feedback');
    }

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable')->latest();
    }

    public function jobs()
    {
        return $this->hasMany('App\Job');
    }

    public function jobWatchlist()
    {
        return $this->morphedByMany('App\Job', 'watchable')
                    ->withPivot('id');
    }

    public function memberships()
    {
        return $this->belongsToMany('App\Membership')
            ->as('membership')
            ->withPivot(
                'sales_count',
                'sales_cost'
            )
            ->withTimestamps();
    }

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function productWatchlist()
    {
        return $this->morphedByMany('App\Product', 'watchable')
                    ->withPivot('id');
    }

    public function realEstates()
    {
        return $this->hasMany('App\RealEstate');
    }

    public function realEstateWatchlist()
    {
        return $this->morphedByMany('App\RealEstate', 'watchable')
                    ->withPivot('id');
    }

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function serviceWatchlist()
    {
        return $this->morphedByMany('App\Service', 'watchable')
                    ->withPivot('id');
    }

    public function travels()
    {
        return $this->hasMany('App\Travel');
    }

    public function travelWatchlist()
    {
        return $this->morphedByMany('App\Travel', 'watchable')
                    ->withPivot('id');
    }

    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }

    public function vehicleWatchlist()
    {
        return $this->morphedByMany('App\Vehicle', 'watchable')
                    ->withPivot('id');
    }

    public function requests()
    {
        return $this->hasMany('App\WholesaleRequest', 'vendor_id');
    }

    public function wholesaleRequests()
    {
        return $this->hasMany('App\WholesaleRequest', 'buyer_id');
    }

    public function getFullNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function getProfilePlaceholderAttribute()
    {
        return $this->gender === 'Male' ? asset('img/matt.jpg') : asset('img/kristy.jpg');
    }

    public function getIsActiveAttribute()
    {
        return $this->status === 'active' ? true : false;
    }

    public function getIsBusinessManAttribute()
    {
        return $this->type === 'Business' ? true : false;
    }

    public function scopeActive($query)
    {
        return $query->whereStatus('active');
    }
}
