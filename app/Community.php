<?php

namespace App;

use App\Scopes\StatusScope;
use App\Scopes\UserStatusScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Community extends Model
{
    use SearchableTrait, SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new StatusScope);
        static::addGlobalScope(new UserStatusScope);
    }

    protected $guarded = ['status'];

    protected $dates = [
        'date_time',
        'start_time',
        'finish_time',
    ];

    protected $searchable = [
        'columns' => [
            'communities.name' => 10,
        ],
    ];

    public function categories()
    {
        return $this->morphToMany('App\Category', 'categorizable');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function video()
    {
        return $this->morphOne('App\Video', 'videoable');
    }
}
