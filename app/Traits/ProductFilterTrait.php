<?php

namespace App\Traits;

use App\Membership;
use App\Product;
use App\RealEstate;
use App\Travel;
use App\Vehicle;

trait ProductFilterTrait
{
    public function filterProduct($id, $type)
    {
        switch ($type) {
            case 'membership':
                return Membership::withoutGlobalScopes()->findOrFail($id);
                break;
            case 'activation':
                return (object) [
                    'name' => 'GC Trader Credit Card Verfication',
                    'description' => 'In order to take further actions, you need to verify you email, phone number and credit card. Upon verifying your credit card, you will be charged 10 AUD in the process. This will serve as a fee for joining GC Trader.',
                    'price' => 10,
                ];
                break;
            case 'product':
                return Product::withoutGlobalScopes()->findOrFail($id)->load(['user', 'bids']);
                break;
            case 'real_estate':
                return RealEstate::withoutGlobalScopes()->findOrFail($id)->load('user');
                break;
            case 'travel':
                return Travel::withoutGlobalScopes()->findOrFail($id)->load('user');
                break;
            default:
                return Vehicle::withoutGlobalScopes()->findOrFail($id)->load('user');
                break;
        }
    }

    public function updateProductPurchase($id, $type, $quantity = 1)
    {
        switch ($type) {
            case 'activation':
                $this->activateUser();
                break;
            case 'membership':
                $this->attachMembership($id);
                break;
            case 'product':
                $this->purchaseProduct($id, $quantity);
                break;
            case 'real_estate':
                $this->purchaseRealEstate($id);
                break;
            case 'travel':
                $this->purchaseTravel($id);
                break;

            default:
                $this->purchaseVehicle($id);
                break;
        }
    }
}
