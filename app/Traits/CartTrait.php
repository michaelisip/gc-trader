<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait CartTrait
{
    public function cartGetAll()
    {
        $cartJson = json_encode(session('cart'));
        return collect(json_decode($cartJson));
    }

    public function cartAdd(Request $request, $id, $itemID, string $itemType, string $name, float $price, int $quantity)
    {
        $cartJson = json_encode(session('cart'));
        $cart = collect(json_decode($cartJson));
        $isExist = $cart->search(function ($item) use ($itemID) {
            return $item->item_id === $itemID;
        });

        $cartItems = [
            $id => [
                'id' => $id,
                'item_id' => $itemID,
                'item_type' => $itemType,
                'name' => $name,
                'price' => $price,
                'quantity' => $quantity,
            ]
        ];

        if ($request->session()->has('cart')) {
            foreach ($cart->all() as $cartItem) {
                array_push($cartItems, $cartItem);
            }
        }

        if (!$isExist) {
            session(['cart' => collect($cartItems)]);
        }
    }

    public function cartUpdate(Request $request, $id, int $quantity)
    {
        if ($request->session()->has('cart')) {
            $cartJson = json_encode(session('cart'));
            $cartItems = collect(json_decode($cartJson));

            $newCartItems = $cartItems->transform(function ($item) use ($id, $quantity) {
                if ($item->id === $id) {
                    $item->quantity = $quantity;
                }

                return $item;
            });

            session(['cart' => $newCartItems->all()]);
        }
    }

    public function cartRemove(Request $request, $id)
    {
        if ($request->session()->has('cart')) {
            $cartJson = json_encode(session('cart'));
            $cartItems = collect(json_decode($cartJson));

            $newCartItems = $cartItems->reject(function ($value) use ($id) {
                return $value->id === $id;
            });

            session(['cart' => $newCartItems->all()]);
        }
    }

    public function cartClear(Request $request)
    {
        $request->session()->forget('cart');
    }
}
