<?php

namespace App\Traits;

use App\Vehicle;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait VehiclePurchaseTrait
{
    public function purchaseVehicle($id)
    {
        DB::beginTransaction();

        try {
            $vehicle = Vehicle::findOrFail($id);
            $vehicle->status = 'inactive';
            $vehicle->save();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update vehicle. Please contact support.');
            return redirect()->route('home');
        }
    }
}
