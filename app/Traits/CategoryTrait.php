<?php

namespace App\Traits;

use App\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait CategoryTrait
{
    public function storeCategory($values, $model)
    {
        DB::beginTransaction();

        try {
            $model->categories()->save(new Category($values));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add category.');
            return back();
        }

        Alert::success('Success!', 'Category added.');
        return back();
    }
}
