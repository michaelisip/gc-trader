<?php

namespace App\Traits;

use App\Address;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait AddressTrait
{
    public function storeAddress($values, $model)
    {
        DB::beginTransaction();

        try {
            $model->address()->save(new Address($values));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add address.');
            return back();
        }

        Alert::success('Success!', 'Address added.');
        return back();
    }
}
