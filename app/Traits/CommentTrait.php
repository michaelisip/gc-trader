<?php

namespace App\Traits;

use App\Comment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait CommentTrait
{
    public function storeComment($values, $model)
    {
        DB::beginTransaction();

        try {
            $model->comments()->save(new Comment($values));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add comment.');
            return back();
        }

        Alert::success('Success!', 'Comment added.');
        return back();
    }
}
