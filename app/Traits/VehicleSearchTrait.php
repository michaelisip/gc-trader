<?php

namespace App\Traits;

use App\Vehicle;
use Illuminate\Http\Request;

trait VehicleSearchTrait
{
    public function searchVehicle(Request $request)
    {
        $vehicles = new Vehicle();

        if ($request->input('model')) {
            $vehicles = $vehicles->where('model', 'like', '%' . $request->input('model') . '%');
        }

        if ($request->input('manufacturer')) {
            $vehicles = $vehicles->where('manufacturer', 'like', '%' . $request->input('manufacturer') . '%');
        }

        if ($request->input('price_from')) {
            $vehicles = $vehicles->where('price', '>=', $request->input('price_from'));
        }

        if ($request->input('price_to')) {
            $vehicles = $vehicles->where('price', '<=', $request->input('price_to'));
        }

        if ($request->input('year')) {
            $vehicles = $vehicles->where('year', $request->input('year'));
        }

        if ($request->input('keywords')) {
            $vehicles = $vehicles->search($request->input('keywords'));
        }

        return view('app.vehicles')->with('vehicles', $vehicles->paginate());
    }
}
