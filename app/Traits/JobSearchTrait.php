<?php

namespace App\Traits;

use App\Category;
use App\Job;
use Illuminate\Http\Request;

trait JobSearchTrait
{
    public function searchJob(Request $request)
    {
        $jobs = new Job();

        if ($request->input('category')) {
            $jobs = $jobs->whereHas('categories', function ($query) use ($request) {
                $query->whereName($request->input('category'));
            });
        }

        if ($request->input('type')) {
            $jobs = $jobs->whereType($request->input('type'));
        }

        if ($request->input('country')) {
            $jobs = $jobs->whereHas('address', function ($query) use ($request) {
                $query->whereCountry($request->input('country'));
            });
        }

        if ($request->input('state')) {
            $jobs = $jobs->whereHas('address', function ($query) use ($request) {
                $query->where('state', 'like', '%' . $request->input('state') . '%');
            });
        }

        if ($request->input('suburb')) {
            $jobs = $jobs->whereHas('address', function ($query) use ($request) {
                $query->where('suburb', 'like', '%' . $request->input('suburb') . '%');
            });
        }

        if ($request->input('city')) {
            $jobs = $jobs->whereHas('address', function ($query) use ($request) {
                $query->where('city', 'like', '%' . $request->input('city') . '%');
            });
        }

        if ($request->input('post_code')) {
            $jobs = $jobs->whereHas('address', function ($query) use ($request) {
                $query->where('post_code', 'like', '%' . $request->input('post_code') . '%');
            });
        }

        if ($request->input('street_name')) {
            $jobs = $jobs->whereHas('address', function ($query) use ($request) {
                $query->where('street_name', 'like', '%' . $request->input('street_name') . '%');
            });
        }

        if ($request->input('number')) {
            $jobs = $jobs->whereHas('address', function ($query) use ($request) {
                $query->where('number', 'like', '%' . $request->input('number') . '%');
            });
        }

        if ($request->input('keywords')) {
            $jobs = $jobs->search($request->input('keywords'));
        }

        return view('app.jobs')->with([
            'jobs' => $jobs->paginate(),
            'jobCategories' => Category::job()->get(),
        ]);
    }
}
