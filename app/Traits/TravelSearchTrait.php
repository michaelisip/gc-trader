<?php

namespace App\Traits;

use App\Category;
use App\Travel;
use Illuminate\Http\Request;

trait TravelSearchTrait
{
    public function searchTravel(Request $request)
    {
        $travels = new Travel();

        if ($request->input('category')) {
            $travels = $travels->whereHas('categories', function ($query) use ($request) {
                $query->whereName($request->input('category'));
            });
        }

        if ($request->input('country')) {
            $travels = $travels->whereHas('address', function ($query) use ($request) {
                $query->whereCountry($request->input('country'));
            });
        }

        if ($request->input('price_from')) {
            $travels = $travels->where('price', '>=', $request->input('price_from'));
        }

        if ($request->input('price_to')) {
            $travels = $travels->where('price', '<=', $request->input('price_to'));
        }

        if ($request->input('date_from')) {
            $travels = $travels->whereDateFrom($request->input('date_from'));
        }

        if ($request->input('date_to')) {
            $travels = $travels->whereDateTo($request->input('date_to'));
        }

        if ($request->input('state')) {
            $travels = $travels->whereHas('address', function ($query) use ($request) {
                $query->where('state', 'like', '%' . $request->input('state') . '%');
            });
        }

        if ($request->input('suburb')) {
            $travels = $travels->whereHas('address', function ($query) use ($request) {
                $query->where('suburb', 'like', '%' . $request->input('suburb') . '%');
            });
        }

        if ($request->input('city')) {
            $travels = $travels->whereHas('address', function ($query) use ($request) {
                $query->where('city', 'like', '%' . $request->input('city') . '%');
            });
        }

        if ($request->input('post_code')) {
            $travels = $travels->whereHas('address', function ($query) use ($request) {
                $query->where('post_code', 'like', '%' . $request->input('post_code') . '%');
            });
        }

        if ($request->input('street_name')) {
            $travels = $travels->whereHas('address', function ($query) use ($request) {
                $query->where('street_name', 'like', '%' . $request->input('street_name') . '%');
            });
        }

        if ($request->input('number')) {
            $travels = $travels->whereHas('address', function ($query) use ($request) {
                $query->where('number', 'like', '%' . $request->input('number') . '%');
            });
        }

        if ($request->input('keywords')) {
            $travels = $travels->search($request->input('keywords'));
        }

        return view('app.travels')->with([
            'travels' =>  $travels->paginate(),
            'travelCategories' => Category::travel()->get(),
        ]);
    }
}
