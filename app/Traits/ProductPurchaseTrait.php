<?php

namespace App\Traits;

use App\User;
use App\Product;
use App\WholesaleRequest;
use App\Mail\WholesaleRequest as WholesaleRequestMail;
use Stripe\PaymentIntent;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Auth;

trait ProductPurchaseTrait
{
    public function purchaseProduct($id, $quantity = 1)
    {
        DB::beginTransaction();

        try {
            $product = Product::with('user')->findOrFail($id);
            $vendor = User::findOrFail($product->user->id);

            switch ($product->type) {
                case 'auction':
                    $memberships = $vendor->memberships()
                        ->where('name', 'Basic')
                        ->orWhere('name', 'Busines')
                        ->get();

                    $this->updateVendorMembership($product, $vendor, $memberships, $quantity);
                    break;
                case 'retail':
                    $memberships = $vendor->memberships()
                        ->where('name', 'Basic')
                        ->orWhere('name', 'Business')
                        ->get();

                    $this->updateVendorMembership($product, $vendor, $memberships, $quantity);
                    break;
                case 'reverse_auction':
                    $memberships = $vendor->memberships()
                        ->where('name', 'Basic')
                        ->orWhere('name', 'Business')
                        ->get();

                    $this->updateVendorMembership($product, $vendor, $memberships, $quantity);
                    break;

                default:
                    $memberships = $vendor->memberships()
                        ->whereName('Wholesale')
                        ->get();

                    $this->updateVendorMembership($product, $vendor, $memberships, $quantity);
                    break;
            }

            if (!($product->quantity - 1)) {
                $product->status = 'inactive';
            }

            $product->quantity--;
            $product->sales++;
            $product->save();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update product. Please contact support.');
            return redirect()->route('home');
        }
    }

    public function updateVendorMembership($product, $vendor, $memberships, $quantity)
    {
        if ($memberships->count()) {
            foreach ($memberships as $membership) {
                if ($membership->sales_count_limit && $membership->sales_cost_limit) {
                    if ($membership->sales_count_limit > $membership->membership->sales_count) {
                        $vendor->memberships()->updateExistingPivot($membership->membership->membership_id, [
                            'sales_count' => $membership->membership->sales_count + $quantity,
                            'sales_cost' => $membership->membership->sales_cost + ($quantity * (filter_var($product->price, FILTER_SANITIZE_NUMBER_INT))),
                        ]);
                        break;
                    } else {
                        $vendor->memberships()->detach($membership->membership->membership_id);
                    }
                }
            }
        }
    }
}
