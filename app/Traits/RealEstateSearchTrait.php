<?php

namespace App\Traits;

use App\RealEstate;
use Illuminate\Http\Request;

trait RealEstateSearchTrait
{
    public function searchRealEstate(Request $request)
    {
        $realEstates = new RealEstate();

        if ($request->input('price_from')) {
            $realEstates = $realEstates->where('price', '>=', $request->input('price_from'));
        }

        if ($request->input('price_to')) {
            $realEstates = $realEstates->where('price', '<=', $request->input('price_to'));
        }

        if ($request->input('bedrooms')) {
            $realEstates = $realEstates->whereBedrooms($request->input('bedrooms'));
        }

        if ($request->input('bathrooms')) {
            $realEstates = $realEstates->whereBathrooms($request->input('bathrooms'));
        }

        if ($request->input('type')) {
            $types = explode(' ', $request->input('type'));
            $realEstates = $realEstates->whereIn('type', str_replace('-', ' ', $types));
        }

        if ($request->input('country')) {
            $realEstates = $realEstates->whereHas('address', function ($query) use ($request) {
                $query->whereCountry($request->input('country'));
            });
        }

        if ($request->input('state')) {
            $realEstates = $realEstates->whereHas('address', function ($query) use ($request) {
                $query->where('state', 'like', '%' . $request->input('state') . '%');
            });
        }

        if ($request->input('suburb')) {
            $realEstates = $realEstates->whereHas('address', function ($query) use ($request) {
                $query->where('suburb', 'like', '%' . $request->input('suburb') . '%');
            });
        }

        if ($request->input('city')) {
            $realEstates = $realEstates->whereHas('address', function ($query) use ($request) {
                $query->where('city', 'like', '%' . $request->input('city') . '%');
            });
        }

        if ($request->input('post_code')) {
            $realEstates = $realEstates->whereHas('address', function ($query) use ($request) {
                $query->where('post_code', 'like', '%' . $request->input('post_code') . '%');
            });
        }

        if ($request->input('street_name')) {
            $realEstates = $realEstates->whereHas('address', function ($query) use ($request) {
                $query->where('street_name', 'like', '%' . $request->input('street_name') . '%');
            });
        }

        if ($request->input('number')) {
            $realEstates = $realEstates->whereHas('address', function ($query) use ($request) {
                $query->where('number', 'like', '%' . $request->input('number') . '%');
            });
        }

        if ($request->input('keywords')) {
            $realEstates = $realEstates->search($request->input('keywords'));
        }

        return view('app.real-estates')->with('realEstates', $realEstates->paginate());
    }
}
