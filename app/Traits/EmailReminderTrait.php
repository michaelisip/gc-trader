<?php

namespace App\Traits;

use App\Community;
use App\Mail\ReminderMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait EmailReminderTrait
{
    public function send(Request $request)
    {
        try {
            $communitiesEmails = Community::pluck('email')->toArray();

            Mail::to($communitiesEmails) 
                  ->send(new ReminderMail($request));
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Something went wrong!');
            return back();
        }

        Alert::success('Success!', 'Successfully sent email reminder.');
        return back();
    }
}
