<?php

namespace App\Traits;

use App\RealEstate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait RealEstatePurchaseTrait
{
    public function purchaseRealEstate($id)
    {
        DB::beginTransaction();

        try {
            $realEstate = RealEstate::findOrFail($id);

            $realEstate->status = 'inactive';
            $realEstate->save();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update real estate. Please contact support.');
            return redirect()->route('home');
        }
    }
}
