<?php

namespace App\Traits;

use App\User;
use App\Product;
use Stripe\Transfer;
use Stripe\PaymentIntent;
use Stripe\Checkout\Session;
use RealRashid\SweetAlert\Facades\Alert;

use Throwable;
use Illuminate\Support\Facades\Log;

trait StripePaymentTrait
{
    public function createSession($lineItems)
    {
        try {
            $session = Session::create([
                'payment_method_types' => ['card'],
                'line_items' => $lineItems,
                'success_url' => route('stripe.success'),
                'cancel_url' => route('stripe.cancel'),
            ]);

            session([
                'payment_intent' => $session->payment_intent
            ]);

            Log::info($session);
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            return response()->json([
                'success' => false,
                'message' => 'Something went wrong. Please contact support.',
                'response' => null,
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => 'Checkout Session Successful.',
            'response' => $session,
        ]);
    }

    public function transferVendorPayouts($itemID, $itemType, $paymentType)
    {
        if ($itemType === 'membership' || $itemType === 'activation') {
            return false;
        }

        $totalAmount = 0;
        $transferGroup = uniqid('tg_');
        $transferItems = [];

        if ($paymentType === 'single') {
            $item = $this->filterProduct($itemID, $itemType);
            $totalAmount += floatval($item->price);
            $vendor = User::findOrFail($item->user->id);
            $amount = floatval($item->price);

            if ($itemType === 'product') {
                $product = Product::findOrFail($itemID);
                $availableProductMembership = $this->checkProductMembership($product, $vendor);
                $amount = $amount - ($amount * ($availableProductMembership->sales_percentage / 100));
            }

            if ($item->is_reverse_auction) {
                $winningBid = $item->bids()->orderBy('bid')->first();
                $desination = $winningBid->user->stripe_user_id;
            } else {
                $desination = $item->user->stripe_user_id;
            }

            array_push($transferItems, [
                'amount' => $amount * 100,
                'destination' => $desination,
            ]);
        } else {
            foreach ($this->cartGetAll()->all() as $cartItem) {
                $item = $this->filterProduct($cartItem->item_id, $cartItem->item_type);
                $totalAmount += floatval($item->price);
                $vendor = User::findOrFail($item->user->id);
                $amount = floatval($item->price);

                if ($itemType === 'product') {
                    $product = Product::findOrFail($itemID);
                    $availableProductMembership = $this->checkProductMembership($product, $vendor);
                    $amount = $amount - ($amount * ($availableProductMembership->sales_percentage / 100));
                }

                array_push($transferItems, [
                    'amount' => ($amount * 100) * $cartItem->quantity,
                    'destination' => $item->user->stripe_user_id,
                ]);
            }
        }

        try {
            $paymentIntent = PaymentIntent::create([
                'amount' => $totalAmount,
                'currency' => config('services.cashier.currency'),
                'payment_method_types' => ['card'],
                'transfer_group' => $transferGroup,
            ]);

            Log::info($paymentIntent);

            foreach ($transferItems as $transferItem) {
                $transfer = Transfer::create([
                    'amount' => (int) $transferItem['amount'],
                    'currency' => config('services.cashier.currency'),
                    'destination' => $transferItem['destination'],
                    'transfer_group' => $transferGroup,
                ]);

                Log::info(($transfer));
            }
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update product. Please contact support.');
            return redirect()->route('home');
        }
    }
}
