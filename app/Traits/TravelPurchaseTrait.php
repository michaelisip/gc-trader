<?php

namespace App\Traits;

use App\Travel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait TravelPurchaseTrait
{
    public function purchaseTravel($id)
    {
        DB::beginTransaction();

        try {
            $travel = Travel::findOrFail($id);
            $travel->status = 'inactive';
            $travel->save();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t update travel. Please contact support.');
            return redirect()->route('home');
        }
    }
}
