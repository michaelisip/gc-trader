<?php

namespace App\Traits;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait UserActivationTrait
{
    public function activateUser()
    {
        DB::beginTransaction();

        try {
            $user = User::findOrFail(Auth::id());
            $user->status = 'active';
            $user->save();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t activate user at the moment. Please contact support.');
            return redirect()->route('account');
        }
    }
}
