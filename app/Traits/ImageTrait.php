<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Throwable;

trait ImageTrait
{
    public function uploadImage(Request $request)
    {
        try {
            if (config('services.aws.access_key_id')) {
                $path = Storage::disk('s3')->put('gctrader/img', $request->file('photo'), 'public');
                $path = config('services.aws.url') . '/' . $path;
            } else {
                $path = Storage::put(public_path('storage'), $request->file('photo'));
            }
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            return response()->json([
                'success' => false,
                'image' => null,
            ]);
        }

        return $path;
    }
}
