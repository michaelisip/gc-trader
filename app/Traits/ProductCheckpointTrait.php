<?php

namespace App\Traits;

trait ProductCheckpointTrait
{
    public function checkKeywords($input)
    {
        $input = strtolower($input);

        return str_contains($input, [
            'make me an offer',
            'price negotiable',
            '$$$',
        ]);
    }
}
