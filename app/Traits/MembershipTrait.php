<?php

namespace App\Traits;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait MembershipTrait
{
    public function attachMembership($id)
    {
        DB::beginTransaction();

        try {
            $user = User::findOrFail(Auth::id());
            $user->memberships()->attach($id);
            $user->save();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t link membership to your account. Please contact support.');
            return redirect()->route('account');
        }
    }

    public function checkProductMembership($product, $vendor)
    {
        switch ($product->type) {
            case 'auction':
                return $vendor->memberships()
                    ->where('name', 'Basic')
                    ->orWhere('name', 'Busines')
                    ->first();
                break;
            case 'retail':
                return $vendor->memberships()
                    ->where('name', 'Basic')
                    ->orWhere('name', 'Business')
                    ->first();
                break;
            case 'reverse_auction':
                return $vendor->memberships()
                    ->where('name', 'Basic')
                    ->orWhere('name', 'Business')
                    ->first();
                break;

            default:
                return $vendor->memberships()
                    ->whereName('Wholesale')
                    ->first();
                break;
        }
    }
}
