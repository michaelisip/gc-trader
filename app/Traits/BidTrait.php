<?php

namespace App\Traits;

use App\Bid;
use App\Product;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait BidTrait
{
    public function storeBid($values, $model)
    {
        DB::beginTransaction();

        try {
            Model::unguard();
            $model->bids()->save(new Bid($values));
            Model::reguard();

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add bid.');
            return back();
        }

        Alert::success('Success!', 'Bid added.');
        return back();
    }

    public function payReverseAuctionVendor(Product $product)
    {
        try {
            // 
            
            $product->status = "pending";
            $product->save();
        } catch (Throwable $th) {
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t accept bid.');
            return back();
        }

        Alert::success('Success!', 'Bid accepted.');
        return back();
    }

    public function processUserBid($request, $id, $bid)
    {
        DB::beginTransaction();

        try {
            $product = Product::findOrFail($id);
            $user = User::findOrFail(Auth::id());

            $paymentIntent = $request->session()->pull('payment_intent', null);

            $newBid = new Bid();
            $newBid->bid = $bid;
            $newBid->payment_intent = $paymentIntent;
            $newBid->save();

            $product->bids()->save($newBid);
            $user->bids()->save($newBid);

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add bid.');
            return redirect()->route('home');
        }
    }
}
