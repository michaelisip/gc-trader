<?php

namespace App\Traits;

use App\Feedback;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use RealRashid\SweetAlert\Facades\Alert;
use Throwable;

trait FeedbackTrait
{
    public function storeFeedback($values, $model)
    {
        DB::beginTransaction();

        try {
            $model->feedbacks()->save(new Feedback($values));

            DB::commit();
        } catch (Throwable $th) {
            DB::rollBack();
            Log::error($th->getMessage());

            Alert::error('Oops!', 'Can\'t add feedback.');
            return back();
        }

        Alert::success('Success!', 'Feedback added.');
        return back();
    }
}
