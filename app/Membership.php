<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Membership extends Model
{
    use SoftDeletes;

    protected $guarded = [
        'price',
        'sales_count_limit',
        'sales_cost_limit',
        'sales_percentage',
    ];

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

    public function getFormattedPriceAttribute()
    {
        return number_format($this->price, 2, '.', ',');
    }
}
