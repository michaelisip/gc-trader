<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\SoftDeletes;

class MembershipUser extends Pivot
{
    use SoftDeletes;

    protected $guarded = [
        'sales_count',
        'sales_cost',
    ];
}
