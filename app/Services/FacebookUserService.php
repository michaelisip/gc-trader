<?php

namespace App\Services;

use App\FacebookUser;
use App\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Contracts\User as ProviderUser;

class FacebookUserService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $facebookAccount = FacebookUser::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if (isset($facebookAccount)) {
            return $facebookAccount->user;
        } else {
            $newFacebookAccount = new FacebookUser([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook',
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (is_null($user)) {
                $user = User::create([
                    'first_name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'email_verified_at' => now(),
                    'type' => 'Non-Business',
                    'password' => Hash::make(rand(1, 10000)),
                ]);
            }

            $newFacebookAccount->user()
                ->associate($user);
            $newFacebookAccount->save();

            return $user;
        }
    }
}
