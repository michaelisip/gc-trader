<?php

namespace App\Services;

use App\GoogleUser;
use App\User;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Contracts\User as ProviderUser;

class GoogleUserService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $googleAccount = GoogleUser::whereProvider('google')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if (isset($googleAccount)) {
            return $googleAccount->user;
        } else {
            $newGoogleAccount = new GoogleUser([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'google',
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (is_null($user)) {
                $user = User::create([
                    'first_name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'email_verified_at' => now(),
                    'type' => 'Non-Business',
                    'password' => Hash::make(rand(1, 10000)),
                ]);
            }

            $newGoogleAccount->user()
                ->associate($user);
            $newGoogleAccount->save();

            return $user;
        }
    }
}
