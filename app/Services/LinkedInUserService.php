<?php

namespace App\Services;

use App\User;
use App\LinkedinUser;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Contracts\User as ProviderUser;

class LinkedInUserService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $linkedInAccount = LinkedinUser::whereProvider('linkedin')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if (isset($linkedInAccount)) {
            return $linkedInAccount->user;
        } else {
            $newLinkedInramAccount = new LinkedinUser([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'linkedin',
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (is_null($user)) {
                $user = User::create([
                    'first_name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'email_verified_at' => now(),
                    'type' => 'Non-Business',
                    'password' => Hash::make(rand(1, 10000)),
                ]);
            }

            $newLinkedInramAccount->user()
                ->associate($user);
            $newLinkedInramAccount->save();

            return $user;
        }
    }
}
