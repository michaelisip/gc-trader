<?php

namespace App\Services;

use App\User;
use App\InstagramUser;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Contracts\User as ProviderUser;

class InstagramUserService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $instagramAccount = InstagramUser::whereProvider('instagram')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if (isset($instagramAccount)) {
            return $instagramAccount->user;
        } else {
            $newInstagramAccount = new InstagramUser([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'instagram',
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (is_null($user)) {
                $user = User::create([
                    'first_name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'email_verified_at' => now(),
                    'type' => 'Non-Business',
                    'password' => Hash::make(rand(1, 10000)),
                ]);
            }

            $newInstagramAccount->user()
                ->associate($user);
            $newInstagramAccount->save();

            return $user;
        }
    }
}
