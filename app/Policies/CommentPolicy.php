<?php

namespace App\Policies;

use App\Comment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user, Comment $comment)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->is_active;
    }

    public function update(User $user, Comment $comment)
    {
        return $user->id === $comment->commentable->user->id;
    }

    public function delete(User $user, Comment $comment)
    {
        return true;
    }

    public function restore(User $user, Comment $comment)
    {
        return true;
    }

    public function forceDelete(User $user, Comment $comment)
    {
        return true;
    }
}
