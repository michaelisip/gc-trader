<?php

namespace App\Policies;

use App\Ad;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Builder;

class AdPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user, Ad $ad)
    {
        return true;
    }

    public function create(User $user)
    {
        return true;
    }
    public function createGold(User $user)
    {
        $goldUser = $user->whereHas('memberships', function (Builder $query) {
            $query->whereName('Gold Tier');
        })->get();

        return $goldUser->count();
    }

    public function createPlatinum(User $user)
    {
        $platinumUser = $user->whereHas('memberships', function (Builder $query) {
            $query->whereName('Platinum Tier');
        })->get();

        return $platinumUser->count();
    }

    public function update(User $user, Ad $ad)
    {
        return true;
    }

    public function delete(User $user, Ad $ad)
    {
        return true;
    }

    public function restore(User $user, Ad $ad)
    {
        return true;
    }

    public function forceDelete(User $user, Ad $ad)
    {
        return true;
    }
}
