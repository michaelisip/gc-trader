<?php

namespace App\Policies;

use App\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Builder;

class ProductPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function viewAnyAuctions(User $user)
    {
        return $user->is_business_man;
    }

    public function viewAnyRetails(User $user)
    {
        return $user->is_business_man;
    }

    public function viewAnyReverseAuctions(?User $user)
    {
        return true;
    }

    public function viewAnyWholesales(User $user)
    {
        return true;
    }

    public function viewAuction(User $user, Product $product)
    {
        return $user->is_business_man;
    }

    public function viewRetail(User $user, Product $product)
    {
        return $user->is_business_man;
    }

    public function viewReverseAuction(?User $user, Product $product)
    {
        return true;
    }

    public function viewWholesale(User $user, Product $product)
    {
        return true;
    }

    public function view(User $user, Product $product)
    {
        return true;
    }

    public function create(User $user)
    {
        return true;
    }

    public function createAuction(User $user, $price)
    {
        if (!$price) {
            return true;
        }

        if (!$user->is_business_man) {
            return false;
        }

        $basicUser = $user->whereHas('memberships', function (Builder $query) {
            $query->whereName('Basic');
        })->get();

        $businessUser = $user->whereHas('memberships', function (Builder $query) {
            $query->whereName('Business');
        })->get();

        return $basicUser->count() || $businessUser->count();
    }

    public function createRetail(User $user, $price)
    {
        if (!$price) {
            return true;
        }

        if (!$user->is_business_man) {
            return false;
        }

        $basicUser = $user->whereHas('memberships', function (Builder $query) {
            $query->whereName('Basic');
        })->get();

        $businessUser = $user->whereHas('memberships', function (Builder $query) {
            $query->whereName('Business');
        })->get();

        return $basicUser->count() || $businessUser->count();
    }

    public function createReverseAuction(User $user, $price)
    {
        return true;
    }

    public function createWholesale(User $user, $price)
    {
        if (!$price) {
            return true;
        }

        if (!$user->is_business_man) {
            return false;
        }

        $wholesaleUser = $user->whereHas('memberships', function (Builder $query) {
            $query->whereName('Wholesale');
        })->get();

        return $wholesaleUser->count();
    }

    public function update(User $user, Product $product)
    {
        return true;
    }

    public function delete(User $user, Product $product)
    {
        return true;
    }

    public function restore(User $user, Product $product)
    {
        return true;
    }

    public function forceDelete(User $user, Product $product)
    {
        return true;
    }
}
