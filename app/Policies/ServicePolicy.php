<?php

namespace App\Policies;

use App\Service;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Database\Eloquent\Builder;

class ServicePolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user)
    {
        return true;
    }

    public function view(User $user, Service $service)
    {
        return true;
    }

    public function create(User $user)
    {
        if (!$user->is_business_man) {
            return false;
        }

        $serviceUser = $user->whereHas('memberships', function (Builder $query) {
            $query->whereName('Service');
        })->get();

        return $serviceUser->count();
    }

    public function update(User $user, Service $service)
    {
        return true;
    }

    public function delete(User $user, Service $service)
    {
        return true;
    }

    public function restore(User $user, Service $service)
    {
        return true;
    }

    public function forceDelete(User $user, Service $service)
    {
        return true;
    }
}
