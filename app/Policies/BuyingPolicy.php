<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BuyingPolicy
{
    use HandlesAuthorization;

    public function buy(User $user)
    {
        return $user->is_active;
    }

    public function bid(User $user)
    {
        return $user->is_active;
    }

    public function watch(User $user)
    {
        return $user->is_active;
    }
}
