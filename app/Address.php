<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function addressable()
    {
        return $this->morphTo();
    }

    public function getPartialAttribute()
    {
        $partialAddress = "";

        if (isset($this->suburb) && isset($this->state)) {
            $partialAddress = "{$this->suburb}, {$this->state}";
        } else if (isset($this->city) && isset($this->country)) {
            $partialAddress = "{$this->city}, {$this->country}";
        }
        
        return $partialAddress;
    }
}
