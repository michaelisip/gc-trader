<?php

namespace App;

use App\Scopes\StatusScope;
use App\Scopes\UserStatusScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Nicolaslopezj\Searchable\SearchableTrait;

class RealEstate extends Model
{
    use SearchableTrait, SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new StatusScope);
        static::addGlobalScope(new UserStatusScope);
    }

    protected $guarded = [
        'price',
        'status',
    ];

    protected $searchable = [
        'columns' => [
            'real_estates.name' => 10,
        ],
    ];

    public function address()
    {
        return $this->morphOne('App\Address', 'addressable');
    }

    public function bids()
    {
        return $this->morphMany('App\Bid', 'biddable');
    }

    public function categories()
    {
        return $this->morphToMany('App\Category', 'categorizable');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function users()
    {
        return $this->morphToMany('App\User', 'watchable');
    }

    public function scopeNotFromUser($query)
    {
        return $query->whereHas('user', function ($subQuery) {
            $subQuery->whereNotIn('id', [Auth::id()]);
        });
    }

    public function getFormattedPriceAttribute()
    {
        return number_format($this->price, 2, '.', ',');
    }
}
