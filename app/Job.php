<?php

namespace App;

use App\Scopes\StatusScope;
use App\Scopes\UserStatusScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Job extends Model
{
    use SearchableTrait, SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new StatusScope);
        static::addGlobalScope(new UserStatusScope);
    }

    protected $guarded = ['status'];

    protected $searchable = [
        'columns' => [
            'jobs.name' => 10,
        ],
    ];

    public function address()
    {
        return $this->morphOne('App\Address', 'addressable');
    }

    public function categories()
    {
        return $this->morphToMany('App\Category', 'categorizable');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function users()
    {
        return $this->morphToMany('App\User', 'watchable');
    }
}
