<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function communities()
    {
        return $this->morphedByMany('App\Community', 'categorizable');
    }

    public function jobs()
    {
        return $this->morphedByMany('App\Job', 'categorizable');
    }

    public function products()
    {
        return $this->morphedByMany('App\Product', 'categorizable');
    }

    public function realEstates()
    {
        return $this->morphedByMany('App\RealEstate', 'categorizable');
    }

    public function services()
    {
        return $this->morphedByMany('App\Service', 'categorizable');
    }

    public function travels()
    {
        return $this->morphedByMany('App\Travel', 'categorizable');
    }

    public function vehicles()
    {
        return $this->morphedByMany('App\Vehicle', 'categorizable');
    }

    public function scopeCommunity($query)
    {
        return $query->where('type', 'community');
    }

    public function scopeJob($query)
    {
        return $query->where('type', 'job');
    }

    public function scopeProduct($query)
    {
        return $query->where('type', 'product');
    }

    public function scopeRealEstate($query)
    {
        return $query->where('type', 'real_estate');
    }

    public function scopeService($query)
    {
        return $query->where('type', 'service');
    }

    public function scopeTravel($query)
    {
        return $query->where('type', 'travel');
    }

    public function scopeVehicle($query)
    {
        return $query->where('type', 'vehicle');
    }
}
