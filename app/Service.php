<?php

namespace App;

use App\Scopes\StatusScope;
use App\Scopes\UserStatusScope;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Nicolaslopezj\Searchable\SearchableTrait;

class Service extends Model
{
    use SearchableTrait, SoftDeletes;

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new StatusScope);
        static::addGlobalScope(new UserStatusScope);
    }

    protected $guarded = ['status'];

    protected $searchable = [
        'columns' => [
            'services.name' => 10,
        ],
    ];

    public function address()
    {
        return $this->morphOne('App\Address', 'addressable');
    }

    public function bids()
    {
        return $this->morphMany('App\Bid', 'biddable');
    }

    public function categories()
    {
        return $this->morphToMany('App\Category', 'categorizable');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

    public function feedbacks()
    {
        return $this->hasMany('App\Feedback');
    }

    public function images()
    {
        return $this->morphMany('App\Image', 'imageable');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function users()
    {
        return $this->morphToMany('App\User', 'watchable');
    }
}
