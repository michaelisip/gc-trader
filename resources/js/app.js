$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $(`meta[name='csrf-token']`).attr('content'),
    },
});

$(document).ready(function () {
    $('#goToTop').hide();
});

$(document).scroll(function () {
    if ($(this).scrollTop() > 800) {
        $('#goToTop').fadeIn();
    } else {
        $('#goToTop').fadeOut();
    }
});

$('.ui.checkbox').checkbox();
$('.ui.dropdown').dropdown();
$('.ui.accordion').accordion();
$('.popup').popup();
$('.special.cards .image').dimmer({
    on: 'hover'
});
$('.message .close').on('click', function () {
    $(this).closest('.message')
        .transition('fade');
});

$('#goToTop').on('click', () => {
    $('html, body').animate({ scrollTop: 0 }, 'slow');
});

$('.add-to-watchlist').on('click', function () {
    $('#addToWatchlistForm').attr('action', $(this).data('url'));
    $('#addToWatchlistForm').submit();
});

$('.remove-from-watchlist').on('click', function () {
  $('#removeFromWatchlistForm').attr('action', $(this).data('url'));
  $('#removeFromWatchlistForm').submit();
});

$('.add-to-cart').on('click', function () {
    $(`#addToCartForm input[name='id']`).val($(this).data('id'));
    $(`#addToCartForm input[name='type']`).val($(this).data('type'));
    $('#addToCartForm').submit();
});

$('.update-cart').on('click', function () {
    $('#updateCartForm').attr('action', $(this).data('url'));
    $(`#updateCartForm input[name='quantity']`).val($(this).data('quantity'));
    $('#updateCartForm').submit();
});

$('.remove-to-cart').on('click', function () {
    $('#removeToCartForm').attr('action', $(this).data('url'));
    $('#removeToCartForm').submit();
});

$('.wholesale-request').on('click', function () {
    $(`#wholesaleRequestForm input[name='product_id']`).val($(this).data('product-id'));
    $('#wholesaleRequestForm').submit();
});

$('.wholesale-request-approve').on('click', function () {
    $('#wholesaleRequestApproveForm').attr('action', $(this).data('url'));
    $('#wholesaleRequestApproveForm').submit();
});

$('.wholesale-request-deny').on('click', function () {
    $('#wholesaleRequestDenyForm').attr('action', $(this).data('url'));
    $('#wholesaleRequestDenyForm').submit();
});

$('.ui.search').search({
    apiSettings: {
        url: `${$('input[name="search_route"]').val()}?search={query}`,
    },
    type: 'category',
    minCharacters: 3,
    cache: true,
    selectFirstResult: true,
    showNoResults: true,
    selector: {
        prompt: '#searchBar',
    },
});