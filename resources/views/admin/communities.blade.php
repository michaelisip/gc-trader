@extends('layouts.admin')

@section('title')
    Communities
@endsection

@section('content')
    <div class="row">
        <div class="col">
            @unless ($communities->count())
                <div class="alert alert-default" role="alert">
                    There are no communities registered as of now.
                </div>
            @endunless
            @if ($communities->count())
                <div class="card shadow">
                    <div class="card-header border-0">
                        <h3 class="mb-0">Communities</h3>
                    </div>
                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">Name</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($communities as $community)
                                    <tr>
                                        <td>{{ $community->name }}</td>
                                        <td class="font-bold">
                                            {{ ucwords($community->status) }}
                                        </td>
                                        <td>
                                            <button
                                                class="btn btn-primary btn-sm view-community"
                                                type="button"
                                                data-toggle="modal"
                                                data-target="#communityInfo"
                                                data-id="{{ $community->id }}"
                                            >
                                                View
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer py-4">
                        {{ $communities->links('vendor.pagination.argon-dashboard') }}
                    </div>                    
                </div>
            @endif
        </div>
    </div>
    <div
        class="modal fade"
        id="communityInfo"
        tabindex="-1"
        role="dialog"
        aria-labelledby="communityInfoLabel"
        aria-hidden="true"
    >
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="communityInfoLabel"></h5>
                    <button
                        type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close"
                    >
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <dl class="row">
                        <dt class="col-sm-5 my-2">Name</dt>
                        <dd class="col-sm-7 my-2" id="communityName"></dd>
                        <dt class="col-sm-5 my-2">User</dt>
                        <dd class="col-sm-7 my-2" id="communityUser"></dd>
                        <dt class="col-sm-5 my-2">Email</dt>
                        <dd class="col-sm-7 my-2" id="communityEmail"></dd>
                        <dt class="col-sm-5 my-2">Company Number</dt>
                        <dd class="col-sm-7 my-2" id="communityCompanyNumber"></dd>
                        <dt class="col-sm-5 my-2">Registration</dt>
                        <dd class="col-sm-7 my-2" id="communityRegistration"></dd>
                        <dt class="col-sm-5 my-2">PO Box Address</dt>
                        <dd class="col-sm-7 my-2" id="communityPOBoxAddress"></dd>
                        <dt class="col-sm-5 my-2">Description</dt>
                        <dd class="col-sm-7 my-2" id="communityDescription"></dd>
                        <dt class="col-sm-5 my-2">Date Time</dt>
                        <dd class="col-sm-7 my-2" id="communityDateTime"></dd>
                        <dt class="col-sm-5 my-2">Start Time</dt>
                        <dd class="col-sm-7 my-2" id="communityStartTime"></dd>
                        <dt class="col-sm-5 my-2">Finish Time</dt>
                        <dd class="col-sm-7 my-2" id="communityFinishTime"></dd>
                        <dt class="col-sm-5 my-2">Parking/Disablity Dropbox</dt>
                        <dd class="col-sm-7 my-2" id="communityDropbox"></dd>
                        <dt class="col-sm-5 my-2">Pick Up Type</dt>
                        <dd class="col-sm-7 my-2" id="communityPickUpType"></dd>
                        <dt class="col-sm-5 my-2">Status</dt>
                        <dd class="col-sm-7 my-2" id="communityStatus"></dd>
                    </dl>
                </div>
                <div class="modal-footer">
                    <button
                        type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal"
                    >
                        Close
                    </button>
                    <button
                        id="communityAction"
                        type="button"
                        class="btn btn-primary"
                    ></button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(".view-community").on("click", function() {
            let url = '{{ route("admin.communities.show", ":id") }}'.replace(':id', $(this).data('id'));

            $.ajax({
                url: url,
                success: function (response) {
                    var community = response.community
                    $("#communityInfoLabel").text(community.name);
                    $("#communityUser").text(`${community.user.first_name} ${community.user.last_name}`);
                    $("#communityName").text(community.name);

                    $("#communityEmail").text(community.email);
                    $("#communityCompanyNumber").text(community.company_number || 'None');
                    $("#communityRegistration").text(community.registration || 'None');
                    $("#communityPOBoxAddress").text(community.po_box_address);
                    $("#communityDescription").text(community.description);

                    let date = new Date(community.date_time).toLocaleDateString();
                    let start = new Date(community.start_time).toLocaleTimeString();
                    let finish = new Date(community.finish_time).toLocaleTimeString();
                    $("#communityDateTime").text(community.date_time ? date : 'None');
                    $("#communityStartTime").text(community.start_time ? start : 'None');
                    $("#communityFinishTime").text(community.finish_time ? finish : 'None');

                    $("#communityDropbox").text(community.parking_or_disability_drop_box ? 'Yes' : 'No');
                    $("#communityPickUpType").text(community.pick_up_type);

                    let status = community.status
                    $("#communityStatus").text(status.charAt(0).toUpperCase() + status.slice(1));

                    $("#communityAction").text(
                        community.status === 'active' ? 'Set as "Inactive"' :  'Set as Active'
                    );

                    $("#communityAction").on("click", function() {
                        $(`#communityToggleForm input[name='status']`).val(
                            community.status === "active" ? "inactive" : "active"
                        );
                        $("#communityToggleForm").attr(
                            "action",
                            `/admin/communities/${community.id}/toggle`
                        );
                        $("#communityToggleForm").submit();
                    });

                }
            })


        });
    </script>
@endpush
