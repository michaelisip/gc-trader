<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Admin Login</title>
        <link
            rel="icon"
            href="{{ asset('favicon.ico') }}"
            type="image/x-icon"
        />
        <link rel="stylesheet" href="{{ asset('css/normalize.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/themify-icons.css') }}" />
        <link
            rel="stylesheet"
            href="{{ asset('css/argon-dashboard.min.css') }}"
        />
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    </head>
    <body>
        <div class="main-content">
            <nav
                class="navbar navbar-top navbar-horizontal navbar-expand-md navbar-dark"
            >
                <div class="container">
                    <button
                        class="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#navbar-collapse-main"
                        aria-controls="navbarSupportedContent"
                        aria-expanded="false"
                        aria-label="Toggle navigation"
                    >
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div
                        class="collapse navbar-collapse"
                        id="navbar-collapse-main"
                    >
                        <div class="navbar-collapse-header d-md-none">
                            <div class="row">
                                <div class="col-12 collapse-close">
                                    <button
                                        type="button"
                                        class="navbar-toggler"
                                        data-toggle="collapse"
                                        data-target="#navbar-collapse-main"
                                        aria-controls="sidenav-main"
                                        aria-expanded="false"
                                        aria-label="Toggle sidenav"
                                    >
                                        <span></span>
                                        <span></span>
                                    </button>
                                </div>
                            </div>
                        </div>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item">
                                <a
                                    class="nav-link nav-link-icon"
                                    href="{{ route('home') }}"
                                >
                                    <i class="ti ti-desktop"></i> Go To App
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="header bg-gradient-info py-7 py-lg-8">
                <div class="container">
                    <div class="header-body text-center mb-5">
                        <div class="row justify-content-center">
                            <div class="col-lg-5 col-md-6">
                                <h1 class="text-white text-xl font-black">
                                    GC Trader Admin Dashboard
                                </h1>
                                <p class="text-white">
                                    Please login using your admin account.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container mt--8">
                <div class="row justify-content-center">
                    <div class="col-lg-5 col-md-7">
                        <div class="card bg-secondary shadow border-0">
                            <div class="card-body px-lg-5 py-lg-5">
                                <form
                                    role="form"
                                    method="POST"
                                    action="{{ route('admin.login') }}"
                                >
                                    @csrf
                                    <div class="form-group">
                                        <div
                                            class="input-group input-group-alternative"
                                        >
                                            <input
                                                class="form-control"
                                                placeholder="Email"
                                                type="email"
                                                name="email"
                                                autocomplete="off"
                                                value="{{ old('email') }}"
                                                required
                                            />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div
                                            class="input-group input-group-alternative"
                                        >
                                            <input
                                                class="form-control"
                                                placeholder="Password"
                                                type="password"
                                                name="password"
                                                autocomplete="off"
                                                required
                                            />
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button
                                            type="submit"
                                            class="btn btn-primary"
                                        >
                                            Login
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @include('sweetalert::alert')
        <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"
        ></script>
        <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"
        ></script>
        <script src="{{ asset('js/argon-dashboard.min.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
    </body>
</html>
