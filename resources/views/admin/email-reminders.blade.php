@extends('layouts.admin')

@section('title')
    Email Reminder
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div class="card shadow">
                <div class="card-header border-0">
                    <h3 class="mb-0">Email Reminder</h3>
                </div>
                <div class="card body p-4">
                    <form
                        method="POST"
                        action="{{ route('admin.email-reminders.send') }}"
                    >
                        @csrf
                        <div class="form-group">
                            <label for="subject">Subject</label>
                            <input
                                id="subject"
                                name="subject"
                                type="text"
                                class="form-control"
                                placeholder="Subject"
                                value="{{ old('subject') }}"
                                required
                                autofocus
                            />
                        </div>
                        <div class="form-group">
                            <label for="body">Message</label>
                            <textarea
                                id="body"
                                name="body"
                                class="form-control"
                                rows="5"
                                required
                            >{{ old('body') }}</textarea>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block">
                            Send Reminder
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
