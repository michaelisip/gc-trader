@extends('layouts.app')

@section('title')
    Home
@endsection

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.default.min.css') }}">
@endpush

@section('content')
    @if ($carouselAds->count())
        <div class="my-5 mx-3 lg:mx-20 flex flex-col-reverse lg:flex-row">
            <div class="w-full lg:w-2/6 my-auto mr-5">
                <h3 class="ui top attached header section-header">Categories</h3>
                <div class="ui attached segment">
                    <div class="ui link list overflow-auto h-64">
                        <a
                            href="{{ route('search', ['filter' => 'All links for Categories']) }}"
                            class="item px-2 hover:bg-gray-200"
                            ><span class="text-black">All links for Categories</span></a
                        >
                        @foreach ($productCategories as $productCategory)
                            <a
                                href="{{ route('search', ['filter' => $productCategory->name]) }}"
                                class="item px-2 hover:bg-gray-200"
                                ><span class="text-black">{{ $productCategory->name }}</span></a
                            >
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="w-full lg:w-4/6 mb-5 lg:mb-auto lg:mt-auto">
                <div id="adCarousel" class="owl-carousel owl-theme">
                    @foreach ($carouselAds->shuffle() as $ad)
                        @if ($loop->iteration === 5) @break @endif
                        <div class="item">
                            <img src="{{ asset($ad->image->src) }}" />
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @auth
        @if (auth()->user()->is_business_man)
            {{-- Retails --}}
            <div class="my-5 mx-3 lg:mx-20">
                <a href="{{ route('retails') }}">
                    <h3 class="ui top attached header section-header">Retail</h3>
                    <div class="ui attached segment">
                        @forelse ($retails->chunk(4) as $chunk)
                            <div class="md:flex md:flex-wrap">
                                @foreach ($chunk as $retail)
                                    <div class="px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                                        <a href="{{ route('products.show', $retail->id) }}">
                                            <div
                                                style="height: 350px" 
                                                class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                                            >
                                                <div style="height: 200px">
                                                    @if ($retail->images->count())
                                                        <div
                                                            class="rounded-t-lg relative w-full h-full overflow-hidden"
                                                        >
                                                            <img
                                                                class="z-10 relative w-full object-cover"
                                                                src="{{ asset($retail->images->random()->src) }}"
                                                            />
                                                        </div>
                                                    @else
                                                        <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                            <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="flex flex-col mx-5">
                                                    <span class="text-gray-900 my-2">
                                                        @isset($retail->address)
                                                            {{ Str::limit($retail->address->partial, 25) }}
                                                        @endisset
                                                    </span>
                                                    <span class="item text-blue-700 font-bold">
                                                        {{ Str::limit($retail->name, 50) }}
                                                    </span>
                                                </div>
                                                <div class="m-5 h-full font-bold flex flex-col-reverse">
                                                    <span class="text-xl">${{ $retail->formatted_price }}</span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        @empty
                            <div class="ui disabled header centered">
                                No Retail Listing Yet.
                            </div>
                        @endforelse
                    </div>
                </a>
                @if ($retails->count())
                    <a href="{{ route('retails') }}">
                        <h4 class="ui center aligned bottom attached header">
                            View More
                        </h4>
                    </a>
                @endif
            </div>
            {{-- Auctions --}}
            <div class="my-5 mx-3 lg:mx-20">
                <a href="{{ route('auctions') }}">
                    <h3 class="ui top attached header section-header">Auction</h3>
                    <div class="ui attached segment">
                        @forelse ($auctions->chunk(4) as $chunk)
                            <div class="md:flex md:flex-wrap">
                                @foreach ($chunk as $auction)
                                    <div class="px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                                        <a href="{{ route('products.show', $auction->id) }}">
                                            <div
                                                style="height: 350px" 
                                                class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                                            >
                                                <div style="height: 200px">
                                                    @if ($auction->images->count())
                                                        <div
                                                            class="rounded-t-lg relative w-full h-full overflow-hidden bg-cover"
                                                        >
                                                            <img
                                                                class="z-10 relative w-full object-cover bg-contain"
                                                                src="{{ asset($auction->images->random()->src) }}"
                                                            />
                                                        </div>
                                                    @else
                                                        <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                            <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="flex flex-col mx-5">
                                                    <span class="text-gray-900 my-2">
                                                        @isset($auction->address)
                                                            {{ Str::limit($auction->address->partial, 25) }}
                                                        @endisset
                                                    </span>
                                                    <span
                                                        class="item text-blue-700 font-bold"
                                                        >{{ Str::limit($auction->name, 50) }}</span
                                                    >
                                                </div>
                                                <div class="m-5 h-full font-bold flex flex-col-reverse">
                                                    <span class="text-xl">
                                                        <small class="font-thin italic"> 
                                                            {{ $auction->bids->count() ? 'Current Bid' : 'Starting Bid' }}:&nbsp;
                                                        </small>
                                                        ${{ $auction->formatted_starting_bid }}
                                                    </span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        @empty
                            <div class="ui disabled header centered">
                                No Auction Listing Yet.
                            </div>
                        @endforelse
                    </div>
                </a>
                @if ($auctions->count())
                    <a href="{{ route('auctions') }}">
                        <h4 class="ui center aligned bottom attached header">
                            View More
                        </h4>
                    </a>
                @endif
            </div>
            {{-- Wholesales --}}
            <div class="my-5 mx-3 lg:mx-20">
                <a href="{{ route('wholesales') }}">
                    <h3 class="ui top attached header section-header">Wholesale</h3>
                    <div class="ui attached segment">
                        @forelse ($wholesales->chunk(4) as $chunk)
                            <div class="md:flex md:flex-wrap">
                                @foreach ($chunk as $wholesale)
                                    <div class="px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                                        <a href="{{ route('products.show', $wholesale->id) }}">
                                            <div
                                                style="height: 350px" 
                                                class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                                            >
                                                <div style="height: 200px">
                                                    @if ($wholesale->images->count())
                                                        <div
                                                            class="rounded-t-lg relative w-full h-full overflow-hidden"
                                                        >
                                                            <img
                                                                class="z-10 relative w-full object-cover"
                                                                src="{{ asset($wholesale->images->random()->src) }}"
                                                            />
                                                        </div>
                                                    @else
                                                        <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                            <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                                        </div>
                                                    @endif
                                                </div>
                                                <div class="flex flex-col mx-5">
                                                    <span class="text-gray-900 my-2">
                                                        @isset($wholesale->address)
                                                            {{ Str::limit($wholesale->address->partial, 25) }}
                                                        @endisset
                                                    </span>
                                                    <span
                                                        class="item text-blue-700 font-bold"
                                                        >{{ Str::limit($wholesale->name, 50) }}</span
                                                    >
                                                </div>
                                                <div class="m-5 mt-8 h-full font-bold flex flex-col">
                                                    <span class="text-xl">
                                                        <small class="font-thin italic"> RRP: &nbsp; </small>
                                                        ${{ $wholesale->formatted_price }}
                                                    </span>
                                                    @auth
                                                        @if (auth()->user()->memberships->where('name', 'Wholesale')->count())
                                                            <span class="text-base">
                                                                <small class="font-thin italic"> Wholesale Price: &nbsp; </small>
                                                                ${{ $wholesale->formatted_wholesale_price }}
                                                            </span>
                                                        @endif
                                                    @endauth
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        @empty
                            <div class="ui disabled header centered">
                                No Wholesale Listing Yet.
                            </div>
                        @endforelse
                    </div>
                </a>
                @if ($wholesales->count())
                    <a href="{{ route('wholesales') }}">
                        <h4 class="ui center aligned bottom attached header">
                            View More
                        </h4>
                    </a>
                @endif
            </div>
        @endif
    @endauth
    <div class="my-5 mx-3 lg:mx-20 @auth @if (auth()->user()->is_business_man) mb-20 @endif @endauth">
        <a href="{{ route('reverse-auctions') }}">
            <h3 class="ui top attached header section-header">Reverse Auction</h3>
            <div class="ui attached segment">
                @forelse ($reverseAuctions->chunk(4) as $chunk)
                    <div class="md:flex md:flex-wrap">
                        @foreach ($chunk as $reverseAuction)
                            <div class="mb-3 px-5 w-full md:w-1/2 lg:w-1/4">
                                <a href="{{ route('products.show', $reverseAuction->id) }}">
                                    <div
                                        style="height: 350px" 
                                        class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                                    >
                                        <div style="height: 200px">
                                            @if ($reverseAuction->images->count())
                                                <div
                                                    class="rounded-t-lg relative w-full h-full overflow-hidden"
                                                >
                                                    <img
                                                        class="z-10 relative w-full h-full object-cover"
                                                        src="{{ asset($reverseAuction->images->random()->src) }}"
                                                    />
                                                </div>
                                            @else
                                                <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                    <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                                </div>
                                            @endif
                                        </div>
                                        <div class="flex flex-col mx-5">
                                            <span class="text-gray-900 my-2">
                                                @isset($reverseAuction->address)
                                                    {{ Str::limit($reverseAuction->address->partial, 25) }}
                                                @endisset
                                            </span>
                                            <span class="item text-blue-700 font-bold">
                                                {{ Str::limit($reverseAuction->name, 50) }}
                                            </span>
                                        </div>
                                        <div class="m-5 h-full font-bold flex flex-col-reverse">
                                            <span class="text-xl">
                                                <small class="font-thin italic">
                                                    {{ $reverseAuction->bids->count() ? 'Current Bid' : 'Starting Bid' }}:&nbsp;
                                                </small>
                                                ${{ $reverseAuction->formatted_starting_bid }}
                                            </span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @empty
                    <div class="ui disabled header centered">
                        No Reverse Auction Listing Yet.
                    </div>
                @endforelse
            </div>
        </a>
        @if ($reverseAuctions->count())
            <a href="{{ route('reverse-auctions') }}">
                <h4 class="ui center aligned bottom attached header">
                    View More
                </h4>
            </a>
        @endif
    </div>
    @auth
        @if (!auth()->user()->is_business_man)
            <div class="my-5 mx-3 lg:mx-20 mb-20">
                <div class="ui warning message" >
                    <div class="content">
                        <div class="header">
                            Business Account
                        </div>
                        <p> You can view more products by converting your account into a Business type. </p>
                    </div>
                </div>
            </div>
        @endif
    @endauth
@endsection

@push('scripts')
    <script src="{{ asset('js/owl.carousel.min.js') }}"></script>
    <script>
        $("#adCarousel").owlCarousel({
            items: 1,
            center: true,
            loop: true,
            autoplay: true,
            autoplayTimeout: 5000,
            autoplayHoverPause: true,
            dots: false,
        });
    </script>
@endpush
