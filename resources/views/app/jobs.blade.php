@extends('layouts.app')

@section('title')
    Jobs
@endsection

@section('content')
    <div class="my-5 mt-10 mx-3 lg:mx-20 md:flex items-center justify-center">
        <div class="ui raised segments w-full lg:w-3/4">
            <div class="ui padded segment blue">
                <form
                    class="ui form"
                    action="{{ route('search.job') }}"
                    method="GET"
                >
                    @csrf
                    <div class="four fields">
                        <div class="field">
                            <input
                                type="text"
                                name="keywords"
                                placeholder="Keywords"
                                value="{{ request('keywords') }}"
                            />
                        </div>
                        <div class="field">
                            <div class="ui fluid search selection dropdown">
                                <input type="hidden" name="category" />
                                <i class="dropdown icon"></i>
                                <div class="default text">Category</div>
                                <div class="menu">
                                    @foreach ($jobCategories as $jobCategory)
                                        <div
                                            class="item"
                                            data-value="{{ $jobCategory->name }}"
                                        >
                                            {{ $jobCategory->name }}
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui selection dropdown">
                                <input
                                    type="hidden"
                                    name="type"
                                    value="{{ request('type') }}"
                                />
                                <i class="dropdown icon"></i>
                                <div class="default text">Type</div>
                                <div class="menu">
                                    <div class="item" data-value="">
                                        All Types
                                    </div>
                                    <div class="item" data-value="Full Time">
                                        Full Time
                                    </div>
                                    <div class="item" data-value="Part Time">
                                        Part Time
                                    </div>
                                    <div class="item" data-value="Contract">
                                        Contract/Temp
                                    </div>
                                    <div class="item" data-value="Casual">
                                        Casual/Vacaiont
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <button class="ui button fluid blue">Search</button>
                        </div>
                    </div>
                    <div class="ui fluid accordion">
                        <div class="title">
                            <i class="dropdown icon"></i>
                            Advanced Search
                        </div>
                        <div class="content">
                            <div class="four fields">
                                <div class="field">
                                    <div class="ui fluid selection dropdown">
                                        <input
                                            type="hidden"
                                            name="country"
                                            value="{{ request('country') }}"
                                        />
                                        <i class="dropdown icon"></i>
                                        <div class="default text">Country</div>
                                        <div class="menu">
                                            <div class="item" data-value="">
                                                Anywhere
                                            </div>
                                            <div
                                                class="item"
                                                data-value="New Zealand"
                                            >
                                                New Zealand
                                            </div>
                                            <div
                                                class="item"
                                                data-value="Australia"
                                            >
                                                Australia
                                            </div>
                                            <div
                                                class="item"
                                                data-value="International"
                                            >
                                                International
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="field">
                                    <input
                                        type="text"
                                        name="state"
                                        placeholder="State"
                                        value="{{ request('state') }}"
                                    />
                                </div>
                                <div class="field">
                                    <input
                                        type="text"
                                        name="suburb"
                                        placeholder="Suburb"
                                        value="{{ request('suburb') }}"
                                    />
                                </div>
                                <div class="field">
                                    <input
                                        type="text"
                                        name="city"
                                        placeholder="City"
                                        value="{{ request('city') }}"
                                    />
                                </div>
                            </div>
                            <div class="three fields">
                                <div class="field">
                                    <input
                                        type="number"
                                        name="post_code"
                                        placeholder="Post Code"
                                        onkeypress="return this.value.length < 4;"
                                        value="{{ request('post_code') }}"
                                    />
                                </div>
                                <div class="field">
                                    <input
                                        type="text"
                                        name="street_name"
                                        placeholder="Street Name"
                                        value="{{ request('street_name') }}"
                                    />
                                </div>
                                <div class="field">
                                    <input
                                        type="number"
                                        name="number"
                                        placeholder="Number"
                                        value="{{ request('number') }}"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="mb-20">
        <div class="my-5 mx-3 lg:mx-20 md:flex flex-col items-center justify-center">
            @forelse ($jobs as $job)
                <div class="w-full lg:w-1/2 mb-5">
                    <a href="{{ route('jobs.show', $job->id) }}">
                        <div
                            class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl p-5"
                        >
                            <div class="mb-5">
                                <span
                                    class="item text-2xl text-blue-700 font-bold"
                                    >{{ $job->name }}</span
                                >
                                <br />
                                <span class="text-sm text-gray-900 my-2">{{ $job->company_name }}</span>
                            </div>
                            <div class="mb-5">
                                <p class="text-black text-lg">
                                    {{ Str::limit($job->description, 300) }}
                                </p>
                            </div>
                            <p class="text-black text-lg">
                                <span class="font-bold text-blue-700"> Type: </span> {{ $job->type }} 
                            </p>
                            @isset($job->address)
                                <p class="text-black text-lg">
                                    <span class="font-bold text-blue-700"> Address: </span>
                                    {{ Str::limit($job->address->partial, 25) }}
                                </p>
                            @endisset
                        </div>
                    </a>
                </div>
            @empty
                <div class="ui disabled header centered">
                    No Job Listing Yet.
                </div>
            @endforelse
        </div>
        @if ($jobs->count())
            <div class="my-5 mx-3 lg:mx-20 flex items-center justify-center">
                {{ $jobs->appends([ 'keywords' => request('keywords'), 'category' =>
                request('category'), 'type' => request('type'), 'country' =>
                request('country'), 'state' => request('state'), 'suburb' =>
                request('suburb'), 'city' => request('city'), 'post_code' =>
                request('post_code'), ])->links('vendor.pagination.semantic-ui') }}
            </div>
        @endif    
    </div>
@endsection
