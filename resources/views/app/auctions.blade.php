@extends('layouts.app')

@section('title')
    Auctions
@endsection

@section('content')
    <div class="mt-10 mb-20">
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            @forelse ($auctions->chunk(4) as $chunk)
                <div class="md:flex md:flex-wrap mb-5 -mx-3">
                    @foreach ($chunk as $auction)
                        <div class="px-3 lg:px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                            <a href="{{ route('products.show', $auction->id) }}">
                                <div
                                    style="height: 350px" 
                                    class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                                >
                                    <div style="height: 200px">
                                        @if ($auction->images->count())
                                            <div
                                                class="rounded-t-lg relative w-full h-full overflow-hidden"
                                            >
                                                <img
                                                    class="z-10 relative w-full h-full object-cover"
                                                    src="{{ asset($auction->images->random()->src) }}"
                                                />
                                            </div>
                                        @else
                                            <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="flex flex-col mx-5">
                                        <span class="text-gray-900 my-2">
                                            @isset($auction->address)
                                                {{ Str::limit($auction->address->partial, 25) }}                                                
                                            @endisset
                                        </span>
                                        <span
                                            class="item text-blue-700 font-bold"
                                            >{{ Str::limit($auction->name, 50) }}</span
                                        >
                                    </div>
                                    <div class="m-5 h-full font-bold flex flex-col-reverse">
                                        <span class="text-xl">
                                            <small class="font-thin italic"> 
                                                {{ $auction->bids->count() ? 'Current Bid' : 'Starting Bid' }}:&nbsp;
                                            </small>
                                            ${{ $auction->formatted_starting_bid }}
                                        </span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @empty
                <div class="ui disabled header centered">
                    No Auction Listing Yet.
                </div>
            @endforelse
        </div>
        @if ($auctions->count())
            <div class="my-5 mx-3 lg:mx-20 flex items-center justify-center">
                {{ $auctions->links('vendor.pagination.semantic-ui') }}
            </div>
        @endif
    </div>
@endsection
