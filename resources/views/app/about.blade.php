@extends('layouts.app')

@section('title')
    About
@endsection

@section('content')
    <div class="my-10 mb-20 mx-10 lg:mx-20 flex justify-center">
        <div class="w-full lg:w-2/3">
            <h1 class="text-3xl text-blue-700 font-bold mb-5">
                Welcome to GC Trader your online Business to Business trading
                platform.
            </h1>
            <p class="my-5 text-justify">
                GC TRADER connects people and businesses, and provides them with
                the information and tools they need to buy or sell and make
                quick and safe transactions, through the Retail, Auction and
                Wholesale platforms provided.
            </p>
            <p class="my-5 text-justify">
                GC Trader community provides services that are available in your
                local suburbs throughout Australia and New Zealand Whether you
                are looking for your closest church or finding a location for
                food, clothing, support groups and so much you will be able to
                find it at the GC Trader Community link.
            </p>
            <p class="my-5 text-justify">
                We have web businesses specialising in accommodation and online
                dating and we also sell advertising across our portfolio of GC
                Trader which is very simple to do. With everything at your
                fingertips why look anywhere else.
            </p>
            <p class="my-5 text-justify">
                The website GC TRADER operates in Varsity Lakes, Queensland
                Australia.Whether you're starting a brand new business or you're
                an established retailer, taking your business online is a
                fantastic and exciting growth opportunity. In fact, there has
                never been a better time to join an ecommerce business. Gc
                trader has connected the world and tools to make it possible for
                anyone to get started, regardless of your technical skill.
            </p>
            <p class="my-5 text-justify">
                However, with all of these advantages come some complex legal
                issues. Different countries have different laws and knowing
                which ones apply to you is critical. In addition to the large
                variety of laws to take into account, you must remember that the
                law is fluid and subject to change. In order to reap the rewards
                that a successful online store can bring, you must stay informed
                and protect yourself and your business.
            </p>
            <p class="my-5 text-justify">
                And that's exactly why we created this guide.
            </p>
            <p class="my-5 text-justify">
                We want to make you aware of all the important implications that
                come with conducting business online so you can prepare yourself
                and carry out all the necessary steps to ensure everything
                you're doing meets the legal requirements for an online business
                operating in Australia.
            </p>
            <p class="my-5 text-justify">
                Having said that, this guide is not meant to be taken as
                official legal advice. Rather, it's simply meant to point you in
                the right direction and it's always best to seek professional
                legal counsel if you have questions about your specific
                situation. We would like to welcome you to GC trader , and enjoy
                your shopping buying or selling experience through you very own
                virtual online stores.
            </p>
        </div>
    </div>
@endsection
