@extends('layouts.app') 

@section('title')
    Account
@endsection

@section('content')
    @include('partials.app-account-nav')
    @unless ($user->address)
        <div class="my-5 mx-3 lg:mx-20">
            <div class="ui warning message w-full">
                <div class="header">
                    Notice
                </div>
                <p>
                    You must have an address. You might be forbidden to do some action
                    if you have an empty address.
                </p>
            </div>
        </div>
    @endunless
    @if($errors->any())
        <div class="my-5 mx-3 lg:mx-20">
            <div class="ui error message w-full">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 lg:my-10 mb-20 lg:mb-40 mx-3 lg:mx-20 md:flex justify-center">
        <div class="w-full lg:w-1/3 mr-5 mb-3">
            <div class="ui raised segments">
                <div class="ui padded segment blue">
                    <div id="imageFormErrorContainer" class="ui warning message mb-5 hidden">
                        <p>Something went wrong.</p>
                    </div>
                    <form
                        id="imageForm"
                        class="ui form error flex justify-center"
                        enctype="multipart/form-data"
                    >
                        <div class="ui special cards">
                            <div class="card">
                                <div class="blurring dimmable image">
                                    <div class="ui dimmer">
                                        <div class="content">
                                            <div class="center">
                                                <div
                                                    id="chooseImageButton"
                                                    class="ui inverted button"
                                                >
                                                    Drag or Choose Photo
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <img
                                        src="{{ $user->image->src ?? asset($user->profile_placeholder) }}"
                                    />
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="flex justify-center">
                        <div class="dropzone-previews"></div>
                    </div>
                </div>
                <p class="text-center pb-5">
                    <i>Click Save Primary Information button to update.</i>
                </p>
            </div>
        </div>
        <div class="w-full lg:w-2/3 ml-0 lg:ml-5">
            <div class="ui raised segments">
                <div class="ui padded segment blue">
                    <div
                        class="ui icon @if($user->is_active) success @else warning @endif message"
                    >
                        <i
                            class="@if ($user->is_active) shield alternate  @else exclamation  @endif icon"
                        ></i>
                        <div class="content">
                            <div class="header">
                                Account Status: {{ ucwords($user->status) }}
                            </div>
                            @if ($user->is_active)
                                <p>
                                    Your account has been verified and eligable to take
                                    further actions.
                                </p>
                            @else
                                <p>
                                    Your account has not been verified yet, plese verify
                                    your account using email, mobile number or credit
                                    card to take further actions.
                                </p>
                            @endif
                        </div>
                    </div>
                    @unless ($user->is_business_man)
                        <div
                            class="ui icon warning  message"
                        >
                            <i
                                class="exclamation icon"
                            ></i>
                            <div class="content">
                                <div class="header">
                                    Account Type: {{ ucwords($user->type) }}
                                </div>
                                <p>
                                    @if ($user->abn_registration)
                                        Convert your account to a Business type to enjoy more features.
                                    @else
                                        You need an ABN to be able to convert your account to a Business type.
                                    @endif
                                </p>
                            </div>
                        </div>
                    @endunless
                    <div class="my-5">
                        @unless ($user->is_active)
                            <button
                                class="ui mini button primary show-paymemt-options"
                                data-item-id=""
                                data-item-type="activation"
                                data-payment-type="single"
                            >
                                <i class="check circle icon"></i> Activate Account
                            </button>
                        @endunless
                        @if ($user->abn_registration && $user->is_active && !$user->is_business_man)
                            <a href="{{ $stripeOAuthLink }}" class="ui mini button primary"
                                ><i class="industry icon"></i> Convert To Business</a
                            >
                        @endif
                    </div>
                    <h4 class="ui block header blue">Primary Information</h4>
                    <form
                        method="POST"
                        class="ui form error"
                        action="{{ route('users.update', Auth::id()) }}"
                    >
                        @csrf @method('PUT')
                        <div id="photoContainer" class="hidden"></div>
                        <div class="field">
                            <label> Name</label>
                            <div class="two fields">
                                <div
                                    class="field @error('first_name') error @enderror"
                                >
                                    <input
                                        type="text"
                                        name="first_name"
                                        placeholder="First Name"
                                        value="{{ $user->first_name ?? old('first_name') }}"
                                    />
                                </div>
                                <div
                                    class="field @error('last_name') error @enderror"
                                >
                                    <input
                                        type="text"
                                        name="last_name"
                                        placeholder="Last Name"
                                        value="{{ $user->last_name ?? old('last_name') }}"
                                    />
                                </div>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field @error('gender') error @enderror">
                                <label>Gender</label>
                                <div class="ui selection dropdown">
                                    <input
                                        type="hidden"
                                        name="gender"
                                        value="{{ $user->gender ?? old('gender') }}"
                                    />
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Gender</div>
                                    <div class="menu">
                                        <div class="item" data-value="Male">
                                            Male
                                        </div>
                                        <div class="item" data-value="Female">
                                            Female
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field @error('birth_date') error @enderror">
                                <label
                                    >Birth Date ({{ isset($user->birth_date) ?
                                    $user->birth_date->format('M d, Y') : 'Not Indicated' }})</label
                                >
                                <input
                                    type="date"
                                    name="birth_date"
                                    value="{{ isset($user->birth_date) ? $user->birth_date->format('Y-m-d') : null }}"
                                />
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field @error('abn_registration') error @enderror">
                                <label>ABN Registration</label>
                                <input
                                    type="text"
                                    name="abn_registration"
                                    placeholder="ABN Registration"
                                    onkeypress="return this.value.length < 11;"
                                    value="{{ $user->abn_registration ?? old('abn_registration') }}"
                                />
                            </div>
                            <div class="field @error('phone_number') error @enderror">
                                <label>Phone Number</label>
                                <input
                                    type="text"
                                    name="phone_number"
                                    placeholder="Phone Number"
                                    value="{{ $user->phone_number ?? old('phone_number') }}"
                                />
                            </div>
                        </div>
                        <button
                            type="submit"
                            class="ui button primary"
                            tabindex="0"
                        >
                            Save Primary Information
                        </button>
                    </form>
                    <h4 class="ui block header blue">Address Details</h4>
                    <form
                        method="POST"
                        class="ui form error"
                        @if ($user->address)
                            action="{{ route('addresses.update', ['address' => $user->address->id]) }}"
                        @else
                            action="{{ route('users.addresses.store', Auth::id()) }}"
                        @endif
                    >
                        @csrf
                        @if ($user->address) @method('PUT') @endif
                        <div class="two fields">
                            <div class="field @error('country') error @enderror">
                                <label>Country</label>
                                <div class="ui selection dropdown">
                                    <input
                                        type="hidden"
                                        name="country"
                                        value="{{ $user->address->country ?? old('country') }}"
                                    />
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Country</div>
                                    <div class="menu">
                                        <div
                                            class="item"
                                            data-value="International"
                                        >
                                            International
                                        </div>
                                        <div class="item" data-value="Australia">
                                            <i class="au flag"></i> Australia
                                        </div>
                                        <div class="item" data-value="New Zealand">
                                            <i class="nz flag"></i> New Zealand
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field @error('state') error @enderror">
                                <label>State</label>
                                <input
                                    type="text"
                                    name="state"
                                    placeholder="State"
                                    value="{{ $user->address->state ?? old('state') }}"
                                />
                            </div>
                        </div>
                        <div class="three fields">
                            <div class="field @error('suburb') error @enderror">
                                <label>Suburb</label>
                                <input
                                    type="text"
                                    name="suburb"
                                    placeholder="Suburb"
                                    value="{{ $user->address->suburb ?? old('suburb') }}"
                                />
                            </div>
                            <div class="field @error('city') error @enderror">
                                <label>City</label>
                                <input
                                    type="text"
                                    name="city"
                                    placeholder="City"
                                    value="{{ $user->address->city ?? old('city') }}"
                                />
                            </div>
                            <div class="field @error('post_code') error @enderror">
                                <label>Post Code</label>
                                <input
                                    type="number"
                                    name="post_code"
                                    placeholder="Post Code"
                                    onkeypress="return this.value.length < 4;"
                                    value="{{ $user->address->post_code ?? old('post_code') }}"
                                />
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field @error('street_name') error @enderror">
                                <label>Street Name</label>
                                <input
                                    type="text"
                                    name="street_name"
                                    placeholder="Street Name"
                                    value="{{ $user->address->street_name ?? old('street_name') }}"
                                />
                            </div>
                            <div class="field @error('number') error @enderror">
                                <label>Number</label>
                                <input
                                    type="number"
                                    name="number"
                                    placeholder="Number"
                                    value="{{ $user->address->number ?? old('number') }}"
                                />
                            </div>
                        </div>
                        <button
                            type="submit"
                            class="ui button primary"
                            tabindex="0"
                        >
                            Save Changes
                        </button>
                    </form>
                    <h4 class="ui block header blue">Active Memberships</h4>
                    <div class="ui stackable two column grid">
                        @forelse ($user->memberships as $membership)
                            <div class="column">
                                <div class="ui raised fluid card h-full">
                                    <div class="content">
                                        <div class="header">{{ $membership->name }}</div>
                                        <div class="meta">{{ $membership->span }}</div>
                                        <div class="description">
                                            <p>{{ $membership->description }}</p>
                                        </div>
                                        <div class="ui small feed">
                                            <div class="event">
                                                <div class="content">
                                                    <div class="summary">
                                                        @if ($membership->sales_count_limit)
                                                            <span class="text-blue-700"
                                                                >Sales Count Limit:</span
                                                            >
                                                            {{ $membership->sales_count_limit }}
                                                            <br />
                                                        @endif
                                                        @if ($membership->sales_cost_limit)
                                                            <span class="text-blue-700"
                                                                >Sales Cost Limit:</span
                                                            >
                                                            {{ $membership->sales_cost_limit }}
                                                            <br />
                                                        @endif
                                                        @if ($membership->sales_percentage)
                                                            <span class="text-blue-700"
                                                                >Sales Percentage:</span
                                                            >
                                                            {{ $membership->sales_percentage }}%
                                                            <br />
                                                        @endif
                                                        <span class="text-blue-700"
                                                            >Purchased Date:</span
                                                        >
                                                        {{ $membership->membership->created_at ? $membership->membership->created_at->diffForHumans() : 'Not Indicated' }}
                                                        <br />
                                                        <span class="text-blue-700"
                                                            >Expiration Date:</span
                                                        >
                                                        @if ($membership->span === 'Annually')
                                                            {{ $membership->membership->created_at ? $membership->membership->created_at->addYear()->diffForHumans() : 'Not Indicated' }}
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @empty
                            <span class="mt-5">No Active Memberships Yet.</span>
                        @endforelse
                    </div>
                    <h4 class="ui block header blue">Available Memberships</h4>
                    <div class="ui stackable two column grid">
                        @forelse ($memberships->except($user->memberships->modelkeys()) as $membership)
                            @unless ($membership->is_disabled)
                                <div class="column">
                                    <div
                                        class="ui raised link fluid card h-full show-paymemt-options"
                                        data-item-id="{{ $membership->id }}"
                                        data-item-type="membership"
                                        data-payment-type="single"
                                    >
                                        <div class="content">
                                            <div class="header">{{ $membership->name }}</div>
                                            <div class="meta">{{ $membership->span }}</div>
                                            <div class="description">
                                                <p>{{ $membership->description }}</p>
                                            </div>
                                            <div class="ui small feed">
                                                <div class="event">
                                                    <div class="content">
                                                        <div class="summary">
                                                            @if ($membership->sales_count_limit)
                                                                <span class="text-blue-700"
                                                                    >Sales Count Limit:</span
                                                                >
                                                                {{ $membership->sales_count_limit }}
                                                                <br />
                                                            @endif
                                                            @if ($membership->sales_cost_limit)
                                                                <span class="text-blue-700"
                                                                    >Sales Cost Limit:</span
                                                                >
                                                                {{ $membership->sales_cost_limit }}
                                                                <br />
                                                            @endif
                                                            @if ($membership->sales_percentage)
                                                                <span class="text-blue-700"
                                                                    >Sales Percentage:</span
                                                                >
                                                                {{ $membership->sales_percentage }}%
                                                                <br />
                                                            @endif
                                                            <div
                                                                class="ui top right attached red label"
                                                            >
                                                                ${{ $membership->formatted_price }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endunless
                        @empty
                            <span class="mt-5">No Available Memberships Yet.</span>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>    
    @include('partials.app-dropzone-preview')
    @include('partials.modals.payment-gateways')
@endsection

@push('scripts')
    <script src="https://js.stripe.com/v3/"></script>
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script>
        $("#imageForm").dropzone({
            url: "{{ route('images.store') }}",
            headers: {
                "X-CSRF-TOKEN": $(`meta[name='csrf-token']`).attr("content")
            },
            paramName: "photo",
            clickable: ["#chooseImageButton"],
            maxFiles: 1,
            acceptedFiles: "image/*",
            previewsContainer: ".dropzone-previews",
            previewTemplate: document.querySelector("#tpl").innerHTML,
            success: function(file) {
                let serverResponse = JSON.parse(file.xhr.response);

                if (!file.status === "success") {
                    $("#imageFormErrorContainer").removeClass("hidden");
                    return false;
                }

                if (!serverResponse.success) {
                    $("#imageFormErrorContainer p").text(
                        "Something went wrong. Please contact support."
                    );
                    $("#imageFormErrorContainer").removeClass("hidden");
                    $("div[data-dz-remove]").click()
                    return false;
                }

                $("#imageFormErrorContainer").addClass("hidden");
                $('#photoContainer').append(`<input type="hidden" name="photo[]" value="${serverResponse.src}">`)
                $('#imageForm img').attr('src', serverResponse.src);
            },
            uploadprogress: function(file, progress, bytesSent) {
                $('.ui.progress').progress({
                    percent: progress
                })
            },
        });
    </script>
    <script>
        const stripe = Stripe("{{ config('services.stripe.key') }}");

        $(".show-paymemt-options").on("click", function() {
            $("#paymentOptions")
                .modal({
                    blurring: true,
                    inverted: true
                })
                .modal("show");

            let itemID = $(this).data("item-id");
            let itemType = $(this).data("item-type");
            let paymentType = $(this).data("payment-type");

            $("#stripePaymentOption").on("click", function() {
                $.post({
                    url: "{{ route('stripe.checkout') }}",
                    data: {
                        item_id: itemID,
                        item_type: itemType,
                        payment_type: paymentType
                    },
                    success: async function(res) {
                        if (res.success) {
                            const { error } = await stripe.redirectToCheckout({
                                sessionId: res.response.id
                            });
                        }
                    }
                });
            });
        });

        $("#verifyCreditCardButton").on("click", function() {
            let itemType = $(this).data("item-type");
            let paymentType = $(this).data("payment-type");

            $.post({
                url: "{{ route('stripe.checkout') }}",
                data: {
                    item_id: null,
                    item_type: itemType,
                    payment_type: paymentType
                },
                success: async function(res) {
                    if (res.success) {
                        const { error } = await stripe.redirectToCheckout({
                            sessionId: res.response.id
                        });
                    }
                }
            });
        });
    </script>
@endpush
