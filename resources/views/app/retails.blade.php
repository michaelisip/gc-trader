@extends('layouts.app')

@section('title')
    Retails
@endsection

@section('content')
    <div class="mt-10 mb-20">
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            @forelse ($retails->chunk(4) as $chunk)
                <div class="md:flex md:flex-wrap mb-5 -mx-3">
                    @foreach ($chunk as $retail)
                        <div class="px-3 lg:px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                            <a href="{{ route('products.show', $retail->id) }}">
                                <div
                                    style="height: 350px" 
                                    class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                                >
                                    <div style="height: 200px">
                                        @if ($retail->images->count())
                                            <div
                                                class="rounded-t-lg relative w-full h-full overflow-hidden"
                                            >
                                                <img
                                                    class="z-10 relative w-full h-full object-cover"
                                                    src="{{ asset($retail->images->random()->src) }}"
                                                />
                                            </div>
                                        @else
                                            <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="flex flex-col mx-5">
                                        <span class="text-gray-900 my-2">
                                            @isset($retail->address)
                                                {{ Str::limit($retail->address->partial, 25) }}
                                            @endisset
                                        </span>
                                        <span
                                            class="item text-blue-700 font-bold"
                                            >{{ Str::limit($retail->name, 50) }}</span
                                        >
                                    </div>
                                    <div class="m-5 h-full font-bold flex flex-col-reverse">
                                        <span class="text-xl">${{ $retail->formatted_price }}</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @empty
                <div class="ui disabled header centered">
                    No Retail Listing Yet.
                </div>
            @endforelse
        </div>
        @if ($retails->count())
            <div class="my-5 mx-3 lg:mx-20 flex items-center justify-center">
                {{ $retails->links('vendor.pagination.semantic-ui') }}
            </div>
        @endif
    </div>
@endsection
