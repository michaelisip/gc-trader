@extends('layouts.app')

@section('title')
    Vehicles
@endsection

@section('content')
    <div class="my-10 mt-10 mx-3 md:mx-5 lg:mx-20 md:flex items-center justify-center">
        <div class="ui segments w-full lg:w-4/5">
            <div class="ui padded segment blue">
                <form
                    class="ui form"
                    method="GET"
                    action="{{ route('search.vehicle') }}"
                >
                    @csrf
                    <div class="two fields">
                        <div class="twelve wide field">
                            <input
                                type="text"
                                name="keywords"
                                placeholder="Vehicle Name or Keywords"
                                value="{{ request('keywords') }}"
                            />
                        </div>
                        <div class="four wide field">
                            <button type="submit" class="ui button fluid primary">
                                Search
                            </button>
                        </div>
                    </div>
                    <div class="ui fluid accordion">
                        <div class="title">
                            <i class="dropdown icon"></i>
                            Advanced Search
                        </div>
                        <div class="content">
                            <div class="four fields">
                                <div class="field">
                                    <input
                                        type="number"
                                        name="price_from"
                                        placeholder="Price From"
                                        value="{{ request('price_from') }}"
                                    />
                                </div>
                                <div class="field">
                                    <input
                                        type="number"
                                        name="price_to"
                                        placeholder="Price To"
                                        value="{{ request('price_to') }}"
                                    />
                                </div>
                                <div class="field">
                                    <input
                                        type="text"
                                        name="model"
                                        placeholder="Model"
                                        value="{{ request('model') }}"
                                    />
                                </div>
                                <div class="field">
                                    <input
                                        type="text"
                                        name="manufacturer"
                                        placeholder="Manufacturer"
                                        value="{{ request('manufacturer') }}"
                                    />
                                </div>
                                <div class="field">
                                    <input
                                        type="number"
                                        name="year"
                                        placeholder="Year"
                                        value="{{ request('year') }}"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="mb-20">
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            @forelse ($vehicles->chunk(4) as $chunk)
                <div class="md:flex md:flex-wrap justify-center mb-5 -mx-3">
                    @foreach ($chunk as $vehicle)
                        <div class="px-3 lg:px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                            <a href="{{ route('vehicles.show', $vehicle->id) }}">
                                <div
                                    class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-2xl"
                                >
                                    <div style="height: 200px">
                                        @if ($vehicle->images->count())
                                            <div
                                                class="rounded-t-lg relative w-full h-full overflow-hidden"
                                            >
                                                <img
                                                    class="z-10 relative w-full h-full object-cover"
                                                    src="{{ asset($vehicle->images->random()->src) }}"
                                                />
                                            </div>
                                        @else
                                            <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="flex flex-col mx-5">
                                        <span class="text-gray-900 my-2">
                                            @isset($vehicle->address)
                                                {{ Str::limit($vehicle->address->partial, 25) }}
                                            @endisset
                                        </span>
                                        <span
                                            class="item text-blue-700 font-bold"
                                            >{{ Str::limit($vehicle->name, 50) }}</span
                                        >
                                    </div>
                                    <div class="m-5 h-full font-bold flex flex-col-reverse">
                                        <span class="text-xl">${{ $vehicle->formatted_price }}</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @empty
                <div class="ui disabled header centered">
                    No Vehicle Listing Yet.
                </div>
            @endforelse
        </div>
        @if ($vehicles->count())
            <div class="my-5 mx-3 lg:mx-20 flex items-center justify-center">
                {{ $vehicles->appends([ 'keywords' => request('keywords'), 'model' =>
                request('model'), 'price_form' => request('price_form'), 'price_to' =>
                request('price_to'), 'year' => request('year'),
                ])->links('vendor.pagination.semantic-ui') }}
            </div>
        @endif
    </div>
@endsection
