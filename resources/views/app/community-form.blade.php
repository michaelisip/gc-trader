@extends('layouts.app')

@section('title')
    New Community
@endsection

@section('content')
    @include('partials.app-account-nav')
    @if($errors->any())
        <div class="my-5 mx-3 md:hidden">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 mx-3 md:mx-5 lg:mx-20 md:flex justify-center">
        <div class="w-full md:w-1/3 mr-5 mb-3">
            <div class="ui raised segments">
                <div class="ui padded segment blue overflow-hidden">
                    <div
                        id="imageFormErrorContainer"
                        class="ui warning message mb-5 hidden"
                    >
                        <p>Something went wrong.</p>
                    </div>
                    <form
                        id="imageForm"
                        class="ui form error flex justify-center"
                        enctype="multipart/form-data"
                    >
                        <div class="ui special cards">
                            <div class="card">
                                <div class="blurring dimmable image">
                                    <div class="ui dimmer">
                                        <div class="content">
                                            <div class="center">
                                                <div
                                                    id="chooseImageButton"
                                                    class="ui inverted button"
                                                >
                                                    Drag or Choose Photos
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="{{ old('photo')[0] ?? asset('img/placeholder.jpg') }}" width=""/>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="flex justify-center">
                        <div class="dropzone-images-previews">
                            @if (old('photo'))
                                <div class="mt-3 -mb-3">
                                    <span class="font-bold"> {{ count(old('photo')) }} image/s </span>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <p class="text-center pb-5">
                    <i> Maximum of 5 photos. </i>
                </p>

            </div>
            <div class="ui raised segments">
                <div class="ui padded segment blue overflow-hidden">
                    <div 
                        id="videoFormErrorContainer"
                        class="ui warning message mb-5 hidden"
                    >
                        <p>Something went wrong.</p>
                    </div>
                    <form
                        id="videoForm"
                        class="ui form error flex justify-center"
                        enctype="multipart/form-data"
                    >
                        <div class="ui special cards">
                            <div class="card">
                                <div class="blurring dimmable image hidden">
                                    <div class="ui dimmer">
                                        <div class="content">
                                            <div class="center">
                                                <div
                                                    id="chooseVideoButton"
                                                    class="ui inverted button"
                                                >
                                                    Drag or Choose Video
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <video controls autoplay class="@if (! old('video')) hidden @endif">
                                        <source src="{{ old('video') }}" type="video/mp4" />
                                    </video>
                                    @if (! old('video'))
                                        <img src="{{ asset('img/placeholder.jpg') }}" />
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="flex justify-center">
                        <div class="dropzone-video-previews"></div>
                    </div>
                </div>
                <p class="text-center pb-5">
                    <i> Maximum of 1 video only. </i>
                </p>
            </div>
        </div>      
        <div class="w-full md:w-2/3 lg:ml-5">
            <div class="ui raised segments">
                <div class="ui padded segment blue">
                    <h4 class="ui block header blue">Community Form</h4>
                    <div class="hidden md:block mb-5">
                        @if($errors->any())
                            <div class="ui error message w-full">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <form
                        method="POST"
                        class="ui form error"
                        action="{{ route('users.communities.store', Auth::id()) }}"
                    >
                        @csrf
                        <div id="photoContainer" class="hidden">
                            @if (old('photo'))
                                @foreach (old('photo') as $photo)
                                    <input type="hidden" name="photo[]" value="{{ $photo }}">
                                @endforeach
                            @endif
                        </div>
                        <div id="videoContainer" class="hidden">
                            @if (old('video'))
                                <input type="hidden" name="video" value="{{ old('video') }}">
                            @endif
                        </div>
                        <div class="two fields">
                            <div class="field  @error('name') error @enderror">
                                <label>Name</label>
                                <input
                                    type="text"
                                    name="name"
                                    placeholder="Name"
                                    value="{{ old('name') }}"
                                    autofocus
                                />
                            </div>
                            <div class="field  @error('company_number') error @enderror">
                                <label>Company number</label>
                                <input
                                    type="text"
                                    name="company_number"
                                    placeholder="Company Number"
                                    value="{{ old('company_number') }}"
                                />
                            </div>
                        </div>
                        <div class="three fields">
                            <div class="field  @error('registration') error @enderror">
                                <label>ABN Registration</label>
                                <input
                                    type="text"
                                    name="registration"
                                    placeholder="Registration"
                                    onkeypress="return this.value.length < 11;"
                                    value="{{ old('registration') }}"
                                />
                            </div>
                            <div class="field  @error('email') error @enderror">
                                <label>Email Address</label>
                                <input
                                    type="email"
                                    name="email"
                                    placeholder="Name"
                                    value="{{ old('email') }}"
                                />
                            </div>
                            <div class="field  @error('facebook_page') error @enderror">
                                <label>Facebook Page URL</label>
                                <input
                                    type="url"
                                    name="facebook_page"
                                    placeholder="Facebook Page URL"
                                    value="{{ old('facebook_page') }}"
                                />
                            </div>
                        </div>
                        <div class="field  @error('po_box_address') error @enderror">
                            <label>PO Box Address</label>
                            <input
                                type="text"
                                name="po_box_address"
                                placeholder="PO Box Address"
                                value="{{ old('po_box_address') }}"
                            />
                        </div>
                        <div class="field  @error('description') error @enderror">
                            <label>Description</label>
                            <textarea name="description">{{ old('description') }}</textarea>
                        </div>
                        <div class="two fields">
                            <div class="field  @error('date_time') error @enderror">
                                <label>Date/Time</label>
                                <input
                                    type="date"
                                    name="date_time"
                                    placeholder="Date/Time"
                                    value="{{ old('date_time') }}"
                                />
                            </div>
                            <div class="field @error('pick_up_type') error @enderror">
                                <label>Pick-Up/Drop-Off</label>
                                <div class="ui selection dropdown">
                                    <input
                                        type="hidden"
                                        name="pick_up_type"
                                        value="{{ old('pick_up_type') ?? 'Pick-Up' }}"
                                    />
                                    <i class="dropdown icon"></i>
                                    <div class="default text">
                                      {{ 'Pick Up Type' }}
                                    </div>
                                    <div class="menu">
                                        <div class="item" data-value="Pick-Up">
                                            Pick-Up
                                        </div>
                                        <div class="item" data-value="Drop-Off">
                                            Drop-Off
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field  @error('start_time') error @enderror">
                                <label>Start Time</label>
                                <input
                                    type="datetime-local"
                                    name="start_time"
                                    placeholder="Start Time"
                                    value="{{ old('start_time') }}"
                                />
                            </div>
                            <div class="field  @error('finish_time') error @enderror">
                                <label>Finish Time</label>
                                <input
                                    type="datetime-local"
                                    name="finish_time"
                                    placeholder="Finish Time"
                                    value="{{ old('finish_time') }}"
                                />
                            </div>
                        </div>
                        <div class="field">
                            <div class="ui toggle checkbox">
                                <input type="checkbox" name="parking_or_disability_drop_box" @if (old('parking_or_disability_drop_box')) checked @endif />
                                <label>Parking/Disability Drop Box</label>
                            </div>
                        </div>
                        @if ($communityCategories->count())
                            <h4 class="ui block header blue">Categories</h4>
                            <div class="field">
                                <select
                                    name="categories[]"
                                    class="ui fluid search dropdown"
                                    multiple=""
                                >
                                    <option value="">Categories</option>
                                    @foreach ($communityCategories as $category)
                                        <option value="{{ $category->id }}"
                                            @if (old('categories') && in_array($category->id, old('categories'))) selected @endif >
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        @endif
                        <button type="submit" class="ui button primary" tabindex="0">
                            Add New Community
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>    
    @include('partials.app-dropzone-preview')
@endsection

@push('scripts')
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script>
        $('#imageForm').dropzone({
            url: '{{ route('images.store') }}',
            headers: {
                'X-CSRF-TOKEN': $(`meta[name='csrf-token']`).attr('content'),
            },
            paramName: 'photo',
            clickable: ['#chooseImageButton'],
            chunking: true,
            maxFiles: 5,
            acceptedFiles: 'image/*',
            previewsContainer: '.dropzone-images-previews',
            previewTemplate: document.querySelector('#tpl').innerHTML,
            success: function (file) {
                let serverResponse = JSON.parse(file.xhr.response);

                if (!file.status === 'success') {
                    $('#imageFormErrorContainer p').text('Something went wrong.');
                    $('#imageFormErrorContainer').removeClass('hidden');
                    return false;
                }

                if (!serverResponse.success) {
                    $('#imageFormErrorContainer p').text('Something went wrong. Please contact support.');
                    $('#imageFormErrorContainer').removeClass('hidden');
                    $("div[data-dz-remove]").click()
                    return false;
                }

                $("#imageFormErrorContainer").addClass("hidden");
                $('#photoContainer').append(`<input type="hidden" name="photo[]" value="${serverResponse.src}">`)
                $('#imageForm img').attr('src', serverResponse.src)
            },
            uploadprogress: function(file, progress, bytesSent) {
                $('.ui.progress').progress({
                    percent: progress
                })
            },
            init: function() {
                this.on("addedfile", function(event) {
                    while (this.files.length > this.options.maxFiles) {
                        this.removeFile(this.files[0]);
                    }
                });
            }
        });
        $('#videoForm').dropzone({
            url: '{{ route('videos.store') }}',
            headers: {
                'X-CSRF-TOKEN': $(`meta[name='csrf-token']`).attr('content'),
            },
            paramName: 'video',
            clickable: ['#chooseVideoButton'],
            chunking: true,
            maxFiles: 1,
            acceptedFiles: 'video/mp4,video/x-m4v,video/*',
            previewsContainer: '.dropzone-video-previews',
            previewTemplate: document.querySelector('#tpl-video').innerHTML,
            success: function (file) {
                let serverResponse = JSON.parse(file.xhr.response);

                if (!file.status === 'success') {
                    $('#videoFormErrorContainer p').text('Something went wrong.');
                    $('#videoFormErrorContainer').removeClass('hidden');
                    return false;
                }

                if (!serverResponse.success) {
                    $('#videoFormErrorContainer p').text('Something went wrong. Please contact support.');
                    $('#videoFormErrorContainer').removeClass('hidden');
                    return false;
                }

                $('#videoContainer').append(`<input type="hidden" name="video" value="${serverResponse.src}">`)
                $('#videoForm source').attr('src', serverResponse.src)
                $('#videoForm video')[0].load()
                $('#videoForm video').removeClass('hidden')
                $('#videoForm img').css('display', 'none')
            },
            uploadprogress: function(file, progress, bytesSent) {
                $('.ui.progress').progress({
                    percent: progress
                })
            },
        });
    </script>
@endpush
