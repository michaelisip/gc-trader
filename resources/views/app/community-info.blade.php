@extends('layouts.app')

@section('title')
    {{ $community->name }}
@endsection

@push('styles')
    <link
    href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css"
    rel="stylesheet"
    />
@endpush

@section('content')
    @if($errors->any())
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 mt-10 mx-3 md:mx-5 lg:mx-20 md:flex justify-center">
        @if ($community->images->count() || $community->video)
            <div class="w-full md:w-3/5 lg:w-2/6 mr-5">
                <div
                    class="fotorama"
                    data-nav="thumbs"
                    data-allowfullscreen="true"
                    data-loop="true"
                    data-autoplay="true"
                    data-width="100%"
                >
                    @if ($community->images->count())
                        @foreach ($community->images as $image)
                            <a href="{{ asset($image->src) }}">
                                <img src="{{ asset($image->src) }}" />
                            </a>
                        @endforeach
                    @endif
                </div>
                @if ($community->video)
                    <div class="my-3 flex justify-center">
                        <video controls class="w-full">
                            <source src="{{ asset($community->video->src) }}" type="video/mp4" />
                        </video>
                    </div>
                @endif
            </div>
        @else
            <div class="w-full md:w-3/5 lg:w-2/6 mr-5">
                <div class="ui rounded-lg">
                    <img src="{{ asset('img/placeholder.jpg') }}" />
                </div>
            </div>
        @endif
        <div class="w-full md:w-2/5 lg:w-4/6 ml-0 lg:ml-5">
            <div class="rounded-lg flex bg-gray-100 shadow hover:shadow-lg mt-10 md:mt-0 w-full md:w-1/2 mb-5 p-5">
                <div class="flex items-center">
                    <img
                        class="w-20 h-20 rounded-full mr-5"
                        src="{{ asset($community->user->image->src ?? $community->user->profile_placeholder) }}"
                    />
                    <div>
                        <p class="text-blue-700 leading-none">
                            {{ $community->user->full_name }}
                        </p>
                        <p class="text-gray-900">
                            @isset($community->user->address)
                                {{ Str::limit($community->user->address->partial, 25) }}
                            @endisset
                        </p>
                    </div>
                </div>
            </div>
            <h1 class="font-bold text-gray-900 text-4xl mb-5">
                {{ $community->name }}
            </h1>
            <div class="my-10">
                <div class="text-blue-700 font-bold text-2xl mb-5">Description</div>
                <p class="text-gray-900 text-lg">
                    {!! $community->description !!}
                </p>
            </div>
            <div class="my-10">
                <div class="text-blue-700 font-bold text-2xl mb-5">Details</div>
                <div class="flex">
                    <div class="w-1/2">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="text-blue-700 font-bold">
                                    Company Number
                                </div>
                                <p class="text-gray-900 text-lg">
                                    {{ $community->company_number ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="text-blue-700 font-bold">ABN Registration</div>
                                <p class="text-gray-900 text-lg">
                                    <a href="https://www.acnc.gov.au" target="__blank" rel="noopener noreferrer">
                                        {{ $community->registration ?? 'Not Indicated' }}
                                        <i class="globe icon"></i>
                                    </a>
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="text-blue-700 font-bold">Email Address</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $community->email ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="text-blue-700 font-bold">
                                    PO Box Address
                                </div>
                                <p class="text-gray-900 text-lg">
                                    {{ $community->po_box_address ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="pl-20 w-1/2">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="text-blue-700 font-bold">Date Time</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $community->date_time ?
                                    $community->date_time->diffForHumans() : 'Not
                                    Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="text-blue-700 font-bold">Start Time</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $community->start_time ? $community->start_time->format('h:i A') : 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="text-blue-700 font-bold">Finish Time</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $community->finish_time ? $community->finish_time->format('h:i A') : 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="text-blue-700 font-bold">Pick Up Type</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $community->pick_up_type ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="my-10 mb-20 mx-3 lg:mx-20 flex items-center justify-center">
        <div class="ui threaded comments w-full md:w-1/2">
            <h3 class="ui dividing header blue">
                Questions and Answers ({{ $community->comments->count() }})
            </h3>
            @foreach ($community->comments as $comment)
                <div class="comment">
                    <span class="avatar">
                        <img
                            src="{{ asset($comment->user->image->src ?? $comment->user->profile_placeholder) }}"
                        />
                    </span>
                    <div class="content">
                        <span class="author">{{ $comment->user->full_name }}</span>
                        <div class="metadata">
                            <span class="date"
                                >{{ $comment->created_at ? $comment->created_at->diffForHumans() : 'Not Indicated' }}</span
                            >
                            @if ($comment->reply)
                                <span class="ui mini green label">Answered</span>
                            @endif
                        </div>
                        <div class="text">
                            <p class="text-lg">
                                {{ $comment->comment }}
                            </p>
                        </div>
                        @if (!$comment->reply)
                            @can('update-comment', $comment)
                                <div class="actions reply-comment">
                                    <a class="reply">Reply</a>
                                </div>
                                <form
                                    action="{{ route('comments.update', $comment->id) }}"
                                    method="POST"
                                    class="ui reply form hidden"
                                >
                                    @csrf
                                    @method('PUT')
                                    <input
                                        type="hidden"
                                        name="user_id"
                                        value="{{ Auth::id() }}"
                                    />
                                    <input
                                        type="hidden"
                                        name="comment"
                                        value="{{ $comment->comment }}"
                                    />
                                    <input
                                        type="hidden"
                                        name="replied_at"
                                        value="{{ now() }}"
                                    />
                                    <div class="field">
                                        <textarea name="reply"></textarea>
                                    </div>
                                    <button
                                        type="submit"
                                        class="ui primary submit labeled icon button"
                                    >
                                        <i class="icon edit"></i> Add Reply
                                    </button>
                                </form>
                            @endcan
                        @endif
                    </div>
                    @if ($comment->reply)
                        <div class="comments">
                            <div class="comment">
                                <span class="avatar">
                                    <img
                                        src="{{ asset($comment->commentable->user->image->src ?? $comment->commentable->user->profile_placeholder) }}"
                                    />
                                </span>
                                <div class="content">
                                    <span class="author"
                                        >{{ $comment->commentable->user->full_name }}</span
                                    >
                                    <div class="metadata">
                                        <span class="date"
                                            >{{ $comment->replied_at->diffForHumans()
                                            }}</span
                                        >
                                    </div>
                                    <div class="text">
                                        <p class="text-lg">
                                            {{ $comment->reply }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
            @auth
                @if (Gate::check('create-comment'))
                    <form
                        action="{{ route('communities.comments.store', $community->id) }}"
                        method="post"
                        class="ui reply form"
                    >
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::id() }}" />
                        <div class="field">
                            <textarea name="comment"></textarea>
                        </div>
                        <button type="submit" class="ui primary submit labeled icon button">
                            <i class="icon edit"></i> Add Comment
                        </button>
                    </form>
                @else
                    <div class="ui message">
                        <p> Please activate your account in order to post a comment. </p>
                    </div>
                @endif
            @endauth
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
    <script>
        $(".reply-comment").on("click", function() {
            $(this)
                .next()
                .toggleClass("hidden");
        });
    </script>
@endpush
