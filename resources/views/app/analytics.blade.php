@extends('layouts.app')

@section('title')
    Analytics
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.css">
@endpush

@section('content')
    @include('partials.app-account-nav')
    <div class="mb-20 lg:mb-40">
        <div class="my-5 lg:my-10 mx-3 md:mx-5 lg:mx-20">
            <h3 class="ui top attached header section-header">Top Listing</h3>
            <div class="ui attached segment">
                <canvas id="topSellingChart" width="1080" height="400"></canvas>
            </div>
        </div>

        <div class="my-5 lg:my-10 mx-3 md:mx-5 lg:mx-20">
            <h3 class="ui top attached header section-header">Listing Duration</h3>
            <div class="ui attached segment">
                <canvas id="durationChart" width="1080" height="400"></canvas>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>
    <script>
        new Chart(document.getElementById("topSellingChart").getContext("2d"), {
            type: "horizontalBar",
            data: {
                labels: [
                    @foreach ($user->products()->withSales()->get() as $product)
                        "{{ Str::limit($product->name, 30) }}",
                    @endforeach
                ],
                datasets: [
                    {
                        label: "Number of Sales",
                        data: [
                            @foreach ($user->products()->withSales()->get() as $product)
                                {{ $product->sales }},
                            @endforeach
                        ],
                        backgroundColor: [
                            @foreach ($user->products()->withSales()->get() as $product)
                                "#4299e16c",
                            @endforeach
                        ],
                        borderColor: [
                            @foreach ($user->products()->withSales()->get() as $product)
                                "#4299e1",
                            @endforeach
                        ],
                        borderWidth: 2.5
                    }
                ]
            },
            options: {
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true
                            }
                        }
                    ]
                }
            }
        });

        new Chart(document.getElementById("durationChart").getContext("2d"), {
            type: "horizontalBar",
            data: {
                labels: [
                    @foreach ($user->products()->withSales()->get() as $product)
                        "{{ Str::limit($product->name, 30) }}",
                    @endforeach
                ],
                datasets: [
                    {
                        label: "Number of Days",
                        data: [
                            @foreach ($user->products()->withSales()->get() as $product)
                                {{ $product->created_at->diffInDays() }},
                            @endforeach
                        ],
                        backgroundColor: [
                            @foreach ($user->products()->withSales()->get() as $product)
                                "#4299e16c",
                            @endforeach
                        ],
                        borderColor: [
                            @foreach ($user->products()->withSales()->get() as $product)
                                "#4299e1",
                            @endforeach
                        ],
                        borderWidth: 2.5
                    }
                ]
            },
            options: {
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true
                            }
                        }
                    ]
                }
            }
        });
    </script>
@endpush
