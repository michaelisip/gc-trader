@extends('layouts.app')

@section('title')
    Services
@endsection

@section('content')
    <div class="mt-10 mb-20">
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            @forelse ($services->chunk(4) as $chunk)
                <div class="md:flex md:flex-wrap justify-center mb-5 -mx-3">
                    @foreach ($chunk as $service)
                        <div class="px-3 lg:px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                            <a href="{{ route('services.show', $service->id) }}">
                                <div
                                    style="height: 300px"
                                    class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                                >
                                    <div style="height: 200px">
                                        @if ($service->images->count())
                                            <div
                                                class="rounded-t-lg relative w-full h-full overflow-hidden"
                                            >
                                                <img
                                                    class="z-10 relative w-full h-full object-cover"
                                                    src="{{ asset($service->images->random()->src) }}"
                                                />
                                            </div>
                                        @else
                                            <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="flex flex-col m-5">
                                        <span class="text-gray-900">
                                            @isset($service->address)
                                                {{ Str::limit($service->address->partial, 25) }}
                                            @endisset
                                        </span>
                                        <span
                                            class="item text-blue-700 font-bold"
                                            >{{ Str::limit($service->name, 50) }}</span
                                        >
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @empty
                <div class="ui disabled header centered">
                    No Service Listing Yet.
                </div>
            @endforelse
        </div>
        @if ($services->count())
            <div class="my-5 mx-3 lg:mx-20 flex items-center justify-center">
                {{ $services->links('vendor.pagination.semantic-ui') }}
            </div>
        @endif
    </div>
@endsection
