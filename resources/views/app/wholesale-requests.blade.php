@extends('layouts.app') 

@section('title')
    Wholesale Requests
@endsection

@section('content')
    @include('partials.app-account-nav')
    @if (Gate::check('create-wholesale-product', auth()->user()))
        <div class="mb-20 lg:mb-40">
            <div class="my-5 lg:my-10 mx-3 md:mx-5 lg:mx-20">
                <h3 class="ui top attached header section-header">Your Requests</h3>
                <div class="ui attached segment">
                    @if ($myRequests->count())
                        <table class="w-full table-auto">
                            <thead>
                                <tr>
                                    <th class="px-4 py-2">Vendor</th>
                                    <th class="px-4 py-2">Product</th>
                                    <th class="px-4 py-2">Date</th>
                                    <th class="px-4 py-2"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($myRequests as $request)
                                    <tr>
                                        <td class="border px-4 py-2">
                                            {{ $request->vendor->full_name }}
                                        </td>
                                        <td class="border px-4 py-2">
                                            @if ($request->product->quantity > 0)
                                                <a href="{{ route('products.show', $request->product->id) }}" target="__blank" rel="noopener noreferrer">
                                                    {{ $request->product->name }}
                                                </a>
                                            @else
                                                {{ $request->product->name }}
                                            @endif
                                        </td>
                                        <td class="border px-4 py-2">
                                            {{ $request->created_at->diffForHumans() }}
                                        </td>
                                        <td class="border px-4 py-2">
                                            @if ($request->product->quantity > 0)
                                                <a href="{{ route('products.show', $request->product->id) }}" class="ui button blue">
                                                    Proceed to Payment
                                                </a>
                                            @else
                                                <i> Insufficient Product Quantity </i>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        <div class="ui disabled header centered">
                            No Requests Yet.
                        </div>
                    @endif
                </div>
            </div>
            @if ($wholesales->count())
                <div class="my-5 lg:my-10 mx-3 md:mx-5 lg:mx-20">
                    <h3 class="ui top attached header section-header">Buyer Requests</h3>
                    <div class="ui attached segment">
                        @if ($buyerRequests->count())
                            <table class="w-full table-auto">
                                <thead>
                                    <tr>
                                        <th class="px-4 py-2">User</th>
                                        <th class="px-4 py-2">Product</th>
                                        <th class="px-4 py-2">Date</th>
                                        <th class="px-4 py-2"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($buyerRequests as $request)
                                        <tr>
                                            <td class="border px-4 py-2">
                                                {{ $request->buyer->full_name }}
                                            </td>
                                            <td class="border px-4 py-2">
                                                @if ($request->product->quantity > 0)
                                                    <a href="{{ route('products.show', $request->product->id) }}" target="__blank" rel="noopener noreferrer">
                                                        {{ $request->product->name }}
                                                    </a>
                                                @else
                                                    {{ $request->product->name }}
                                                @endif
                                            </td>
                                            <td class="border px-4 py-2">
                                                {{ $request->created_at->diffForHumans() }}
                                            </td>
                                            <td class="border px-4 py-2">
                                                @if ($request->product->quantity > 0)
                                                    <button class="ui button blue wholesale-request-approve" data-url="{{ route('wholesales.requests.approve', $request->id) }}">
                                                        Approve
                                                    </button>
                                                    <button class="ui button red wholesale-request-deny mt-2" data-url="{{ route('wholesales.requests.deny', $request->id) }}">
                                                        Deny
                                                    </button>
                                                @else
                                                    <i> Insufficient Product Quantity </i>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="ui disabled header centered">
                                No Requests Yet.
                            </div>
                        @endif
                    </div>
                </div>
            @endif
        </div>
    @else
        <div class="mb-20 lg:mb-40">
            <div class="my-5 lg:my-10 mx-3 md:mx-5 lg:mx-20">
                <h3 class="ui top attached header section-header">Wholesale Requests</h3>
                <div class="ui attached segment">
                    <i class="text-center"> Please purchase a Wholesale Membership to be able to create Wholesale products. </i>
                </div>
            </div>
        </div>
    @endif


@endsection

@push('scripts')
    <script>
        $('.approve')
    </script>
@endpush