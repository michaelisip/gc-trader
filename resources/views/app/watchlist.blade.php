@extends('layouts.app')

@section('head')
@endsection

@section('title')
    Watchlist
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css">
@endpush

@section('content')
    @include('partials.app-account-nav')

    <div class="my-5 lg:my-10 mb-20 lg:mb-40">
        {{-- Product Watchlist --}}
        <div class="my-5 mx-3 lg:mx-20">
            <h3 class="ui top attached header section-header">Products</h3>
            <div class="ui attached segment">
                @forelse ($user->productWatchlist->chunk(4) as $chunk)
                    <div class="md:flex md:flex-wrap justify-center">
                        @foreach ($chunk as $product)
                            <div class="px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                                <div
                                    style="height: 350px" 
                                    class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                                >
                                    <a href="{{ route('products.show', $product->id) }}">

                                        <div style="height: 200px">
                                            @if ($product->images->count())
                                                <div
                                                    class="rounded-t-lg relative w-full h-full overflow-hidden"
                                                >
                                                    <img
                                                        class="z-10 relative w-full object-cover"
                                                        src="{{ asset($product->images->random()->src) }}"
                                                    />
                                                </div>
                                            @else
                                                <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                    <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                                </div>
                                            @endif
                                        </div>
                                        <div class="flex flex-col mx-5">
                                            <span class="text-gray-900 my-2">
                                                @isset($product->address)
                                                    {{ Str::limit($product->address->partial, 25) }}
                                                @endisset
                                            </span>
                                            <span
                                                class="item text-blue-700 font-bold"
                                                >{{ Str::limit($product->name, 50) }}</span
                                            >
                                        </div>
                                    </a>
                                    <div class="m-5 h-full font-bold flex flex-col">
                                        <button
                                            data-url="{{ route('watchables.destroy', $product->pivot->id) }}"
                                            class="ui red button tiny remove-from-watchlist"
                                        >
                                            <i class="eye slash icon"></i> Remove
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @empty
                    <div class="ui disabled header centered">
                        No Product Watchlist Yet.
                    </div>
                @endforelse
            </div>
        </div>

        {{-- Job Watchlist --}}
        {{-- <div class="my-5 mx-3 lg:mx-20">
            <h3 class="ui top attached header section-header">Jobs</h3>
            <div class="ui attached segment">
                @forelse ($user->jobWatchlist->chunk(2) as $chunk)
                    <div class="md:flex md:flex-wrap justify-center">
                        @foreach ($chunk as $job)
                            <div class="px-5 mb-3 w-full md:w-1/2">
                                <a href="{{ route('jobs.show', $job->id) }}">
                                    <div
                                        class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl p-5"
                                    >
                                        <div class="mb-5">
                                            <span
                                                class="item text-2xl text-blue-700 font-bold"
                                                >{{ $job->name }}</span
                                            >
                                            <br />
                                            <span class="text-sm text-gray-900 my-2">{{ $job->company_name }}</span>
                                        </div>
                                        <div class="mb-5">
                                            <p class="text-black text-lg">
                                                {{ Str::limit($job->description, 300) }}
                                            </p>
                                        </div>
                                        <p class="text-black text-lg">
                                            <span class="font-bold text-blue-700"> Type: </span> {{ $job->type }} 
                                        </p>
                                        @isset($job->address)
                                            <p class="text-black text-lg">
                                                <span class="font-bold text-blue-700"> Address: </span>
                                                {{ Str::limit($job->address->partial, 25) }}
                                              </p>
                                        @endisset
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                @empty
                    <div class="ui disabled header centered">
                        No Job Watchlist Yet.
                    </div>
                @endforelse
            </div>
        </div> --}}

        {{-- Real Estate Watchlist --}}
        <div class="my-5 mx-3 lg:mx-20">
            <h3 class="ui top attached header section-header">Real Estates</h3>
            <div class="ui attached segment">
                @forelse ($user->realEstateWatchlist->chunk(2) as $chunk)
                    <div class="md:flex md:flex-wrap justify-center mb-5 -mx-3">
                        @foreach ($chunk as $realEstate)
                            <div class="py-5 px-3 w-full md:w-1/2">
                                <div
                                    class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                                >
                                    <div style="height: 250px">
                                        @if ($realEstate->images->count())
                                            <div class="rounded-t-lg relative w-full h-full overflow-hidden">
                                                <div
                                                    class="fotorama"
                                                    data-nav="false"
                                                    data-allowfullscreen="true"
                                                    data-loop="true"
                                                    data-autoplay="true"
                                                    data-height="250"
                                                    data-fit="cover"
                                                >
                                                    @foreach ($realEstate->images as $image)
                                                        <a href="{{ asset($image->src) }}">
                                                            <img src="{{ asset($image->src) }}" />
                                                        </a>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @else
                                            <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                            </div>
                                        @endif
                                    </div>
                                    <a href="{{ route('real-estates.show', $realEstate->id) }}">
                                        <div class="md:flex mx-5 real-estate-card-info">
                                            <div class="flex items-center w-full lg:w-1/2 mx-2 lg:mx-5 my-4">
                                                <img
                                                    class="w-20 h-20 rounded-full mr-2 lg:mr-5"
                                                    src="{{ asset($realEstate->user->image->src ?? $realEstate->user->profile_placeholder) }}"
                                                />
                                                <div>
                                                    <p class="text-blue-700 leading-none">
                                                        {{ $realEstate->user->full_name }}
                                                    </p>
                                                    <p class="text-gray-900">
                                                        @isset($realEstate->user->adddress)
                                                            {{ Str::limit($realEstate->user->address->partial, 25) }}
                                                        @endisset
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="md:flex flex-col w-full lg:w-1/2">
                                                <div class="flex flex-col mx-2 lg:mx-5">
                                                    <span class="text-gray-900 my-2">
                                                        @isset($realEstate->addresss)
                                                            {{ Str::limit($realEstate->address->partial, 25) }}
                                                        @endisset
                                                    </span>
                                                    <span
                                                        class="item text-lg text-blue-700 font-bold"
                                                        >{{ Str::limit($realEstate->name, 50) }}</span
                                                    >
                                                </div>
                                                <div class="mx-2 my-4 lg:m-5 h-full font-bold">
                                                    <span class="text-xl">${{ $realEstate->formatted_price }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                    <div class="m-5 h-full font-bold flex flex-col">
                                      <button
                                          data-url="{{ route('watchables.destroy', $realEstate->pivot->id) }}"
                                          class="ui red button tiny remove-from-watchlist"
                                      >
                                          <i class="eye slash icon"></i> Remove
                                      </button>
                                  </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @empty
                    <div class="ui disabled header centered">
                        No Real Estate Watchlist Yet.
                    </div>
                @endforelse
            </div>
        </div>

        {{-- Service Watchlist --}}
        {{-- <div class="my-5 mx-3 lg:mx-20">
            <h3 class="ui top attached header section-header">Services</h3>
            <div class="ui attached segment">
              @forelse ($user->serviceWatchlist->chunk(4) as $chunk)
                  <div class="md:flex md:flex-wrap justify-center mb-5 -mx-3">
                      @foreach ($chunk as $service)
                          <div class="px-3 lg:px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                              <a href="{{ route('services.show', $service->id) }}">
                                  <div
                                      style="height: 300px"
                                      class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                                  >
                                      <div style="height: 200px">
                                          @if ($service->images->count())
                                              <div
                                                  class="rounded-t-lg relative w-full h-full overflow-hidden"
                                              >
                                                  <img
                                                      class="z-10 relative w-full h-full object-cover"
                                                      src="{{ asset($service->images->random()->src) }}"
                                                  />
                                              </div>
                                          @else
                                              <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                  <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                              </div>
                                          @endif
                                      </div>
                                      <div class="flex flex-col m-5">
                                          <span class="text-gray-900">
                                              @isset($service->addresss)
                                                  {{ Str::limit($service->address->partial, 25) }}
                                              @endisset
                                          </span>
                                          <span
                                              class="item text-blue-700 font-bold"
                                              >{{ Str::limit($service->name, 50) }}</span
                                          >
                                      </div>
                                  </div>
                              </a>
                          </div>
                      @endforeach
                  </div>
              @empty
                  <div class="ui disabled header centered">
                      No Service Watchlist Yet.
                  </div>
              @endforelse
            </div>
        </div> --}}

        {{-- Travel Watchlist --}}
        <div class="my-5 mx-3 lg:mx-20">
            <h3 class="ui top attached header section-header">Travels</h3>
            <div class="ui attached segment">
              @forelse ($user->travelWatchlist->chunk(4) as $chunk)
                  <div class="md:flex md:flex-wrap justify-center mb-5 -mx-3">
                      @foreach ($chunk as $travel)
                          <div class="px-3 lg:px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                              <div
                                  style="height: 320px"
                                  class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-2xl"
                              >
                                  <a href="{{ route('travels.show', $travel->id) }}">

                                      <div style="height: 200px">
                                          @if ($travel->images->count())
                                              <div
                                                  class="rounded-t-lg relative w-full h-full overflow-hidden"
                                              >
                                                  <img
                                                      class="z-10 relative w-full h-full object-cover"
                                                      src="{{ asset($travel->images->random()->src) }}"
                                                  />
                                              </div>
                                          @else
                                              <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                  <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                              </div>
                                          @endif
                                      </div>
                                      <div class="flex flex-col mx-5">
                                          <span class="text-gray-900 my-2">
                                              @isset($travel->address)
                                                  {{ Str::limit($travel->address->partial, 25) }}
                                              @endisset
                                          </span>
                                          <span
                                              class="item text-blue-700 font-bold"
                                              >{{ Str::limit($travel->name, 50) }}</span
                                          >
                                      </div>
                                  </a>
                                  <div class="m-5 h-full font-bold flex flex-col">
                                      <button
                                          data-url="{{ route('watchables.destroy', $travel->pivot->id) }}"
                                          class="ui red button tiny remove-from-watchlist"
                                      >
                                          <i class="eye slash icon"></i> Remove
                                      </button>
                                  </div>
                              </div>
                          </div>
                      @endforeach
                  </div>
              @empty
                  <div class="ui disabled header centered">
                      No Travel Watchlist Yet.
                  </div>
              @endforelse
            </div>
        </div>

        {{-- Vehicle Watchlist --}}
        <div class="my-5 mx-3 lg:mx-20">
            <h3 class="ui top attached header section-header">Vehicles</h3>
            <div class="ui attached segment">
              @forelse ($user->vehicleWatchlist->chunk(4) as $chunk)
                  <div class="md:flex md:flex-wrap justify-center mb-5 -mx-3">
                      @foreach ($chunk as $vehicle)
                          <div class="px-3 lg:px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                              <div
                                  class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-2xl"
                              >
                                  <a href="{{ route('vehicles.show', $vehicle->id) }}">
                                      <div style="height: 200px">
                                          @if ($vehicle->images->count())
                                              <div
                                                  class="rounded-t-lg relative w-full h-full overflow-hidden"
                                              >
                                                  <img
                                                      class="z-10 relative w-full h-full object-cover"
                                                      src="{{ asset($vehicle->images->random()->src) }}"
                                                  />
                                              </div>
                                          @else
                                              <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                  <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                              </div>
                                          @endif
                                      </div>
                                      <div class="flex flex-col mx-5">
                                          <span class="text-gray-900 my-2">
                                              @isset($vehicle->address)
                                                 {{ Str::limit($vehicle->address->partial, 25) }}
                                              @endisset
                                          </span>
                                          <span
                                              class="item text-blue-700 font-bold"
                                              >{{ Str::limit($vehicle->name, 50) }}</span
                                          >
                                      </div>
                                  </a>
                                  <div class="m-5 h-full font-bold flex flex-col">
                                      <button
                                          data-url="{{ route('watchables.destroy', $vehicle->pivot->id) }}"
                                          class="ui red button tiny remove-from-watchlist"
                                      >
                                          <i class="eye slash icon"></i> Remove
                                      </button>
                                  </div>
                              </div>
                          </div>
                      @endforeach
                  </div>
              @empty
                  <div class="ui disabled header centered">
                      No Vehicle Watchlist Yet.
                  </div>
              @endforelse
            </div>
        </div>
    </div>
    
@endsection

@push('scripts')
    <script src='https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js'></script>
@endpush
