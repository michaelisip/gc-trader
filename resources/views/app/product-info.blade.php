@extends('layouts.app')

@section('title')
    {{ $product->name }}
@endsection

@push('styles')
    <link
    href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css"
    rel="stylesheet"
    />
@endpush

@section('content')
    @if($errors->any())
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 mx-3 mt-10 md:mx-5 lg:mx-20 md:flex justify-center">
        @if ($product->images->count())
            <div class="w-full md:w-1/2 lg:w-1/3 mr-5">
                <div
                    class="fotorama"
                    data-nav="thumbs"
                    data-allowfullscreen="true"
                    data-loop="true"
                    data-autoplay="true"
                    data-width="100%"
                >
                    @foreach ($product->images as $image)
                        <a href="{{ asset($image->src) }}">
                            <img src="{{ asset($image->src) }}" />
                        </a>
                    @endforeach
                </div>
            </div>
        @else
            <div class="w-full md:w-1/2 lg:w-1/3 mr-5">
                <div class="ui rounded-lg">
                    <img src="{{ asset('img/placeholder.jpg') }}" />
                </div>
            </div>
        @endif
        <div class="w-full md:w-1/2 lg:w-3/6 ml-0 mt-5 md:mt-0 lg:ml-5">
            <h1 class="font-bold text-gray-900 text-4xl mb-5">
                {{ $product->name }}
            </h1>
            @if (! $product->user()->find(auth()->id()))
                <div class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-lg p-5 w-full lg:w-2/3">
                    @if ($product->is_retail || $product->is_wholesale)
                        <div class="text-center">
                            <div class="mb-5">
                                <h1 class="font-bold text-gray-900 text-3xl">
                                    @if ($product->is_wholesale) <small class="font-thin text-sm italic"> RRP:</small> @endif 
                                    ${{ $product->formatted_price }}
                                </h1>
                                @if ($product->is_wholesale && Gate::check('create-wholesale-product', $product->price))
                                    <small>
                                        <i> Wholesale Price: </i>
                                        <span class="font-bold text-lg"> ${{ $product->formatted_wholesale_price }} </span>
                                    </small>
                                @endif
                            </div>
                            <div class="ui fluid error message @if (Gate::check('bid-items')) hidden @endif">
                                <p >Please activate your account in order to buy items.</p>
                            </div>
                            @if ($product->is_retail)
                                <div class="mb-3">
                                    <a
                                        class="ui button primary fluid show-payment-options mb-3 @if (!Gate::check('buy-items')) disabled @endif"
                                        @if (Auth::check())
                                            @can('buy-items', Product::class)
                                                data-item-id="{{ $product->id }}"
                                                data-item-type="product"
                                                data-payment-type="single"
                                            @endcan
                                        @else
                                            href="{{ route('login') }}"
                                        @endif
                                    >
                                        Buy Now
                                    </a>
                                </div>
                            @endif
                        </div>
                    @endif
                    @if (($product->is_auction || $product->is_reverse_auction) && $product->status !== 'pending')
                        <div class="mb-5 text-center">
                            <p class="text-gray-900">
                                {{ $product->bids->count() ? 'Current Bid' : 'Starting Bid' }}:&nbsp;
                            </p>
                            <h1 class="font-bold text-gray-900 text-3xl mb-5">
                                ${{ $product->formatted_starting_bid }}
                            </h1>
                            @if (!$product->bids->count())
                                <p class="text-gray-900 text-center my-3">
                                    No Reserve • No Bids
                                </p>
                            @endif
                        </div>
                    @endif
                    @auth
                        @php
                            $request = auth()->user()->wholesaleRequests()->withoutGlobalScopes()->where('product_id', $product->id)->first();
                        @endphp
                    @endauth
                    @if ($product->is_retail || $product->is_wholesale)
                        <div class="ui fluid @if (! $request || ($request && $request->status === 'approved')) two icon buttons @endif">
                            @if ($product->is_retail)
                                <button
                                    data-id="{{ $product->id }}"
                                    data-type="product"
                                    class="ui blue button add-to-cart @if (!Gate::check('buy-items')) disabled @endif"
                                >
                                    <i class="cart icon"></i> Cart
                                </button>
                            @else
                                @if ($request)
                                    @if ($request->status === 'approved')
                                        <button
                                            class="ui blue button show-payment-options"
                                            data-item-id="{{ $product->id }}"
                                            data-item-type="product"
                                            data-payment-type="single"
                                        >
                                            Pay Now
                                        </button>
                                    @endif
                                @else
                                    <button
                                        class="ui blue button wholesale-request"
                                        data-product-id="{{ $product->id }}"
                                        @if (!auth()->user()->is_active || ($request && $request->status === 'pending'))
                                            disabled
                                        @endif 
                                    >
                                        <i class="clipboard list icon"></i> Request
                                    </button>
                                @endif
                            <div class="or"></div>
                            @endif
                            <button
                                data-url="{{ route('products.watchables.store', $product->id) }}"
                                class="ui yellow @if ($request) fluid @endif button add-to-watchlist"
                                @if (!Gate::check('watch-items', auth()->user()) 
                                      || auth()->user()->productWatchlist()->where(['watchable_type' => 'App\Product', 'watchable_id' => $product->id])->exists())
                                    disabled
                                @endif >
                                <i class="eye icon"></i> Watchlist
                            </button>
                        </div>
                        @if ($product->is_wholesale)
                            @if ($request)
                                @if ($request && $request->status === 'approved')
                                    <i class="text-center mt-2"> Request approved, please proceed to payment. </i>
                                @elseif ($request && $request->status === 'pending')
                                    <i class="text-center mt-2"> Pending Request, please wait for vendor response. </i>
                                @else
                                    <i class="text-center mt-2"> Request denied, please contact support. </i>
                                @endif
                            @else
                                <i class="text-center mt-2"> The Seller will have to approve your <strong> Request </strong> first. </i>                            
                            @endif
                        @endif
                    @else
                        @if ($product->status === 'pending')
                            <div class="ui fluid warning message @if (!Auth::check() || Gate::check('bid-items')) hidden @endif">
                                <p >This product is expired and is currently being processed.</p>
                            </div>
                        @else
                            @if (!$product->bids()->where('user_id', Auth::id())->count())
                                <i class="text-center mb-2"> Please enter amount to place bid. </i>
                                <div class="ui fluid error message @if (!Auth::check() || Gate::check('bid-items')) hidden @endif">
                                    <p >Please activate your account in order to bid.</p>
                                </div>
                                <div class="ui fluid error message bid-error-message hidden">
                                    <p >Bid must be {{ $product->is_auction ? 'higher' : 'lower' }}  than the Starting Bid.</p>
                                </div>
                                <div class="ui fluid left action input mb-2 place-bid-button">
                                    <a 
                                        @guest
                                            href="{{ route('login') }}"
                                        @endguest
                                        type="submit"
                                        class="ui blue labeled icon button @auth @if ($product->is_auction) show-payment-options @else place-reverse-auction-bid @endif @endauth disabled"
                                        data-item-id="{{ $product->id }}"
                                        data-item-type="product"
                                        data-payment-type="bid"
                                    >
                                        <i class="money bill alternate outline icon"></i>
                                        Place Bid
                                    </a>
                                    <input type="number" name="bid" min="0.01" step="0.01" placeholder="$0.00" @if (!Auth::check() || !Gate::check('bid-items')) disabled @endif>
                                </div>
                            @endif
                        @endif
                        <button
                            data-url="{{ route('products.watchables.store', $product->id) }}"
                            class="ui fluid yellow button add-to-watchlist mt-2"
                            @if (!Gate::check('watch-items', auth()->user()) 
                                  || auth()->user()->productWatchlist()->where(['watchable_type' => 'App\Product', 'watchable_id' => $product->id])->exists())
                                disabled
                            @endif 
                        >
                            <i class="eye icon"></i> Watchlist
                        </button>
                    @endif
                </div>
            @endif
            @if ($product->is_reverse_auction && $product->status === 'pending' && Auth::id() === $product->user->id)
                <div class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-lg p-5 w-full lg:w-2/3">
                    <div class="text-center">
                        @php
                            $winningBid = $product->bids()->orderBy('bid')->first();
                            $winner = $winningBid->user;
                        @endphp
                        <div class="flex items-center">
                            <img
                                class="w-24 h-24 rounded-full mr-5"
                                src="{{ asset($winner->image->src ?? $winner->profile_placeholder) }}"
                            />
                            <div class="w-full">
                                <div class="mb-2">
                                    <p class="text-blue-700 leading-none">
                                        {{ $winner->full_name }}
                                    </p>
                                    <p class="text-gray-900">
                                        @isset($winner->address)
                                            {{ Str::limit($winner->address->partial, 25) }}
                                        @endisset
                                    </p>
                                </div>
                                <button
                                    class="ui fluid blue button show-payment-options"
                                    data-item-id="{{ $product->id }}"
                                    data-item-type="product"
                                    data-payment-type="single"
                                >
                                    Pay Winner
                                </button>    
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </div>
    </div>
    <div class="my-5 mx-3 md:mx-5 lg:mx-20 md:flex justify-between">
        <div class="w-full md:w-2/3">
            <div class="my-10">
                <div class="font-bold text-blue-700 text-2xl mb-5">Description</div>
                <p class="text-gray-900 text-base">
                    {!! $product->description !!}
                </p>
            </div>
            <div class="my-10">
                <div class="font-bold text-blue-700 text-2xl mb-5">Details</div>
                <div class="flex">
                    <div class="mr-20 w-1/2">
                        @if ($product->quantity)
                            <div class="ui list">
                                <div class="item mb-5">
                                    <div class="font-bold text-blue-700">Quantity</div>
                                    <p class="text-gray-900 text-lg">
                                        {{ $product->quantity }}
                                    </p>
                                </div>
                            </div>
                        @endif
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Condition</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $product->condition ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="ml-20 w-1/2">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Pick Up Type</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $product->pick_up_type ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        @if ($product->closed_at)
                            <div class="ui list">
                                <div class="item mb-5">
                                    <div class="font-bold text-blue-700">Close Time</div>
                                    <p class="text-gray-900 text-lg">
                                        {{ $product->closed_at ? $product->closed_at->diffForHumans() : 'Not Indicated' }}
                                    </p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>            
            <div class="my-10">
                <div class="font-bold text-blue-700 text-2xl mb-5">Address</div>
                <div class="flex">
                    <div class="mr-20 w-1/2">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Country</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $product->address->country ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">State</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $product->address->state ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Suburb</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $product->address->suburb ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="ml-20 w-1/2">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">City</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $product->address->city ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Post Code</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $product->address->post_code ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Street Name</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $product->address->street_name ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Number</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $product->address->number ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>
        <div class="w-full md:w-1/3 md:m-10 mt-10">
            <div class="rounded-lg flex bg-gray-100 shadow hover:shadow-lg w-full p-5">
                <div class="flex items-center">
                    <img
                        class="w-20 h-20 rounded-full mr-5"
                        src="{{ asset($product->user->image->src ?? $product->user->profile_placeholder) }}"
                    />
                    <div>
                        <p class="text-blue-700 leading-none">
                            {{ $product->user->full_name }}
                        </p>
                        <p class="text-gray-900">
                            @isset($product->user->address)
                                {{ Str::limit($product->user->address->partial, 25) }}
                            @endisset
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="my-5 mx-3 mb-20 md:mx-5 lg:mx-20 flex items-center justify-center">
        <div class="ui threaded comments w-full md:w-1/2">
            <h3 class="ui dividing header blue">
                Questions and Answers ({{ $product->comments->count() }})
            </h3>
            @foreach ($product->comments as $comment)
                <div class="comment">
                    <span class="avatar">
                        <img
                            src="{{ asset($comment->user->image->src ?? $comment->user->profile_placeholder) }}"
                        />
                    </span>
                    <div class="content">
                        <span class="author">{{ $comment->user->full_name }}</span>
                        <div class="metadata">
                            <span class="date"
                                >{{ $comment->created_at ? $comment->created_at->diffForHumans() : 'Not Indicated' }}</span
                            >
                            @if ($comment->reply)
                                <span class="ui mini green label">Answered</span>
                            @endif
                        </div>
                        <div class="text">
                            <p class="text-lg">
                                {{ $comment->comment }}
                            </p>
                        </div>
                        @if (!$comment->reply)
                            @can('update-comment', $comment)
                                <div class="actions reply-comment">
                                    <a class="reply">Reply</a>
                                </div>
                                <form
                                    action="{{ route('comments.update', $comment->id) }}"
                                    method="POST"
                                    class="ui reply form hidden"
                                >
                                    @csrf
                                    @method('PUT')
                                    <input
                                        type="hidden"
                                        name="user_id"
                                        value="{{ Auth::id() }}"
                                    />
                                    <input
                                        type="hidden"
                                        name="comment"
                                        value="{{ $comment->comment }}"
                                    />
                                    <input
                                        type="hidden"
                                        name="replied_at"
                                        value="{{ now() }}"
                                    />
                                    <div class="field">
                                        <textarea name="reply"></textarea>
                                    </div>
                                    <button
                                        type="submit"
                                        class="ui primary submit labeled icon button"
                                    >
                                        <i class="icon edit"></i> Add Reply
                                    </button>
                                </form>
                            @endcan
                        @endif
                    </div>
                    @if ($comment->reply)
                        <div class="comments">
                            <div class="comment">
                                <span class="avatar">
                                    <img
                                        src="{{ asset($comment->commentable->user->image->src ?? $comment->commentable->user->profile_placeholder) }}"
                                    />
                                </span>
                                <div class="content">
                                    <span class="author"
                                        >{{ $comment->commentable->user->full_name }}</span
                                    >
                                    <div class="metadata">
                                        <span class="date"
                                            >{{ $comment->replied_at->diffForHumans()
                                            }}</span
                                        >
                                    </div>
                                    <div class="text">
                                        <p class="text-lg">
                                            {{ $comment->reply }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
            @auth
                @if (Gate::check('create-comment'))
                    <form
                        action="{{ route('products.comments.store', $product->id) }}"
                        method="post"
                        class="ui reply form"
                    >
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::id() }}" />
                        <div class="field">
                            <textarea name="comment"></textarea>
                        </div>
                        <button type="submit" class="ui primary submit labeled icon button">
                            <i class="icon edit"></i> Add Comment
                        </button>
                    </form>
                @else
                    <div class="ui message">
                        <p> Please activate your account in order to post a comment. </p>
                    </div>
                @endif
            @endauth
        </div>
    </div>      
    @include('partials.modals.payment-gateways')
    @include('partials.modals.place-bid')
@endsection

@push('scripts')
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
    <script>
        const stripe = Stripe("{{ config('services.stripe.key') }}");

        $(".show-payment-options").on("click", function() {
            $("#paymentOptions")
                .modal({
                    blurring: true,
                    inverted: true
                })
                .modal("show");

            let itemID = $(this).data("item-id");
            let itemType = $(this).data("item-type");
            let paymentType = $(this).data("payment-type");
            let bid = $(this).data("bid");

            $("#stripePaymentOption").on("click", function() {
                $.post({
                    url: "{{ route('stripe.checkout') }}",
                    data: {
                        item_id: itemID,
                        item_type: itemType,
                        payment_type: paymentType,
                        bid: bid
                    },
                    success: async function(res) {
                        if (res.success) {
                            const { error } = await stripe.redirectToCheckout({
                                sessionId: res.response.id
                            });
                        }
                    }
                });
            });
        });
    </script>
    <script>
        $(".reply-comment").on("click", function() {
            $(this)
                .next()
                .toggleClass("hidden");
        });
    </script>
    <script>
        let type = "{{ $product->type }}";
        let startingBid =
            "{{ $product->bids->count() ? $product->bids->max('bid') : $product->formatted_price }}";
        startingBid = parseInt(startingBid.replace(",", ""));
    
        $("input[name='bid']").on("change keyup keydown keypress", function (
            event
        ) {
            let invalidChars = ["-", "+", "e"];
    
            if (invalidChars.includes(event.key)) {
                event.preventDefault();
            }
    
            if (type === "auction" && $(this).val() <= startingBid) {
                $(".place-bid-button a").addClass("disabled");
                $(".place-bid-button").addClass("error");
                $(".bid-error-message").removeClass("hidden");
            } else if (type === "reverse_auction" && $(this).val() >= startingBid) {
                $(".place-bid-button a").addClass("disabled");
                $(".place-bid-button").addClass("error");
                $(".bid-error-message").removeClass("hidden");
            } else {
                $(".place-bid-button").removeClass("error");
                $(".place-bid-button a").removeClass("disabled");
                $(".place-bid-button a").attr("data-bid", $(this).val());
                $(".bid-error-message").addClass("hidden");
            }
        });
    </script>
    <script>
        $('.place-reverse-auction-bid').on("click", function () {
            var action = "{{ route('products.bids.store', ':id') }}"
            var action = action.replace(':id', $(this).data("item-id"))

            $('#reverseAuctionBidForm').attr('action', action)
            $('#reverseAuctionBidForm [name="user_id"]').val("{{ Auth::id() }}")
            $('#reverseAuctionBidForm [name="bid"]').val($(this).data("bid"))
            $('#reverseAuctionBidForm').submit()
        })
    </script>
@endpush
