@extends('layouts.app')

@section('title')
    Contact Us
@endsection

@section('content')
    @if($errors->any())
        <div class="my-5 mx-3 lg:mx-20">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 lg:my-10 mb-20 mx-3 lg:mx-20 flex items-center justify-center">
        <div class="ui segments w-full lg:w-1/2">
            <div class="ui padded segment blue">
                <h4 class="ui block header blue">Contact Us</h4>
                <form
                    method="POST"
                    class="ui error form"
                    action="{{ route('contact') }}"
                >
                    @csrf
                    <div class="two fields">
                        <div class="field">
                            <label>Name</label>
                            <input
                                type="text"
                                name="name"
                                placeholder="Full Name"
                                value="{{ old('name') }}"
                                required
                            />
                        </div>
                        <div class="field">
                            <label>Subject</label>
                            <input type="text" name="subject" placeholder="Ex. Partnership" value="{{ old("subject") }}" required />
                        </div>
                    </div>
                    <div class="field">
                        <label>Email</label>
                        <input
                            type="email"
                            name="email"
                            placeholder="Someone@gmail.com"
                            value="{{ old('email') }}"
                            required
                        />
                    </div>
                    <div class="field">
                        <label>Message</label>
                        <textarea name="message" required></textarea
                        >
                    </div>
                    <button type="submit" class="ui fluid button primary" tabindex="0">
                        Send
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
