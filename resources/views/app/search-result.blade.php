@extends('layouts.app')

@section('title')
    Search Result
@endsection

@section('content')
    <div class="mt-10 mb-20">
        <div class="my-5 mx-3 lg:mx-20">
            @forelse ($products->chunk(4) as $chunk)
                <div class="md:flex justify-center mb-5">
                    @foreach ($chunk as $product)
                        <div class="px-0 lg:px-5 mb-3 w-full lg:w-1/4">
                            <a href="{{ route('products.show', $product->id) }}">
                                <div
                                    style="height: 350px" 
                                    class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl">
                                    <div style="height: 200px">
                                        @if ($product->images->count())
                                            <div
                                                class="rounded-t-lg relative w-full h-full overflow-hidden"
                                            >
                                                <img
                                                    class="z-10 relative w-full h-full object-cover"
                                                    src="{{ asset($product->images->random()->src) }}"
                                                />
                                            </div>
                                        @else
                                            <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="flex flex-col mx-5">
                                        <span class="text-gray-900 my-2">
                                            @isset($product->address)
                                                {{ Str::limit($product->address->partial, 25) }}
                                            @endisset
                                        </span>
                                        <span class="item text-blue-700 font-bold">
                                            {{ Str::limit($product->name, 50) }}
                                        </span>
                                    </div>
                                    <div class="m-5 h-full font-bold flex flex-col-reverse">
                                        @if ($product->is_auction || $product->is_reverse_auction)
                                            <span class="text-xl">
                                                <small class="font-thin italic"> 
                                                    {{ $product->bids->count() ? 'Current Bid' : 'Starting Bid' }}:&nbsp;
                                                </small>
                                                ${{ $product->formatted_starting_bid }}
                                            </span>
                                        @elseif ($product->is_wholesale)
                                            <span class="text-base mt-1">
                                                <small class="font-thin italic"> Wholesale Price: &nbsp; </small>
                                                ${{ $product->formatted_wholesale_price }}
                                            </span>
                                            <span class="text-xl">
                                                <small class="font-thin italic"> RRP: &nbsp; </small>
                                                ${{ $product->formatted_price }}
                                            </span>
                                        @else
                                            <span class="text-xl">${{ $product->formatted_price }}</span>
                                        @endif
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @empty
                <div class="ui disabled header centered">
                    No Result Found.
                </div>
            @endforelse
        </div>
        @if ($products->count())
            <div class="my-5 mx-3 lg:mx-20 flex items-center justify-center">
                {{ $products->appends([ 
                    'sort' => request('sort'), 
                    'desc' => request('desc'), 
                    'filter' => request('filter')
                ])->links('vendor.pagination.semantic-ui') }}
            </div>
        @endif
    </div>
@endsection
