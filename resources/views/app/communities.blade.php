@extends('layouts.app')

@section('title')
    Communities
@endsection

@section('content')
    <div class="mt-10 mb-20">
        <div class="my-5 mx-3 md:mx-5 lg:mx-20 ">
            @forelse ($communities->chunk(3) as $chunk)
                <div class="md:flex md:flex-wrap justify-center mb-5 -mx-3">
                    @forelse($chunk as $community)
                        <div class="px-3 lg:px-5 mb-3 w-full md:w-1/2 lg:w-1/3">
                            <a href="{{ route('communities.show', $community->id) }}">
                                <div
                                    style="height: 100px"
                                    class="rounded-lg flex shadow hover:shadow-xl"
                                >
                                    <div class="w-1/3">
                                        @if ($community->images->count())
                                            <div
                                                class="rounded-l-lg relative w-full h-full overflow-hidden"
                                            >
                                                <img
                                                    class="z-10 relative w-full h-full object-cover"
                                                    src="{{ asset($community->images->random()->src) }}"
                                                />
                                            </div>
                                        @else
                                            <div
                                                class="rounded-l-lg relative w-full h-full overflow-hidden"
                                            >
                                                <img
                                                    class="z-10 relative w-full h-full object-cover"
                                                    src="{{ asset('img/placeholder.jpg') }}"
                                                />
                                            </div>
                                        @endif
                                    </div>
                                    <div class="w-2/3">
                                        <div class="flex flex-col mx-5">
                                            <span class="text-gray-900 my-2 mt-6">
                                                {{ Str::limit($community->po_box_address, 25) }}
                                            </span>
                                            <span class="item text-blue-700 font-bold">
                                                {{ Str::limit($community->name, 50) }}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @empty
                        <div class="ui disabled header centered">
                            No Community Listing Yet.
                        </div>
                    @endforelse
                </div>
            @empty
                <div class="ui disabled header centered">
                    No Community Listing Yet.
                </div>
            @endforelse
        </div>
        @if ($communities->count())
            <div class="my-5 mx-3 flex items-center justify-center">
                {{ $communities->links('vendor.pagination.semantic-ui') }}
            </div>
        @endif
    </div>
@endsection
