@extends('layouts.app')

@section('title')
    Travel
@endsection

@push('styles')
    <style>
        input[type="date"]:before {
            content: attr(placeholder) !important;
            color: #aaa;
            margin-right: 0.5em;
        }
        input[type="date"]:focus:before,
        input[type="date"]:valid:before {
            content: "";
        }
    </style>
@endpush

@section('content')
    <div class="my-5 mt-10 mx-3 md:mx-5 lg:mx-20 flex items-center justify-center">
        <div class="ui raised segments w-full lg:w-5/6">
            <div class="ui padded segment blue">
                <form
                    id="travelSearchForm"
                    class="ui form"
                    action="{{ route('search.travel') }}"
                    method="GET"
                >
                    @csrf
                    <input type="hidden" name="category" value="{{ request('category') }}">
                    <div class="three fields">
                        <div class="field">
                            <div class="ui fluid selection dropdown">
                                <input
                                    type="hidden"
                                    name="country"
                                    value="{{ request('country') }}"
                                />
                                <i class="dropdown icon"></i>
                                <div class="default text">Country</div>
                                <div class="menu">
                                    <div class="item" data-value="">
                                        Anywhere
                                    </div>
                                    <div class="item" data-value="New Zealand">
                                        New Zealand
                                    </div>
                                    <div class="item" data-value="Australia">
                                        Australia
                                    </div>
                                    <div class="item" data-value="International">
                                        International
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <input
                                type="text"
                                name="state"
                                placeholder="State"
                                value="{{ request('state') }}"
                            />
                        </div>
                        <div class="field">
                            <input
                                type="text"
                                name="suburb"
                                placeholder="Suburb"
                                value="{{ request('suburb') }}"
                            />
                        </div>
                    </div>
                    <div class="five fields">
                        <div class="field">
                            <input
                                type="text"
                                name="city"
                                placeholder="City"
                                value="{{ request('city') }}"
                            />
                        </div>
                        <div class="field">
                            <input
                                type="number"
                                name="post_code"
                                placeholder="Post Code"
                                onkeypress="return this.value.length < 4;"
                                value="{{ request('post_code') }}"
                            />
                        </div>
                        <div class="field">
                            <input
                                type="text"
                                name="street_name"
                                placeholder="Street Name"
                                value="{{ request('street_name') }}"
                            />
                        </div>
                        <div class="field">
                            <input
                                type="number"
                                name="number"
                                placeholder="Number"
                                value="{{ request('number') }}"
                            />
                        </div>
                        <div class="field">
                            <button type="submit" class="ui button fluid blue">
                                Search
                            </button>
                        </div>
                    </div>
                    <div class="ui fluid accordion">
                        <div class="title">
                            <i class="dropdown icon"></i>
                            Advanced Search
                        </div>
                        <div class="content">
                            <div class="two fields">
                                <div class="eight wide field">
                                    <input
                                        type="text"
                                        name="keywords"
                                        placeholder="Keywords"
                                        value="{{ request('keywords') }}"
                                    />
                                </div>
                                <div class="eight wide field">
                                    <div class="ui fluid search selection dropdown">
                                        <input type="hidden" name="type" />
                                        <i class="dropdown icon"></i>
                                        <div class="default text">
                                            Property Type
                                        </div>
                                        <div class="menu">
                                            @foreach ($travelCategories as $travelCategory)
                                                <div class="item" data-value="{{ $travelCategory->name }}">
                                                    {{ $travelCategory->name }}
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="four fields">
                                <div class="three wide field">
                                    <input
                                        type="number"
                                        name="price_from"
                                        placeholder="Price From"
                                        value="{{ request('price_from') }}"
                                    />
                                </div>
                                <div class="three wide field">
                                    <input
                                        type="number"
                                        name="price_to"
                                        placeholder="Price To"
                                        value="{{ request('price_to') }}"
                                    />
                                </div>
                                <div class="five wide field">
                                    <input
                                        type="date"
                                        name="date_from"
                                        placeholder="Date From: "
                                        value="{{ request('date_from') }}"
                                    />
                                </div>
                                <div class="five wide field">
                                    <input
                                        type="date"
                                        name="date_to"
                                        placeholder="Date To: "
                                        value="{{ request('date_to') }}"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="my-10 mb-20 mx-3 md:mx-5 lg:mx-20 hidden md:flex">
        @foreach ($travelCategories->chunk(20) as $chunk)
            <div class="lg:w-1/5">
                <div class="ui link list">
                    @foreach ($chunk as $travelCategory)
                        <a
                            type="button"
                            class="item category-search"
                            data-category="{{ $travelCategory->name }}"
                            >{{ $travelCategory->name }}</a
                        >
                    @endforeach

                </div>
            </div>
        @endforeach
    </div>
    <div class="mb-20">
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            @forelse ($travels->chunk(4) as $chunk)
                <div class="md:flex md:flex-wrap justify-center mb-5 -mx-3">
                    @foreach ($chunk as $travel)
                        <div class="px-3 lg:px-5 mb-3 w-full md:w-1/2 lg:w-1/4">
                            <a href="{{ route('travels.show', $travel->id) }}">
                                <div
                                    style="height: 320px"
                                    class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-2xl"
                                >
                                    <div style="height: 200px">
                                        @if ($travel->images->count())
                                            <div
                                                class="rounded-t-lg relative w-full h-full overflow-hidden"
                                            >
                                                <img
                                                    class="z-10 relative w-full h-full object-cover"
                                                    src="{{ asset($travel->images->random()->src) }}"
                                                />
                                            </div>
                                        @else
                                            <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                                <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                            </div>
                                        @endif
                                    </div>
                                    <div class="flex flex-col mx-5">
                                        <span class="text-gray-900 my-2">
                                            @isset($travel->address)
                                                {{ Str::limit($travel->address->partial, 25) }}
                                            @endisset
                                        </span>
                                        <span
                                            class="item text-blue-700 font-bold"
                                            >{{ Str::limit($travel->name, 50) }}</span
                                        >
                                    </div>
                                    <div class="m-5 h-full font-bold flex flex-col-reverse">
                                        <span class="text-xl">${{ $travel->formatted_price }}</span>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @empty
                <div class="ui disabled header centered">
                    No Travel Listing Yet.
                </div>
            @endforelse
        </div>
        @if ($travels->count())
            <div class="my-5 mx-3 lg:mx-20 flex items-center justify-center">
                {{ $travels->appends([ 'keywords' => request('keywords'), 'category' =>
                request('category'), 'country' => request('country'), 'state' =>
                request('state'), 'suburb' => request('suburb'), 'city' => request('city'),
                'post_code' => request('post_code'), 'price_form' => request('price_form'),
                'price_to' => request('price_to'), 'date_from' => request('bedrooms'),
                'date_to' => request('bathrooms'),
                ])->links('vendor.pagination.semantic-ui') }}
            </div>
        @endif 
    </div>
@endsection

@push('scripts')
    <script>
        $(".category-search").on("click", function() {
            $("input[name='category']").val($(this).data("category"));
            $("#travelSearchForm").submit();
        });
    </script>
@endpush