@extends('layouts.app')

@section('title')
    Cart
@endsection

@section('content')
    @if($errors->any())
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 lg:my-10 mb-20 lg:mb-40 mx-3 md:mx-5 lg:mx-20 flex flex-col lg:flex-row justify-center">
        <div class="w-full lg:w-2/6 mr-5 mb-3">
            <div class="shadow rounded-lg p-5">
                <div class="font-bold text-gray-900 text-xl mb-3">
                    {{ auth()->user()->full_name }}
                </div>
                <hr class="mb-3" />
                <div class="mb-3 flex justify-between items-center">
                    <p class="leading-none">Quantity</p>
                    <p class="text-xl text-gray-900 font-bold">
                        {{ $cart->count() ?? 0 }} Item(s)
                    </p>
                </div>
                <div class="mb-3 flex justify-between items-center">
                    <p class="leading-none">Total Quantity</p>
                    <p class="text-xl text-gray-900 font-bold">
                        {{ $cart->totalCartItems() ?? 0 }} Item(s)
                    </p>
                </div>
                <div class="mb-3 flex justify-between items-center">
                    <p class="leading-none">Total Price</p>
                    <p class="text-xl text-gray-900 font-bold">
                        ${{ number_format($cart->totalCartPrice(), 2, '.', ',') ?? 0 }}
                    </p>
                </div>
                @if ($cart->count())
                    <button
                        class="ui fluid primary button show-paymemt-options"
                        data-payment-type="cart"
                    >
                        Pay
                    </button>
                @endif
            </div>
        </div>
        <div class="w-full lg:w-4/6 ml-0 lg:ml-5">
            @forelse ($cart as $item)
                <div class="shadow hover:shadow-lg rounded-lg p-5 mb-5">
                    <div class="flex justify-between mb-3">
                        <div class="font-bold text-gray-900 text-xl">
                            {{ Str::limit($item->name, 50) }}
                        </div>
                        <div>
                            <div class="ui icon compact buttons">
                                <button
                                    class="ui button update-cart"
                                    data-url="{{ route('cart.update', ['id' => $item->id]) }}"
                                    data-quantity="{{ $item->quantity + 1 }}"
                                >
                                    <i class="plus icon"></i>
                                </button>
                                <button
                                    class="ui button update-cart"
                                    data-url="{{ route('cart.update', ['id' => $item->id]) }}"
                                    data-quantity="{{ $item->quantity - 1 }}"
                                >
                                    <i class="minus icon"></i>
                                </button>
                                <button
                                    class="ui red button remove-to-cart"
                                    data-url="{{ route('cart.remove', ['id' => $item->id]) }}"
                                >
                                    <i class="trash icon"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <hr class="mb-3" />
                    <div class="mb-3 flex justify-between items-center">
                        <p class="leading-none">Quantity</p>
                        <p class="text-xl text-gray-900 font-bold">
                            {{ $item->quantity }} Item(s)
                        </p>
                    </div>
                    <div class="mb-3 flex justify-between items-center">
                        <p class="leading-none">Price</p>
                        <p class="text-xl text-gray-900 font-bold">
                            ${{ number_format($item->price, 2, '.', ',') }}
                        </p>
                    </div>
                    <div class="flex justify-between items-center">
                        <p class="leading-none">Total Price</p>
                        <p class="text-xl text-gray-900 font-bold">
                            ${{ number_format(($item->quantity * $item->price), 2, '.', ',') }}
                        </p>
                    </div>
                </div>
            @empty
                <div class="shadow hover:shadow-lg rounded-lg p-5 mb-5">
                    <div class="font-bold text-gray-900 text-xl">Empty Cart.</div>
                </div>
            @endforelse
        </div>
    </div>
    @include('partials.modals.payment-gateways')
@endsection

@push('scripts')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        const stripe = Stripe("{{ config('services.stripe.key') }}");

        $(".show-paymemt-options").on("click", function() {
            $("#paymentOptions")
                .modal({
                    blurring: true,
                    inverted: true
                })
                .modal("show");

            let paymentType = $(this).data("payment-type");

            $("#stripePaymentOption").on("click", function() {
                $.post({
                    url: "{{ route('stripe.checkout') }}",
                    data: {
                        item_id: null,
                        item_type: null,
                        payment_type: paymentType
                    },
                    success: async function(res) {
                        if (res.success) {
                            const { error } = await stripe.redirectToCheckout({
                                sessionId: res.response.id
                            });
                        }
                    }
                });
            });
        });
    </script>
@endpush
