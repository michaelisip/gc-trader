@extends('layouts.app')

@section('title')
    {{ $service->name }}
@endsection

@push('styles')
    <link
    href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css"
    rel="stylesheet"
    />
@endpush

@section('content')
    @if($errors->any())
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 mt-10 mx-3 md:mx-5 lg:mx-20 md:flex justify-center">
        @if ($service->images->count())
            <div class="w-full md:w-2/5 lg:w-2/6 mr-5">
                <div
                    class="fotorama"
                    data-nav="thumbs"
                    data-allowfullscreen="true"
                    data-loop="true"
                    data-autoplay="true"
                    data-width="100%"
                >
                @foreach ($service->images as $image)
                    <a href="{{ asset($image->src) }}">
                        <img src="{{ asset($image->src) }}" />
                    </a>
                @endforeach
                </div>
            </div>              
        @else
            <div class="w-full md:w-2/5 lg:w-2/6 mr-5">
                <div class="ui rounded-lg">
                    <img src="{{ asset('img/placeholder.jpg') }}" />
                </div>
            </div>             
        @endif
        <div class="w-full md:w-3/5 lg:w-4/6 lg:ml-5 mt-5 md:mt-0">
            <h1 class="font-bold text-gray-900 text-4xl mb-5">
                {{ $service->name }}
            </h1>
            <div class="my-10">
                <div class="font-bold text-blue-700 text-2xl mb-5">Description</div>
                <p class="text-gray-900 text-lg">
                    {!! $service->description !!}
                </p>
            </div>
            <div class="my-10">
                <div class="font-bold text-blue-700 text-2xl mb-5">Details</div>
                <div class="flex">
                    <div class="mr-20 w-1/2">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Company Name</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $service->company_name }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Is ABN Registered</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $service->is_abn_registered ? 'Yes' : 'No' }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="ml-20 w-1/2">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">ABN Registration</div>
                                <p class="text-gray-900 text-lg">
                                    @if ($service->abn_registration)
                                        <a href="https://www.acnc.gov.au" target="__blank" rel="noopener noreferrer">
                                            {{ $service->abn_registration }}
                                            <i class="globe icon"></i>
                                        </a>
                                    @else
                                        Not Indicated
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Website URL</div>
                                <p class="text-gray-900 text-lg">
                                    @if ($service->website_url)
                                        <a href="{{ $service->website_url }}" target="__blank" rel="noopener noreferrer">
                                            {{ parse_url($service->website_url)['host'] }}
                                        </a>
                                    @else
                                        Not Indicated
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
            <div class="my-5">
                <div class="font-bold text-blue-700 text-2xl mb-5">Address</div>
                <div class="flex">
                    <div class="mr-20">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Country</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $service->address->country ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">State</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $service->address->state ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Suburb</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $service->address->suburb ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="ml-20">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">City</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $service->address->city ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Post Code</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $service->address->post_code ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Street Name</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $service->address->street_name ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Number</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $service->address->number ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rounded-lg flex bg-gray-100 shadow hover:shadow-lg w-full lg:w-2/3 p-5">
                <div class="flex items-center">
                    <img
                        class="w-20 h-20 rounded-full mr-5"
                        src="{{ asset($service->user->image->src ?? $service->user->profile_placeholder) }}"
                    />
                    <div>
                        <p class="text-blue-700 leading-none">
                            {{ $service->user->full_name }}
                        </p>
                        <p class="text-gray-900">
                            @isset($service->user->address)
                                {{ Str::limit($service->user->address->partial, 25) }}
                            @endisset
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="my-10 mb-20 mx-3 lg:mx-20 flex items-center justify-center">
        <div class="ui threaded comments w-full md:w-1/2">
            <h3 class="ui dividing header blue">
                Questions and Answers ({{ $service->comments->count() }})
            </h3>
            @foreach ($service->comments as $comment)
                <div class="comment">
                    <span class="avatar">
                        <img
                            src="{{ asset($comment->user->image->src ?? $comment->user->profile_placeholder) }}"
                        />
                    </span>
                    <div class="content">
                        <span class="author">{{ $comment->user->full_name }}</span>
                        <div class="metadata">
                            <span class="date"
                                >{{ $comment->created_at ? $comment->created_at->diffForHumans() : 'Not Indicated' }}</span
                            >
                            @if ($comment->reply)
                                <span class="ui mini green label">Answered</span>
                            @endif
                        </div>
                        <div class="text">
                            <p class="text-lg">
                                {{ $comment->comment }}
                            </p>
                        </div>
                        @if (!$comment->reply)
                            @can('update-comment', $comment)
                                <div class="actions reply-comment">
                                    <a class="reply">Reply</a>
                                </div>
                                <form
                                    action="{{ route('comments.update', $comment->id) }}"
                                    method="POST"
                                    class="ui reply form hidden"
                                >
                                    @csrf
                                    @method('PUT')
                                    <input
                                        type="hidden"
                                        name="user_id"
                                        value="{{ Auth::id() }}"
                                    />
                                    <input
                                        type="hidden"
                                        name="comment"
                                        value="{{ $comment->comment }}"
                                    />
                                    <input
                                        type="hidden"
                                        name="replied_at"
                                        value="{{ now() }}"
                                    />
                                    <div class="field">
                                        <textarea name="reply"></textarea>
                                    </div>
                                    <button
                                        type="submit"
                                        class="ui primary submit labeled icon button"
                                    >
                                        <i class="icon edit"></i> Add Reply
                                    </button>
                                </form>
                            @endcan
                        @endif
                    </div>
                    @if ($comment->reply)
                        <div class="comments">
                            <div class="comment">
                                <span class="avatar">
                                    <img
                                        src="{{ asset($comment->commentable->user->image->src ?? $comment->commentable->user->profile_placeholder) }}"
                                    />
                                </span>
                                <div class="content">
                                    <span class="author"
                                        >{{ $comment->commentable->user->full_name }}</span
                                    >
                                    <div class="metadata">
                                        <span class="date"
                                            >{{ $comment->replied_at->diffForHumans()
                                            }}</span
                                        >
                                    </div>
                                    <div class="text">
                                        <p class="text-lg">
                                            {{ $comment->reply }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
            @auth
                @if (Gate::check('create-comment'))
                    <form
                        action="{{ route('services.comments.store', $service->id) }}"
                        method="post"
                        class="ui reply form disabled"
                    >
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::id() }}" />
                        <div class="field">
                            <textarea name="comment"></textarea>
                        </div>
                        <button type="submit" class="ui primary submit labeled icon button">
                            <i class="icon edit"></i> Add Comment
                        </button>
                    </form>
                @else
                    <div class="ui message">
                        <p> Please activate your account in order to post a comment. </p>
                    </div>
                @endif
            @endauth
        </div>
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
    <script>
        $(".reply-comment").on("click", function() {
            $(this)
                .next()
                .toggleClass("hidden");
        });
    </script>
@endpush
