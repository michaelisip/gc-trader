@extends('layouts.app')

@section('title')
    Real Estates
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css">
    <style>
        @media (min-width: 768px) {
            .real-estate-card-info {
                height: 180px;
            }
        }

        @media (min-width: 1024px) {
            .real-estate-card-info {
                height: 150px;
            }
        }
    </style>
@endpush

@section('content')
    <div class="my-5 mt-10 mx-3 md:mx-5 lg:mx-20 md:flex items-center justify-center">
        <div class="ui raised segments w-full md:w-5/6">
            <div class="ui padded segment blue">
                <form
                    class="ui form"
                    action="{{ route('search.real-estate') }}"
                    method="GET"
                >
                    @csrf
                    <div class="three fields">
                        <div class="field">
                            <div class="ui fluid selection dropdown">
                                <input
                                    type="hidden"
                                    name="country"
                                    value="{{ request('country') }}"
                                />
                                <i class="dropdown icon"></i>
                                <div class="default text">Country</div>
                                <div class="menu">
                                    <div class="item" data-value="">
                                        Anywhere
                                    </div>
                                    <div class="item" data-value="New Zealand">
                                        New Zealand
                                    </div>
                                    <div class="item" data-value="Australia">
                                        Australia
                                    </div>
                                    <div class="item" data-value="International">
                                        International
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="field">
                            <input
                                type="text"
                                name="state"
                                placeholder="State"
                                value="{{ request('state') }}"
                            />
                        </div>
                        <div class="field">
                            <input
                                type="text"
                                name="suburb"
                                placeholder="Suburb"
                                value="{{ request('suburb') }}"
                            />
                        </div>
                    </div>
                    <div class="five fields">
                        <div class="field">
                            <input
                                type="text"
                                name="city"
                                placeholder="City"
                                value="{{ request('city') }}"
                            />
                        </div>
                        <div class="field">
                            <input
                                type="number"
                                name="post_code"
                                placeholder="Post Code"
                                onkeypress="return this.value.length < 4;"
                                value="{{ request('post_code') }}"
                            />
                        </div>
                        <div class="field">
                            <input
                                type="text"
                                name="street_name"
                                placeholder="Street Name"
                                value="{{ request('street_name') }}"
                            />
                        </div>
                        <div class="field">
                            <input
                                type="number"
                                name="number"
                                placeholder="Number"
                                value="{{ request('number') }}"
                            />
                        </div>
                        <div class="field">
                            <button type="submit" class="ui button fluid blue">
                                Search
                            </button>
                        </div>
                    </div>
                    <div class="ui fluid accordion">
                        <div class="title">
                            <i class="dropdown icon"></i>
                            Advanced Search
                        </div>
                        <div class="content">
                            <div class="two fields">
                                <div class="eight wide field">
                                    <input
                                        type="text"
                                        name="keywords"
                                        placeholder="Keywords"
                                        value="{{ request('keywords') }}"
                                    />
                                </div>
                                <div class="eight wide field">
                                    <select name="type" multiple="" class="ui dropdown">
                                        <option value="">Property Type</option>
                                        <option value="Apartment" 
                                            @if (in_array('Apartment', explode(' ', request('type')))) selected @endif>
                                                Apartment</option>
                                        <option value="Car Park" 
                                            @if (in_array('Car Park', explode(' ', request('type')))) selected @endif>
                                                Car park</option>
                                        <option value="House" 
                                            @if (in_array('House', explode(' ', request('type')))) selected @endif>
                                                House</option>
                                        <option value="Lifestyle Bare Land" 
                                            @if (in_array('Lifestyle Bare Land', explode(' ', request('type')))) selected @endif>
                                                Lifestyle bare land</option>
                                        <option value="Lifestyle Dwelling" 
                                            @if (in_array('Lifestyle Dwelling', explode(' ', request('type')))) selected @endif>
                                                Lifestyle dwelling</option>
                                        <option value="Section" 
                                            @if (in_array('Section', explode(' ', request('type')))) selected @endif>
                                                Section</option>
                                        <option value="Townhouse" 
                                            @if (in_array('Townhouse', explode(' ', request('type')))) selected @endif>
                                                Townhouse</option>
                                        <option value="Unit" 
                                            @if (in_array('Unit', explode(' ', request('type')))) selected @endif>
                                                Unit</option>
                                    </select>
                                </div>
                            </div>
                            <div class="four fields">
                                <div class="four wide field">
                                    <input
                                        type="number"
                                        name="price_from"
                                        placeholder="Price From"
                                        value="{{ request('price_from') }}"
                                    />
                                </div>
                                <div class="four wide field">
                                    <input
                                        type="number"
                                        name="price_to"
                                        placeholder="Price To"
                                        value="{{ request('price_to') }}"
                                    />
                                </div>
                                <div class="four wide field">
                                    <input
                                        type="number"
                                        name="bedrooms"
                                        placeholder="Bedrooms"
                                        value="{{ request('bedrooms') }}"
                                    />
                                </div>
                                <div class="four wide field">
                                    <input
                                        type="number"
                                        name="bathrooms"
                                        placeholder="Bathrooms"
                                        value="{{ request('bathrooms') }}"
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="mb-20">
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            @forelse ($realEstates->chunk(2) as $chunk)
                <div class="md:flex md:flex-wrap justify-center mb-5 -mx-3">
                    @foreach ($chunk as $realEstate)
                        <div class="py-5 px-3 w-full md:w-1/2">
                            <div
                                class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-xl"
                            >
                                <div style="height: 250px">
                                    @if ($realEstate->images->count())
                                        <div class="rounded-t-lg relative w-full h-full overflow-hidden">
                                            <div
                                                class="fotorama"
                                                data-nav="false"
                                                data-allowfullscreen="true"
                                                data-loop="true"
                                                data-autoplay="true"
                                                data-height="250"
                                                data-fit="cover"
                                            >
                                                @foreach ($realEstate->images as $image)
                                                    <a href="{{ asset($image->src) }}">
                                                        <img src="{{ asset($image->src) }}" />
                                                    </a>
                                                @endforeach
                                            </div>
                                        </div>
                                    @else
                                        <div class="ui placeholder rounded-lg-t relative w-full h-full overflow-hidden">
                                            <img src="{{ asset('img/placeholder.jpg') }}" class="w-full h-full object-contain">
                                        </div>
                                    @endif
                                </div>
                                <a href="{{ route('real-estates.show', $realEstate->id) }}">
                                    <div class="md:flex items-center mx-5 real-estate-card-info">
                                        <div class="flex items-center w-full lg:w-1/2 mx-2 lg:mx-5 my-4">
                                            <img
                                                class="w-20 h-20 rounded-full mr-2 lg:mr-5"
                                                src="{{ asset($realEstate->user->image->src ?? $realEstate->user->profile_placeholder) }}"
                                            />
                                            <div>
                                                <p class="text-blue-700 leading-none">
                                                    {{ $realEstate->user->full_name }}
                                                </p>
                                                <p class="text-gray-900">
                                                    @isset($realEstate->user->address)
                                                        {{ Str::limit($realEstate->user->address->partial, 25) }}
                                                    @endisset
                                                </p>
                                            </div>
                                        </div>
                                        <div class="md:flex flex-col w-full lg:w-1/2">
                                            <div class="flex flex-col mx-2 lg:mx-5">
                                                <span class="text-gray-900 my-2">
                                                    @isset($realEstate->address)
                                                        {{  Str::limit($realEstate->address->partial, 25) }}
                                                    @endisset
                                                </span>
                                                <span
                                                    class="item text-lg text-blue-700 font-bold"
                                                    >{{ Str::limit($realEstate->name, 50) }}</span
                                                >
                                            </div>
                                            <div class="mx-2 my-4 lg:m-5 h-full font-bold">
                                                <span class="text-xl">${{ $realEstate->formatted_price }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                </div>
            @empty
                <div class="ui disabled header centered">
                    No Real Estate Listing Yet.
                </div>
            @endforelse
        </div>     
        @if ($realEstates->count())
            <div class="my-5 mx-3 lg:mx-20 flex items-center justify-center">
                {{ $realEstates->appends([ 'keywords' => request('keywords'), 'country' =>
                request('country'), 'state' => request('state'), 'suburb' =>
                request('suburb'), 'city' => request('city'), 'post_code' =>
                request('post_code'), 'price_form' => request('price_form'), 'price_to' =>
                request('price_to'), 'bedrooms' => request('bedrooms'), 'bathrooms' =>
                request('bathrooms'), 'type' => request('type'),
                ])->links('vendor.pagination.semantic-ui') }}
            </div>
        @endif
    </div>
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
@endpush
