@extends('layouts.app')

@section('title')
    New Real Estate
@endsection

@section('content')
    @include('partials.app-account-nav')
    @if($errors->any())
        <div class="my-5 mx-3 md:hidden">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 mx-3 md:mx-5 lg:mx-20 md:flex justify-center">
        <div class="w-full md:w-1/3 mr-5 mb-3">
            <div class="ui raised segments">
                <div class="ui padded segment blue overflow-hidden">
                    <div
                        id="imageFormErrorContainer"
                        class="ui warning message mb-5 hidden"
                    >
                        <p>Something went wrong.</p>
                    </div>
                    <form
                        id="imageForm"
                        class="ui form error flex justify-center"
                        enctype="multipart/form-data"
                    >
                        <div class="ui special cards">
                            <div class="card">
                                <div class="blurring dimmable image">
                                    <div class="ui dimmer">
                                        <div class="content">
                                            <div class="center">
                                                <div
                                                    id="chooseImageButton"
                                                    class="ui inverted button"
                                                >
                                                    Drag or Choose Photo
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <img src="{{ old('photo')[0] ?? asset('img/placeholder.jpg') }}" />
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="flex justify-center">
                        <div class="dropzone-previews">
                          @if (old('photo'))
                              <div class="mt-3 -mb-3">
                                  <span class="font-bold"> {{ count(old('photo')) }} image/s </span>
                              </div>
                          @endif
                        </div>
                    </div>
                </div>
                <p class="text-center pb-5">
                    <i> Maximum of 5 photos. </i>
                </p>
            </div>
        </div>
        <div class="w-full md:w-2/3 tg:ml-5">
            <div class="ui raised segments">
                <div class="ui padded segment blue">
                    <h4 class="ui block header blue">Real Estate Form</h4>
                    <div class="hidden md:block mb-5">
                        @if($errors->any())
                            <div class="ui error message w-full">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    <form
                        method="POST"
                        class="ui form error"
                        action="{{ route('users.real-estates.store', Auth::id()) }}"
                    >
                        @csrf
                        <div id="photoContainer" class="hidden">
                            @if (old('photo'))
                                @foreach (old('photo') as $photo)
                                    <input type="hidden" name="photo[]" value="{{ $photo }}">
                                @endforeach
                            @endif
                        </div>
                        <div class="field @error('name') error @enderror">
                            <label>Name</label>
                            <input
                                type="text"
                                name="name"
                                placeholder="Name"
                                value="{{ old('name') }}"
                            />
                        </div>
                        <div class="field @error('company_name') error @enderror">
                            <label>Company Name</label>
                            <input
                                type="text"
                                name="company_name"
                                placeholder="Company Name"
                                value="{{ old('company_name') }}"
                            />
                        </div>
                        <div class="two fields">
                            <div
                                class="field  @error('bedrooms') error @enderror"
                            >
                                <label>Bedrooms</label>
                                <input
                                    type="number"
                                    name="bedrooms"
                                    placeholder="Bedrooms"
                                    value="{{ old('bedrooms') }}"
                                    min="0"
                                />
                            </div>
                            <div
                                class="field  @error('bathrooms') error @enderror"
                            >
                                <label>Bathrooms</label>
                                <input
                                    type="number"
                                    name="bathrooms"
                                    placeholder="Bathrooms"
                                    value="{{ old('bathrooms') }}"
                                    min="0"
                                />
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field @error('type') error @enderror">
                                <label>Type</label>
                                <div class="ui selection dropdown">
                                    <input type="hidden" name="type" value="{{ old('type') }}" />
                                    <i class="dropdown icon"></i>
                                    <div class="default text">
                                        Type
                                    </div>
                                    <div class="menu">
                                        <div class="item" data-value="Apartment">
                                            Apartment
                                        </div>
                                        <div class="item" data-value="Car Park">
                                            Car Park
                                        </div>
                                        <div class="item" data-value="House">
                                            House
                                        </div>
                                        <div class="item" data-value="Lifestyle Bare Land">
                                            Lifestyle bare land
                                        </div>
                                        <div class="item" data-value="Lifestyle Dwelling">
                                            Lifestyle dwelling
                                        </div>
                                        <div class="item" data-value="Section">
                                            Section
                                        </div>
                                        <div class="item" data-value="Townhouse">
                                            Townhouse
                                        </div>
                                        <div class="item" data-value="Unit">
                                            Unit
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field @error('property_status') error @enderror">
                                <label>Property Status</label>
                                <div class="ui selection dropdown">
                                    <input property_status="hidden" name="property_status" value="{{ old('property_status') }}" />
                                    <i class="dropdown icon"></i>
                                    <div class="default text">
                                        Property Status
                                    </div>
                                    <div class="menu">
                                        <div class="item" data-value="New">
                                            New
                                        </div>
                                        <div class="item" data-value="Used">
                                            Used
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="two fields">
                            <div
                                class="field  @error('price') error @enderror"
                            >
                                <label>Price</label>
                                <input
                                    type="number"
                                    name="price"
                                    placeholder="Price"
                                    value="{{ old('price') }}"
                                    min="0"
                                    step=".01"
                                />
                            </div>
                            <div
                                class="field  @error('size') error @enderror"
                            >
                                <label>Size <small>(m&sup2;)</small></label>
                                <input
                                    type="number"
                                    name="size"
                                    placeholder="Size"
                                    value="{{ old('size') }}"
                                    min="0"
                                />
                            </div>
                        </div>
                        <div class="field  @error('description') error @enderror">
                            <label>Description</label>
                            <textarea name="description">{{ old('description') }}</textarea>
                        </div>
                        @if ($realEstateCategories->count())
                            <h4 class="ui block header blue">Categories</h4>
                            <select
                                name="categories[]"
                                class="ui fluid search dropdown"
                                multiple=""
                            >
                                <option value="">Categories</option>
                                @foreach ($realEstateCategories as $category)
                                    <option value="{{ $category->id }}"
                                        >{{ $category->name }}</option
                                    >
                                @endforeach
                            </select>
                        @endif
                        <h4 class="ui block header blue">Address Details</h4>
                        <div class="two fields">
                            <div class="field @error('country') error @enderror">
                                <label>Country</label>
                                <div class="ui selection dropdown">
                                    <input
                                        type="hidden"
                                        name="country"
                                        value="{{ old('country') }}"
                                    />
                                    <i class="dropdown icon"></i>
                                    <div class="default text">{{ old('country') ?? 'Country' }}</div>
                                    <div class="menu">
                                        <div
                                            class="item"
                                            data-value="International"
                                        >
                                            International
                                        </div>
                                        <div class="item" data-value="Australia">
                                            <i class="au flag"></i> Australia
                                        </div>
                                        <div class="item" data-value="New Zealand">
                                            <i class="nz flag"></i> New Zealand
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="field @error('state') error @enderror">
                                <label>State</label>
                                <input
                                    type="text"
                                    name="state"
                                    placeholder="State"
                                    value="{{ old('state') }}"
                                />
                            </div>
                        </div>
                        <div class="three fields">
                            <div class="field @error('suburb') error @enderror">
                                <label>Suburb</label>
                                <input
                                    type="text"
                                    name="suburb"
                                    placeholder="Suburb"
                                    value="{{ old('suburb') }}"
                                />
                            </div>
                            <div class="field @error('city') error @enderror">
                                <label>City</label>
                                <input
                                    type="text"
                                    name="city"
                                    placeholder="City"
                                    value="{{ old('city') }}"
                                />
                            </div>
                            <div class="field @error('post_code') error @enderror">
                                <label>Post Code</label>
                                <input
                                    type="number"
                                    name="post_code"
                                    placeholder="Post Code"
                                    onkeypress="return this.value.length < 4;"
                                    value="{{ old('post_code') }}"
                                />
                            </div>
                        </div>
                        <div class="two fields">
                            <div class="field @error('street_name') error @enderror">
                                <label>Street Name</label>
                                <input
                                    type="text"
                                    name="street_name"
                                    placeholder="Street Name"
                                    value="{{ old('street_name') }}"
                                />
                            </div>
                            <div class="field @error('number') error @enderror">
                                <label>Number</label>
                                <input
                                    type="number"
                                    name="number"
                                    placeholder="Number"
                                    value="{{ old('number') }}"
                                />
                            </div>
                        </div>
                        <button
                            type="submit"
                            class="ui button primary"
                            tabindex="0"
                        >
                            Add New Real Estate
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('partials.app-dropzone-preview')
@endsection

@push('scripts')
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script>
        $('#imageForm').dropzone({
            url: '{{ route('images.store') }}',
            headers: {
                'X-CSRF-TOKEN': $(`meta[name='csrf-token']`).attr('content'),
            },
            paramName: 'photo',
            clickable: ['#chooseImageButton'],
            chunking: true,
            maxFiles: 5,
            acceptedFiles: 'image/*',
            previewsContainer: '.dropzone-previews',
            previewTemplate: document.querySelector('#tpl').innerHTML,
            success: function (file) {
                let serverResponse = JSON.parse(file.xhr.response);

                if (!file.status === 'success') {
                    $('#imageFormErrorContainer p').text('Something went wrong.');
                    $('#imageFormErrorContainer').removeClass('hidden');
                    return false;
                }

                if (!serverResponse.success) {
                    $('#imageFormErrorContainer p').text('Something went wrong. Please contact support.');
                    $('#imageFormErrorContainer').removeClass('hidden');
                    $("div[data-dz-remove]").click()
                    return false;
                }

                $("#imageFormErrorContainer").addClass("hidden");
                $('#photoContainer').append(`<input type="hidden" name="photo[]" value="${serverResponse.src}">`)
                $('#imageForm img').attr('src', serverResponse.src)
            },
            uploadprogress: function(file, progress, bytesSent) {
                $('.ui.progress').progress({
                    percent: progress
                })
            },
            init: function() {
                this.on("addedfile", function(event) {
                    while (this.files.length > this.options.maxFiles) {
                        this.removeFile(this.files[0]);
                    }
                });
            }
        });
    </script>
@endpush
