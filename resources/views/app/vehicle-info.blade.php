@extends('layouts.app')

@section('title')
    {{ $vehicle->name }}
@endsection

@push('styles')
    <link
    href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css"
    rel="stylesheet"
    />
@endpush

@section('content')
    @if($errors->any())
        <div class="my-5 mx-3 md:mx-5 lg:mx-20">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 mx-3 md:mx-5 lg:mx-20 md:flex justify-center">
        @if ($vehicle->images->count())
            <div class="w-full md:w-1/2 mr-5">
                <div
                    class="fotorama"
                    data-nav="thumbs"
                    data-allowfullscreen="true"
                    data-loop="true"
                    data-autoplay="true"
                    data-width="100%"
                >
                    @foreach ($vehicle->images as $image)
                        <a href="{{ asset($image->src) }}">
                            <img src="{{ asset($image->src) }}" />
                        </a>
                    @endforeach
                </div>
            </div>              
        @else
            <div class="w-full md:w-1/2 mr-5">
                <div class="ui rounded-lg">
                    <img src="{{ asset('img/placeholder.jpg') }}" />
                </div>
            </div>
        @endif
        <div class="w-full md:w-1/2 lg:ml-5 mt-5 md:mt-0">
            <h1 class="font-bold text-gray-900 text-3xl mb-5">
                {{ $vehicle->name }}
            </h1>
            @if (!$vehicle->user()->find(auth()->id()))
                <div class="rounded-lg flex flex-col bg-gray-100 shadow hover:shadow-lg p-5 w-full lg:w-2/3">
                    <div class="mb-5 text-center">
                        <h1 class="font-bold text-gray-900 text-3xl mb-5">
                            ${{ $vehicle->formatted_price }}
                        </h1>
                        <a
                            class="ui button primary fluid @if (Auth::check() && Gate::check('buy-items', Vehicle::class)) show-paymemt-options @endif"
                            @if (Auth::check())
                                @can('buy-items', Vehicle::class)
                                    data-item-id="{{ $vehicle->id }}"
                                    data-item-type="vehicle"
                                    data-payment-type="single"
                                @endcan
                            @else
                                href="{{ route('login') }}"
                            @endif
                        >
                            Buy Now
                        </a>
                    </div>
                    <div class="ui fluid two icon buttons">
                        <button
                            data-id="{{ $vehicle->id }}"
                            data-type="vehicle"
                            class="ui blue button add-to-cart"
                        >
                            <i class="cart icon"></i> Cart
                        </button>
                        <div class="or"></div>
                        <button
                            data-url="{{ route('vehicles.watchables.store', $vehicle->id) }}"
                            class="ui yellow button add-to-watchlist"
                            @if (!Gate::check('watch-items', auth()->user()) 
                                  || auth()->user()->vehicleWatchlist()->where(['watchable_type' => 'App\Vehicle', 'watchable_id' => $vehicle->id])->exists())
                                disabled
                            @endif 
                        >
                            <i class="eye icon"></i> Watchlist
                        </button>
                    </div>                
                </div>
            @endif
        </div>        
    </div>
    <div class="my-5 mx-3 md:mx-5 lg:mx-20 md:flex justify-between">
        <div class="w-full md:w-2/3">
            <div class="my-10">
                <div class="font-bold text-blue-700 text-2xl mb-5">Description</div>
                <p class="text-gray-900 text-lg">
                    {!! $vehicle->description !!}
                </p>
            </div>
            <div class="my-10">
                <div class="font-bold text-blue-700 text-2xl mb-5">Details</div>
                <div class="flex">
                    <div class="mr-20 w-1/2">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Model</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $vehicle->model ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Manufacturer</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $vehicle->manufacturer ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Year</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $vehicle->year ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
            <div class="my-10">
                <div class="font-bold text-blue-700 text-2xl mb-5">Address</div>
                <div class="flex">
                    <div class="mr-20 w-1/2">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Country</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $vehicle->address->country ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">State</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $vehicle->address->state ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Suburb</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $vehicle->address->suburb ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="ml-20 w-1/2">
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">City</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $vehicle->address->city ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Post Code</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $vehicle->address->post_code ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Street Name</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $vehicle->address->street_name ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                        <div class="ui list">
                            <div class="item mb-5">
                                <div class="font-bold text-blue-700">Number</div>
                                <p class="text-gray-900 text-lg">
                                    {{ $vehicle->address->number ?? 'Not Indicated' }}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </div>        
        <div class="w-full md:w-1/3 m-10">
            <div class="rounded-lg flex bg-gray-100 shadow hover:shadow-lg w-full p-5">
                <div class="flex items-center">
                    <img
                        class="w-20 h-20 rounded-full mr-5"
                        src="{{ asset($vehicle->user->image->src ?? $vehicle->user->profile_placeholder) }}"
                    />
                    <div>
                        <p class="text-blue-700 leading-none">
                            {{ $vehicle->user->full_name }}
                        </p>
                        <p class="text-gray-900">
                            @isset($vehicle->user->address)
                                {{ Str::limit($vehicle->user->address->partial, 25) }}
                            @endisset
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="my-10 mx-3 lg:mx-20 md:flex items-center justify-center">
        <div class="ui threaded comments w-full md:w-1/2">
            <h3 class="ui dividing header blue">
                Questions and Answers ({{ $vehicle->comments->count() }})
            </h3>
            @foreach ($vehicle->comments as $comment)
                <div class="comment">
                    <span class="avatar">
                        <img
                            src="{{ asset($comment->user->image->src ?? $comment->user->profile_placeholder) }}"
                        />
                    </span>
                    <div class="content">
                        <span class="author">{{ $comment->user->full_name }}</span>
                        <div class="metadata">
                            <span class="date"
                                >{{ $comment->created_at ? $comment->created_at->diffForHumans() : 'Not Interested' }}</span
                            >
                            @if ($comment->reply)
                                <span class="ui mini green label">Answered</span>
                            @endif
                        </div>
                        <div class="text">
                            <p class="text-lg">
                                {{ $comment->comment }}
                            </p>
                        </div>
                        @if (!$comment->reply)
                            @can('update-comment', $comment)
                                <div class="actions reply-comment">
                                    <a class="reply">Reply</a>
                                </div>
                                <form
                                    action="{{ route('comments.update', $comment->id) }}"
                                    method="POST"
                                    class="ui reply form hidden"
                                >
                                    @csrf
                                    @method('PUT')
                                    <input
                                        type="hidden"
                                        name="user_id"
                                        value="{{ Auth::id() }}"
                                    />
                                    <input
                                        type="hidden"
                                        name="comment"
                                        value="{{ $comment->comment }}"
                                    />
                                    <input
                                        type="hidden"
                                        name="replied_at"
                                        value="{{ now() }}"
                                    />
                                    <div class="field">
                                        <textarea name="reply"></textarea>
                                    </div>
                                    <button
                                        type="submit"
                                        class="ui primary submit labeled icon button"
                                    >
                                        <i class="icon edit"></i> Add Reply
                                    </button>
                                </form>
                            @endcan
                        @endif
                    </div>
                    @if ($comment->reply)
                        <div class="comments">
                            <div class="comment">
                                <span class="avatar">
                                    <img
                                        src="{{ asset($comment->commentable->user->image->src ?? $comment->commentable->user->profile_placeholder) }}"
                                    />
                                </span>
                                <div class="content">
                                    <span class="author"
                                        >{{ $comment->commentable->user->full_name }}</span
                                    >
                                    <div class="metadata">
                                        <span class="date"
                                            >{{ $comment->replied_at->diffForHumans()
                                            }}</span
                                        >
                                    </div>
                                    <div class="text">
                                        <p class="text-lg">
                                            {{ $comment->reply }}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
            @auth
                @if (Gate::check('create-comment'))
                    <form
                        action="{{ route('vehicles.comments.store', $vehicle->id) }}"
                        method="post"
                        class="ui reply form"
                    >
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::id() }}" />
                        <div class="field">
                            <textarea name="comment"></textarea>
                        </div>
                        <button type="submit" class="ui primary submit labeled icon button">
                            <i class="icon edit"></i> Add Comment
                        </button>
                    </form>
                @else
                    <div class="ui message">
                        <p> Please activate your account in order to post a comment. </p>
                    </div>
                @endif
            @endauth
        </div>
    </div>      
    @include('partials.modals.payment-gateways')
@endsection

@push('scripts')
    <script src="https://js.stripe.com/v3/"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
    <script>
        const stripe = Stripe("{{ config('services.stripe.key') }}");

        $(".show-paymemt-options").on("click", function() {
            $("#paymentOptions")
                .modal({
                    blurring: true,
                    inverted: true
                })
                .modal("show");

            let itemID = $(this).data("item-id");
            let itemType = $(this).data("item-type");
            let paymentType = $(this).data("payment-type");

            $("#stripePaymentOption").on("click", function() {
                $.post({
                    url: "{{ route('stripe.checkout') }}",
                    data: {
                        item_id: itemID,
                        item_type: itemType,
                        payment_type: paymentType
                    },
                    success: async function(res) {
                        if (res.success) {
                            const { error } = await stripe.redirectToCheckout({
                                sessionId: res.response.id
                            });
                        }
                    }
                });
            });
        });
    </script>
    <script>
        $(".reply-comment").on("click", function() {
            $(this)
                .next()
                .toggleClass("hidden");
        });
    </script>
@endpush
