@extends('layouts.app') 

@section('title')
    Advertisements
@endsection

@push('styles')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet" />
@endpush

@section('content')
    @include('partials.app-account-nav')
    @if($errors->any())
        <div class="my-5 mx-3 lg:mx-20">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 lg:my-10 mb-20 lg:mb-40 mx-3 md:mx-5 lg:mx-20 md:flex justify-center">
        <div class="w-full md:w-2/5 lg:w-1/3 mr-5 mb-3">
            <div class="ui raised segments">
                <div class="ui padded segment blue">
                    @if (Gate::check('create-platinum-ad') || Gate::check('create-gold-ad'))
                        <div id="imageFormErrorContainer" class="ui warning message mb-5 hidden">
                            <p>Something went wrong.</p>
                        </div>
                        <form
                            id="imageForm"
                            class="ui form error flex justify-center"
                            enctype="multipart/form-data"
                        >
                            <div class="ui special cards">
                                <input type="hidden" name="ad_type" required />
                                <div class="card">
                                    <div class="blurring dimmable image disabled">
                                        <div class="ui dimmer">
                                            <div class="content">
                                                <div class="center">
                                                    <div
                                                        id="chooseImageButton"
                                                        class="ui inverted button"
                                                    >
                                                        Drag or Choose Photo
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="{{ asset('img/placeholder.jpg') }}" />
                                    </div>
                                </div>
                            </div>
                        </form>
                        <form
                            id="new-user-ad-form"
                            action="{{ route('users.ads.store', Auth::id()) }}"
                            method="post"
                        >
                            @csrf
                            <div id="photoContainer" class="hidden"></div>
                            <div class="ui fluid selection dropdown my-3">
                                <input type="hidden" name="type" required />
                                <i class="dropdown icon"></i>
                                <div class="default text">Type</div>
                                <div class="menu">
                                    @can('create-platinum-ad', auth()->user())
                                        <div class="item" data-value="Platinum Tier ">
                                            Platinum Tier
                                        </div>
                                    @endcan
                                    @can('create-gold-ad', auth()->user())
                                        <div class="item" data-value="Gold Tier ">
                                            Gold Tier
                                        </div>
                                    @endcan
                                </div>
                            </div>
                            <div>
                                <button
                                    type="submit"
                                    id="place-ad"
                                    class="ui fluid button blue"
                                    disabled
                                >
                                    Place Ad
                                </button>
                            </div>
                            <div class="mt-2 text-center instruction">
                                <i> Select Ad Type first before uploading an image. </i>
                            </div>
                        </form>
                        <div class="flex justify-center">
                            <div class="dropzone-previews"></div>
                        </div>
                    @else
                        <i>
                            Please purchase an Ad Membership first before uploading an
                            advertisement.
                        </i>
                    @endif
                </div>
            </div>
        </div>
        <div class="w-full md:w-3/5 lg:w-2/3 ml-0 lg:ml-5">
            <div class="ui raised segments">
                <div class="ui padded segment blue">
                    <h4 class="ui block header blue">Platinum Tier</h4>
                    @can('create-platinum-ad', auth()->id())
                        @if ($platinumTierAds->count())
                            <div
                                class="fotorama"
                                data-allowfullscreen="true"
                                data-loop="true"
                                data-autoplay="true"
                                data-width="100%"
                            >
                                @foreach ($platinumTierAds as $platinumTierAd)
                                    <img
                                        src="{{ $platinumTierAd->image->src }}"
                                        alt="{{ $platinumTierAd->type }}"
                                        class="my-3"
                                    />
                                @endforeach
                            </div>
                        @else
                            <i>No Platinum Tier Ads found...</i>
                        @endif
                    @else
                        <i>Please purchase an Platinum Ad Membership.</i>
                    @endcan
                    <h4 class="ui block header blue">Gold Tier</h4>
                    @can('create-gold-ad', auth()->id())
                        @if ($goldTierAds->count())
                            <div
                                class="fotorama"
                                data-allowfullscreen="true"
                                data-loop="true"
                                data-autoplay="true"
                                data-width="100%"
                            >
                            @foreach ($goldTierAds as $goldTierAd)
                                <img
                                    src="{{ $goldTierAd->image->src }}"
                                    alt="{{ $goldTierAd->type }}"
                                    class="my-3"
                                />
                            @endforeach
                        @else
                            <i>No Gold Tier Ads found...</i>
                        @endif
                    @else
                        <i>Please purchase a Gold Ad Membership.</i>
                    @endcan
                </div>
            </div>
        </div>
    </div>     
    @include('partials.app-dropzone-preview')
@endsection

@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
    <script src="{{ asset('js/dropzone.js') }}"></script>
    <script>
        $("#imageForm").dropzone({
            url: "{{ route('images.store', Auth::id()) }}",
            headers: {
                "X-CSRF-TOKEN": $(`meta[name='csrf-token']`).attr("content")
            },
            paramName: "photo",
            clickable: ["#chooseImageButton"],
            maxFiles: 1,
            acceptedFiles: "image/*",
            previewsContainer: ".dropzone-previews",
            previewTemplate: document.querySelector("#tpl").innerHTML,
            success: function(file) {
                let serverResponse = JSON.parse(file.xhr.response);

                if (!file.status === "success") {
                    $("#imageFormErrorContainer").removeClass("hidden");
                    return false;
                }

                if (!serverResponse.success) {
                    $("#imageFormErrorContainer p").text(
                        serverResponse.message ? serverResponse.message : "Something went wrong. Please contact support."
                    );
                    $("#imageFormErrorContainer").removeClass("hidden");
                    $("div[data-dz-remove]").click()
                    return false;
                }

                $("#imageFormErrorContainer").addClass("hidden");
                $("#photoContainer").append(
                    `<input type="hidden" name="photo" value="${serverResponse.src}">`
                );
                $("#imageForm img").attr("src", serverResponse.src);
                $("#place-ad").removeAttr('disabled');
            },
            uploadprogress: function(file, progress, bytesSent) {
                $('.ui.progress').progress({
                    percent: progress
                })
            },
        });
    </script>
    <script>
        $('input[name="type"]').on('change', function() {
            if ($(this).val()) {
                $('.dimmable').removeClass('disabled')
                $('input[name="ad_type"]').val($(this).val())
                $('.instruction').addClass('hidden');
            } else {
                $('.dimmable').addClass('disabled')
                $('.instruction').removeClass('hidden');
            }
        })
    </script>
@endpush
