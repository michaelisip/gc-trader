@extends('layouts.app')

@section('title')
    Mobile Verification
@endsection

@section('content')
    <div class="my-5 mx-3 lg:mx-20 md:flex justify-center">
        <div class="w-full lg:w-1/3">
            <div class="ui raised segments">
                <div class="ui padded segment blue">
                    <div id="loader" class="ui inverted dimmer">
                        <div class="ui text loader">Sending code.</div>
                    </div>
                    <div id="message" class="ui warning message hidden">
                        <p>Something went wrong.</p>
                    </div>
                    <form id="requestForm" class="ui form">
                        <div class="field">
                            <label>Phone Number</label>
                            <input
                                id="phoneNumberInput"
                                type="text"
                                name="phone_number"
                                placeholder="Phone Number"
                            />
                        </div>
                        <button
                            id="requestFormSubmitButton"
                            type="button"
                            class="ui button primary"
                            tabindex="0"
                        >
                            Send Code
                        </button>
                    </form>
                    <form id="verificationForm" class="ui form">
                        <div class="field">
                            <label>Verfication Code</label>
                            <input
                                id="verificationCodeInput"
                                type="text"
                                name="verification_code"
                                placeholder="Ex. 123456"
                            />
                        </div>
                        <button
                            id="verificationFormSubmitButton"
                            type="button"
                            class="ui button primary"
                            tabindex="0"
                        >
                            Verify Phone Number
                        </button>
                        <button
                            id="resendCodeButton"
                            type="button"
                            class="ui button"
                            tabindex="0"
                        >
                            Resend Code
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        let phoneNumber = "";

        $(document).ready(function() {
            $("#verificationForm").hide();
        });

        $("#requestFormSubmitButton").on("click", function() {
            phoneNumber = $("#phoneNumberInput").val();

            if (phoneNumber) {
                $("#message").addClass("hidden");
                $("#message p").text("Something went wrong.");
                $("#loader").addClass("active");

                $.post({
                    url: "{{ route('mobile.verification.request') }}",
                    data: {
                        phone_number: phoneNumber,
                    },
                    success: function(res) {
                        if (res.success) {
                            $("#requestForm").hide();
                            $("#verificationForm").show();
                            $("#message").removeClass("warning");
                            $("#message").addClass("success");
                        } else {
                            $("#message").removeClass("success");
                            $("#message").addClass("warning");
                        }

                        $("#message").removeClass("hidden");
                        $("#message p").text(res.message);
                        $("#loader").removeClass("active");
                    }
                });
            } else {
                $("#message").removeClass("hidden");
                $("#message p").text("Phone number is required.");
            }
        });

        $("#verificationFormSubmitButton").on("click", function() {
            let verificationCode = $("#verificationCodeInput").val();

            if (verificationCode) {
                $("#message").addClass("hidden");
                $("#message p").text("Something went wrong.");
                $("#loader").addClass("active");
                $("#loader .loader").text("Verifying code.");

                $.post({
                    url: "{{ route('mobile.verification.verify') }}",
                    data: {
                        verification_code: verificationCode,
                        phone_number: phoneNumber,
                    },
                    success: function(res) {
                        if (res.success) {
                            $("#message").removeClass("warning");
                            $("#message").addClass("success");
                            $("#verificationFormSubmitButton").attr(
                                "disable",
                                true
                            );
                            $("#resendCodeButton").attr("disable", true);
                        } else {
                            $("#message").removeClass("success");
                            $("#message").addClass("warning");
                        }

                        $("#message").removeClass("hidden");
                        $("#message p").text(res.message);
                        $("#loader").removeClass("active");
                    }
                });
            } else {
                $("#message").removeClass("hidden");
                $("#message p").text("Verification is required.");
            }
        });

        $("#resendCodeButton").on("click", function() {
            $.post({
                url: "{{ route('mobile.verification.request') }}",
                data: {
                    phone_number: phoneNumber,
                },
                success: function(res) {
                    if (res.success) {
                        $("#requestForm").hide();
                        $("#verificationForm").show();
                        $("#message").removeClass("warning");
                        $("#message").addClass("success");
                    } else {
                        $("#message").removeClass("success");
                        $("#message").addClass("warning");
                    }

                    $("#message").removeClass("hidden");
                    $("#message p").text(res.message);
                    $("#loader").removeClass("active");
                }
            });
        });
    </script>
@endpush
