@extends('layouts.app')

@section('title')
    Posting Policy
@endsection

@section('content')
    <div class="my-5 lg:my-10 mb-20 lg:mb-20 mx-10 lg:mx-20 flex justify-center">
        <div class="w-full lg:w-2/3">
            <div class="mb-16">
                <h1 class="text-3xl text-blue-700 font-bold mb-5">
                    General Gc trader Posting Policies
                </h1>
                <p class="my-5 text-justify">
                    At Gc trader we want to make sure that the site is as clean,
                    friendly and usable as possible for everyone. Ads that fall
                    outside the posting rules stated in our Help sections or our
                    Terms of Use may be removed from the site.
                </p>
                <p class="my-5 text-justify">
                    You are solely responsible for all information that you submit
                    to Gc trader and any consequences that may result from your
                    post. We reserve the right at our discretion to refuse or delete
                    content that we believe is inappropriate or breaching our Terms
                    of Use. We also reserve the right at our discretion to restrict
                    a user's usage of the site either temporarily or permanently, or
                    refuse a user's registration.
                </p>
            </div>
            <div class="mb-16">
                <h1 class="text-3xl text-blue-700 font-bold mb-5">
                    General reasons for ads being deleted are:
                </h1>
                <ol class="list-decimal text-justify mx-5">
                    <li class="my-2">
                        Ad breaches General Gc trader Posting Policies
                    </li>
                    <li class="my-2">
                        Breaches of Australian law. It is the responsibility of
                        the advertiser before posting an ad on Gc trader to ensure
                        that content advertised adheres to Gc trader posting
                        policies as well as Australian applicable laws. As a
                        condition of your use of Gc trader specified under our Terms
                        of Use, you agree that you will not violate any laws
                    </li>
                    <li class="my-2">
                        The item being offered for sale is not allowed to be sold
                        on Gc trader. Click HERE to see what can't be posted for
                        sale on Gc trader.
                    </li>
                    <li class="my-2">
                        information in your ad which Gc trader believes
                        is designed to manipulate search
                    </li>
                    <li class="my-2">
                        The ad is a duplicate of another ad previously posted
                    </li>
                    <li class="my-2">
                        Posted under wrong category (You must choose the single
                        most relevant category for your ad)
                    </li>
                    <li class="my-2">
                        Ads posted in a language other than English. We only
                        accept ads in English. It's acceptable to include a
                        translation of your ad in another language in addition to
                        English
                    </li>
                    <li class="my-2">
                        Ads posted from overseas or from behind a VPN: No
                        overseas ads, or ads posted without an Australian IP address
                        are accepted, Gc trader is for Australian based individuals
                        and businesses only.
                    </li>
                    <li class="my-2">
                        Ad contains external links: No external website links are
                        allowed within your ad to other property / job / classified
                        or auction sites
                    </li>
                    <li class="my-2">
                        Not descriptive enough: Ads that do not provide enough
                        detail will be placed on hold or removed as this makes for a
                        bad browsing experience
                    </li>
                    <li class="my-2">
                        Inappropriate language
                    </li>
                    <li class="my-2">
                        Inappropriate photo / image
                    </li>
                    <li class="my-2">
                        Discriminatory on race / religion / nationality / gender
                        / etc
                    </li>
                    <li class="my-2">
                        Ads that report other fraudulent ads. Please report
                        potentially fraudulent ads via the "report ad" option
                        located within each ad or Contact Us with ad details (ad id,
                        email address) and reasons why these ads should be reviewed
                    </li>
                </ol>
                <p class="my-5 text-justify">
                    Gc trader reserves the right to remove any ad that we feel is
                    not relevant, or of value to the Gc trader community, with or
                    without notice to the ad poster.
                </p>
            </div>
        </div>
    </div>
@endsection
