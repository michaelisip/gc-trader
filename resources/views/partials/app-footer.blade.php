<footer class="md:flex flex-col text-white p-10 md:px-5 md:py-10 lg:p-20">
    <div class="container mx-auto">
        <div class="md:flex justify-between">
            <div class="md:mx-3">
                <h4 class="font-bold">
                    Subscribe to receive upcoming auctions
                    <br />
                    and special offers!
                </h4>
                <form
                    method="POST"
                    action="{{ route('email-subscription.store') }}"
                    class="ui form my-5"
                >
                    @csrf
                    <div class="field">
                        <label>Email Address</label>
                        <input
                            type="email"
                            name="email"
                            placeholder="Someone@email.com"
                            required
                        />
                    </div>
                    <button class="ui button primary" type="submit">
                        Subscribe
                    </button>
                </form>
            </div>
            <div class="mt-10 md:mt-0 md:mx-3">
                <h4 class="font-bold">Top Categories</h4>
                <div class="ui link list">
                    <a href="{{ route('communities.index') }}" class="item">Community</a>
                    <a href="{{ route('jobs.index') }}" class="item">Jobs</a>
                    <a href="{{ route('real-estates.index') }}" class="item">Real Estate</a>
                    <a href="{{ route('services.index') }}" class="item">Services</a>
                    <a href="{{ route('travels.index') }}" class="item">Travel</a>
                    <a href="{{ route('vehicles.index') }}" class="item">Vehicle</a>
                </div>
            </div>
            <div class="mt-10 md:mt-0 md:mx-3">
                <h4 class="font-bold">Tips & help</h4>
                <div class="ui link list">
                    <a href="{{ route('about') }}" class="item">About GC Trader</a>
                    <a href="{{ route('faqs') }}" class="item">FAQs</a>
                    <a href="{{ route('contact-us') }}" class="item">Contact Us</a>
                    @if (Auth::guest())
                        <a href="{{ route('login') }}" class="item">Login</a>
                        <a href="{{ route('register') }}" class="item">Register</a>
                    @else
                        <div></div>
                    @endif
                    
                </div>
            </div>
            <div class="mt-10 md:mt-0 md:mx-3">
                <h4 class="font-bold">Legal</h4>
                <div class="ui link list">
                    <a href="{{ route('terms-of-use') }}" class="item">Terms of Use</a>
                    <a href="{{ route('privacy-policy') }}" class="item">Privacy Policy</a>
                    <a href="{{ route('posting-policy') }}" class="item">Posting Policy</a>
                </div>
            </div>
            <div class="mt-10 md:mt-0 md:mx-3">
                <h4 class="font-bold">Payment Methods</h4>
                <div class="my-3">
                    <i class="cc stripe icon big"></i>
                    {{-- <i class="cc amex icon big"></i>
                    <i class="cc mastercard icon big"></i>
                    <i class="cc paypal icon big"></i>
                    <i class="cc visa icon big"></i> --}}
                </div>
            </div>
        </div>
        <h4 class="text-center my-20">© GC Trader 2020</h4>
    </div>
</footer>
