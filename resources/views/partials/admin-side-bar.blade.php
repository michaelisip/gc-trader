<nav
    class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white"
    id="sidenav-main"
>
    <div class="container-fluid">
        <button
            class="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#sidenav-collapse-main"
            aria-controls="sidenav-main"
            aria-expanded="false"
            aria-label="Toggle navigation"
        >
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{ route('admin.index') }}">
            <img
                src="{{ asset('img/logo.jpg') }}"
                class="navbar-brand-img mx-auto"
            />
        </a>
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a
                    class="nav-link"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-lg-circle">
                            <img src="{{ asset('img/matt.jpg') }}" />
                        </span>
                    </div>
                </a>
                <div
                    class="dropdown-menu dropdown-menu-arrow dropdown-menu-right"
                >
                    <div class="dropdown-header">
                        <h6 class="text-overflow m-0">
                            Welcome! {{ auth('admin')->user()->first_name }}
                        </h6>
                    </div>
                    <a href="{{ route('admin.logout') }}" class="dropdown-item">
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('admin.index') }}">
                            <h1>GC Trader</h1>
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button
                            type="button"
                            class="navbar-toggler"
                            data-toggle="collapse"
                            data-target="#sidenav-collapse-main"
                            aria-controls="sidenav-main"
                            aria-expanded="false"
                            aria-label="Toggle sidenav"
                        >
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>
            <ul class="navbar-nav">
                <li
                    class="nav-item @if (Route::currentRouteName() === 'admin.index') active @endif"
                >
                    <a
                        class="nav-link @if(Route::currentRouteName() === 'admin.index') active @endif"
                        href="{{ route('admin.index') }}"
                    >
                        Communities
                    </a>
                </li>
                <li
                    class="nav-item @if (Route::currentRouteName() === 'admin.email-reminders') active @endif"
                >
                    <a
                        class="nav-link @if(Route::currentRouteName() === 'admin.email-reminders') active @endif"
                        href="{{ route('admin.email-reminders') }}"
                    >
                        Email Reminders
                    </a>
                </li>

            </ul>
        </div>
    </div>
</nav>
