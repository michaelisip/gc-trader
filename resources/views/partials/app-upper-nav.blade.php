<div class="flex justify-center md:hidden">
    @if ($bannerAds->count())
        <img
            class="w-100 mx-auto"
            src="{{ asset($bannerAds->random()->image->src) }}"
        />
    @else
        <div class="ui centered banner test ad" data-text="PUT YOUR AD HERE"></div>
    @endif
</div>
<nav class="flex m-3 items-center justify-between md:hidden">
    <a href="{{ route('home') }}">
        <img src="{{ asset('img/logo.jpg') }}" width="100px" />
    </a>
    <div class="ui floating labeled icon dropdown button">
        <i class="bars icon"></i>
        <span class="text">Menu</span>
        <div class="menu">
            @auth
                @if (auth()->user()->is_business_man)
                    <a href="{{ route('auctions') }}" class="item">
                        Auction
                    </a>
                @endif
                @if (Gate::check('view-any-wholesale-product'))
                    <a href="{{ route('wholesales') }}" class="item">
                        Wholesale
                    </a>
                @endif
            @endauth

            <a href="{{ route('communities.index') }}" class="item">
                Community
            </a>
            <a href="{{ route('jobs.index') }}" class="item">
                Job
            </a>
            <a href="{{ route('real-estates.index') }}" class="item">
                Real Estate
            </a>
            <a href="{{ route('travels.index') }}" class="item">
                Travel
            </a>
            <a href="{{ route('vehicles.index') }}" class="item">
                Vehicle
            </a>
            <div class="divider"></div>
            @guest
                <a href="{{ route('login') }}" class="item">
                    Login
                </a>
                <a href="{{ route('register') }}" class="item">
                    Register
                </a>
            @endguest
            @auth
                <a href="{{ route('cart.show') }}" class="item">
                    Cart
                </a>
                <a href="{{ route('account') }}" class="item">
                    {{ auth()->user()->first_name }}
                </a>
                <a href="{{ route('logout') }}" class="item">
                    Logout
                </a>
            @endauth
        </div>
    </div>
</nav>
<nav class="hidden md:block container mx-auto">
    <div class="flex justify-between my-5 mx-3 lg:mx-20">
        <div class="w-1/6">
            <a href="{{ route('home') }}" class="my-auto">
                <img src="{{ asset('img/logo.jpg') }}" width="150px" />
            </a>
        </div>
        <div class="w-5/6">
            <div class="flex flex-col justify-between h-full">
                @if ($bannerAds->count())
                    <div class="mx-auto">
                        <img src="{{ asset($bannerAds->random()->image->src) }}" />
                    </div>
                @else
                    <div
                        class="ui centered banner test ad"
                        data-text="PUT YOUR AD HERE"
                    ></div>
                @endif
                <div class="flex justify-between">
                    <ul class="flex">
                        <li class="mx-5 text-blue-700 font-bold">
                            <a href="{{ route('communities.index') }}">Community</a>
                        </li>
                        <li class="mx-5 text-blue-700 font-bold">
                            <a href="{{ route('jobs.index') }}">Job</a>
                        </li>
                        <li class="mx-5 text-blue-700 font-bold">
                            <a href="{{ route('real-estates.index') }}">Real Estate</a>
                        </li>
                        <li class="mx-5 text-blue-700 font-bold">
                            <a href="{{ route('services.index') }}">Service</a>
                        </li>
                        <li class="mx-5 text-blue-700 font-bold">
                            <a href="{{ route('travels.index') }}">Travel</a>
                        </li>
                        <li class="mx-5 text-blue-700 font-bold">
                            <a href="{{ route('vehicles.index') }}">Vehicle</a>
                        </li>
                    </ul>
                    <ul class="flex">
                        @guest
                            <li class="mx-5 text-blue-700 font-bold">
                                <a href="{{ route('login') }}">Login</a>
                            </li>
                            <li class="mx-5 text-blue-700 font-bold hidden lg:block">
                                <a href="{{ route('register') }}">Register</a>
                            </li>
                        @endguest
                        @auth
                            <li class="mx-5 text-blue-700 font-bold hidden lg:block">
                                <a href="{{ route('account') }}"
                                    >{{ auth()->user()->first_name }}</a
                                >
                            </li>
                            <li class="mx-5 text-blue-700 font-bold">
                                <a href="{{ route('logout') }}">Logout</a>
                            </li>
                        @endauth
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>