<div id="placeBidModal" class="ui tiny modal">
      <div class="content">
          <form @auth action="{{ route('users.bids.store', auth()->id()) }}" @endauth method="post" class="ui form">
              @csrf
              <div class="field">
                  <label for="bid">Bid</label>
                  <input type="number" placeholder="Bid" name="bid" id="bid" required>
              </div>
              <div class="flex flex-col md:flex-row">
                  <button type="submit" class="ui fluid primary button w-full md:w-2/3"> Place Bid </button>
                  <div class="actions w-full md:w-1/3 mt-1 md:mt-0">
                      <div class="ui fluid black deny button" style="margin-left: 0 !important">
                          Cancel
                      </div>
                  </div>
              </div>
          </form>
      </div>
      <div class="actions">
      </div>
</div>
