<div id="paymentOptions" class="ui tiny modal">
    <div class="header">
        Select Payment Gateway
    </div>
    <div class="content">
        <button id="stripePaymentOption" class="ui stripe violet button">
            <i class="stripe icon"></i> Pay with Stripe
        </button>
    </div>
    <div class="actions">
        <div class="ui black deny button">
            Cancel
        </div>
    </div>
</div>
