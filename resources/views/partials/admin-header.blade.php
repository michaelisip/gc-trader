<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row">
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase mb-0">
                                        {{ $userCount }} Active Users
                                    </h5>
                                </div>
                                <div class="col-auto">
                                    <div
                                        class="icon icon-shape bg-danger text-white rounded-lg-circle shadow"
                                    >
                                        <i class="fas fa-users"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase mb-0">
                                        {{ $productCount }} Active Product
                                        Listing
                                    </h5>
                                </div>
                                <div class="col-auto">
                                    <div
                                        class="icon icon-shape bg-warning text-white rounded-lg-circle shadow"
                                    >
                                        <i class="fas fa-box"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase mb-0">
                                        {{ $subscriptionCount }} Subscribers
                                    </h5>
                                </div>
                                <div class="col-auto">
                                    <div
                                        class="icon icon-shape bg-yellow text-white rounded-lg-circle shadow"
                                    >
                                        <i class="fas fa-users"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6">
                    <div class="card card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase mb-0">
                                        {{ $adCount }} Active Ads
                                    </h5>
                                </div>
                                <div class="col-auto">
                                    <div
                                        class="icon icon-shape bg-info text-white rounded-lg-circle shadow"
                                    >
                                        <i class="fas fa-image"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
