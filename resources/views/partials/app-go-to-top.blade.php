<div class="fixed bottom-0 right-0 mx-8 my-32 md:mx-20 md:mb-10 md:px-10 z-50">
    <div class="popup" data-content="Go up." data-variation="mini">
        <button id="goToTop" class="ui icon button">
            <i class="arrow up icon"></i>
        </button>
    </div>
</div>
