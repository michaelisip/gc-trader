<nav class="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
    <div class="container-fluid">
        <ul class="navbar-nav ml-auto align-items-center d-none d-md-flex">
            <li class="nav-item dropdown">
                <a
                    class="nav-link pr-0"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                >
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-lg-circle">
                            <img
                                alt="Profile"
                                src="{{ asset('img/matt.jpg') }}"
                            />
                        </span>
                        <div class="media-body ml-2 d-none d-lg-block">
                            <span class="mb-0 text-sm font-weight-bold">
                                {{ auth('admin')->user()->first_name }}
                            </span>
                        </div>
                    </div>
                </a>
                <div
                    class="dropdown-menu dropdown-menu-arrow dropdown-menu-right"
                >
                    <div class="dropdown-header">
                        <h6 class="text-overflow m-0">
                            Welcome! {{ auth('admin')->user()->first_name }}
                        </h6>
                    </div>
                    <a href="{{ route('admin.logout') }}" class="dropdown-item">
                        <span>Logout</span>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</nav>
