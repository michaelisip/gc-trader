<form id="addToCartForm" action="{{ route('cart.add') }}" method="POST">
    @csrf
    <input type="hidden" name="id" />
    <input type="hidden" name="type" />
</form>
<form id="updateCartForm" method="POST">
    @csrf @method('PUT')
    <input type="hidden" name="quantity" />
</form>
<form id="removeToCartForm" method="POST">
    @csrf @method('DELETE')
</form>
<form id="addToWatchlistForm" method="POST">
    @csrf
    <input type="hidden" name="listing_id" />
    <input type="hidden" name="listing_type" />
</form>
<form id="removeFromWatchlistForm" method="POST">
  @csrf
  @method("DELETE")
</form>
<form id="wholesaleRequestForm" method="POST" action="{{ route('wholesales.requests.store') }}">
  @csrf 
  <input type="hidden" name="product_id" />
</form>
<form id="wholesaleRequestApproveForm" method="POST">
  @csrf @method('PUT')
</form>
<form id="wholesaleRequestDenyForm" method="POST">
  @csrf @method('PUT')
</form>
<input type="hidden" name="search_route" value="{{ route('suggestion') }}" />
<form id="reverseAuctionBidForm" method="POST">
  @csrf
  <input type="hidden" name="user_id" />
  <input type="hidden" name="bid" />
</form>
