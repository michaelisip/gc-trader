<div class="hidden" id="tpl">
    <div class="dz-preview dz-file-preview mt-5">
        <div class="ui blue indicating tiny progress" style="margin-bottom: 0 !important">
            <div class="bar" data-dz-uploadprogress></div>
        </div>
        <div class="ui very relaxed list text-center">
            <div class="item" data-dz-remove>
                <img class="ui avatar image" data-dz-thumbnail />
                <div class="content">
                    <span class="header w-56 overflow-hidden" data-dz-name></span>
                    <div class="description">
                        <span data-dz-size></span> loaded
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="hidden" id="tpl-video">
  <div class="dz-preview dz-file-preview mt-5">
      <div class="ui blue indicating tiny progress" style="margin-bottom: 0 !important">
          <div class="bar" data-dz-uploadprogress></div>
      </div>
      <div class="ui very relaxed list text-center">
          <div class="item" data-dz-remove>
              <video controls autoplay class="ui avatar image center">
                  <source type="video/mp4" data-dz-thumbnail>
              </video>
              <div class="content">
                  <span class="header w-56 overflow-hidden" data-dz-name></span>
                  <div class="description">
                      <span data-dz-size></span> loaded
                  </div>
              </div>
          </div>
      </div>
  </div>
</div>
