<div class="my-5 mx-3 md:mx-5 lg:mx-20 flex flex-col md:flex-row justify-between hidden md:block">
    <div class="ui secondary menu">
        <a
            href="{{ route('account') }}"
            class="@if(Route::currentRouteName() === 'account') active @endif item"
        >
            My account
        </a>
        <a
            href="{{ route('listings') }}"
            class="@if(Route::currentRouteName() === 'listings') active @endif item"
        >
            My Listings
        </a>
        <a
            href="{{ route('analytics') }}"
            class="@if(Route::currentRouteName() === 'analytics') active @endif item"
        >
            Analytics
        </a>
        <a
            href="{{ route('wholesales.requests.index') }}"
            class="@if(Route::currentRouteName() === 'wholesales.requests.index') active @endif item"
        >
            Wholesale Requests
        </a>
        {{-- <a
            href="{{ route('users.ads.index', Auth::id()) }}"
            class="@if(Route::currentRouteName() === 'users.ads.index') active @endif item"
        >
            Ads
        </a> --}}
    </div>
    @if (Auth::user()->is_active)
        <div>
            <div class="ui floating labeled icon dropdown button primary">
                <i class="plus icon"></i>
                <span class="text">Add Listing</span>
                <div class="menu">
                    <a
                        href="{{ route('users.products.create', Auth::id()) }}"
                        class="item"
                    >
                        New Product
                    </a>
                    <a
                        href="{{ route('users.vehicles.create', Auth::id()) }}"
                        class="item"
                    >
                        New Vehicle
                    </a>
                    <a
                        href="{{ route('users.travels.create', Auth::id()) }}"
                        class="item"
                    >
                        New Travel
                    </a>
                    @can('create-service', Service::class)
                        <a
                            href="{{ route('users.services.create', Auth::id()) }}"
                            class="item"
                        >
                            New Service
                        </a>
                    @endcan
                    <a
                        href="{{ route('users.real-estates.create', Auth::id()) }}"
                        class="item"
                    >
                        New Real Estate
                    </a>
                    <a
                        href="{{ route('users.communities.create', Auth::id()) }}"
                        class="item"
                    >
                        New Community
                    </a>
                    <a
                        href="{{ route('users.jobs.create', Auth::id()) }}"
                        class="item"
                    >
                        New Job
                    </a>
                </div>
            </div>
        </div>
    @endif
</div>
<nav class="flex m-3 items-center justify-start md:hidden">
    @if (Auth::user()->is_active)
        <div class="ui floating icon dropdown button primary p-2">
            <i class="plus icon"></i>
            <div class="menu">
                <a
                    href="{{ route('users.products.create', Auth::id()) }}"
                    class="item"
                >
                    New Product
                </a>
                <a
                    href="{{ route('users.vehicles.create', Auth::id()) }}"
                    class="item"
                >
                    New Vehicle
                </a>
                <a
                    href="{{ route('users.travels.create', Auth::id()) }}"
                    class="item"
                >
                    New Travel
                </a>
                @can('create-service', Service::class)
                    <a
                        href="{{ route('users.services.create', Auth::id()) }}"
                        class="item"
                    >
                        New Service
                    </a>
                @endcan
                <a
                    href="{{ route('users.real-estates.create', Auth::id()) }}"
                    class="item"
                >
                    New Real Estate
                </a>
                <a
                    href="{{ route('users.communities.create', Auth::id()) }}"
                    class="item"
                >
                    New Community
                </a>
                <a
                    href="{{ route('users.jobs.create', Auth::id()) }}"
                    class="item"
                >
                    New Job
                </a>
            </div>
        </div>
    @endif
    <div class="ui floating labeled icon dropdown button">
        <i class="bars icon"></i>
        <span class="text">Account</span>
        <div class="menu">
            <a href="{{ route('account') }}" class="item">
                My Account
            </a>
            <a href="{{ route('listings') }}" class="item">
                My Listings
            </a>
            <a href="{{ route('users.watchables.index', auth()->id()) }}" class="item">
                Watchlist
            </a>
            <a href="{{ route('analytics') }}" class="item">
                Analytics
            </a>
            <a href="{{ route('wholesales.requests.index') }}" class="item">
                Wholesale Requests
            </a>
            <a href="{{ route('users.ads.index', Auth::id()) }}" class="item">
                Ads
            </a>
        </div>
    </div>
</nav>
