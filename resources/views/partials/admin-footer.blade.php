<footer class="footer">
    <div class="row align-items-center justify-content-xl-between">
        <div class="col-xl-6">
            <div class="copyright text-center text-xl-left text-muted">
                &copy; 2020 GC Trader
            </div>
        </div>
        <div class="col-xl-6">
            <ul
                class="nav nav-footer justify-content-center justify-content-xl-end"
            >
                <li class="nav-item">
                    <a href="{{ route('terms-of-use') }}" class="nav-link"
                        >Terms of Use</a
                    >
                </li>
                <li class="nav-item">
                    <a href="{{ route('privacy-policy') }}" class="nav-link"
                        >Privacy Policy</a
                    >
                </li>
                <li class="nav-item">
                    <a href="{{ route('posting-policy') }}" class="nav-link"
                        >Posting Policy</a
                    >
                </li>
            </ul>
        </div>
    </div>
</footer>
