<div class="container mx-auto">
    <div class="my-5 mx-3 lg:mx-20">
        <div class="flex justify-between bg-blue-500 p-5 rounded-lg">
            <div class="w-full md:flex md:w-1/2">
                <div class="ui w-full block md:w-1/2 lg:w-1/3 md:flex floating dropdown labeled search icon button">
                    <i class="filter icon"></i>
                    <span class="text">
                        @if (request('filter'))
                            {{ request('filter') }}
                        @else
                            Filter By
                        @endif
                    </span>
                    <div class="menu">
                        <a
                            href="{{ route('search', ['filter' => 'All links for Categories']) }}"
                            class="item @if(request('filter') == "All links for Categories")active selected @endif"
                            >All links for Categories</a
                        >
                        @foreach ($productCategories as $productCategory)
                            <a
                                href="{{ route('search', ['filter' => $productCategory->name]) }}"
                                class="item @if(request('filter') == $productCategory->name)active selected @endif"
                                >{{ $productCategory->name }}</a
                            >
                        @endforeach
                    </div>
                </div>
                <div class="w-full md:w-2/3 mt-2 md:mt-0">
                    <div class="ui category search">
                        <div class="ui icon fluid input">
                            <input
                                id="searchBar"
                                type="text"
                                placeholder="Search in GC Trader"
                                value="{{ request('search') }}"
                            />
                            <i class="search icon"></i>
                        </div>
                        <div class="results"></div>
                    </div>
                </div>
                <div class="results"></div>
            </div>
            <ul class="hidden md:flex">
                @auth
                    @if (auth()->user()->is_business_man)
                        <li class="mx-5 my-auto text-white text-xl font-bold">
                            <a href="{{ route('auctions') }}">Auction</a>
                        </li>
                        @can('view-any-wholesale-product')
                            <li class="mx-5 my-auto text-white text-xl font-bold">
                                <a href="{{ route('wholesales') }}">Wholesale</a>
                            </li>
                        @endcan
                    @endif
                @endauth
                <li class="mx-5 my-auto text-white text-xl font-bold">
                    <a
                        href="{{ Auth::check() ? route('users.watchables.index', ['user' => auth()->id()]) : route('login') }}"
                        >Watchlist</a
                    >
                </li>
                <li class="mx-5 my-auto text-white text-xl font-bold">
                    <a href="{{ route('cart.show') }}"><i class="shopping cart icon"></i></a>
                </li>
            </ul>
        </div>
    </div>
</div>