@component('mail::message')
# Wholesale Request

{{ $message->body }}

@component('mail::button', ['url' => $message->url])
{{ $message->button }}
@endcomponent

Thanks,
{{ config('app.name') }}
@endcomponent