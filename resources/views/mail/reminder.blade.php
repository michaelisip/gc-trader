@component('mail::message')
# Email Reminder

{{ $message->body }}

Thanks,
{{ config('app.name') }}
@endcomponent