<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>
            @yield('title')
        </title>
        <link
            rel="icon"
            href="{{ asset('favicon.ico') }}"
            type="image/x-icon"
        />
        <link rel="stylesheet" href="{{ asset('css/semantic.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/normalize.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
        @stack('styles')
    </head>
    <body>
        @include('partials.app-upper-nav')
        @include('partials.app-main-nav')
        <div class="container mx-auto">
            @yield('content')
        </div>
        @include('partials.app-footer')
        @include('sweetalert::alert')
        @include('partials.app-facebook-messenger')
        @include('partials.app-go-to-top') 
        @include('partials.app-forms')
        <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"
        ></script>
        <script src="{{ asset('js/semantic.min.js') }}"></script>
        <script src="{{ asset('js/app.js') }}"></script>
        @stack('scripts')
    </body>
</html>
