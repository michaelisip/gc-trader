@extends('layouts.app')

@section('title')
    Reset Password
@endsection

@section('content')
    @if($errors->any())
        <div class="my-5 mx-3 lg:mx-20">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 mx-3 lg:mx-20 md:flex items-center justify-center">
        <div class="ui raised segments w-full lg:w-1/2">
            <div class="ui padded segment blue">
                <h4 class="ui block header blue">Reset Password</h4>
                <form
                    method="POST"
                    class="ui form error"
                    action="{{ route('password.update') }}"
                >
                    @csrf
                    <input type="hidden" name="token" value="{{ $token }}" />
                    <input type="hidden" name="email" value="{{ $email ?? old('email') }}" />
                    <div class="field @error('password') error @enderror">
                        <label>Password</label>
                        <input type="password" name="password" required autofocus />
                    </div>
                    <div class="field @error('password') error @enderror">
                        <label>Confirm Password</label>
                        <input
                            type="password"
                            name="password_confirmation"
                            required
                        />
                    </div>
                    <button type="submit" class="ui button primary" tabindex="0">
                        Reset Password
                    </button>
                </form>
            </div>
        </div>
    </div>    
@endsection
