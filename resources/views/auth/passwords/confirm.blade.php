@extends('layouts.app')

@section('title')
    Confirm Password
@endsection

@section('content')
    @if($errors->any())
        <div class="my-5 mx-3 lg:mx-20">
            <div class="ui error message w-full lg:w-1/2">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </div>
        </div>
    @endif
    <div class="my-5 mx-3 lg:mx-20 md:flex items-center justify-center">
        <div class="ui raised segments w-full lg:w-1/2">
            <div class="ui padded segment blue">
                <h4 class="ui block header blue">
                    Please confirm your password before continuing.
                </h4>
                <form
                    method="POST"
                    class="ui form error"
                    action="{{ route('password.confirm') }}"
                >
                    @csrf
                    <div class="field @error('password') error @enderror">
                        <label>Password</label>
                        <input type="password" name="password" required autofocus/>
                    </div>
                    <button type="submit" class="ui button primary" tabindex="0">
                        Confirm Password
                    </button>
                    @if (Route::has('password.request'))
                        <a
                            href="{{ route('password.request') }}"
                            class="ui button"
                            tabindex="0"
                        >
                            Reset Password
                        </a>                
                    @endif
                </form>
            </div>
        </div>
    </div>
@endsection
