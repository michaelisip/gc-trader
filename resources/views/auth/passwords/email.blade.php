@extends('layouts.app')

@section('title')
    Reset Password
@endsection

@section('content')
    <div class="my-5 mx-3 lg:mx-20 md:flex items-center justify-center">
        <div class="ui raised segments w-full lg:w-1/2">
            <div class="ui padded segment blue">
                <h4 class="ui block header blue">Reset Password</h4>
                @if(session('status'))
                    <div class="ui positive message w-full">
                        {{ session('status') }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="ui error message w-full">
                        @foreach ($errors->all() as $error)
                            {{ $error }}
                        @endforeach
                    </div>
                @endif
                <form
                    method="POST"
                    class="ui form error"
                    action="{{ route('password.email') }}"
                >
                    @csrf
                    <div class="field @error('email') error @enderror">
                        <label>Email</label>
                        <input
                            type="email"
                            name="email"
                            placeholder="Someone@gmail.com"
                            value="{{ old('email') }}"
                            autofocus
                            required
                        />
                    </div>
                    <button type="submit" class="ui button primary" tabindex="0">
                        Send Password Reset Link
                    </button>
                </form>
            </div>
        </div>
    </div>
@endsection
