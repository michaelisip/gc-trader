@extends('layouts.app')

@section('title')
    Verify Your Email Address
@endsection

@section('content')
    <div class="my-5 mx-3 lg:mx-20 md:flex items-center justify-center">
        <div class="ui raised segments w-full lg:w-1/2">
            <div class="ui padded segment blue">
                <h4 class="ui block header blue">
                    Verify Your Email Address
                </h4>
                @if(session('resent'))
                    <div class="ui positive message w-full">
                        A fresh verification link has been sent to your email address.
                    </div>
                @endif
                <p class="text-black text-base">
                    Before processing, please check your email for a verification link. If you did not receive the email, 
                    <a id="resend-button" class="italic text-blue-700 cursor-pointer"> click here to request another. </a>
                </p>
                <form
                    method="POST"
                    class="ui form"
                    action="{{ route('verification.resend') }}"
                    id="resend"
                >
                    @csrf
                </form>
            </div>
        </div>
    </div>    
@endsection

@push('scripts')
    <script>
        $('#resend-button').on('click', function () {
            $('#resend').submit()
        })
    </script>
@endpush