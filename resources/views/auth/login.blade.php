<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Login</title>
        <link
            rel="icon"
            href="{{ asset('favicon.ico') }}"
            type="image/x-icon"
        />
        <link rel="stylesheet" href="{{ asset('css/semantic.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/normalize.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
    </head>
    <body>
        <div class="flex h-screen">
            <div class="hidden md:flex max-h-screen md:w-1/2 login-feature"></div>
            <div class="h-screen w-full md:w-1/2 mx-5">
                <div class="flex flex-row-reverse my-5">
                    <a href="{{ route('about') }}" class="mx-3">About Us</a>
                    <a href="{{ route('home') }}" class="mx-3">Home</a>
                    <a href="{{ route('register') }}" class="mx-3">Register</a>
                </div>
                <div class="mx-auto mt-10 w-full lg:w-1/2">
                    <div class="text-center mb-5">
                        <img
                            class="mx-auto w-1/2 lg:w-2/3"
                            src="{{ asset('img/logo.jpg') }}"
                        />
                    </div>
                    @if($errors->any())
                        <div class="ui error message">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    @endif
                    <form
                        class="ui form error"
                        method="POST"
                        action="{{ route('login') }}"
                    >
                        @csrf
                        <div class="field @error('email') error @enderror">
                            <label>Email</label>
                            <input
                                type="email"
                                name="email"
                                placeholder="Someone@email.com"
                                value="{{ old('email') }}"
                                autofocus
                            />
                        </div>
                        <div class="field @error('password') error @enderror">
                            <label>Password</label>
                            <input
                                type="password"
                                name="password"
                                placeholder="Secure Password"
                            />
                        </div>
                        <div class="field flex justify-between">
                            <div class="ui checkbox">
                                <input
                                    type="checkbox"
                                    tabindex="0"
                                    class="hidden"
                                    name="remember"
                                />
                                <label>Remember Me</label>
                            </div>
                            <a href="{{ route('password.request') }}"
                                >Forgot Password</a
                            >
                        </div>
                        <button class="ui button fluid primary" type="submit">
                            Login
                        </button>
                    </form>
                    <div class="mt-10">
                        @if (config('services.facebook.client_id'))
                            <div class="field my-3">
                                <a
                                    href="{{ route('facebook.login') }}"
                                    class="ui facebook button fluid"
                                >
                                    <i class="facebook icon"></i> Login with
                                    Facebook
                                </a>
                            </div>
                        @endif
                        @if (config('services.google.client_id'))
                            <div class="field my-3">
                                <a
                                    href="{{ route('google.login') }}"
                                    class="ui google plus button fluid"
                                >
                                    <i class="google icon"></i> Login with Google
                                </a>
                            </div>
                        @endif
                        @if (config('services.linkedin.client_id'))
                            <div class="field my-3">
                                <a
                                    href="{{ route('linkedin.login') }}"
                                    class="ui linkedin button fluid"
                                >
                                    <i class="linkedin icon"></i> Login with
                                    LinkedIn
                                </a>
                            </div>
                        @endif
                        @if (config('services.instagram.client_id'))
                            <div class="field my-3">
                                <a
                                    href="{{ route('instagram.login') }}"
                                    class="ui instagram button fluid"
                                >
                                    <i class="instagram icon"></i> Login with
                                    Instagram
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @include('sweetalert::alert')
        <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"
        ></script>
        <script src="js/semantic.min.js"></script>
        <script src="js/app.js"></script>
    </body>
</html>
