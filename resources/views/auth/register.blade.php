<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Register</title>
        <link
            rel="icon"
            href="{{ asset('favicon.ico') }}"
            type="image/x-icon"
        />
        <link rel="stylesheet" href="{{ asset('css/semantic.min.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/normalize.css') }}" />
        <link rel="stylesheet" href="{{ asset('css/app.css') }}" />
        <style>
            body ::-webkit-scrollbar {
                display: none !important;
            }
        </style>
    </head>
    <body>
        <div class="flex h-full">
            <div class="hidden md:flex h-full md:w-1/2 register-feature"></div>
            <div class="h-screen w-full h-full overflow-auto md:w-1/2 mx-5">
                <div class="flex flex-row-reverse my-5">
                    <a href="{{ route('about') }}" class="mx-3">About Us</a>
                    <a href="{{ route('home') }}" class="mx-3">Home</a>
                    <a href="{{ route('login') }}" class="mx-3">Login</a>
                </div>
                <div class="mx-auto mt-10 w-full lg:w-1/2">
                    <div class="text-center mb-5">
                        <img
                            class="mx-auto w-1/2 lg:w-2/3"
                            src="{{ asset('img/logo.jpg') }}"
                        />
                    </div>
                    @if($errors->any())
                        <div class="ui error message">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </div>
                    @endif
                    <form
                        class="ui form error"
                        method="POST"
                        action="{{ route('register') }}"
                    >
                        @csrf
                        <div class="field @error('first_name') error @enderror">
                            <label>First Name</label>
                            <input
                                type="text"
                                name="first_name"
                                placeholder="Your First Name"
                                value="{{ $first_name ?? old('first_name') }}"
                                autofocus
                            />
                        </div>
                        <div class="field @error('last_name') error @enderror">
                            <label>Last Name</label>
                            <input
                                type="text"
                                name="last_name"
                                placeholder="Your Last Name"
                                value="{{ $last_name ?? old('last_name') }}"
                            />
                        </div>
                        <div class="field @error('email') error @enderror">
                            <label>Email</label>
                            <input
                                type="email"
                                name="email"
                                placeholder="Someone@email.com"
                                value="{{ $email ?? old('email') }}"
                            />
                        </div>
                        <div class="field @error('password') error @enderror">
                            <label>Password</label>
                            <input
                                type="password"
                                name="password"
                                placeholder="Secure Password"
                            />
                        </div>
                        <div class="field @error('password') error @enderror">
                            <label>Confirm Password</label>
                            <input
                                type="password"
                                name="password_confirmation"
                                placeholder="Confirm Password"
                            />
                        </div>
                        <button class="ui button fluid primary" type="submit">
                            Register
                        </button>
                    </form>
                    <div class="mt-10">
                        @if (config('services.facebook.client_id'))
                            <div class="field my-3">
                                <a
                                    href="{{ route('facebook.login') }}"
                                    class="ui facebook button fluid"
                                >
                                    <i class="facebook icon"></i> Login with
                                    Facebook
                                </a>
                            </div>
                        @endif
                        @if (config('services.google.client_id'))
                            <div class="field my-3">
                                <a
                                    href="{{ route('google.login') }}"
                                    class="ui google plus button fluid"
                                >
                                    <i class="google icon"></i> Login with Google
                                </a>
                            </div>
                        @endif
                        @if (config('services.linkedin.client_id'))
                            <div class="field my-3">
                                <a
                                    href="{{ route('linkedin.login') }}"
                                    class="ui linkedin button fluid"
                                >
                                    <i class="linkedin icon"></i> Login with
                                    LinkedIn
                                </a>
                            </div>
                        @endif
                        @if (config('services.instagram.client_id'))
                            <div class="field my-3">
                                <a
                                    href="{{ route('instagram.login') }}"
                                    class="ui instagram button fluid"
                                >
                                    <i class="instagram icon"></i> Login with
                                    Instagram
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @include('sweetalert::alert')
        <script
            src="https://code.jquery.com/jquery-3.4.1.min.js"
            integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
            crossorigin="anonymous"
        ></script>
        <script src="js/semantic.min.js"></script>
        <script src="js/app.js"></script>
    </body>
</html>
